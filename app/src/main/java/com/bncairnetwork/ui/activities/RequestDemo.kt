package com.bncairnetwork.ui.activities


import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityRequestDemoBinding
import com.bncairnetwork.databinding.SuccessBottomSheetBinding
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.RequestDemoPost
import com.bncairnetwork.pojo.response.GetCompaniesForReqDemo
import com.bncairnetwork.pojo.response.requesDemo.GetCitiesForReqDemoRes
import com.bncairnetwork.pojo.response.requesDemo.GetCountriesForReqDemoRes
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestDemo : AppCompatActivity() {
    lateinit var mInterface: ApiInterface
    var cityHashMap = HashMap<String, String>()
    var companiesLst = ArrayList<String>()
    var companiesHashMap = HashMap<String, String>()
    var cityLst = ArrayList<String>()
    var countryHashMap = HashMap<String, String>()
    var countryLst = ArrayList<String>()
    lateinit var bin: ActivityRequestDemoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityRequestDemoBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
//        runOnUiThread {
//            getMasterData()
//        }
        runOnUiThread {
            getcountries()
        }

    }

    fun initView() {
        mInterface = ServiceBuilder.buildService(ApiInterface::class.java, this)
        bin.rqstDemo.appbarHeader.text = "Request Demo"
        bin.rqtBtn.submitBtn.text = "Request Demo"
    }

    fun listnr() {
        bin.rqstDemo.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.rqtCmnynameEdxt.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= bin.rqtCmnynameEdxt.right - bin.rqtCmnynameEdxt.compoundDrawables[2].bounds.width()
                ) {
                    // your action here
                    if (bin.rqtCmnynameEdxt.text.isNotEmpty()) {
                        runOnUiThread {
                            getcompanies_or_projects()
                        }
                    }

                    return@OnTouchListener true
                }
            }
            false
        })

        bin.rqtCmnynameEdxt.setOnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        if (bin.rqtCmnynameEdxt.text.isNotEmpty()) {
                            runOnUiThread {
                                getcompanies_or_projects()
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            return@setOnKeyListener false

        }
        bin.rqtBtn.submitBtn.setOnClickListener {
            Dialog(this).apply {
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCanceledOnTouchOutside(true)
                val view =
                    SuccessBottomSheetBinding.inflate(LayoutInflater.from(this@RequestDemo))
                setContentView(view.root)
                show()
                view.scsBtn.submitBtn.text = "Okay"

                view.scsBtn.submitBtn.setOnClickListener {
                    dismiss()
                }
                if (window != null) {
                    window?.setLayout(
                        WindowManager.LayoutParams.MATCH_PARENT,
                        WindowManager.LayoutParams.WRAP_CONTENT
                    )
                }
            }
        }

        bin.rqtBtn.submitBtn.setOnClickListener {
            if (validation()) {
                runOnUiThread {
                    makeReqDemo()
                }
            } else {
                Uitls.showToast(this, "Please check inserted data")
            }

        }


        bin.rqtContrySp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position > 0) {
                    runOnUiThread {
                        getCitiesByID(countryHashMap[parent?.selectedItem.toString()] ?: "0")
                    }
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }


    }

    fun validation(): Boolean {
        return bin.rqtNameEdxt.text.trim().toString()
            .isNotEmpty() && bin.rqtCmnynameEdxt.text.trim().toString().isNotEmpty() &&
                bin.rqtDsnynameEdxt.text.trim().toString()
                    .isNotEmpty() && Uitls.isValidEmail(bin.rqtEmailEdxt.text.toString())
                && bin.rqtContrySp.selectedItemPosition > 0 && bin.rqtCtySp.selectedItemPosition > 0 && bin.countryCodeEdxt.text.trim()
            .toString().isNotEmpty()
                && bin.areaCodeEdxt.text.trim().toString()
            .isNotEmpty() && bin.MobileNoEdxt.text.trim().toString().isNotEmpty()
    }

    /****Apis*****/

    fun getcompanies_or_projects() {
        companiesHashMap.clear()
        companiesLst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getCompaniesLst4ReqDemo(bin.rqtCmnynameEdxt.text.toString())
        call.enqueue(object : Callback<GetCompaniesForReqDemo> {
            override fun onResponse(
                call: Call<GetCompaniesForReqDemo>,
                response: Response<GetCompaniesForReqDemo>
            ) {
                Uitls.showProgree(false, this@RequestDemo)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.rqtCmnynameEdxt.text = null
                        response.body()!!.suggestions?.forEach {
                            companiesHashMap[it?.name ?: ""] = it?.id.toString()
                            companiesLst.add(it?.name ?: "")
                        }
                        ArrayAdapter(
                            this@RequestDemo,
                            android.R.layout.simple_list_item_1, companiesLst
                        ).apply {
                            bin.rqtCmnynameEdxt.setAdapter(this)
                            setNotifyOnChange(true)
                            bin.rqtCmnynameEdxt.showDropDown()
                        }
                    } else {
                        Uitls.showToast(this@RequestDemo, "No Companies found.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@RequestDemo)
                }
            }

            override fun onFailure(call: Call<GetCompaniesForReqDemo>, t: Throwable) {
                Uitls.showProgree(false, this@RequestDemo)
                Uitls.handlerError(this@RequestDemo, t)
            }
        })
    }

    fun getCitiesByID(id: String) {
        cityLst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getCitiesForReqDemo(id)
        call.enqueue(object : Callback<GetCitiesForReqDemoRes> {
            override fun onResponse(
                call: Call<GetCitiesForReqDemoRes>,
                response: Response<GetCitiesForReqDemoRes>
            ) {
                Uitls.showProgree(false, this@RequestDemo)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        cityLst.add("Select")
                        response.body()!!.cities?.forEach {
                            cityLst.add(it?.name ?: "")
                        }
                        ArrayAdapter(
                            this@RequestDemo,
                            android.R.layout.simple_spinner_item,
                            cityLst
                        ).apply {
                            setDropDownViewResource(R.layout.sp_row_item_lay)
                            bin.rqtCtySp.adapter = this
                        }
                    } else {
                        Uitls.showToast(this@RequestDemo, "No cities found")
                    }
                } else {
                    Uitls.showToast(this@RequestDemo, getString(R.string.somethingWentWrong))
                }
            }

            override fun onFailure(call: Call<GetCitiesForReqDemoRes>, t: Throwable) {
                Uitls.showProgree(false, this@RequestDemo)
                Uitls.handlerError(this@RequestDemo, t)
            }
        })
    }

    fun getcountries() {
        Uitls.showProgree(true, this)
        val call = mInterface.getCountriesForReqDemo()
        call.enqueue(object : Callback<GetCountriesForReqDemoRes> {
            override fun onResponse(
                call: Call<GetCountriesForReqDemoRes>,
                response: Response<GetCountriesForReqDemoRes>
            ) {
                Uitls.showProgree(false, this@RequestDemo)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        countryLst.add("Select")
                        response.body()!!.countries?.forEach {
                            countryHashMap[it?.name ?: ""] = it?.id.toString()
                            countryLst.add(it?.name ?: "")
                        }
                        ArrayAdapter(
                            this@RequestDemo,
                            android.R.layout.simple_spinner_item,
                            countryLst
                        ).apply {
                            setDropDownViewResource(R.layout.sp_row_item_lay)
                            bin.rqtContrySp.adapter = this
                        }
                    } else {
                        onBackPressed()
                    }
                } else {
                    Uitls.showToast(this@RequestDemo, getString(R.string.somethingWentWrong))
                }
            }

            override fun onFailure(call: Call<GetCountriesForReqDemoRes>, t: Throwable) {
                Uitls.showProgree(false, this@RequestDemo)
                Uitls.handlerError(
                    this@RequestDemo, t
                )
            }
        })
    }

    fun makeReqDemo() {
        Uitls.showProgree(true, this)
        val call = mInterface.requestDemo(
            RequestDemoPost(
                companyPhone = "",
                country = countryHashMap[bin.rqtContrySp.selectedItem.toString()]?.toInt(),
                sourcePage = "Home Page",
                identifier = ApiConstants.identifier,
                extraParams = null,
                city = bin.rqtCtySp.selectedItem.toString(),
                sourceDomain = ApiConstants.source_domain,
                areaCode = bin.areaCodeEdxt.text.toString(),
                demo = false,
                countryCode = bin.countryCodeEdxt.text.toString(),
                samplereport = true,
                phone = bin.countryCodeEdxt.text.toString().plus(bin.areaCodeEdxt.text.toString())
                    .plus(bin.MobileNoEdxt.text.toString()),
                companyName = bin.rqtCmnynameEdxt.text.toString(),
                name = bin.rqtNameEdxt.text.toString(),
                phoneNumber = bin.MobileNoEdxt.text.toString(),
                company = companiesHashMap[bin.rqtCmnynameEdxt.text.toString()],
                designation = bin.rqtDsnynameEdxt.text.toString(),
                email = bin.rqtEmailEdxt.text.toString(),
                bulletin = false
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@RequestDemo)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showDialog(
                            this@RequestDemo,
                            "Thank you! Your request has been successfully submitted. We will contact you shortly to schedule an online demonstration."
                        )
                    } else {
                        Uitls.showToast(
                            this@RequestDemo,
                            response.body()!!.errors
                                ?: "Not able to create your request. try after sometime!"
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@RequestDemo)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@RequestDemo)
                Uitls.handlerError(this@RequestDemo, t)
            }
        })
    }


}