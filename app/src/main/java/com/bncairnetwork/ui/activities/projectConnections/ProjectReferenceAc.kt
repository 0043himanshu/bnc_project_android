package com.bncairnetwork.ui.activities.projectConnections

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.databinding.ActivityProjectReferenceBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.ProjectRefListnr
import com.bncairnetwork.helper.TagListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.response.project.GetProjectRefLstRes
import com.bncairnetwork.pojo.response.project.ProjectsItem
import com.bncairnetwork.ui.activities.AddInteractionAc
import com.bncairnetwork.ui.activities.Tag
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.adapter.project.ProjectReferenceAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectReferenceAc : AppCompatActivity(), ProjectRefListnr, TagListnr {
    lateinit var mInterface: ApiInterface
    var intntID = ""
    lateinit var adptr: ProjectReferenceAptr
    var projectrefLst = ArrayList<ProjectsItem>()
    lateinit var bin: ActivityProjectReferenceBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityProjectReferenceBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()

    }

    fun initView() {
        intntID = intent.getStringExtra(Enums.ID.toString()).toString()
        bin.projectSearchCusAppbar.appbarHeader.text = "Project References"
        bin.projectSearchCusAppbar.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        adptr = ProjectReferenceAptr(
            this@ProjectReferenceAc,
            projectrefLst,
            this@ProjectReferenceAc, this@ProjectReferenceAc
        ).apply {
            bin.refeRcy.layoutManager = LinearLayoutManager(this@ProjectReferenceAc)
            bin.refeRcy.adapter = this
        }
    }

    fun getReferences(id: String?) {
        projectrefLst?.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectRefByID(id ?: "")
        call.enqueue(object : Callback<GetProjectRefLstRes> {
            override fun onResponse(
                call: Call<GetProjectRefLstRes>,
                response: Response<GetProjectRefLstRes>
            ) {
                Uitls.showProgree(false, this@ProjectReferenceAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLayout.noDataLbl.visibility =
                        if (response.body()!!.status == 1) View.GONE else View.VISIBLE
                    if (response.body()!!.status == 1) {
                        bin.referneceCntryTxt.text = response.body()!!.company?.name ?: ""
                        bin.referneceCityTxt.text = response.body()!!.company?.city.plus(",")
                            .plus(response.body()!!.company?.country)
                        response.body()!!.projects?.let { lst ->
                            projectrefLst.addAll(lst)
                            adptr.notifyDataSetChanged()
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectReferenceAc)
                }
            }

            override fun onFailure(call: Call<GetProjectRefLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectReferenceAc)
                Uitls.handlerError(this@ProjectReferenceAc, t)
            }
        })
    }

    override fun onTagClick(name: String?) {

    }

    override fun onProRefClk(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean, comObj: ProjectsItem?
    ) {

        if (isTxt) {
            startActivity(
                Intent(
                    this, ProjectDetailsAc::class.java
                ).putExtra(Enums.ID.toString(), id)
            )
        }
        if (isTag) {
            startActivity(
                Intent(this, Tag::class.java).putExtra(Enums.ID.toString(), intntID)
                    .putExtra(Enums.TYPE.toString(), intent.getStringExtra(Enums.TYPE.toString()))
                    .putExtra(Enums.DATA.toString(), Gson().toJson(comObj))
            )
        }
        if (isAddInteraction) {
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.ID.toString(),
                    id
                )
            )

        }
        if (isReminder) {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(Enums.ID.toString(), id)
                    .putExtra(
                        Enums.TYPE.toString(),
                        ApiConstants.Project
                    )
            )

        }
        if (isFav) {
            runOnUiThread {
                if (!projectrefLst[pos].favourite!!) {
                    makeFavCom(id, pos)
                } else {
                    removeFavCom(id, pos)
                }
            }

        }


    }

    fun makeFavCom(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call =
            mInterface.addFavForCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectReferenceAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        projectrefLst[pos].favourite = true
                        adptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@ProjectReferenceAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectReferenceAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectReferenceAc)
                Uitls.handlerError(this@ProjectReferenceAc, t)
            }
        })
    }

    fun removeFavCom(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call = mInterface.removeFavFromCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectReferenceAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        projectrefLst[pos].favourite = false
                        adptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@ProjectReferenceAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectReferenceAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectReferenceAc)
                Uitls.handlerError(this@ProjectReferenceAc, t)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        runOnUiThread {
            getReferences(id = intntID)
        }
    }
}