package com.bncairnetwork.ui.activities.projectConnections

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityProjectClaimBinding
import com.bncairnetwork.ui.activities.AdvanceSearchAc
import com.bncairnetwork.ui.activities.homeConnections.ChatAc

class ProjectClaim : AppCompatActivity() {
    lateinit var bin: ActivityProjectClaimBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityProjectClaimBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {
        chngbtn(true)
        bin.projectBtn.submitBtn.text = "Projects(134)"
        bin.companiesBtn.submitBtn.text = "Companies(236)"

        bin.tlbrPrjtClm.img.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_back))
        bin.tlbrPrjtClm.img.setOnClickListener {
            onBackPressed()
        }
//        bin.tlbrPrjtClm.img.layoutParams.height = 60
//        bin.tlbrPrjtClm.img.layoutParams.width = 60
            bin.tlbrPrjtClm.appbarHeader.text="Project Claims"

    }

    fun chngbtn(isProjectSelect: Boolean) {
        when (isProjectSelect) {
            true -> {
                bin.companiesBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.grey))
                bin.companiesBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)


                bin.projectBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.projectBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)
            }
            false -> {
                bin.companiesBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.companiesBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)

                bin.projectBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.grey))
                bin.projectBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)
            }
        }
    }

    fun listnr() {
        bin.tlbrPrjtClm.chat.setOnClickListener {
            startActivity(Intent(this, ChatAc::class.java))
        }
        bin.projectBtn.submitBtn.setOnClickListener {
            chngbtn(true)
        }
        bin.companiesBtn.submitBtn.setOnClickListener {
            chngbtn(false)
        }
        bin.moreSercOptnBtn.setOnClickListener {
            startActivity(Intent(this, AdvanceSearchAc::class.java))
        }
        bin.prjctClmFltr.setOnClickListener {
//            startActivity(Intent(this,FilterAc::class.java))
        }

        bin.tlbrPrjtClm.sort.setOnClickListener {
//            Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
//                requestWindowFeature(Window.FEATURE_NO_TITLE)
//                window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
//                val view =
//                    SortByLayBinding.inflate(LayoutInflater.from(this@ProjectClaim))
//                setContentView(view.root)
//                show()
//                view.sortApplyBtn.submitBtn.text = "Ok"
//
//                view.sortApplyBtn.submitBtn.setOnClickListener {
//                    dismiss()
//                }
//
//                view.cancel.setOnClickListener {
//                    dismiss()
//                }
//
//
//            }
        }
        bin.tlbrPrjtClm.map.setOnClickListener {
            startActivity(Intent(this, ProjectNearMeAc::class.java))
        }
    }
}