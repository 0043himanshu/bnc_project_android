package com.bncairnetwork.ui.activities.projectConnections

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityAddCreditBinding
import com.bncairnetwork.databinding.ActivityLiveProjectsBinding
import com.bncairnetwork.databinding.ProjectAlreadyClaimedPopupBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProjectClaimInfoRes
import com.bncairnetwork.pojo.post.project.FavProPost
import com.bncairnetwork.pojo.response.project.LiveProjectRes
import com.bncairnetwork.pojo.response.project.LiveProjectsItem
import com.bncairnetwork.ui.activities.Tag
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.adapter.project.ProjectLiveAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.google.gson.Gson
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LiveProjectsAc : AppCompatActivity(), LiveProjectListner,TagListnr {
    lateinit var bin: ActivityLiveProjectsBinding
    lateinit var mInterface: ApiInterface
    lateinit var liveAptr: ProjectLiveAptr
    private var proID = ""
    var proObj: LiveProjectsItem? = null
    private var packageID = 0
    private var liveResLst: ArrayList<LiveProjectsItem?>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityLiveProjectsBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initViews()
        listnr()
    }

    fun initViews() {
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        liveResLst = ArrayList()
        bin.cusAppbarLay.appbarHeader.text = "Live Projects"
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        proID = intent.getStringExtra(Enums.ID.toString()) ?: ""
        liveAptr = ProjectLiveAptr(
            this@LiveProjectsAc,
            proLst = liveResLst,
            this@LiveProjectsAc, packageID = packageID,this@LiveProjectsAc
        ).apply {
            bin.liveproRv.layoutManager = LinearLayoutManager(this@LiveProjectsAc)
            bin.liveproRv.adapter = this
        }
    }

    fun listnr() {
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
    }

    fun getLiveProject() {
        liveResLst?.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getLiveProjects(proID)
        call.enqueue(object : Callback<LiveProjectRes> {
            override fun onResponse(
                call: Call<LiveProjectRes>,
                response: Response<LiveProjectRes>
            ) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLayout.noDataLbl.visibility =
                        if (response.body()!!.projects?.isNullOrEmpty() != true) View.GONE else View.VISIBLE
                    response.body()!!.projects?.let {
                        liveResLst?.addAll(it)
                    }
                    liveAptr.notifyDataSetChanged()

                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@LiveProjectsAc)
                }
            }

            override fun onFailure(call: Call<LiveProjectRes>, t: Throwable) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                Uitls.handlerError(this@LiveProjectsAc, t)
            }
        })
    }

    fun makeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call =
            mInterface.makeFavForProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        liveResLst?.get(pos)?.tagObj?.isFavourite = true
                        liveAptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@LiveProjectsAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@LiveProjectsAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                Uitls.handlerError(this@LiveProjectsAc, t)
            }
        })
    }

    fun removeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this@LiveProjectsAc)
        val call =
            mInterface.removeFavFromProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        liveResLst?.get(pos)?.tagObj?.isFavourite = false
                        liveAptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@LiveProjectsAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@LiveProjectsAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                Uitls.handlerError(this@LiveProjectsAc, t)
            }
        })
    }

    override fun onTagClick(name: String?) {

    }

    override fun onLiveProClck(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        proObj: LiveProjectsItem?
    ) {

        if (isTxt) {

            this.proObj = proObj
            when (packageID) {
                ApiConstants.professionalID -> {
                    proObj?.authorized?.let { isAuthorised ->
                        if (isAuthorised) {
                            /*this project is in unlocked state*/
                            startActivity(
                                Intent(
                                    this@LiveProjectsAc,
                                    ProjectDetailsAc::class.java
                                ).putExtra(Enums.ID.toString(), id)
                                    .putExtra(
                                        Enums.DATA.toString(),
                                        Gson().toJson(
                                            proObj
                                        )
                                    )
                            )
                        } else {
                            /*this project is in locked state*/
                            runOnUiThread {
                                getProjectInfo(id)
                            }
                        }
                    }
                }
                ApiConstants.ibisID -> {
                    startActivity(
                        Intent(
                            this@LiveProjectsAc,
                            ProjectDetailsAc::class.java
                        ).putExtra(Enums.ID.toString(), id)
                            .putExtra(
                                Enums.DATA.toString(),
                                Gson().toJson(
                                    proObj
                                )
                            )
                    )
                }
                ApiConstants.bi_userID -> {
                    startActivity(
                        Intent(
                            this@LiveProjectsAc,
                            ProjectDetailsAc::class.java
                        ).putExtra(Enums.ID.toString(), id)
                            .putExtra(
                                Enums.DATA.toString(),
                                Gson().toJson(
                                    proObj
                                )
                            )
                    )
                }
            }

            return

        }
        if (isFav) {
            runOnUiThread {
                if (liveResLst?.get(pos)?.tagObj?.isFavourite != true) {
                    makeFavPro(id, pos)
                } else {
                    removeFavPro(id, pos)
                }
            }
            return
        }
        if (isReminder) {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(Enums.ID.toString(), id)
                    .putExtra(
                        Enums.TYPE.toString(),
                        ApiConstants.Project
                    )
            )
            return
        }

        if (isTag) {
            startActivity(
                Intent(this, Tag::class.java).putExtra(Enums.ID.toString(), id).putExtra(
                    Enums.TYPE.toString(),
                    ApiConstants.Project
                ).putExtra(
                    Enums.DATA.toString(),
                    Gson().toJson(proObj)
                )
            )
            return
        }

    }

    fun getProjectInfo(projectID: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectInfo(projectID)
        call.enqueue(object : Callback<ProjectClaimInfoRes> {
            override fun onResponse(
                call: Call<ProjectClaimInfoRes>,
                response: Response<ProjectClaimInfoRes>
            ) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        showProjectClaimDialog(response.body()!!, projectID)

//                        response.body()!!.credits?.let {credit->
//                            if (credit >= 5) {
//                                runOnUiThread {
//                                    //   projectClaimCall()
//                                    updateProjectView(proID = projectID, proObj = proObj)
//                                }
//                            } else {
//                                /*in sufficient credits*/
//                                showInAppOption()
//                            }
//
//                        }

                    } else {
                        Uitls.showToast(
                            this@LiveProjectsAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@LiveProjectsAc)
                }
            }

            override fun onFailure(call: Call<ProjectClaimInfoRes>, t: Throwable) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                Uitls.handlerError(this@LiveProjectsAc, t)
            }
        })
    }

    fun showProjectClaimDialog(obj: ProjectClaimInfoRes, proID: String) {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            val view =
                ProjectAlreadyClaimedPopupBinding.inflate(LayoutInflater.from(this@LiveProjectsAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(view.root)
            view.defBtnLay.submitBtn.text = "View Projects"
            view.rightNotNowLbl.setOnClickListener {
                dismiss()
            }
            view.ownerslblCount.text = obj.project?.owners.toString()
            view.bidderCount.text = obj.project?.bidders.toString()
            view.consultantCount.text = obj.project?.consultants.toString()
            view.contractorCount.text = obj.project?.contractors.toString()
            view.lbl.text = obj.project?.name ?: ""
            view.defBtnLay.submitBtn.setOnClickListener {
                dismiss()
                obj.credits?.let {
                    if (it >= 5) {
                        runOnUiThread {
                            exhaustCredits(proID = proID, proObj = obj)
                        }
                    } else {
                        /*in sufficient credits*/
                        Uitls.showToast(this@LiveProjectsAc, "Insufficient credits")
                    }
                }


            }
            show()
        }
    }

    fun showInAppOption() {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val view_by_crd =
                ActivityAddCreditBinding.inflate(LayoutInflater.from(this@LiveProjectsAc))
            setContentView(view_by_crd.root)
            show()
            view_by_crd.addCrdtInviteLay.setOnClickListener {
                dismiss()
            }
            view_by_crd.addCrdtShareLay.setOnClickListener {
                dismiss()
                runOnUiThread {
                    // getInvitationCall()
                }
            }
            view_by_crd.addCrdtBuyLay.setOnClickListener {
                dismiss()
                /*start follow for in app*/
            }
        }
    }

    fun exhaustCredits(proID: String, proObj: ProjectClaimInfoRes?) {
        Uitls.showProgree(true, this)
        val call = mInterface.exhaustCredit(projectID = proID)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        startActivity(
                            Intent(
                                this@LiveProjectsAc,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), proID)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
                    } else {
                        Uitls.showToast(
                            this@LiveProjectsAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@LiveProjectsAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@LiveProjectsAc)
                Uitls.handlerError(this@LiveProjectsAc, t)
            }
        })
    }


    override fun onResume() {
        super.onResume()
        runOnUiThread {
            getLiveProject()
        }

    }
}