package com.bncairnetwork.ui.activities.homeConnections

import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.adapter.ManageWidgetAptr
import com.bncairnetwork.databinding.ActivityManageWidgetBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.widget.ActivateWidgetPost
import com.bncairnetwork.pojo.post.widget.DeactivateWidgetPost
import com.bncairnetwork.pojo.post.widget.RearrangeWidgetPost
import com.bncairnetwork.pojo.post.widget.UserWidgetsItem
import com.bncairnetwork.pojo.response.WidgetsItem
import com.bncairnetwork.pojo.response.WidgetsLstRes
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.CompletableFuture


class ManageWidgetAc : AppCompatActivity(), UpdateWidget {
    var lst = ArrayList<WidgetsItem>()
    lateinit var widgetadptr: ManageWidgetAptr
    lateinit var mInterface: ApiInterface
    var userWidgetList = ArrayList<UserWidgetsItem>()
    lateinit var bin: ActivityManageWidgetBinding

    @RequiresApi(Build.VERSION_CODES.N)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityManageWidgetBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
        runOnUiThread {
            getWidgets()
        }

    }

    fun getWidgets() {
        lst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getWidgetsLst()
        call.enqueue(object : Callback<WidgetsLstRes> {
            override fun onResponse(call: Call<WidgetsLstRes>, response: Response<WidgetsLstRes>) {
                Uitls.showProgree(false, this@ManageWidgetAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        response.body()!!.widgets?.let {
                            lst.addAll(it)
                            widgetadptr.apply {
                                notifyItemRangeChanged(0, lst.size)
                                val callback: ItemTouchHelper.Callback = ItemMoveCallback(this)
                                val touchHelper = ItemTouchHelper(callback)
                                touchHelper.attachToRecyclerView(bin.mangeWidgetRv)
                            }

//                            ManageWidgetAptr(this@ManageWidgetAc, lst, this@ManageWidgetAc).apply {
//                                bin.mangeWidgetRv.layoutManager =
//                                    LinearLayoutManager(this@ManageWidgetAc)
//                                bin.mangeWidgetRv.adapter = this
//                            }
                        }

                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ManageWidgetAc)
                }
            }

            override fun onFailure(call: Call<WidgetsLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@ManageWidgetAc)
                Uitls.handlerError(this@ManageWidgetAc, t)
            }
        })
    }

    fun initView() {
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.defBtnLay.submitBtn.text = "Update"
        bin.cusAppbarLay.appbarHeader.text = "Manage Widget"
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
//        bin.bncBannerChat.chatBtn.setOnClickListener {
//            startActivity(Intent(this, ChatAc::class.java))
//        }
        widgetadptr = ManageWidgetAptr(
            this@ManageWidgetAc,
            lst,
            this@ManageWidgetAc
        ).apply {
            bin.mangeWidgetRv.layoutManager =
                LinearLayoutManager(this@ManageWidgetAc)
            bin.mangeWidgetRv.adapter = this
        }

    }

    @RequiresApi(Build.VERSION_CODES.N)
    fun listnr() {
        bin.defBtnLay.submitBtn.setOnClickListener {
            CompletableFuture.completedFuture(getRearrangedWidgetLst()).thenAccept {
                onUpdateWidget()
            }

        }
    }

    override fun onUpdate(pos: Int, id: String, flag: Boolean, priority: String) {
        runOnUiThread {
            updateWidget(
                pos, flag,
                if (!flag) mInterface.DeactivateWidget(
                    DeactivateWidgetPost(
                        action = if (flag) ApiConstants.activate else ApiConstants.deactivate,
                        user_widget = id.toInt()
                    )
                ) else mInterface.ActivateWidget(
                    ActivateWidgetPost(
                        action = if (flag) ApiConstants.activate else ApiConstants.deactivate,
                        widget = id.toInt(),
                        priority = priority.toInt()
                    )
                )
            )
        }

    }


    fun updateWidget(pos: Int, flag: Boolean, call: Call<ComRes>) {
        Uitls.showProgree(true, this)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ManageWidgetAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
//                        lst[pos].status = flag
//                        widgetadptr.notifyItemChanged(pos)
                        Uitls.showToast(
                            this@ManageWidgetAc,
                            if (flag) "Widget Activated" else "Widget Deactivated"
                        )
                        //  getWidgets()
                        lst[pos].status = flag
                        widgetadptr.notifyItemChanged(pos)
                    }
                } else {
                    lst[pos].status = !flag
                    widgetadptr.notifyItemChanged(pos)
                    Uitls.onUnSuccessResponse(response.code(), this@ManageWidgetAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ManageWidgetAc)
                Uitls.handlerError(this@ManageWidgetAc, t)
                lst[pos].status = !flag
            }
        })
    }

    fun onUpdateWidget() {
        Uitls.showProgree(true, this)
        val call = mInterface.RearrangeWidget(
            RearrangeWidgetPost(
                userWidgets = userWidgetList,
                action = "rearrange"
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ManageWidgetAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(
                            this@ManageWidgetAc,
                            "Updated Successfully"
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ManageWidgetAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ManageWidgetAc)
                Uitls.handlerError(this@ManageWidgetAc, t)
            }
        })
    }

    fun getRearrangedWidgetLst(): ArrayList<UserWidgetsItem> {
        userWidgetList.clear()
        widgetadptr.lst.forEachIndexed { index, widgetsItem ->
            userWidgetList.add(
                UserWidgetsItem(
                    id = widgetsItem.userWidgetId ?: 0,
                    priority = index
                )
            )
        }
        return userWidgetList
    }

}