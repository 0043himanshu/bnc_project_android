package com.bncairnetwork.ui.activities.projectConnections

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityProjectNearMeBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.ProjectNearMeLisntr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProjectClaimInfoRes
import com.bncairnetwork.pojo.post.project.FavProPost
import com.bncairnetwork.pojo.response.ProjectNearDataItem
import com.bncairnetwork.pojo.response.ProjectNearMeRes
import com.bncairnetwork.ui.activities.AddInteractionAc
import com.bncairnetwork.ui.activities.Tag
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.adapter.project.ProjectNearMeAptr
import com.bncairnetwork.webservices.ApiInterface
import com.google.gson.Gson
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectNearMeAc : AppCompatActivity(), ProjectNearMeLisntr {
    lateinit var bin: ActivityProjectNearMeBinding
    lateinit var mInterface: ApiInterface
    private var packageID = 0
    var data: ArrayList<ProjectNearDataItem?>? = null
    lateinit var adptr: ProjectNearMeAptr
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityProjectNearMeBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        data = ArrayList()
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.projectCrdHome.prjctNearMeMapImg.layoutParams.height = 400
        bin.projectCrdHome.imageView5.layoutParams.height = 400

        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.cusAppbarLay.appbarHeader.text = "Projects Near me"
        bin.projectCrdHome.projectBtn.setCompoundDrawablesWithIntrinsicBounds(
            0,
            0,
            0,
            0
        )
        adptr = ProjectNearMeAptr(this, data, this, packageID).apply {
            bin.projctNearMeRv.layoutManager = LinearLayoutManager(this@ProjectNearMeAc)
            bin.projctNearMeRv.adapter = this
        }
    }

    fun listnr() {

    }
    /*  latitude = PrefUtils.with(this).getString(ApiConstants.currentLat, "") ?: "",
            longitude = PrefUtils.with(this).getString(ApiConstants.currentLong, "") ?: "",*/

    fun getProjectNearMe(widgetID: String) {
        data?.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectNearMe(
            latitude = PrefUtils.with(this).getString(Enums.CurrentLat.toString(), "") ?: "",
            longitude = PrefUtils.with(this).getString(Enums.CurrentLong.toString(), "") ?: "",
            widgetID = widgetID
        )
        call.enqueue(object : Callback<ProjectNearMeRes> {
            override fun onResponse(
                call: Call<ProjectNearMeRes>,
                response: Response<ProjectNearMeRes>
            ) {
                Uitls.showProgree(false, this@ProjectNearMeAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.widgetData?.content?.get(0)?.data?.isNullOrEmpty() != true) {
                        bin.noDataLayout.noDataLbl.visibility = View.GONE
                        data?.addAll(response.body()!!.widgetData?.content?.get(0)?.data!!)
                        adptr.notifyDataSetChanged()
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectNearMeAc)
                }
            }

            override fun onFailure(call: Call<ProjectNearMeRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectNearMeAc)
                Uitls.handlerError(this@ProjectNearMeAc, t)
            }
        })
    }

    override fun onProjectClick(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        proObj: ProjectNearDataItem?
    ) {

        if (isTxt) {
//            if(proObj?.authorized==true)
            startActivity(
                Intent(
                    this,
                    ProjectDetailsAc::class.java
                ).putExtra(Enums.ID.toString(), id)
                    .putExtra(
                        Enums.DATA.toString(),
                        Gson().toJson(proObj)
                    )
            )
            return
        }
        if (isTag) {
            startActivity(
                Intent(this, Tag::class.java).putExtra(Enums.ID.toString(), id).putExtra(
                    Enums.TYPE.toString(), ApiConstants.Project
                ).putExtra(
                    Enums.DATA.toString(),
                    Gson().toJson(proObj)
                )
            )
            return
        }
        if (isAddInteraction) {
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.ID.toString(),
                    id
                ).putExtra(
                    Enums.TYPE.toString(),
                    ApiConstants.Project
                ).putExtra(
                    Enums.ProjectOrCompanyName.toString(),
                    proObj?.name
                )
                    .putExtra(
                        Enums.ProjectOrCompanyCountry.toString(),
                        proObj?.country
                    )
                    .putExtra(
                        Enums.ProjectOrCompanyCity.toString(),
                        proObj?.city
                    )


            )
            return

        }
        if (isReminder) {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(Enums.ID.toString(), id)
                    .putExtra(
                        Enums.TYPE.toString(),
                        ApiConstants.Project
                    )
            )
            return
        }
        if (isFav) {
            runOnUiThread {
                if (data?.get(pos)?.favourite != true) {
                    makeFavPro(id, pos)
                } else {
                    removeFavPro(id, pos)
                }
            }
            return
        }

    }

    fun makeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call =
            mInterface.makeFavForProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {

                Uitls.showProgree(false, this@ProjectNearMeAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        data?.get(pos)?.favourite = true
                        adptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@ProjectNearMeAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectNearMeAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectNearMeAc)
                Uitls.handlerError(this@ProjectNearMeAc, t)
            }
        })
    }

    fun removeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this@ProjectNearMeAc)
        val call =
            mInterface.removeFavFromProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectNearMeAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        data?.get(pos)?.favourite = false
                        adptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@ProjectNearMeAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectNearMeAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectNearMeAc)
                Uitls.handlerError(this@ProjectNearMeAc, t)
            }
        })
    }

    fun getProjectInfo(projectID: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectInfo(projectID)
        call.enqueue(object : Callback<ProjectClaimInfoRes> {
            override fun onResponse(
                call: Call<ProjectClaimInfoRes>,
                response: Response<ProjectClaimInfoRes>
            ) {
                Uitls.showProgree(false, this@ProjectNearMeAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        //showProjectClaimDialog(response.body()!!, projectID)
                    } else {
                        Uitls.showToast(
                            this@ProjectNearMeAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectNearMeAc)
                }
            }

            override fun onFailure(call: Call<ProjectClaimInfoRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectNearMeAc)
                Uitls.handlerError(this@ProjectNearMeAc, t)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        runOnUiThread {
            getProjectNearMe(intent.getStringExtra(Enums.DATA.toString()) ?: "0")
        }
    }

}