package com.bncairnetwork.ui.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityFilterBinding
import com.bncairnetwork.databinding.MultiSelectionLayBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.MultiSelectionListnr
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ProjectSearchRes
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.ui.adapter.MultiSpinnerAdptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FilterAc : AppCompatActivity(), MultiSelectionListnr {
    lateinit var bin: ActivityFilterBinding
    var tobepass = ArrayList<String>()
    var obj: GetMasterDataRes? = null
    lateinit var dialog: Dialog
    lateinit var mInterface: ApiInterface
    var selectedProSector = HashMap<String, String>()
    var proSectorsHashMap = HashMap<String, String>()
    lateinit var dialogView: MultiSelectionLayBinding
    var selectedproStages = HashMap<String, String>()
    lateinit var mutliSpAptr: MultiSpinnerAdptr
    var proStagesHashMap = HashMap<String, String>()
    var resType =
        0

    /***
     *0-> stageType, 1->sectorType */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityFilterBinding.inflate(layoutInflater)
        setContentView(bin.root)

        initView()
        listnr()
    }

    private fun initView() {
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.projectBtn.submitBtn.text = "Apply"
        bin.cusAppbarLay.appbarHeader.text = "Filter"
        dialog = Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            dialogView =
                MultiSelectionLayBinding.inflate(LayoutInflater.from(this@FilterAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(dialogView.root)
        }
    }

    private fun listnr() {
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.projectBtn.submitBtn.setOnClickListener {
            runOnUiThread {
                makeFilterCall()

            }
        }
        bin.createdfromEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.createdfromEdxt
            )
        }
        bin.createdtoEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.createdtoEdxt
            )
        }
        bin.stageTypeSp.setOnClickListener {
            resType = 0
            showMultiSelctionDialog(
                dialogView,
                if (proStagesHashMap.isNotEmpty()) proStagesHashMap.keys.toList() as ArrayList<String> else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                println(
                    "--------->${
                        selectedproStages.keys.toString().replace("[", "").replace(
                            "]", ""
                        )
                    }"
                )
                bin.stageTypeSp.setText(
                    selectedproStages.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
                dialog.dismiss()
            }
            dialog.show()
        }

        bin.proSectorSp.setOnClickListener {
            resType = 1
            showMultiSelctionDialog(
                dialogView,
                if (proSectorsHashMap.isNotEmpty()) proSectorsHashMap.keys.toList() as ArrayList<String> else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                bin.proSectorSp.setText(
                    selectedProSector.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
//                obj?.types?.forEach {
//                    if (selectedProSector.containsValue(it?.sector.toString())) {
//                        proIndustrytyesHashMap[it?.name ?: ""] = it?.id.toString()
//                    }
//                }
//                println(proIndustrytyesHashMap)
                print(selectedProSector)
                dialog.dismiss()
            }
            dialog.show()


        }

    }

    private fun makeFilterCall() {
        Uitls.showProgree(true, this)
        val call = mInterface.advanceSearchByProject(
            widgetIdentifier = "projects_near_me",
            latitude = PrefUtils.with(this).getString(Enums.CurrentLat.toString(), "") ?: "",
            longitude = PrefUtils.with(this).getString(Enums.CurrentLong.toString(), "") ?: "",
            nearby_stage = "[10]",
            sector = if (selectedProSector.isNotEmpty()) selectedProSector.values.toString() else null,
            project_stage = if (selectedproStages.isNotEmpty()) selectedproStages.values.toList()
                .toString() else null,
            value_from = if (bin.createdfromEdxt.text.toString()
                    .trim().isNotEmpty()
            ) bin.createdfromEdxt.text.toString() else null,
            value_to = if (bin.createdtoEdxt.text.toString()
                    .trim().isNotEmpty()
            ) bin.createdtoEdxt.text.toString() else null
        )
        call.enqueue(object : Callback<ProjectSearchRes> {
            override fun onResponse(
                call: Call<ProjectSearchRes>,
                response: Response<ProjectSearchRes>
            ) {
                Uitls.showProgree(false, this@FilterAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()?.total_projects != "0") {
                        setResult(
                            Activity.RESULT_OK, Intent().putExtra(
                                Enums.mMapViewProjectLst.toString(),
                                Gson().toJson(response.body()?.projects)
                            )
                        )
                        finish()
//                        startActivity(
//                            Intent(
//                                this@FilterAc,
//                                MapViewAc::class.java
//                            ).putExtra(
//                                Enums.mMapViewProjectLst.toString(),
//                                Gson().toJson(response.body()?.projects)
//                            )
//                        )
                    }
                }

            }

            override fun onFailure(call: Call<ProjectSearchRes>, t: Throwable) {
                Uitls.showProgree(false, this@FilterAc)
                Uitls.showToast(this@FilterAc, t.message ?: "")
            }
        })

    }

    fun showMultiSelctionDialog(
        view: MultiSelectionLayBinding,
        lst: ArrayList<String>
    ) {
        view.noDataLbl.noDataLbl.visibility = if (lst.isEmpty()) View.VISIBLE else View.GONE
        tobepass.clear()

        when (resType) {
            0 -> {
                view.lbl.setText(R.string.select_stage)
                selectedproStages.keys.forEach {
                    tobepass.add(it)
                }
            }
            1 -> {
                view.lbl.setText(R.string.select_sector)
                selectedProSector.keys.forEach {
                    tobepass.add(it)
                }
            }
        }
        mutliSpAptr = MultiSpinnerAdptr(
            this@FilterAc,
            lst,
            tobepass,
            this
        ).apply {
            view.multiRv.layoutManager = LinearLayoutManager(this@FilterAc)
            view.multiRv.adapter = this
        }
    }

    fun loadProjectNdCompaniesDropDownsData() {
        runOnUiThread {
            if (proStagesHashMap.isEmpty()) {
                PrefUtils.with(this).apply {
                    obj = Gson().fromJson(
                        getString(Enums.MASTERDATA.toString(), ""),
                        GetMasterDataRes::class.java
                    )
                    obj?.stages?.forEach {
                        proStagesHashMap[it?.name ?: ""] = it?.id.toString()
                    }
                    obj?.sectors?.forEach {
                        proSectorsHashMap[it?.name ?: ""] = it?.id.toString()
                    }

                }

            }

        }


    }

    override fun onMakeSelection(pos: Int, name: String, flag: Boolean) {
        when (resType) {
            0 -> {
                if (!selectedproStages.containsKey(name)) {
                    selectedproStages[name] = proStagesHashMap[name].toString()
                } else {
                    selectedproStages.remove(
                        name
                    )
                }
            }
            1 -> {
                if (!selectedProSector.containsKey(name)) {
                    selectedProSector[name] = proSectorsHashMap[name].toString()
                } else {
                    selectedProSector.remove(
                        name
                    )
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        loadProjectNdCompaniesDropDownsData()
    }

}