package com.bncairnetwork.ui.activities


import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.adapter.ShareReminderAptr
import com.bncairnetwork.databinding.ActivityInteractionBinding
import com.bncairnetwork.databinding.DeleteReminderBinding
import com.bncairnetwork.databinding.ShareReminderLayBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.interaction.DeleteInteractionPost
import com.bncairnetwork.pojo.post.interaction.ShareInteractionWithUserPost
import com.bncairnetwork.pojo.response.FullUsersItem
import com.bncairnetwork.pojo.response.ShareWithUserLstRes
import com.bncairnetwork.pojo.response.company.GetInteractionCompny
import com.bncairnetwork.pojo.response.company.InteractionsItem
import com.bncairnetwork.pojo.response.project.GetInteractionProjectRes
import com.bncairnetwork.pojo.response.project.InteractionDataItem
import com.bncairnetwork.ui.adapter.interaction.IntractionListAdapter
import com.bncairnetwork.webservices.ApiInterface
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class InteractionAc : AppCompatActivity(), InteractionLisntr, ShareWithUserListr {
    lateinit var mInterface: ApiInterface
    var intentType = ""
    var intentID = ""
    var intentName = ""
    var intentRcrdType = ""
    var userCompanyID = ""
    private var packageID = 0
    var shareWithlst = ArrayList<FullUsersItem?>()
    var selectedShareWithLst = ArrayList<Int>()
    lateinit var intrctnAptr: IntractionListAdapter
    var projctlst: ArrayList<InteractionDataItem?>? = null//interaction project list
    var cmylst: ArrayList<InteractionsItem?>? = null//interaction company list,
    lateinit var bin: ActivityInteractionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityInteractionBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()

    }

    fun initView() {
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        UserRestrictionRepo.interactionListScreenRestriction(
            bin,
            packageID
        )
        userCompanyID = PrefUtils.with(this).getString(Enums.USER_COMPANY_ID.toString(), "") ?: ""
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.intractionLay.appbarHeader.text = "Interactions"
        bin.defBtnLay.submitBtn.text = "Add New Interaction"
        if (intent.getStringExtra(Enums.TYPE.toString()) != null) {
            intentType = intent.getStringExtra(Enums.TYPE.toString()) ?: "Project"
            intentID = intent.getStringExtra(Enums.ID.toString()) ?: "0"
            intentRcrdType = intent.getStringExtra(Enums.RECORDTYPE.toString()) ?: ""
            projctlst = ArrayList()
            cmylst = ArrayList()
            intrctnAptr = IntractionListAdapter(
                this@InteractionAc,
                projctlst = if (intentType == ApiConstants.Project) projctlst else null,
                cmylst = if (intentType == ApiConstants.Company) cmylst else null,
                this@InteractionAc,packageID = packageID
            ).apply {
                bin.intractionRv.layoutManager =
                    LinearLayoutManager(this@InteractionAc)
                bin.intractionRv.adapter = this
            }
        }


    }

    fun listnr() {
        bin.defBtnLay.submitBtn.setOnClickListener {
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.TYPE.toString(),
                    intentType
                ).putExtra(Enums.NAME.toString(), intentName)
                    .putExtra(Enums.ID.toString(), intentID)
            )
        }


        bin.intractionLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }

    }


    fun getinteraction_project() {
        //182204
        Uitls.showProgree(true, this)
        val call = mInterface.getInteractionOnProjectByID(
            id = intentID,
            isIBIS = intentRcrdType.contains("IBIS")
        )
        call.enqueue(object : Callback<GetInteractionProjectRes> {
            override fun onResponse(
                call: Call<GetInteractionProjectRes>,
                response: Response<GetInteractionProjectRes>
            ) {
                Uitls.showProgree(false, this@InteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLbl.noDataLbl.visibility =
                        if (response.body()!!.status == 1 && response.body()!!.interactions?.isNotEmpty() == true) View.GONE else View.VISIBLE
                    if (response.body()!!.status == 1) {
                        response.body()!!.interactions?.let {
                            projctlst?.addAll(it)
                            intrctnAptr.notifyDataSetChanged()
                            if (shareWithlst.isEmpty()) {
                                getShareWithUsers()
                            }
                        }

                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@InteractionAc)
                }
            }

            override fun onFailure(call: Call<GetInteractionProjectRes>, t: Throwable) {
                Uitls.showProgree(false, this@InteractionAc)
                Uitls.handlerError(this@InteractionAc, t)
            }
        })
    }

    fun getinteraction_company() {
        Uitls.showProgree(true, this)
        val call = mInterface.getInteractionOnCompnyByID(
            id = intentID,
            isIBIS = intentRcrdType.contains("IBIS")
        )
        call.enqueue(object : Callback<GetInteractionCompny> {
            override fun onResponse(
                call: Call<GetInteractionCompny>,
                response: Response<GetInteractionCompny>
            ) {
                Uitls.showProgree(false, this@InteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLbl.noDataLbl.visibility =
                        if (response.body()!!.status == 1 && response.body()!!.interactions?.isNotEmpty() == true) View.GONE else View.VISIBLE
                    if (response.body()!!.status == 1) {
                        response.body()!!.interactions?.let {
                            val pos = if (cmylst != null) cmylst!!.size + 1 else 0
                            cmylst?.addAll(it)
                            //  intrctnAptr.notifyItemInserted(it.size)
                            intrctnAptr.notifyDataSetChanged()
                            if (shareWithlst.isEmpty()) {
                                getShareWithUsers()
                            }
                        }

                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@InteractionAc)
                }
            }

            override fun onFailure(call: Call<GetInteractionCompny>, t: Throwable) {
                Uitls.showProgree(false, this@InteractionAc)
                Uitls.handlerError(this@InteractionAc, t)
            }
        })
    }

    fun deleteInteractionByIDCall(id: String, dialog: Dialog, pos: Int) {
        Uitls.showProgree(true, this)
        val call = mInterface.deleteInteraction(
            DeleteInteractionPost(
                ids = arrayListOf(id)
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@InteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        if (projctlst != null && projctlst?.isNotEmpty() == true) {
                            projctlst?.removeAt(pos)
                            intrctnAptr.notifyItemRemoved(pos)
                            intrctnAptr.notifyItemRangeChanged(
                                pos,
                                projctlst?.size ?: 0
                            )
                        } else {
                            cmylst?.removeAt(pos)
                            intrctnAptr.notifyItemRemoved(pos)
                            intrctnAptr.notifyItemRangeChanged(
                                pos,
                                cmylst?.size ?: 0
                            )
                        }


                        dialog.dismiss()
                    } else {
                        Uitls.showToast(
                            this@InteractionAc,
                            response.body()!!.errors
                                ?: "Not able to delete interaction at this movement"
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@InteractionAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@InteractionAc)
                Uitls.handlerError(this@InteractionAc, t)
            }
        })

    }

    fun getShareWithUsers() {
        shareWithlst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getShareWithUserLst("[$userCompanyID]")
        call.enqueue(object : Callback<ShareWithUserLstRes> {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(
                call: Call<ShareWithUserLstRes>,
                response: Response<ShareWithUserLstRes>
            ) {
                Uitls.showProgree(false, this@InteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        response.body()!!.companyData?.get(0)?.fullUsers?.let { userObj ->
                            shareWithlst.addAll(userObj)
                        }
                    } else {
                        Uitls.showToast(this@InteractionAc, "")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@InteractionAc)
                }
            }

            override fun onFailure(call: Call<ShareWithUserLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@InteractionAc)
                Uitls.handlerError(this@InteractionAc, t)
            }
        })
    }

    fun submitInteractionWithUsers(interactionID: String, isReminderNoti: Boolean) {
        Uitls.showProgree(true, this)
        val call = mInterface.shareInteractionWithUser(
            ShareInteractionWithUserPost(
                email_notification = isReminderNoti,
                selectedShareWithLst
            ), interactionID = interactionID
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@InteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(this@InteractionAc, "Interaction shared successfully.")
                    } else {
                        Uitls.showToast(
                            this@InteractionAc,
                            "Interaction not shared at this movement."
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@InteractionAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@InteractionAc)
                Uitls.handlerError(this@InteractionAc, t)
            }
        })
    }


//    fun showMultiSelctionDialog(
//        view: MultiSelectionLayBinding,
//        lst: ArrayList<FullUsersItem?>, selection: ArrayList<FullUsersItem?>
//    ) {
//        println(lst)
//        view.lbl.text = "Select"
//        view.okbtn.visibility = View.VISIBLE
//        mutliSpAptr = ShareWithMultiSpAdptr(
//            this@InteractionAc,
//            lst,
//            selection,
//            this
//        ).apply {
//            view.multiRv.layoutManager = LinearLayoutManager(this@InteractionAc)
//            view.multiRv.adapter = this
//        }
//    }

    override fun onSharedWithUserClick(pos: Int, flag: Boolean, obj: FullUsersItem?) {
        if (flag) selectedShareWithLst.add(obj?.id ?: 0) else selectedShareWithLst.remove(
            obj?.id ?: 0
        )
    }


    override fun onInteractionLstnr(
        pos: Int, obj1: InteractionDataItem?, obj2: InteractionsItem?, isDel: Boolean,
        isEdit: Boolean,
        isShare: Boolean
    ) {
        if (isEdit) {
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.OBJ.toString(),
                    Gson().toJson(obj1 ?: obj2)
                ).putExtra(
                    Enums.TYPE.toString(),
                    if (obj1 != null) ApiConstants.Project else ApiConstants.Company
                ).putExtra(Enums.ID.toString(), obj1?.id ?: obj2?.id ?: "")
            )
            return
        }

        if (isDel) {
            Dialog(this).apply {
                val view = DeleteReminderBinding.inflate(LayoutInflater.from(this@InteractionAc))
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCanceledOnTouchOutside(true)
                setContentView(view.root)
                view.noBtn.setOnClickListener {
                    dismiss()
                }
                view.lbl.text = "Delete Interaction"
                view.lbl1.text = "Do you really want to delete this interaction?"
                view.yesBtn.setOnClickListener {
                    runOnUiThread {
                        deleteInteractionByIDCall(obj1?.id ?: obj2?.id ?: "", this, pos = pos)
                    }
                }
                show()
            }
            return
        }

        if (isShare) {
            BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme).apply {
                val view =
                    ShareReminderLayBinding.inflate(LayoutInflater.from(this@InteractionAc))
                setContentView(view.root)
                view.lbl.text = "Share Interaction"
                val adptr =
                    ShareReminderAptr(this@InteractionAc, shareWithlst, this@InteractionAc).apply {
                        view.shareRemRv.layoutManager = LinearLayoutManager(this@InteractionAc)
                        view.shareRemRv.adapter = this
                    }
                view.defBtnLay.submitBtn.text = "Send"
                view.defBtnLay.submitBtn.setOnClickListener {
                    runOnUiThread {
                        submitInteractionWithUsers(
                            obj1?.id ?: obj2?.id ?: "",
                            view.sndNotificaionChkBox.isChecked
                        )
                    }
                    dismiss()
                }
                view.searchPeopleBarEdxt.afterTextChanged {
                    adptr.filter.filter(it)
                }
                show()

            }
            return
        }


    }

    override fun onResume() {
        super.onResume()
        runOnUiThread {

            println(intentType)
            projctlst?.clear()
            cmylst?.clear()
            when (intentType) {
                ApiConstants.Project -> {
                    getinteraction_project()
                }
                ApiConstants.Company -> {
                    getinteraction_company()
                }
            }
        }
    }
}