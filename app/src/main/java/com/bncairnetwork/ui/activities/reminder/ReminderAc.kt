package com.bncairnetwork.ui.activities.reminder

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.format.DateFormat.is24HourFormat
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.adapter.ShareReminderAptr
import com.bncairnetwork.databinding.*
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.interaction.ShareInteractionWithUserPost
import com.bncairnetwork.pojo.post.reminder.EditReminderPost
import com.bncairnetwork.pojo.post.reminder.MarkAsCompletePost
import com.bncairnetwork.pojo.response.FullUsersItem
import com.bncairnetwork.pojo.response.ShareWithUserLstRes
import com.bncairnetwork.pojo.response.reminder.EditReminderRes
import com.bncairnetwork.pojo.response.reminder.GetReminderLstRes
import com.bncairnetwork.pojo.response.reminder.RemindersItem
import com.bncairnetwork.pojo.response.reminder.project_and_company.GetReminderLstByIdRes
import com.bncairnetwork.pojo.response.reminder.project_and_company.RemindersItemByID
import com.bncairnetwork.ui.adapter.reminder.ReminderAptr
import com.bncairnetwork.ui.adapter.reminder.ReminderByIDAptr
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class ReminderAc : AppCompatActivity(), ReminderListnr, ReminderByIDListnr, ShareWithUserListr {
    var reminderResLst = ArrayList<RemindersItem>()
    var reminderResLstByID =
        ArrayList<RemindersItemByID>()
    var intentRcrdType = ""
    var intetntID = ""
    var intentType = ""
    var userCompanyID = ""
    var shareWithlst = ArrayList<FullUsersItem?>()
    var selectedShareWithLst = ArrayList<Int>()
    lateinit var mInterface: ApiInterface
    lateinit var adptr: ReminderAptr
    lateinit var adptrby_id: ReminderByIDAptr
    lateinit var bin: ActivityAddReminderBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityAddReminderBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
        if (intent.getStringExtra(Enums.ID.toString()) == null) {
            bin.defBtnLay.submitBtn.visibility = View.GONE
            adptr = ReminderAptr(this, reminderResLst, this).apply {
                bin.remnderRv.layoutManager = LinearLayoutManager(this@ReminderAc)
                bin.remnderRv.adapter = this
            }
            runOnUiThread {
                getReminderLst()
            }
        }

    }

    fun initView() {
        intentRcrdType = intent.getStringExtra(Enums.RECORDTYPE.toString()) ?: ""
        intetntID = intent.getStringExtra(Enums.ID.toString()) ?: ""
        intentType = intent.getStringExtra(Enums.TYPE.toString()) ?: "Project"
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        userCompanyID = PrefUtils.with(this).getString(Enums.USER_COMPANY_ID.toString(), "") ?: ""
        bin.cusAppbarLay.appbarHeader.text = "Reminders"
        bin.defBtnLay.submitBtn.text = "Add Reminder"


    }

    fun listnr() {
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.defBtnLay.submitBtn.setOnClickListener {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(
                    Enums.ID.toString(),
                    intetntID
                ).putExtra(Enums.TYPE.toString(), intentType)
            )
        }
    }


    fun deleteReminderByIDCall(id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.deleteReminder(id)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        runOnUiThread {
                            if (reminderResLst.isNotEmpty()) {
                                getReminderLst()
                            } else {
                                getReminderByID(intetntID)
                            }

                        }
                        Uitls.showToast(this@ReminderAc, "Reminder deleted.")
                    } else {
                        Uitls.showToast(this@ReminderAc, "Not able to delete reminder.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ReminderAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ReminderAc)
                Uitls.handlerError(this@ReminderAc, t)
            }
        })
    }

    fun mrkAsComCall(id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.markRemAsComp(
            id, MarkAsCompletePost(
                email_notification = "",
                action = Enums.Complete.toString()
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(this@ReminderAc, "Marked as completed.")
                        runOnUiThread {
                            if (reminderResLst.isNotEmpty()) {
                                getReminderLst()
                            } else {
                                getReminderByID(intetntID)
                            }

                        }
                    } else {
                        Uitls.showToast(this@ReminderAc, "Not able to update reminder.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ReminderAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ReminderAc)
                Uitls.handlerError(this@ReminderAc, t)
            }
        })

    }

    fun editReminderCall(id: String, dialog: Dialog, obj: EditReminderLayBinding) {
        Uitls.showProgree(true, this)
        val call = mInterface.editReminder(
            EditReminderPost(
                reminderText = obj.reminderExt.text.toString(),
                date = obj.datePicker.text.toString(),
                note = "",
                mobile = true,
                action = Enums.EDIT.toString(),
                time = obj.TimePicker.text.toString(),
                emailNotification = true
            ), id
        )
        call.enqueue(object : Callback<EditReminderRes> {
            override fun onResponse(
                call: Call<EditReminderRes>,
                response: Response<EditReminderRes>
            ) {
                Uitls.showProgree(false, this@ReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        dialog.dismiss()
                        runOnUiThread {
                            if (reminderResLst.isNotEmpty()) {
                                getReminderLst()
                            } else {
                                getReminderByID(intetntID)
                            }
                        }
                    } else {
                        Uitls.showToast(this@ReminderAc, response.body()!!.errors ?: "")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ReminderAc)
                }
            }

            override fun onFailure(call: Call<EditReminderRes>, t: Throwable) {
                Uitls.showProgree(false, this@ReminderAc)
                Uitls.handlerError(this@ReminderAc, t)
            }
        })
    }

    fun getReminderByID(id: String?) {
        Uitls.showProgree(true, this)
        val call = mInterface.getReminderByID(
            type = if (intentType == ApiConstants.Company) "companies" else "projects",
            id = id ?: "",
            isIBIS = intentRcrdType.contains("Internal") or intentRcrdType.contains("Linked")
        )
        call.enqueue(object : Callback<GetReminderLstByIdRes> {
            override fun onResponse(
                call: Call<GetReminderLstByIdRes>,
                response: Response<GetReminderLstByIdRes>
            ) {
                Uitls.showProgree(false, this@ReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.remnderRv.visibility =
                        if (response.body()!!.status == 1) View.VISIBLE else View.GONE
                    bin.noDataLayout.noDataLbl.visibility =
                        if (response.body()!!.reminders.isEmpty()) View.VISIBLE else View.GONE
                    if (response.body()!!.status == 1) {
                        reminderResLstByID.clear()
                        reminderResLstByID.addAll(response.body()!!.reminders)
                        adptrby_id.notifyDataSetChanged()
                        if (shareWithlst.isEmpty()) {
                            getShareWithUsers()
                        }


                    }

                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ReminderAc)
                }
            }

            override fun onFailure(call: Call<GetReminderLstByIdRes>, t: Throwable) {
                Uitls.showProgree(false, this@ReminderAc)
                Uitls.handlerError(this@ReminderAc, t)
            }
        })
    }

    fun getShareWithUsers() {
        shareWithlst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getShareWithUserLst("[$userCompanyID]")
        call.enqueue(object : Callback<ShareWithUserLstRes> {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(
                call: Call<ShareWithUserLstRes>,
                response: Response<ShareWithUserLstRes>
            ) {
                Uitls.showProgree(false, this@ReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        response.body()!!.companyData?.get(0)?.fullUsers?.let { userObj ->
                            shareWithlst.addAll(userObj)
                        }
                    } else {
                        Uitls.showToast(this@ReminderAc, "")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ReminderAc)
                }
            }

            override fun onFailure(call: Call<ShareWithUserLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@ReminderAc)
                Uitls.handlerError(this@ReminderAc, t)
            }
        })
    }

    fun getReminderLst() {
        Uitls.showProgree(true, this)
        val call = mInterface.getReminderLst()
        call.enqueue(object : Callback<GetReminderLstRes> {
            override fun onResponse(
                call: Call<GetReminderLstRes>,
                response: Response<GetReminderLstRes>
            ) {
                Uitls.showProgree(false, this@ReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.remnderRv.visibility =
                        if (response.body()!!.status == 1) View.VISIBLE else View.GONE
                    bin.noDataLayout.noDataLbl.visibility =
                        if (response.body()!!.status == 1) View.GONE else View.VISIBLE
                    if (response.body()!!.status == 1) {
                        if (response.body()!!.reminders.isNotEmpty()) {
                            reminderResLst.clear()
                            reminderResLst.addAll(response.body()!!.reminders)
                            adptr.notifyDataSetChanged()
                        } else {
                            bin.remnderRv.visibility = View.GONE
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ReminderAc)
                }
            }

            override fun onFailure(call: Call<GetReminderLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@ReminderAc)
                Uitls.handlerError(this@ReminderAc, t)
            }
        })

    }

    override fun onRrminderClk(
        pos: Int,
        id: String,
        isDel: Boolean,
        isEdit: Boolean,
        isShare: Boolean,
        isMarkCom: Boolean,
        data: RemindersItem?
    ) {

        if (isShare) {
            BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme).apply {
                val view =
                    ShareReminderLayBinding.inflate(LayoutInflater.from(this@ReminderAc))
                setContentView(view.root)
                view.lbl.text = "Share Interaction"
                view.searchPeopleBarEdxt.visibility = View.GONE
                view.sndNotificaionChkBox.visibility = View.GONE
                val adptr =
                    ShareReminderAptr(this@ReminderAc, shareWithlst, this@ReminderAc).apply {
                        view.shareRemRv.layoutManager = LinearLayoutManager(this@ReminderAc)
                        view.shareRemRv.adapter = this

                    }

                view.defBtnLay.submitBtn.text = "Send"
                view.defBtnLay.submitBtn.setOnClickListener {
                    dismiss()
                    runOnUiThread {
                        submitInteractionWithUsers(
                            interactionID = data?.id.toString(),
                            view.sndNotificaionChkBox.isChecked
                        )
                    }
                }
                view.searchPeopleBarEdxt.afterTextChanged {
                    adptr.filter.filter(it)
                }
                show()

            }
            return
        }


        if (isDel) {
            Dialog(this).apply {
                val view = DeleteReminderBinding.inflate(LayoutInflater.from(this@ReminderAc))
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCanceledOnTouchOutside(true)
                setContentView(view.root)
                view.noBtn.setOnClickListener {
                    dismiss()
                }
                view.yesBtn.setOnClickListener {
                    runOnUiThread {
                        deleteReminderByIDCall(data?.id.toString())
                    }
                    reminderResLst?.removeAt(pos)
                    adptr.notifyItemRemoved(pos)
                    adptr.notifyItemRangeChanged(pos, reminderResLst?.size ?: 0)
                    dismiss()
                }
                show()
            }
            return
        }

        if (isEdit) {
            Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
                val view = EditReminderLayBinding.inflate(LayoutInflater.from(this@ReminderAc))
                setContentView(view.root)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                view.reminderExt.setText(data?.reminder ?: "")
                if (data?.company != null) {
                    view.compnyLbl.text = data.company.name ?: ""
                } else {
                    view.compnyLbl.text = data?.project?.name ?: ""
                }
                view.TimePicker.setText(
                    SimpleDateFormat("hh:mmaa", Locale.getDefault()).format(
                        SimpleDateFormat(
                            "dd MMM,yyyy hh:mmaa",
                            Locale.getDefault()
                        ).parse(data?.remindOn ?: "") ?: ""
                    )
                )
                view.datePicker.setText(
                    SimpleDateFormat("dd MMM,yyyy", Locale.getDefault()).format(
                        SimpleDateFormat(
                            "dd MMM,yyyy hh:mmaa",
                            Locale.getDefault()
                        ).parse(data?.remindOn ?: "") ?: ""
                    )
                )
                view.datePicker.setOnClickListener {
                    Uitls.getFutureDatePicker(
                        this@ReminderAc,
                        supportFragmentManager,
                        "dd MMM,yyyy",view.datePicker
                    )
//                    MaterialDatePicker.Builder.datePicker().setSelection(Date().time).build()
//                        .apply {
//                            show(supportFragmentManager, this@ReminderAc.toString())
//                            addOnPositiveButtonClickListener {
//                                view.datePicker.setText(
//                                    SimpleDateFormat("dd MMM,yyyy", Locale.getDefault()).format(
//                                        Date(it)
//                                    )
//                                )
//                            }
//                        }
                }
                view.cancelBtn.setOnClickListener {
                    dismiss()
                }
                view.defBtnLay.submitBtn.text = "Save"
                view.TimePicker.setOnClickListener {
                    try {
                        val isSystem24Hour = is24HourFormat(this@ReminderAc)
                        val clockFormat =
                            if (isSystem24Hour) TimeFormat.CLOCK_24H else TimeFormat.CLOCK_12H
                        MaterialTimePicker.Builder()
                            .setTitleText("Select reminder time")
                            .setHour(
                                SimpleDateFormat("hh", Locale.getDefault()).format(Date()).toInt()
                            )
                            .setMinute(
                                SimpleDateFormat("mm", Locale.getDefault()).format(Date()).toInt()
                            )
                            .setTimeFormat(clockFormat)
                            .build().apply {
                                show(supportFragmentManager, this.toString())
                                addOnPositiveButtonClickListener {
                                    view.TimePicker.setText(
                                        SimpleDateFormat("K:mm", Locale.getDefault()).format(
                                            SimpleDateFormat(
                                                "H:mm",
                                                Locale.getDefault()
                                            ).parse(
                                                hour.toString().plus(":").plus(minute.toString())
                                            ) ?: ""
                                        ).plus(if (hour > 12) "PM" else "AM")
                                    )
                                }
                            }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                view.defBtnLay.submitBtn.setOnClickListener {
                    runOnUiThread {
                        editReminderCall(data?.id.toString(), this, view)
                    }
                }
                show()
            }
            return
        }
        if (isMarkCom) {
            runOnUiThread {
                mrkAsComCall(data?.id.toString())
            }
            return
        }

    }

    fun submitInteractionWithUsers(interactionID: String, isReminderNoti: Boolean) {
        Uitls.showProgree(true, this)
        val call = mInterface.shareReminderWithUser(
            ShareInteractionWithUserPost(
                email_notification = isReminderNoti,
                selectedShareWithLst
            ), interactionID = interactionID
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(this@ReminderAc, "Reminder shared successfully.")
                    } else {
                        Uitls.showToast(
                            this@ReminderAc,
                            "Reminder not shared at this movement."
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ReminderAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ReminderAc)
                Uitls.handlerError(this@ReminderAc, t)
            }
        })
    }

    override fun onSharedWithUserClick(pos: Int, flag: Boolean, obj: FullUsersItem?) {
        if (flag) selectedShareWithLst.add(obj?.id ?: 0) else selectedShareWithLst.remove(
            obj?.id ?: 0
        )
    }

    override fun onRrminderClkByID(
        pos: Int,
        id: String,
        isDel: Boolean,
        isEdit: Boolean,
        isShare: Boolean,
        isMarkCom: Boolean,
        data: RemindersItemByID
    ) {
        if (isMarkCom) {
            runOnUiThread {
                mrkAsComCall(data.id.toString())
            }
            return
        }
        if (isShare) {
            BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme).apply {
                val view =
                    ShareReminderLayBinding.inflate(LayoutInflater.from(this@ReminderAc))
                setContentView(view.root)
                val adptr =
                    ShareReminderAptr(this@ReminderAc, shareWithlst, this@ReminderAc).apply {
                        view.shareRemRv.layoutManager = LinearLayoutManager(this@ReminderAc)
                        view.shareRemRv.adapter = this
                    }
                view.defBtnLay.submitBtn.text = "Send"
                view.defBtnLay.submitBtn.setOnClickListener {
                    dismiss()
                    runOnUiThread {
                        submitInteractionWithUsers(
                            interactionID = data.id.toString(),
                            view.sndNotificaionChkBox.isChecked
                        )
                    }
                }
                view.searchPeopleBarEdxt.afterTextChanged {
                    adptr.filter.filter(it)
                }
                show()

            }
            return
        }

        if (isEdit) {
            Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
                val view = EditReminderLayBinding.inflate(LayoutInflater.from(this@ReminderAc))
                setContentView(view.root)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                view.reminderExt.setText(data.reminder ?: "")
                if (data.company_name != null) {
                    view.compnyLbl.text = data.company_name ?: ""
                } else {
                    view.compnyLbl.text = data.company_name ?: ""
                }
                view.TimePicker.setText(
                    SimpleDateFormat("hh:mmaa", Locale.getDefault()).format(
                        SimpleDateFormat(
                            "dd MMM,yyyy hh:mm aa",
                            Locale.getDefault()
                        ).parse(data.reminderDate ?: "") ?: ""
                    )
                )
                view.datePicker.setText(
                    SimpleDateFormat("dd MMM,yyyy", Locale.getDefault()).format(
                        SimpleDateFormat(
                            "dd MMM,yyyy hh:mm aa",
                            Locale.getDefault()
                        ).parse(data.reminderDate ?: "") ?: ""
                    )
                )
                view.datePicker.setOnClickListener {
                    MaterialDatePicker.Builder.datePicker().setSelection(Date().time).build()
                        .apply {
                            show(supportFragmentManager, this@ReminderAc.toString())
                            addOnPositiveButtonClickListener {
                                view.datePicker.setText(
                                    SimpleDateFormat("dd MMM,yyyy", Locale.getDefault()).format(
                                        Date(it)
                                    )
                                )
                            }
                        }
                }
                view.cancelBtn.setOnClickListener {
                    dismiss()
                }
                view.defBtnLay.submitBtn.text = "Save"
                view.TimePicker.setOnClickListener {
                    try {
                        val isSystem24Hour = is24HourFormat(this@ReminderAc)
                        val clockFormat =
                            if (isSystem24Hour) TimeFormat.CLOCK_24H else TimeFormat.CLOCK_12H
                        MaterialTimePicker.Builder()
                            .setTitleText("Select reminder time")
                            .setHour(
                                SimpleDateFormat("hh", Locale.getDefault()).format(Date()).toInt()
                            )
                            .setMinute(
                                SimpleDateFormat("mm", Locale.getDefault()).format(Date()).toInt()
                            )
                            .setTimeFormat(clockFormat)
                            .build().apply {
                                show(supportFragmentManager, this.toString())
                                addOnPositiveButtonClickListener {
                                    view.TimePicker.setText(
                                        SimpleDateFormat("K:mm", Locale.getDefault()).format(
                                            SimpleDateFormat(
                                                "H:mm",
                                                Locale.getDefault()
                                            ).parse(
                                                hour.toString().plus(":").plus(minute.toString())
                                            ) ?: ""
                                        ).plus(if (hour > 12) "PM" else "AM")
                                    )
                                }
                            }
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
                view.defBtnLay.submitBtn.setOnClickListener {
                    runOnUiThread {
                        editReminderCall(data?.id.toString(), this, view)
                    }
                }
                show()
            }
            return
        }
        if (isDel) {
            Dialog(this).apply {
                val view = DeleteReminderBinding.inflate(LayoutInflater.from(this@ReminderAc))
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCanceledOnTouchOutside(true)
                setContentView(view.root)
                view.noBtn.setOnClickListener {
                    dismiss()
                }
                view.yesBtn.setOnClickListener {
                    runOnUiThread {
                        deleteReminderByIDCall(data?.id.toString())
                    }
                    reminderResLstByID?.removeAt(pos)
                    adptrby_id.notifyItemRemoved(pos)
                    adptrby_id.notifyItemRangeChanged(pos, reminderResLstByID?.size ?: 0)
                    dismiss()
                }
                show()
            }
            return
        }


    }

    override fun onResume() {
        super.onResume()
        if (intetntID.isNotEmpty()) {
            adptrby_id = ReminderByIDAptr(this, reminderResLstByID, this).apply {
                bin.remnderRv.layoutManager = LinearLayoutManager(this@ReminderAc)
                bin.remnderRv.adapter = this
            }
            bin.defBtnLay.submitBtn.visibility = View.VISIBLE
            runOnUiThread {
                getReminderByID(intetntID)
            }
        }
    }
}