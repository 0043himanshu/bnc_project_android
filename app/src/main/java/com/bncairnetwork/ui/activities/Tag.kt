package com.bncairnetwork.ui.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.adapter.TagAdapter
import com.bncairnetwork.databinding.*
import com.bncairnetwork.helper.EditTagListnr
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.helper.UpdateAddTagListnr
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProjectDetailsDataObj
import com.bncairnetwork.pojo.post.AddUpdateTagPost
import com.bncairnetwork.pojo.post.tag.EditTagPost
import com.bncairnetwork.pojo.post.tag.RemoveTagPost
import com.bncairnetwork.pojo.response.GetAllTagsRes
import com.bncairnetwork.pojo.response.TagsItem
import com.bncairnetwork.pojo.response.UpdateAddTagRes
import com.bncairnetwork.pojo.response.company.CompaniesItem
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.google.gson.Gson
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Tag : AppCompatActivity(), UpdateAddTagListnr, EditTagListnr {
    lateinit var bin: ActivityTagBinding
    lateinit var tagAdptr: TagAdapter
    var obj: ProjectDetailsDataObj? = null
    var comBiddrObj: CompaniesItem? = null
    var tagLst: ArrayList<TagsItem?>? = null
    var id = ""
    var to_update_tag = ""
    var activeLst = ArrayList<String>()
    var inactiveLst = ArrayList<String>()
    lateinit var mInterface: ApiInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityTagBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {
        tagLst = ArrayList()
        id = intent.getStringExtra(Enums.ID.toString()).toString()
        bin.tagLay.appbarHeader.text = "Add/Remove Tags"
        bin.tagLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        chngtagbtn(true)
        bin.tagAddNewBtn.submitBtn.text = "Add New"
        bin.tagUpdtBtn.submitBtn.text = "Update"
        if (intent.getStringExtra(Enums.FROM.toString()) == null) {
            obj = Gson().fromJson(
                intent.getStringExtra(Enums.DATA.toString()),
                ProjectDetailsDataObj::class.java
            )
        } else if (intent.getStringExtra(Enums.FROM.toString()) == ApiConstants.CompanyNdBidders) {
            comBiddrObj = Gson().fromJson(
                intent.getStringExtra(Enums.DATA.toString()),
                CompaniesItem::class.java
            )
        }
        bin.textView12.text = obj?.name ?: comBiddrObj?.name ?: ""
        bin.textView13.text = obj?.city ?: comBiddrObj?.city ?: ""
        bin.textView14.text = obj?.country ?: comBiddrObj?.country ?: ""

        bin.tagTickImg.setOnClickListener {
            //add new tag
            if (bin.tagEdxt.text.isNotEmpty()) {
                runOnUiThread {
                    addTagCall(bin.tagEdxt.text.toString(), id)
                }
            } else {
                Uitls.showToast(this, "Please add tag")
            }
            changeUpdateBtnState(flag = true)
        }
        bin.tagCrossImg.setOnClickListener {
            bin.tagEdxt.text = null
            bin.tagTickImgMain.visibility = View.GONE
            bin.addTagLayExdt.visibility = View.GONE
            changeUpdateBtnState(flag = true)
        }
//        runOnUiThread {
//            if (intent.getStringExtra(Enums.TYPE.toString()).equals(ApiConstants.Project)) {
//                getProjectDetailsCall(id)
//            } else {
//                getCompanyDetailCall(id)
//            }
//        }
        tagAdptr = TagAdapter(
            this,
            tagLst,
            if (obj != null) {
                if (obj?.tag_obj != null) {
                    /*projectKeyWordSearch*/
                    obj?.tag_obj?.selectedTags
                } else {
                    /*companyByKeyWorkSearch*/
                    obj?.tags
                }
            } else {
                comBiddrObj?.tag_obj?.selectedTags
            },
            this,
            this
        ).apply {
            bin.tagRcy.layoutManager = LinearLayoutManager(this@Tag)
            bin.tagRcy.adapter = this
        }
        runOnUiThread {
            gettags()
        }

    }


    fun listnr() {
        bin.tagAddNewBtn.submitBtn.setOnClickListener {
            chngtagbtn(true)
            bin.tagTickImgMain.visibility = View.VISIBLE
            bin.addTagLayExdt.visibility = View.VISIBLE
            changeUpdateBtnState(flag = false)

        }
        bin.tagUpdtBtn.submitBtn.setOnClickListener {
            chngtagbtn(false)
            bin.tagTickImgMain.visibility = View.GONE
            bin.addTagLayExdt.visibility = View.GONE
            runOnUiThread {
                updateTagCall(id)
            }
        }
    }

    fun changeUpdateBtnState(flag: Boolean) {
        bin.tagUpdtBtn.submitBtn.isEnabled = flag
        bin.tagUpdtBtn.submitBtn.backgroundTintList =
            ContextCompat.getColorStateList(
                this,
                if (flag) R.color.app_blue else R.color.lightBlack
            )
    }


    fun chngtagbtn(isProjectSelect: Boolean) {
        when (isProjectSelect) {
            true -> {
                bin.tagUpdtBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.tagUpdtBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)

                bin.tagAddNewBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.tagAddNewBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.lightBlack)


            }
            false -> {
                bin.tagUpdtBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.tagUpdtBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)

                bin.tagAddNewBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.tagAddNewBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.lightBlack)


            }
        }
    }

    override fun onUpdateOrAddTag(pos: Int, tagName: String, flag: Boolean, isremove: Boolean) {
        if (!isremove) {
            if (flag) {
                if (!activeLst.contains(tagName)) {
                    activeLst.add(tagName)
                }
                if (inactiveLst.contains(tagName)) {
                    inactiveLst.remove(tagName)
                }
            } else {
                if (!inactiveLst.contains(tagName)) {
                    inactiveLst.add(tagName)
                }
                if (activeLst.contains(tagName)) {
                    activeLst.remove(tagName)
                }
            }
        } else {
            Dialog(this).apply {
                val view = DeleteReminderBinding.inflate(LayoutInflater.from(this@Tag))
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                setCanceledOnTouchOutside(true)
                setContentView(view.root)
                view.lbl.text = "Delete Tag"
                view.lbl1.text = "Do you really want to delete this tag?"
                view.noBtn.setOnClickListener {
                    dismiss()
                }
                view.yesBtn.setOnClickListener {
                    runOnUiThread {
                        removetagcall(tagName, pos)
                    }
                    dismiss()
                }
                show()
            }

        }

    }

    fun removetagcall(name: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call = mInterface.removeTagByName(RemoveTagPost(name))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@Tag)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        tagLst?.removeAt(pos)
                        tagAdptr.notifyItemRemoved(pos)
                        tagAdptr.notifyItemRangeChanged(pos, tagLst?.size ?: 0)
                    } else {
                        Uitls.showToast(this@Tag, "Not able to remove tag.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@Tag)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@Tag)
                Uitls.handlerError(this@Tag, t)
            }

        })
    }

    fun addTagCall(tag: String, id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.addOrUpdateTag(
            if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) AddUpdateTagPost(
                active_tags = arrayListOf(tag),
                inactive_tags = arrayListOf(),
                projects = arrayListOf(id),
                companies = arrayListOf()
            ) else AddUpdateTagPost(
                active_tags = arrayListOf(tag),
                inactive_tags = arrayListOf(),
                projects = arrayListOf(),
                companies = arrayListOf(id)
            ),
            type = if (intent.getStringExtra(Enums.TYPE.toString())
                    .equals(ApiConstants.Project)
            ) "projects" else "companies"
        )
        call.enqueue(object : Callback<UpdateAddTagRes> {
            override fun onResponse(
                call: Call<UpdateAddTagRes>,
                response: Response<UpdateAddTagRes>
            ) {
                Uitls.showProgree(false, this@Tag)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.tagEdxt.text = null
                        bin.tagTickImgMain.visibility = View.GONE
                        bin.addTagLayExdt.visibility = View.GONE
                        tagLst?.add(TagsItem(tag))
                        obj?.tag_obj?.selectedTags?.add(tag)
                        comBiddrObj?.tag_obj?.selectedTags?.add(tag)
                        tagAdptr.notifyDataSetChanged()
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@Tag)
                }
            }

            override fun onFailure(call: Call<UpdateAddTagRes>, t: Throwable) {
                Uitls.showProgree(false, this@Tag)
                Uitls.handlerError(this@Tag, t)
            }
        })
    }

    fun updateTagCall(id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.addOrUpdateTag(
            if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) AddUpdateTagPost(
                active_tags = activeLst,
                inactive_tags = inactiveLst,
                projects = arrayListOf(id),
                companies = arrayListOf()
            ) else AddUpdateTagPost(
                active_tags = activeLst,
                inactive_tags = inactiveLst,
                projects = arrayListOf(),
                companies = arrayListOf(id)
            ),
            type = if (intent.getStringExtra(Enums.TYPE.toString())
                    .equals(ApiConstants.Project)
            ) "projects" else "companies"
        )
        call.enqueue(object : Callback<UpdateAddTagRes> {
            override fun onResponse(
                call: Call<UpdateAddTagRes>,
                response: Response<UpdateAddTagRes>
            ) {
                Uitls.showProgree(false, this@Tag)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showToast(
                            this@Tag,
                            "Updated Successfully"
                        )
                    } else {
                        Uitls.showToast(
                            this@Tag,
                            "No able to update tag. please try after sometime"
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@Tag)
                }
            }

            override fun onFailure(call: Call<UpdateAddTagRes>, t: Throwable) {
                Uitls.showProgree(false, this@Tag)
                Uitls.handlerError(this@Tag, t)
            }
        })
    }

    override fun onTagClick(id: String, pos: Int) {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val view =
                EditTagLayBinding.inflate(LayoutInflater.from(this@Tag))
            setContentView(view.root)
            show()
            view.remEdit.setText(id)
            /**contain the value that user wants to update*/
            to_update_tag = id
            view.cancel.setOnClickListener {
                dismiss()
            }

            view.ok.setOnClickListener {
                runOnUiThread {
                    editTag(pos, obj, to_update_tag, view.remEdit.text.toString(), this)
                }

            }
        }

    }

    fun editTag(
        pos: Int,
        obj: ProjectDetailsDataObj?,
        oldtag: String,
        newtag: String,
        view: Dialog
    ) {
        Uitls.showProgree(true, this)
        val call = mInterface.editTag(EditTagPost(new_tag = newtag, old_tag = oldtag))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@Tag)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        if (obj?.tag_obj?.selectedTags?.contains(to_update_tag) == true) {
                            obj.tag_obj.selectedTags!!.add(newtag)
                            activeLst.add(newtag)
                        } else {
                            inactiveLst.add(newtag)
                        }
                        tagLst?.get(pos)?.name = newtag
                        tagAdptr.notifyItemChanged(pos)
                        view.dismiss()
                    } else {
                        Uitls.showToast(this@Tag, "Not able to edit tag.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@Tag)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@Tag)
                Uitls.handlerError(this@Tag, t)
            }
        })
    }

    fun gettags() {
        Uitls.showProgree(true, this)
        val call = mInterface.gettags()
        call.enqueue(object : Callback<GetAllTagsRes> {
            override fun onResponse(call: Call<GetAllTagsRes>, response: Response<GetAllTagsRes>) {
                Uitls.showProgree(false, this@Tag)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        tagLst?.addAll(response.body()!!.tags ?: arrayListOf())
                        tagAdptr.notifyDataSetChanged()
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@Tag)
                }
            }

            override fun onFailure(call: Call<GetAllTagsRes>, t: Throwable) {
                Uitls.showProgree(false, this@Tag)
                Uitls.handlerError(this@Tag, t)
            }
        })
    }
}