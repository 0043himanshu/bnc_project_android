package com.bncairnetwork.ui.activities.onboarding

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityOnboardingTwoBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.ui.activities.MainActivity
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class OnboardingTwo : AppCompatActivity() {
    lateinit var bin: ActivityOnboardingTwoBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityOnboardingTwoBinding.inflate(layoutInflater)
        setContentView(bin.root)
        bin.defBtnLay.submitBtn.text = "Accept and Activate AIR "
        bin.defBtnLay.submitBtn.setOnClickListener {

            runOnUiThread {
                acceptPolicy()
            }

        }
        bin.pdfView.fromAsset("demo_pdf.pdf")
            .spacing(-40)
            .load()
    }

    fun acceptPolicy() {
        Uitls.showProgree(true, this)
        val mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        val call = mInterface.privacyPolicyAccept()
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@OnboardingTwo)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        PrefUtils.with(this@OnboardingTwo).save(
                            Enums.isLogin.toString(),
                            "true"
                        )
                        startActivity(Intent(this@OnboardingTwo, MainActivity::class.java))
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@OnboardingTwo)
                }
            }
            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.handlerError(this@OnboardingTwo, t)
                Uitls.showProgree(false, this@OnboardingTwo)
            }
        })
    }
}