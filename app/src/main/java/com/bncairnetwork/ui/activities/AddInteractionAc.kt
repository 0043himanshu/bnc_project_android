package com.bncairnetwork.ui.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.Window
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.adapter.InteractionProComItemCrossAdapter
import com.bncairnetwork.databinding.ActivityAddInteractionBinding
import com.bncairnetwork.databinding.MultiSelectionLayBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.interaction.AddInteractionProjectNdCompanyPost
import com.bncairnetwork.pojo.response.*
import com.bncairnetwork.pojo.response.company.CompanyDetailByIDRes
import com.bncairnetwork.pojo.response.company.GetAutoComCompaniesRes
import com.bncairnetwork.pojo.response.company.InteractionsItem
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.pojo.response.project.InteractionDataItem
import com.bncairnetwork.ui.activities.homeConnections.AddCompanyAc
import com.bncairnetwork.ui.activities.homeConnections.AddContactAc
import com.bncairnetwork.ui.adapter.InteractionContactMultiSpinnerAdptr
import com.bncairnetwork.ui.adapter.interaction.ShareWithMultiSpAdptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.set

class AddInteractionAc : AppCompatActivity(), InteractionProComDelListnr, MultiSelectionListnr,
    ContactListnr, ShareWithUserListr {
    lateinit var obj: GetMasterDataRes
    var interactiontypeslt = HashMap<String, String>()
    var shareWithlst = ArrayList<FullUsersItem?>()
    var shareWithuserLst = ArrayList<String>()
    private var formatedDate = ""
    private var formatedTime = ""
    var selectedShareWithLst = ArrayList<FullUsersItem?>()
    lateinit var mutliSpAptr: ShareWithMultiSpAdptr
    lateinit var mutliSpAptrForContact: InteractionContactMultiSpinnerAdptr
    var contacthashmap = HashMap<String, String>()
    var id = ""
    var selectedContacthashmap = HashMap<String, String>()
    var selectedComIDS = ArrayList<Int>()
    var userCompanyID = ""
    lateinit var dialogView: MultiSelectionLayBinding
    var comProLst = ArrayList<String>()  //selection list
    lateinit var comProAdptr: InteractionProComItemCrossAdapter
    var companiesHashMap = HashMap<String, String>()
    lateinit var mInterface: ApiInterface
    var interactiontypeAptr: ArrayAdapter<String>? = null
    var selectContByComLst = ArrayList<String>()
    lateinit var dialog: Dialog
    lateinit var bin: ActivityAddInteractionBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityAddInteractionBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {
        UserRestrictionRepo.addInteractionRestriction(
            bin,
            PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        )
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        userCompanyID = PrefUtils.with(this).getString(Enums.USER_COMPANY_ID.toString(), "") ?: ""
        bin.defBtnLay.submitBtn.text = "Save"
        dialog = Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            dialogView =
                MultiSelectionLayBinding.inflate(LayoutInflater.from(this@AddInteractionAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(dialogView.root)
        }
        PrefUtils.with(this).apply {
            obj = Gson().fromJson(
                getString(Enums.MASTERDATA.toString(), ""),
                GetMasterDataRes::class.java
            )
            obj.interactionTypes?.forEach {
                interactiontypeslt[it?.name.toString()] = it?.id.toString()
            }

            if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) {
                bin.compnylbl.text = "Company"
                bin.companyEdixt.hint = "Search Company"
                if (Gson().fromJson(
                        getString(Enums.OBJ.toString(), ""),
                        ProjectResById::class.java
                    ) != null
                ) {
                    Gson().fromJson(
                        getString(Enums.OBJ.toString(), ""),
                        ProjectResById::class.java
                    ).apply {
                        bin.companyNameLbl.text = project?.name
                            ?: intent.getStringExtra(Enums.ProjectOrCompanyName.toString())
                        bin.companyCountryLbl.text = project?.country
                            ?: intent.getStringExtra(Enums.ProjectOrCompanyCountry.toString())
                        bin.companyStateLbl.text = project?.city
                            ?: intent.getStringExtra(Enums.ProjectOrCompanyCity.toString())
                        id = project?.id.toString()
                        //bin.companyEdixt.setText(project?.name ?: "")
                    }
                } else {
                    bin.companyNameLbl.text =
                        intent.getStringExtra(Enums.ProjectOrCompanyName.toString())
                    bin.companyCountryLbl.text =
                        intent.getStringExtra(Enums.ProjectOrCompanyCountry.toString())
                    bin.companyStateLbl.text =
                        intent.getStringExtra(Enums.ProjectOrCompanyCity.toString())
                    id = intent.getStringExtra(Enums.ID.toString()) ?: ""
                }


            } else {
                bin.compnylbl.text = "Project"
                bin.companyEdixt.hint = "Search Project"
                bin.contactlbl.visibility = View.GONE
                bin.Addcompnylbl.visibility = View.GONE
                bin.Addcontactlbl.visibility = View.GONE
                bin.contactvallbl.visibility = View.GONE
                if (Gson().fromJson(
                        getString(Enums.OBJ.toString(), ""),
                        CompanyDetailByIDRes::class.java
                    ) != null
                ) {
                    Gson().fromJson(
                        getString(Enums.OBJ.toString(), ""),
                        CompanyDetailByIDRes::class.java
                    ).apply {
                        bin.companyNameLbl.text = company?.name
                            ?: intent.getStringExtra(Enums.ProjectOrCompanyName.toString())
                        bin.companyCountryLbl.text = company?.country
                            ?: intent.getStringExtra(Enums.ProjectOrCompanyCountry.toString())
                        bin.companyStateLbl.text = company?.city
                            ?: intent.getStringExtra(Enums.ProjectOrCompanyCity.toString())
                        id = company?.id.toString()
                        // bin.companyEdixt.setText(company?.name ?: "")
                    }
                } else {
                    bin.companyNameLbl.text =
                        intent.getStringExtra(Enums.ProjectOrCompanyName.toString())
                    bin.companyCountryLbl.text =
                        intent.getStringExtra(Enums.ProjectOrCompanyCountry.toString())
                    bin.companyStateLbl.text =
                        intent.getStringExtra(Enums.ProjectOrCompanyCity.toString())
                    id = intent.getStringExtra(Enums.ID.toString()) ?: ""
                }

            }

        }
        comProAdptr = InteractionProComItemCrossAdapter(this, comProLst, this).apply {
            bin.rv.layoutManager =
                LinearLayoutManager(this@AddInteractionAc, LinearLayoutManager.HORIZONTAL, false)
            bin.rv.adapter = this
        }

        interactiontypeAptr =
            ArrayAdapter(
                this,
                android.R.layout.simple_spinner_item,
                interactiontypeslt.keys.toList()
            ).apply {
                setDropDownViewResource(R.layout.sp_row_item_lay)
                bin.interactionTimeSp.adapter = this
            }
//        shareWithAptr =
//            ArrayAdapter(
//                this,
//                android.R.layout.simple_spinner_item,
//                shareWithlst.keys.toList()
//            ).apply {
//                setDropDownViewResource(R.layout.sp_row_item_lay)
//                bin.shareWithSp.adapter = this
//            }
    }

    fun listnr() {

        bin.generalArrowBtn.setOnClickListener {
            Uitls.hideUnhideView(
                bin.interactionLay,
                bin.generalArrowBtn
            )
        }

        bin.reminderArrowBtn.setOnClickListener {
            Uitls.hideUnhideView(
                bin.setReminderLay.root,
                bin.reminderArrowBtn
            )
        }

        bin.shareWithSp.setOnClickListener {
            runOnUiThread {
                getShareWithUsers()
            }

        }
        bin.companyEdixt.setOnItemClickListener { parent, view, position, id ->
            if (!selectedComIDS.contains(companiesHashMap[bin.companyEdixt.adapter.getItem(position)]?.toInt()))
                selectedComIDS.add(
                    companiesHashMap[bin.companyEdixt.adapter.getItem(position)]?.toInt() ?: 0
                )

            println("id:::" + companiesHashMap[bin.companyEdixt.adapter.getItem(position)])
            println("value:::" + bin.companyEdixt.adapter.getItem(position))
            comProLst.addAll(listOf(bin.companyEdixt.adapter.getItem(position).toString()))
            comProAdptr.notifyItemRangeInserted(comProLst.size + 1, comProLst.size)
            bin.companyEdixt.text = null
            if (bin.compnylbl.text.toString() == ApiConstants.Project) {
                bin.companyEdixt.isEnabled = false
            }
            // Uitls.showDialog(this,"Interaction will be linked to only one project and you have already selected.")
        }

        bin.companyEdixt.addTextChangedListener {
            it?.let { s ->
                bin.companyEdixt.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    null,
                    if (s.isNotEmpty()) ContextCompat.getDrawable(
                        this,
                        R.drawable.search
                    ) else null,
                    null
                )
            }

        }

        bin.companyEdixt.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP && bin.companyEdixt.text.isNotEmpty()) {
                if (event.rawX >= bin.companyEdixt.right - bin.companyEdixt.compoundDrawables[2].bounds.width()
                ) {
                    // your action here
                    if (bin.companyEdixt.text.isNotEmpty()) {
                        runOnUiThread {
                            if (bin.compnylbl.text == "Project") {
                                getprojectsByKeyword()
                            } else {
                                getcompaniesByKeyword()
                            }

                        }
                    }

                    return@OnTouchListener true
                }
            }
            false
        })

        bin.contactvallbl.setOnClickListener {
            if (selectedComIDS.isNotEmpty()) {
                runOnUiThread {
                    getContactsByID()
                }
            } else {
                Uitls.showToast(this, "Please search for company first")
            }

        }

        bin.interactionDateExt.setOnClickListener {
//            Uitls.getDatePicker(
//                this,
//                supportFragmentManager,
//                ApiConstants.DMY_PATTERN,
//                bin.interactionDateExt
//            )
            MaterialDatePicker.Builder.datePicker().setSelection(Date().time).build().apply {
                show(supportFragmentManager, this@AddInteractionAc.toString())
                addOnPositiveButtonClickListener {
                    bin.interactionDateExt.setText(
                        SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(
                            Date(it)
                        )
                    )
                }
            }
        }

        bin.setReminderLay.datePicker.setOnClickListener {
//            Uitls.getDatePicker(
//                this,
//                supportFragmentManager,
//                ApiConstants.DMY_PATTERN,
//                bin.setReminderLay.datePicker
//            )

            MaterialDatePicker.Builder.datePicker().setSelection(Date().time).build().apply {
                show(supportFragmentManager, this@AddInteractionAc.toString())
                addOnPositiveButtonClickListener {

                    println(
                        "====formatedDAte===>${
                            SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(
                                Date(it)
                            )
                        }"
                    )
                    formatedDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(
                        Date(it)
                    )
                    println(
                        "====formatedTime===>${
                            SimpleDateFormat("'T'HH:mm:SS'Z'", Locale.getDefault()).format(
                                Date(1647843774853)
                            )
                        }"
                    )

                    bin.setReminderLay.datePicker.setText(
                        SimpleDateFormat(ApiConstants.DMY_PATTERN, Locale.getDefault()).format(
                            Date(it)
                        )
                    )
                }
            }

        }

        bin.setReminderLay.TimePicker.setOnClickListener {
            try {
                val isSystem24Hour = DateFormat.is24HourFormat(this)
                val clockFormat =
                    if (isSystem24Hour) TimeFormat.CLOCK_24H else TimeFormat.CLOCK_12H
                MaterialTimePicker.Builder().setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                    .setTitleText("Select reminder time")
                    .setHour(SimpleDateFormat("hh", Locale.getDefault()).format(Date()).toInt())
                    .setMinute(
                        SimpleDateFormat("mm", Locale.getDefault()).format(Date()).toInt()
                    )
                    .build().apply {
                        show(supportFragmentManager, this.toString())
                        addOnPositiveButtonClickListener {
                            bin.setReminderLay.TimePicker.setText(
                                SimpleDateFormat("K:mm", Locale.getDefault()).format(
                                    SimpleDateFormat(
                                        "H:mm",
                                        Locale.getDefault()
                                    ).parse(hour.toString().plus(":").plus(minute.toString())) ?: ""
                                ).plus(if (hour > 12) "PM" else "AM")

                            )
                            println(
                                "---\n--${
                                    "T".plus(
                                        SimpleDateFormat("K:mm", Locale.getDefault()).format(
                                            SimpleDateFormat(
                                                "H:mm",
                                                Locale.getDefault()
                                            ).parse(
                                                hour.toString().plus(":").plus(minute.toString())
                                            ) ?: ""
                                        ).plus(":00Z")
                                    )
                                }--\n"
                            )
                            formatedTime = "T".plus(
                                SimpleDateFormat("K:mm", Locale.getDefault()).format(
                                    SimpleDateFormat(
                                        "H:mm",
                                        Locale.getDefault()
                                    ).parse(
                                        hour.toString().plus(":").plus(minute.toString())
                                    ) ?: ""
                                ).plus(":00Z")
                            )
                        }
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }



        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.cusAppbarLay.appbarHeader.text = "Add Interaction"

        bin.Addcompnylbl.setOnClickListener {
            startActivity(Intent(this, AddCompanyAc::class.java))

        }
        bin.Addcontactlbl.setOnClickListener {
            startActivity(Intent(this, AddContactAc::class.java))

        }

        bin.defBtnLay.submitBtn.setOnClickListener {
//            println(selectedShareWithLst.toString().replace("FullUsersItem(","{\"")
//                .replace("=","\":\"").replace("), ","\"},").replace("),","},{\"").replace(")]","\"}]").replace(", ","\",\""))
            runOnUiThread {
                if (bin.defBtnLay.submitBtn.text == ApiConstants.ApplyChanges) {
                    editInteraction()
                } else {
                    if (bin.interactionDateExt.text.isNotEmpty()) {
//                        if (bin.interactionTimeSp.selectedItemPosition > 0) {
//
//                        } else {
//                            Uitls.showToast(this, "please select type")
//                        }
                        if (bin.interactionEdxt.text.isNotEmpty()) {
                            addInteraction()
                        } else {
                            bin.interactionEdxt.error = "Please add interaction"
                        }

                    } else {
                        bin.interactionDateExt.error = "Please select date"
                    }
                }
            }
        }
    }


//    fun showContactDialog(view: MultiSelectionLayBinding, lst: ArrayList<ContactsItem>) {
//        view.lbl.text = "Select Contact"
//        view.okbtn.visibility = View.GONE
//        ContactsByCompanyAptr(this, lst, this).apply {
//            view.multiRv.layoutManager = LinearLayoutManager(this@AddInteractionAc)
//            view.multiRv.adapter = this
//        }
//    }

    fun showMultiSelctionDialog(
        view: MultiSelectionLayBinding,
        lst: ArrayList<FullUsersItem?>, selection: ArrayList<FullUsersItem?>
    ) {
        println(lst)
        view.lbl.text = "Select"
        view.okbtn.visibility = View.VISIBLE
        mutliSpAptr = ShareWithMultiSpAdptr(
            this@AddInteractionAc,
            lst,
            selection,
            this
        ).apply {
            view.multiRv.layoutManager = LinearLayoutManager(this@AddInteractionAc)
            view.multiRv.adapter = this
        }
    }

    fun showMultiSelctionDialogForContact(
        view: MultiSelectionLayBinding,
        lst: ArrayList<ContactsItem>, selection: ArrayList<String>
    ) {
        println(lst)
        view.lbl.text = "Select"
        view.okbtn.visibility = View.VISIBLE
        mutliSpAptrForContact = InteractionContactMultiSpinnerAdptr(
            this@AddInteractionAc,
            lst,
            selection,
            this
        ).apply {
            view.multiRv.layoutManager = LinearLayoutManager(this@AddInteractionAc)
            view.multiRv.adapter = this
        }
    }

    fun getShareWithUsers() {
        shareWithlst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getShareWithUserLst("[$userCompanyID]")
        call.enqueue(object : Callback<ShareWithUserLstRes> {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(
                call: Call<ShareWithUserLstRes>,
                response: Response<ShareWithUserLstRes>
            ) {
                Uitls.showProgree(false, this@AddInteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        response.body()!!.companyData?.get(0)?.fullUsers?.let { userObj ->
                            shareWithlst.addAll(userObj)
                        }
                        showMultiSelctionDialog(
                            dialogView, shareWithlst,
                            selectedShareWithLst
                        )
                        dialogView.okbtn.setOnClickListener {
                            bin.shareWithSp.setText(
                                shareWithuserLst.toString().replace("[", "").replace("]", "")
                            )
                            dialog.dismiss()
                        }
                        dialog.show()

                    } else {
                        Uitls.showToast(this@AddInteractionAc, "")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddInteractionAc)
                }
            }

            override fun onFailure(call: Call<ShareWithUserLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddInteractionAc)
                Uitls.handlerError(this@AddInteractionAc, t)
            }
        })
    }

    fun getcompaniesByKeyword() {
        companiesHashMap.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getcompanies_autocomp(bin.companyEdixt.text.toString())
        call.enqueue(object : Callback<GetAutoComCompaniesRes> {
            override fun onResponse(
                call: Call<GetAutoComCompaniesRes>,
                response: Response<GetAutoComCompaniesRes>
            ) {
                Uitls.showProgree(false, this@AddInteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.companyEdixt.text = null
                        response.body()!!.suggestions?.forEach {
                            companiesHashMap[it?.name ?: ""] = it?.id.toString()
                        }
                        ArrayAdapter(
                            this@AddInteractionAc,
                            android.R.layout.simple_list_item_1, companiesHashMap.keys.toList()
                        ).apply {
                            bin.companyEdixt.setAdapter(this)
                            setNotifyOnChange(true)
                            bin.companyEdixt.showDropDown()
                        }

                    } else {
                        Uitls.showToast(this@AddInteractionAc, "No Companies found.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddInteractionAc)
                }
            }

            override fun onFailure(call: Call<GetAutoComCompaniesRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddInteractionAc)
                Uitls.handlerError(this@AddInteractionAc, t)
            }
        })
    }

    fun getprojectsByKeyword() {
        companiesHashMap.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getprojects_autocomp(bin.companyEdixt.text.toString())
        call.enqueue(object : Callback<GetAutoComCompaniesRes> {
            override fun onResponse(
                call: Call<GetAutoComCompaniesRes>,
                response: Response<GetAutoComCompaniesRes>
            ) {
                Uitls.showProgree(false, this@AddInteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.companyEdixt.text = null
                        response.body()!!.suggestions?.forEach {
                            companiesHashMap[it?.name ?: ""] = it?.id.toString()
                        }
                        ArrayAdapter(
                            this@AddInteractionAc,
                            android.R.layout.simple_list_item_1, companiesHashMap.keys.toList()
                        ).apply {
                            bin.companyEdixt.setAdapter(this)
                            setNotifyOnChange(true)
                            bin.companyEdixt.showDropDown()
                        }

                    } else {
                        Uitls.showToast(this@AddInteractionAc, "No Companies found.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddInteractionAc)
                }
            }

            override fun onFailure(call: Call<GetAutoComCompaniesRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddInteractionAc)
                Uitls.handlerError(this@AddInteractionAc, t)
            }
        })
    }

    fun getContactsByID() {
        Uitls.showProgree(true, this)
        val call = mInterface.getContactsLstByMultiCompanyIDs(ids = selectedComIDS.toString())
        call.enqueue(object : Callback<GetConLstByMultiComIdsRes> {
            override fun onResponse(
                call: Call<GetConLstByMultiComIdsRes>,
                response: Response<GetConLstByMultiComIdsRes>
            ) {
                Uitls.showProgree(false, this@AddInteractionAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        response.body()!!.contacts?.let {
                            //  selectContByComLst.addAll(listOf(bin.contactvallbl.text.trim().toString()))
                            it.forEach { contactobj ->
                                contacthashmap[contactobj.id ?: ""] = contactobj.name ?: ""
                            }
                            showMultiSelctionDialogForContact(
                                dialogView,
                                it,
                                selectContByComLst
                            )
                            dialogView.okbtn.setOnClickListener {
                                // println(selectedShareWithLst)
                                bin.contactvallbl.text =
                                    if (selectedContacthashmap.isNotEmpty()) selectedContacthashmap.values.toString()
                                        .replace("[", "")
                                        .replace("]", "") else null
                                dialog.dismiss()
                            }
                            dialog.show()
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddInteractionAc)
                }
            }

            override fun onFailure(call: Call<GetConLstByMultiComIdsRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddInteractionAc)
                Uitls.handlerError(this@AddInteractionAc, t)
            }
        })
    }

    fun addInteraction() {
        try {
            val comnyContIDLst = ArrayList<String>()
            comnyContIDLst.addAll(selectedContacthashmap.keys)
            Uitls.showProgree(true, this)
            val call = mInterface.addInteraction(
                AddInteractionProjectNdCompanyPost(
                    reminderText = if (bin.setReminderLay.reminderEdxt.text.trim().toString()
                            .isNotEmpty()
                    ) bin.setReminderLay.reminderEdxt.text.toString() else null,
                    note = "",
                    companyIds = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) selectedComIDS else arrayListOf(
                        id.toInt()
                    ),
                    interaction = if (bin.interactionEdxt.text.toString().trim()
                            .isNotEmpty()
                    ) bin.interactionEdxt.text.toString() else null,
                    reminderDate = if (bin.setReminderLay.datePicker.text.toString().trim()
                            .isNotEmpty()
                    ) formatedDate.plus(formatedTime) else null,
                    interactionType = interactiontypeslt[bin.interactionTimeSp.selectedItem.toString()]?.toInt(),
                    project = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) id.toInt()
                    else if (selectedComIDS.isNotEmpty()) selectedComIDS.toString()
                        .replace("[", "").replace("]", "").toInt() else null,
                    /* company = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Company) id.toInt() else null,*/
                    from = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) "project" else "company",
                    interactionDate = if (bin.interactionDateExt.text.toString().trim()
                            .isNotEmpty()
                    ) bin.interactionDateExt.text.toString() else null,
                    selectedUser = if (selectedShareWithLst.isNotEmpty()) selectedShareWithLst else null,
                    companyContacts = if (selectedContacthashmap.isNotEmpty()) comnyContIDLst else null
                )
            )
            call.enqueue(object : Callback<ComRes> {
                override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                    Uitls.showProgree(false, this@AddInteractionAc)
                    if (response.body() != null && response.isSuccessful) {
                        if (response.body()!!.status == "1") {
                            Uitls.showToast(this@AddInteractionAc, "Interaction added successfully")
                            onBackPressed()
                        } else {
                            Uitls.showToast(
                                this@AddInteractionAc,
                                response.body()!!.errors ?: "Interaction not added at this movement"
                            )
                        }
                    } else {
                        Uitls.onUnSuccessResponse(response.code(), this@AddInteractionAc)
                    }
                }

                override fun onFailure(call: Call<ComRes>, t: Throwable) {
                    Uitls.showProgree(false, this@AddInteractionAc)
                    Uitls.handlerError(this@AddInteractionAc, t)
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    fun editInteraction() {
        try {
            val comnyContIDLst = ArrayList<String>()
            comnyContIDLst.addAll(selectedContacthashmap.keys)
            Uitls.showProgree(true, this)
            val call = mInterface.editInteraction(
                id = intent.getStringExtra(Enums.ID.toString()) ?: "",
                AddInteractionProjectNdCompanyPost(
                    reminderText = if (bin.setReminderLay.reminderEdxt.text.trim().toString()
                            .isNotEmpty()
                    ) bin.setReminderLay.reminderEdxt.text.toString() else null,
                    note = "",
                    companyIds = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) selectedComIDS else selectedComIDS,
                    interaction = if (bin.interactionEdxt.text.toString().trim()
                            .isNotEmpty()
                    ) bin.interactionEdxt.text.toString() else null,
                    reminderDate = if (bin.setReminderLay.datePicker.text.toString().trim()
                            .isNotEmpty()
                    ) bin.setReminderLay.datePicker.text.toString() else null,
                    interactionType = interactiontypeslt[bin.interactionTimeSp.selectedItem.toString()]?.toInt(),
                    project = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) id.toInt() else null,
                    company = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Company) id.toInt() else null,
                    from = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) "project" else "company",
                    interactionDate = if (bin.interactionDateExt.text.toString().trim()
                            .isNotEmpty()
                    ) bin.interactionDateExt.text.toString() else null,
                    selectedUser = if (selectedShareWithLst.isNotEmpty()) selectedShareWithLst else null,
                    companyContacts = if (selectedContacthashmap.isNotEmpty()) comnyContIDLst else null
                )
            )

            call.enqueue(object : Callback<ComRes> {
                override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                    Uitls.showProgree(false, this@AddInteractionAc)
                    if (response.body() != null && response.isSuccessful) {
                        if (response.body()!!.status == "1") {
                            Uitls.showToast(
                                this@AddInteractionAc,
                                "Interaction edited successfully"
                            )
                            onBackPressed()
                        } else {
                            Uitls.showToast(
                                this@AddInteractionAc,
                                response.body()!!.errors
                                    ?: "Interaction not edited at this movement"
                            )
                        }
                    } else {
                        Uitls.onUnSuccessResponse(response.code(), this@AddInteractionAc)
                    }
                }

                override fun onFailure(call: Call<ComRes>, t: Throwable) {
                    Uitls.showProgree(false, this@AddInteractionAc)
                    Uitls.handlerError(this@AddInteractionAc, t)
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }


    }

    override fun onMakeSelection(pos: Int, name: String, flag: Boolean) {
        if (!selectContByComLst.contains(name)) selectContByComLst.add(name) else selectContByComLst.remove(
            name
        )
    }

    override fun onSharedWithUserClick(pos: Int, flag: Boolean, obj: FullUsersItem?) {
        println("==========>" + selectedShareWithLst)
        if (flag) shareWithuserLst.add(obj?.name ?: "") else shareWithuserLst.remove(
            obj?.name ?: ""
        )
    }

    override fun onContactSelect(pos: Int, obj: ContactsItem) {
        println(obj)
        if (!selectedContacthashmap.containsKey(obj.id ?: "")) {
            selectedContacthashmap[obj.id
                ?: ""] = obj.name.toString()
            selectContByComLst.add(obj.name.toString())
        } else {
            selectedContacthashmap.remove(obj.id ?: "")
            selectContByComLst.remove(obj.name.toString())
        }

//        dialog.dismiss()
    }

    override fun onClick(pos: Int, obj: String) {
        selectedComIDS.removeAt(pos)
        comProLst.removeAll(listOf(obj))
        comProAdptr.notifyItemRemoved(pos)
        comProAdptr.notifyItemRangeRemoved(pos + 1, comProLst.size)
        if (bin.compnylbl.text.toString() == ApiConstants.Project) {
            bin.companyEdixt.isEnabled = true
        }
    }

    fun reloadDataToViews(proIntrctnObj: InteractionDataItem?, comIntrnctnObj: InteractionsItem?) {
        if (proIntrctnObj != null || comIntrnctnObj != null) {
            bin.defBtnLay.submitBtn.text = ApiConstants.ApplyChanges
            val comLst = ArrayList<String>()
            bin.interactionDateExt.setText(
                SimpleDateFormat(
                    "dd-MM-yyyy",
                    Locale.getDefault()
                ).format(
                    SimpleDateFormat("dd MMM yyyy", Locale.getDefault()).parse(
                        proIntrctnObj?.date ?: comIntrnctnObj?.date ?: ""
                    ) ?: ""
                )
            )
            bin.interactionTimeSp.setSelection(
                interactiontypeAptr?.getPosition(
                    proIntrctnObj?.interactionType ?: comIntrnctnObj?.interaction_type ?: ""
                ) ?: 0
            )
            proIntrctnObj?.company?.forEach {
                comLst.add(it?.company ?: "")
                selectedComIDS.add(it?.id ?: 0)
            }

            comIntrnctnObj?.project?.let {
                comLst.add(it.project ?: "")
                selectedComIDS.add(it.id ?: 0)
            }
            bin.contactvallbl.text =
                proIntrctnObj?.company_contact?.toString()?.replace("[", "")?.replace("]", "")


            comProLst.addAll(comLst)
            comProAdptr.notifyItemRangeInserted(comProLst.size + 1, comProLst.size)
            bin.interactionEdxt.setText(
                proIntrctnObj?.interaction ?: comIntrnctnObj?.interaction ?: ""
            )

            bin.setReminderLay.reminderEdxt.setText(
                proIntrctnObj?.reminder?.reminder ?: comIntrnctnObj?.reminder?.reminder ?: ""
            )
            try {
                bin.setReminderLay.datePicker.setText(
                    SimpleDateFormat(
                        "yyyy-MM-dd'T'HH:mm:SS'Z'",
                        Locale.getDefault()
                    ).format(
                        SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault()).parse(
                            proIntrctnObj?.reminder?.date ?: comIntrnctnObj?.reminder?.date ?: ""
                        ) ?: ""
                    )
                )

                bin.setReminderLay.TimePicker.setText(
                    SimpleDateFormat(
                        "hh:mm a",
                        Locale.getDefault()
                    ).format(
                        SimpleDateFormat("MMM dd, yyyy hh:mm a", Locale.getDefault()).parse(
                            proIntrctnObj?.reminder?.date ?: comIntrnctnObj?.reminder?.date ?: ""
                        ) ?: ""
                    )
                )
            } catch (e: Exception) {
//                Uitls.showToast(this, "Invalid format for reminder date and time")
            }

        }


    }

    override fun onResume() {
        super.onResume()
        if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) {
            reloadDataToViews(
                proIntrctnObj = Gson().fromJson(
                    intent.getStringExtra(Enums.OBJ.toString()),
                    InteractionDataItem::class.java
                ), comIntrnctnObj = null
            )

        } else {
            reloadDataToViews(
                proIntrctnObj = null, comIntrnctnObj = Gson().fromJson(
                    intent.getStringExtra(Enums.OBJ.toString()),
                    InteractionsItem::class.java
                )
            )
//            println(
//                Gson().fromJson(
//                    intent.getStringExtra(Enums.OBJ.toString()),
//                    InteractionsItem::class.java
//                )
//            )
        }
    }
}
