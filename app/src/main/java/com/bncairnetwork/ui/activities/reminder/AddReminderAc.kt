package com.bncairnetwork.ui.activities.reminder

import android.os.Bundle
import android.text.format.DateFormat
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityEditReminderBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.post.reminder.ReminderForCompanyPost
import com.bncairnetwork.pojo.post.reminder.ReminderForProjectPost
import com.bncairnetwork.pojo.response.reminder.ReminderRes
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class AddReminderAc : AppCompatActivity() {
    var id = ""
    var type = ""
    lateinit var mInterface: ApiInterface
    lateinit var bin: ActivityEditReminderBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityEditReminderBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {
        id = intent.getStringExtra(Enums.ID.toString()) ?: ""
        type = intent.getStringExtra(Enums.TYPE.toString()) ?: ""
        bin.defBtnLay.submitBtn.text = "Save"
        bin.cusAppbarLay.appbarHeader.text = "Add Reminder"
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
    }

    fun listnr() {
        bin.defBtnLay.submitBtn.setOnClickListener {
            if (bin.datePicker.text.isNotEmpty()) {
                if (bin.TimePicker.text.isNotEmpty()) {
                    if (bin.reminderEdxt.text.isNotEmpty()) {
                        runOnUiThread {
                            addReminder(
                                if (type == ApiConstants.Company)
                                    mInterface.addReminderForCompany(
                                        ReminderForCompanyPost(
                                            reminderText = bin.reminderEdxt.text.toString(),
                                            date = bin.datePicker.text.toString(),
                                            time = bin.TimePicker.text.toString(),
                                            action = "add",
                                            emailNotification = true,
                                            mobile = true,
                                            note = "",
                                            content_type = type,
                                            company_id = id,
                                            selected_user = arrayListOf()
                                        )
                                    ) else mInterface.addReminderForProject(
                                    ReminderForProjectPost(
                                        reminderText = bin.reminderEdxt.text.toString(),
                                        date = bin.datePicker.text.toString(),
                                        time = bin.TimePicker.text.toString(),
                                        action = "add",
                                        emailNotification = true,
                                        mobile = true,
                                        note = "",
                                        content_type = type,
                                        project_id = id,
                                        selected_user = arrayListOf()
                                    )
                                )
                            )
                        }
                    } else {
                        bin.reminderEdxt.error = "Please enter reminder"
                    }
                } else {
                    bin.TimePicker.error = "Please select valid time"
                }
            } else {
                bin.datePicker.error = "Please select valid date"
            }

        }
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.datePicker.setOnClickListener {
            Uitls.getFutureDatePicker(this,supportFragmentManager, ApiConstants.DMY_PATTERN,bin.datePicker)
        }
        bin.TimePicker.setOnClickListener {
            try {
                val isSystem24Hour = DateFormat.is24HourFormat(this)
                val clockFormat =
                    if (isSystem24Hour) TimeFormat.CLOCK_24H else TimeFormat.CLOCK_12H
                MaterialTimePicker.Builder().setInputMode(MaterialTimePicker.INPUT_MODE_CLOCK)
                    .setTitleText("Select reminder time")
                    .setHour(SimpleDateFormat("hh", Locale.getDefault()).format(Date()).toInt())
                    .setMinute(
                        SimpleDateFormat("mm", Locale.getDefault()).format(Date()).toInt()
                    )
                    .build().apply {
                        show(supportFragmentManager, this.toString())
                        addOnPositiveButtonClickListener {
                            bin.TimePicker.setText(
                                SimpleDateFormat("K:mm", Locale.getDefault()).format(
                                    SimpleDateFormat(
                                        "H:mm",
                                        Locale.getDefault()
                                    ).parse(hour.toString().plus(":").plus(minute.toString())) ?: ""
                                ).plus(if (hour > 12) "PM" else "AM")
                            )
                        }
                    }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }


    }

    fun addReminder(call: Call<ReminderRes>) {
        Uitls.showProgree(true, this)
        call.enqueue(object : Callback<ReminderRes> {
            override fun onResponse(call: Call<ReminderRes>, response: Response<ReminderRes>) {
                Uitls.showProgree(false, this@AddReminderAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showToast(this@AddReminderAc, "Reminder added successfully")
                        onBackPressed()
                    } else {
                        Uitls.showToast(
                            this@AddReminderAc,
                            response.body()!!.errors ?: "Unable create reminder"
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddReminderAc)
                }
            }

            override fun onFailure(call: Call<ReminderRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddReminderAc)
                Uitls.handlerError(this@AddReminderAc, t)
            }
        })
    }
}