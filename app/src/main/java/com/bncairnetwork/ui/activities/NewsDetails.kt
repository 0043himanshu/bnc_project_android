package com.bncairnetwork.ui.activities

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityAddCreditBinding
import com.bncairnetwork.databinding.ActivityNewsDetailsBinding
import com.bncairnetwork.databinding.ProjectAlreadyClaimedPopupBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProjectClaimInfoRes
import com.bncairnetwork.pojo.response.InvitationUrlRes
import com.bncairnetwork.pojo.response.NewsDetailRes
import com.bncairnetwork.pojo.response.project.ProjectNewsDataItem
import com.bncairnetwork.ui.activities.projectConnections.ProjectDetailsAc
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.bumptech.glide.Glide
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewsDetails : AppCompatActivity() {
    lateinit var bin: ActivityNewsDetailsBinding
    private var obj: ProjectNewsDataItem? = null
    lateinit var mInterface: ApiInterface
    private var packageID = 0
    private var projectID = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityNewsDetailsBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }


    fun initView() {
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.cusAppbarLay.appbarHeader.text = "News"
        obj = Gson().fromJson(
            intent.getStringExtra(Enums.DATA.toString()),
            ProjectNewsDataItem::class.java
        )
//        obj?.let {
//            bin.textView.text = obj?.name ?: ""
//            bin.textView2.text = if (!obj?.city.isNullOrEmpty()) obj?.city!!.plus(", ")
//                .plus(obj?.country ?: "") else obj?.country ?: ""
//            bin.textView3.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
//                Html.fromHtml(obj?.description ?: "", Html.FROM_HTML_MODE_COMPACT)
//            } else {
//                Html.fromHtml(obj?.description ?: "")
//            }
//            Glide.with(this).load(obj?.image ?: "").into(bin.imageView)
//        }
//        bin.textView5.text = if (obj?.project_id == null) "0" else "1"

        runOnUiThread {
            getNewsDetail()
        }

    }

    fun listnr() {
        bin.prjctNewsTxt.setOnClickListener {
            if (bin.textView5.text != "0") {
                when (packageID) {
                    ApiConstants.professionalID -> {
                        obj?.authorized?.let { isAuthorised ->
                            if (isAuthorised) {
                                /*this project is in unlocked state*/
                                startActivity(
                                    Intent(
                                        this@NewsDetails,
                                        ProjectDetailsAc::class.java
                                    ).putExtra(Enums.ID.toString(), projectID)
                                        .putExtra(
                                            Enums.DATA.toString(),
                                            Gson().toJson(
                                                obj
                                            )
                                        )
                                )
                            } else {
                                /*this project is in locked state*/
                                runOnUiThread {
                                    getProjectInfo(projectID)
                                }
                            }
                        }
                    }
                    ApiConstants.ibisID -> {
                        startActivity(
                            Intent(
                                this@NewsDetails,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), projectID)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        obj
                                    )
                                )
                        )
                    }
                    ApiConstants.bi_userID -> {
                        startActivity(
                            Intent(
                                this@NewsDetails,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), projectID)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        obj
                                    )
                                )
                        )
                    }
                }
            }
//            if (obj?.project_id == null) {
//                Uitls.showToast(this, "No Project Found")
//            }
//            obj?.project_id?.let { id ->
//
//            }
        }
    }

    fun showProjectClaimDialog(obj: ProjectClaimInfoRes, proID: String) {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            val view =
                ProjectAlreadyClaimedPopupBinding.inflate(LayoutInflater.from(this@NewsDetails))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(view.root)
            view.defBtnLay.submitBtn.text = "View Projects"
            view.rightNotNowLbl.setOnClickListener {
                dismiss()
            }
            view.ownerslblCount.text = obj.project?.owners.toString()
            view.bidderCount.text = obj.project?.bidders.toString()
            view.consultantCount.text = obj.project?.consultants.toString()
            view.contractorCount.text = obj.project?.contractors.toString()
            view.lbl.text = obj.project?.name ?: ""
            view.defBtnLay.submitBtn.setOnClickListener {
                dismiss()
                obj.credits?.let {
                    if (it >= 5) {
                        runOnUiThread {
                            exhaustCredits(proID = proID, proObj = obj)
                        }
                    } else {
                        /*in sufficient credits*/
                        showInAppOption()
                    }
                }


            }
            show()
        }
    }

    fun showInAppOption() {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val view_by_crd =
                ActivityAddCreditBinding.inflate(LayoutInflater.from(this@NewsDetails))
            setContentView(view_by_crd.root)
            show()

            view_by_crd.addCrdtInviteLay.setOnClickListener {
                dismiss()
            }

            view_by_crd.addCrdtShareLay.setOnClickListener {
                dismiss()
                runOnUiThread {
                    getInvitationCall()
                }
            }

            view_by_crd.addCrdtBuyLay.setOnClickListener {
                dismiss()
                /*start follow for in app*/
            }
        }


    }

    fun exhaustCredits(proID: String, proObj: ProjectClaimInfoRes?) {
        Uitls.showProgree(true, this)
        val call = mInterface.exhaustCredit(projectID = proID)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@NewsDetails)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        startActivity(
                            Intent(
                                this@NewsDetails,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), proID)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
                    } else {
                        Uitls.showToast(
                            this@NewsDetails,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@NewsDetails)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@NewsDetails)
                Uitls.handlerError(this@NewsDetails, t)
            }
        })
    }


    fun getInvitationCall() {
        Uitls.showProgree(true, this)
        val call = mInterface.getInvitationUrl()
        call.enqueue(object : Callback<InvitationUrlRes> {
            override fun onResponse(
                call: Call<InvitationUrlRes>,
                response: Response<InvitationUrlRes>
            ) {
                Uitls.showProgree(false, this@NewsDetails)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showSharingDialog(
                            response.body()!!.message ?: "",
                            response.body()!!.shareUrl ?: "",
                            this@NewsDetails
                        )
                    } else {
                        Uitls.showToast(
                            this@NewsDetails,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@NewsDetails)
                }
            }

            override fun onFailure(call: Call<InvitationUrlRes>, t: Throwable) {
                Uitls.showProgree(false, this@NewsDetails)
                Uitls.handlerError(this@NewsDetails, t)
            }
        })
    }

    fun getProjectInfo(projectID: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectInfo(projectID)
        call.enqueue(object : Callback<ProjectClaimInfoRes> {
            override fun onResponse(
                call: Call<ProjectClaimInfoRes>,
                response: Response<ProjectClaimInfoRes>
            ) {
                Uitls.showProgree(false, this@NewsDetails)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        showProjectClaimDialog(response.body()!!, projectID)

//                        response.body()!!.credits?.let {credit->
//                            if (credit >= 5) {
//                                runOnUiThread {
//                                    //   projectClaimCall()
//                                    updateProjectView(proID = projectID, proObj = proObj)
//                                }
//                            } else {
//                                /*in sufficient credits*/
//                                showInAppOption()
//                            }
//
//                        }

                    } else {
                        Uitls.showToast(
                            this@NewsDetails,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@NewsDetails)
                }
            }

            override fun onFailure(call: Call<ProjectClaimInfoRes>, t: Throwable) {
                Uitls.showProgree(false, this@NewsDetails)
                Uitls.handlerError(this@NewsDetails, t)
            }
        })
    }

    fun getNewsDetail() {
        bin.mainView.visibility = View.GONE
        Uitls.showProgree(true, this)
        mInterface.getNewsDetails(id = intent.getStringExtra(Enums.ID.toString()) ?: "").apply {
            Uitls.showProgree(false, this@NewsDetails)
            enqueue(object : Callback<NewsDetailRes> {
                override fun onResponse(
                    call: Call<NewsDetailRes>,
                    response: Response<NewsDetailRes>
                ) {
                    if (response.body() != null && response.isSuccessful) {
                        bin.mainView.visibility = View.VISIBLE
                        response.body()!!.news?.let {
                            bin.textView.text = it.name ?: ""
                            bin.textView2.text =
                                if (!it.city.isNullOrEmpty()) it.city.plus(", ")
                                    .plus(it.country ?: "") else it.country ?: ""
                            bin.textView3.text =
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                    Html.fromHtml(
                                        it.description ?: "",
                                        Html.FROM_HTML_MODE_COMPACT
                                    )
                                } else {
                                    Html.fromHtml(it.description ?: "")
                                }
                            Glide.with(this@NewsDetails).load(it.image ?: "")
                                .into(bin.imageView)
                        }

                        response.body()!!.attributes?.let {
                            bin.textView5.text = it.size.toString()
                            if (it.isNotEmpty()) {
                                projectID =
                                    it[0]?.url?.substring(10, it[0]?.url!!.length - 1)
                                        .toString()
                            }

                        }

                    } else {
                        Uitls.showToast(
                            this@NewsDetails,
                            getString(R.string.somethingWentWrong)
                        )
                    }
                }

                override fun onFailure(call: Call<NewsDetailRes>, t: Throwable) {
                    Uitls.showToast(this@NewsDetails, t.message ?: "")
                }
            })
        }
    }
}