package com.bncairnetwork.ui.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.adapter.ContactAptr
import com.bncairnetwork.databinding.ActivityContactBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.KeyContact.GetKEyContactRes
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ContactAc : AppCompatActivity() {
    lateinit var bin: ActivityContactBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityContactBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initVew()
        listnr()
        if (intent.getStringExtra(Enums.ID.toString())?.isNotEmpty() == true) {
            runOnUiThread {
                getKeyContactsCall(intent.getStringExtra(Enums.ID.toString())!!)
            }
        }
    }

    fun initVew() {
        bin.bncBannerChat.appbarHeader.text = "Contacts"
        bin.bncBannerChat.backIcImgv.setOnClickListener {
            onBackPressed()
        }
    }

    fun listnr() {

    }

    fun getKeyContactsCall(id: String) {
        Uitls.showProgree(true, this)
        val mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        val call = mInterface.getkeycontactslst(
            id,
            if (intent.getBooleanExtra(Enums.isLinked.toString(), false)) "true" else null
        )
        call.enqueue(object : Callback<GetKEyContactRes> {
            override fun onResponse(
                call: Call<GetKEyContactRes>,
                response: Response<GetKEyContactRes>
            ) {
                Uitls.showProgree(false, this@ContactAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLayout.noDataLbl.visibility =
                        if ((response.body()!!.status != 1) || (response.body()!!.keyContacts.isNullOrEmpty())) View.VISIBLE else View.GONE
                    if (response.body()!!.status == 1) {
                        ContactAptr(this@ContactAc, response.body()!!.keyContacts).apply {
                            bin.contactRv.layoutManager = LinearLayoutManager(this@ContactAc)
                            bin.contactRv.adapter = this
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ContactAc)
                }
            }

            override fun onFailure(call: Call<GetKEyContactRes>, t: Throwable) {
                Uitls.showProgree(false, this@ContactAc)
                Uitls.handlerError(this@ContactAc, t)
            }
        })
    }
}