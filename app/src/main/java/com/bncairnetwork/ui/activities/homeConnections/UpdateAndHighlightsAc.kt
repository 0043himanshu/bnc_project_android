package com.bncairnetwork.ui.activities.homeConnections

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.databinding.ActivityUpdateAndHighlightsBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.project.GetProjectUpdatesNdHighlight
import com.bncairnetwork.ui.adapter.project.ProUpdateNdHighlightsAptr
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class UpdateAndHighlightsAc : AppCompatActivity() {
    lateinit var bin: ActivityUpdateAndHighlightsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityUpdateAndHighlightsBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initViews()
        listnr()


    }

    fun initViews() {
        bin.upHg.appbarHeader.text = "Updates And Highlights"
        runOnUiThread {
            getUpdatesNDHighlight()
        }
    }

    fun listnr() {
        bin.upHg.backIcImgv.setOnClickListener {
            onBackPressed()
        }
    }

    fun getUpdatesNDHighlight() {
        Uitls.showProgree(true, this)
        val mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        val call = mInterface.getUpdatesNdHighlights4Project(
            id = intent.getStringExtra(Enums.ID.toString()) ?: "0"
        )
        call.enqueue(object : Callback<GetProjectUpdatesNdHighlight> {
            override fun onResponse(
                call: Call<GetProjectUpdatesNdHighlight>,
                response: Response<GetProjectUpdatesNdHighlight>
            ) {
                Uitls.showProgree(false, this@UpdateAndHighlightsAc)
                if (response.body() != null && response.isSuccessful) {
                    databining(response.body()!!)
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@UpdateAndHighlightsAc)
                }


            }

            override fun onFailure(call: Call<GetProjectUpdatesNdHighlight>, t: Throwable) {
                Uitls.showProgree(false, this@UpdateAndHighlightsAc)
                Uitls.handlerError(this@UpdateAndHighlightsAc, t)
            }
        })


    }

    fun databining(obj: GetProjectUpdatesNdHighlight) {
        bin.textView48.text =
            "Last Updated ".plus(obj.project?.updated ?: "")
        bin.noDataLbl.noDataLbl.visibility =
            if (obj.notes?.isNotEmpty() == true) View.GONE else View.VISIBLE
        bin.companyDetailsGeneralDetailLay.textView45.text = obj.project?.name ?: ""
        bin.companyDetailsGeneralDetailLay.lbl1.text =
            obj.project?.typeOfProject ?: "".plus(" & ").plus(obj.project?.projectSubtype ?: "")
        bin.companyDetailsGeneralDetailLay.country.text =
            obj.project?.country.plus(",").plus(obj.project?.city ?: "")
        ProUpdateNdHighlightsAptr(
            this@UpdateAndHighlightsAc,
            obj.notes
        ).apply {
            bin.rv.layoutManager = LinearLayoutManager(this@UpdateAndHighlightsAc)
            bin.rv.adapter = this
        }
    }
}