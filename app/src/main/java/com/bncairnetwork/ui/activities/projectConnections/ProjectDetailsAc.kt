package com.bncairnetwork.ui.activities.projectConnections

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.updateLayoutParams
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.adapter.AssigneUserAptr
import com.bncairnetwork.adapter.BottomOptionsAdapter
import com.bncairnetwork.databinding.ActivityProjectDetailsBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.AttributesItem
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProTagObj
import com.bncairnetwork.pojo.ProjectDetailsDataObj
import com.bncairnetwork.pojo.post.project.FavProPost
import com.bncairnetwork.pojo.response.InvitationUrlRes
import com.bncairnetwork.pojo.response.ProjectResById
import com.bncairnetwork.ui.activities.*
import com.bncairnetwork.ui.activities.homeConnections.CompaniesBildder
import com.bncairnetwork.ui.activities.homeConnections.UpdateAndHighlightsAc
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.activities.reminder.ReminderAc
import com.bncairnetwork.ui.adapter.project.ProjectTagAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.bumptech.glide.Glide
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class ProjectDetailsAc : AppCompatActivity(), BottomOptionListnr,TagListnr {
    lateinit var bin: ActivityProjectDetailsBinding
    var projectID = ""
    var recordType = ""
    var isImageUrl = false
    private var proObj: ProjectDetailsDataObj? = null
    var isLinked = false
    private var packageID = 0
    private var isBncVersionView = false
    var bncVersionID = ""
    private var bottomAdptr: BottomOptionsAdapter? = null
    lateinit var mInterfce: ApiInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityProjectDetailsBinding.inflate(layoutInflater)
        setContentView(bin.root)
        listnr()
        init()
    }

    fun init() {
        PrefUtils.with(this).apply {
            UserRestrictionRepo.projectDetailRestricition(
                bin,
                getInt(Enums.USER_TYPE.toString(), 0)
            )
            packageID = getInt(Enums.USER_TYPE.toString(), 0)
        }

        mInterfce = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.shareInfoChatViewAppbarLay.backBtn.setOnClickListener {
            onBackPressed()
        }
        bin.root.visibility = View.INVISIBLE
        bin.compnyDetailAssignedUserLay.assignedUserRootLay.visibility = View.GONE
        bin.projectDetailOverviewLay.collectionProgresVal.visibility = View.GONE
        bin.projectDetailOverviewLay.progressbarPredict.visibility = View.GONE
        bin.projectDetailOverviewLay.collectionLbl.visibility = View.GONE
        if (intent.getStringExtra(Enums.ID.toString())?.isNotEmpty() == true) {
            projectID = intent.getStringExtra(Enums.ID.toString()).toString()
            proObj = Gson().fromJson(
                intent.getStringExtra(Enums.DATA.toString()),
                ProjectDetailsDataObj::class.java
            )
            isLinked = proObj?.record_type?.linked ?: false
        }
    }

    fun listnr() {
        bin.shareInfoChatViewAppbarLay.infoBtn.setOnClickListener {
            startActivity(
                Intent(this, RequestInformation::class.java).putExtra(
                    Enums.ID.toString(),
                    projectID
                ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
            )
        }
        bin.shareInfoChatViewAppbarLay.shareBtn.setOnClickListener {
            runOnUiThread { getInvitationCall() }
        }
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.setOnClickListener {
            if (!isBncVersionView) {
                runOnUiThread {
                    getProjectDetailsCall(
                        id = bncVersionID,
                        isBncVersion = true,
                        isFromBtnEvent = true
                    )
                }
            } else {
                runOnUiThread {
                    getProjectDetailsCall(
                        id = projectID,
                        isBncVersion = false,
                        isFromBtnEvent = true
                    )
                }
            }
        }
        bin.shareInfoChatViewAppbarLay.backBtn.setOnClickListener {
            onBackPressed()
        }
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.interactionImg.setOnClickListener {
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.TYPE.toString(),
                    ApiConstants.Project
                )
            )
        }
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.tagImg.setOnClickListener {
            startActivity(
                Intent(this, Tag::class.java).putExtra(
                    Enums.ID.toString(), projectID
                ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                    .putExtra(
                        Enums.DATA.toString(), Gson().toJson(proObj)
                    )
            )

        }
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.reminderImg.setOnClickListener {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(
                    Enums.ID.toString(),
                    projectID
                ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
            )
        }
        bin.projectDetailOverviewLay.minusBTn.setOnClickListener {
            if (bin.projectDetailOverviewLay.crd.visibility == View.VISIBLE) {
                updateOverviewLat(flag = false)
            } else {
                updateOverviewLat(flag = true)
            }


        }

    }

    fun updateOverviewLat(flag: Boolean) {
        bin.projectDetailOverviewLay.projectImg.visibility = if (flag) View.VISIBLE else View.GONE
        bin.projectDetailOverviewLay.crd.visibility = if (flag) View.VISIBLE else View.GONE
        bin.projectDetailOverviewLay.desLbl.visibility = if (flag) View.VISIBLE else View.GONE
        bin.projectDetailOverviewLay.desVal.visibility = if (flag) View.VISIBLE else View.GONE
        bin.projectDetailOverviewLay.projectImg.visibility =
            if (isImageUrl && flag) View.VISIBLE else View.GONE
    }

    fun getProjectDetailsCall(id: String, isBncVersion: Boolean, isFromBtnEvent: Boolean) {
        Uitls.showProgree(true, this)
        val call = mInterfce.getProjectDetailsByID(
            id,
            if (isLinked) "true" else null
        )
        call.enqueue(object : Callback<ProjectResById> {
            override fun onResponse(
                call: Call<ProjectResById>,
                response: Response<ProjectResById>
            ) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.noDataLayout.noDataLbl.visibility = View.GONE
                        bin.root.visibility = View.VISIBLE
                        isBncVersionView = isBncVersion
                        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.setImageResource(
                            if (isBncVersion) R.drawable.ic_linked_project else R.drawable.ic_bnc_icon
                        )
                        if (!isBncVersion) {
                            bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.setPadding(
                                12,
                                12,
                                12,
                                12
                            )
                        } else {
                            bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.setPadding(
                                0,
                                0,
                                0,
                                0
                            )
                        }
                        databinding(response.body()!!, isBncVersion)
                        if (isFromBtnEvent) {
                            Uitls.createCustomeToast(this@ProjectDetailsAc, isBncVersion)
                            return
                        } else if (packageID == ApiConstants.professionalID) {
                            runOnUiThread {
                                updateViewCount()
                            }
                            return
                        }

                    } else {
                        bin.noDataLayout.noDataLbl.visibility = View.VISIBLE
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectDetailsAc)
                }
            }

            override fun onFailure(call: Call<ProjectResById>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                Uitls.handlerError(this@ProjectDetailsAc, t)
            }
        })
    }

    fun databinding(obj: ProjectResById, isBncVersion: Boolean) {
        proObj = ProjectDetailsDataObj(
            authorised = null,
            id = proObj?.id ?: "",
            name = obj.project?.name ?: "",
            industry = "",
            updated = "",
            record_type = proObj?.record_type,
            value = "",
            tag_obj = ProTagObj(
                selectedTags = obj.tags,
                isTypeIcon = false,
                isFavourite = false,
                isProjects = null
            ),
            reminder = null,
            description = "",
            country = obj.project?.country ?: "",
            city = obj.project?.city ?: "",
            stage = "",
            views_left = "",
            image = "",
            identfierObj = null
        )


        /*isBncVersion is true ----> only user can view details no clicking*/
        bncVersionID = obj.project?.bncVersion?.id.toString()
        PrefUtils.with(this).save(Enums.OBJ.toString(), Gson().toJson(obj))
        bin.projectDetailOverviewLay.viewMapBtn.visibility =
            if (obj.project?.latlongstring != null) View.VISIBLE else View.GONE
        bin.projectDetailOverviewLay.viewMapBtn.setOnClickListener {
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?q=+${obj.project?.latlongstring ?: ""}+${obj.project?.name ?: ""}")
                )
            )
        }
        /*bncVersion On--> constrols*/

        recordType = obj.project?.record_type ?: ""
        ProjectTagAptr( obj.tags ?: arrayListOf(),this).apply {
            bin.companyDetailsGeneralDetailLay.tagRv.layoutManager =
                LinearLayoutManager(this@ProjectDetailsAc, LinearLayoutManager.HORIZONTAL, false)
            bin.companyDetailsGeneralDetailLay.tagRv.adapter = this
        }

        obj.project?.assignees_obj?.let { ob ->
            if (ob.isNotEmpty()) {
                bin.compnyDetailAssignedUserLay.assignedUserRootLay.visibility = View.VISIBLE
                AssigneUserAptr(this, ob).apply {
                    bin.compnyDetailAssignedUserLay.assinedUserRv.layoutManager =
                        LinearLayoutManager(
                            this@ProjectDetailsAc,
                            LinearLayoutManager.HORIZONTAL,
                            false
                        )
                    bin.compnyDetailAssignedUserLay.assinedUserRv.adapter = this
                }
            }
        }



        bin.companyDetailsGeneralDetailLay.projIc.setBackgroundResource(
            if (obj.project?.record_type?.equals(ApiConstants.LinkedProject) == true
            ) R.drawable.ic_linked_project else if (obj.project?.record_type?.equals(
                    ApiConstants.BncProject
                ) == true
            ) R.drawable.ic_b else R.drawable.ic_internal
        )


//        bin.companyDetailsGeneralDetailLay.lbl0.setCompoundDrawablesWithIntrinsicBounds(
//            ContextCompat.getDrawable(
//                this,
//                if (obj.project?.record_type?.equals(ApiConstants.LinkedProject) == true
//                ) R.drawable.ic_linked_project else if (obj.project?.record_type?.equals(
//                        ApiConstants.BncProject
//                    ) == true
//                ) R.drawable.ic_b else R.drawable.ic_internal
//            ), null, null, null
//        )
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
            if (obj.favourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive
        )
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.tagImg.setImageResource(
            if (obj.tags?.isNotEmpty() == true) R.drawable.ic_tag_active else R.drawable.ic_tag_inactive
        )
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.reminderImg.setImageResource(
            if (obj.has_active_reminder == true) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive
        )

        bin.projectDetailOverviewLay.projectImg.visibility =
            if (obj.project?.image != null) View.VISIBLE else View.GONE

        isImageUrl = obj.project?.image != null

        Glide.with(this).load(obj.project?.image ?: "").placeholder(Uitls.GliderPlaceHolder(this))
            .into(bin.projectDetailOverviewLay.projectImg)

        bin.companyDetailsGeneralDetailLay.lbl0.text = obj.project?.name

        bin.companyDetailsGeneralDetailLay.country.text =
            obj.project?.location?.plus("\n").plus(obj.project?.city ?: "").plus(" , ")
                .plus(obj.project?.country ?: "")

        bin.projectDetailOverviewLay.bncProNoVal.text = obj.project?.bncProjectNumber
        bin.projectDetailOverviewLay.projectSectorVal.text = obj.project?.sector
        bin.projectDetailOverviewLay.projectIndustryVal.text =
            obj.project?.projectType ?: ""
        bin.projectDetailOverviewLay.projectStageVal.text =
            obj.project?.project_stage ?: ""

        bin.projectDetailOverviewLay.projectTypeVal.text = obj.project?.projectSubtype ?: ""
        if (obj.project?.completionPercentage != null) {
            bin.projectDetailOverviewLay.collectionLbl.visibility = View.VISIBLE
            bin.projectDetailOverviewLay.collectionProgresVal.visibility = View.VISIBLE
            bin.projectDetailOverviewLay.progressbarPredict.visibility = View.VISIBLE
            bin.projectDetailOverviewLay.collectionProgresVal.text =
                if (obj.project.completionPercentage.equals(
                        "100",
                        false
                    )
                ) "Completed" else "${obj.project.completionPercentage} %"
            bin.projectDetailOverviewLay.progressbarPredict.progress =
                if (obj.project.completionPercentage.equals(
                        "100",
                        false
                    )
                ) 100 else obj.project.completionPercentage.toInt()
        }

        bin.projectDetailOverviewLay.projectEstSpentVal.text =
            "USD ".plus(
                Uitls.getMillionBillionTrillion(
                    (obj.project?.spentValue ?: "0.0").toFloat()
                )
            )


        bin.projectDetailOverviewLay.projectEstmateVal.text =
            "USD ${
                Uitls.getMillionBillionTrillion((obj.project?.value ?: "0.0").toFloat())
                    .replace(".0", "")
            }"


        bin.projectDetailOverviewLay.desVal.text =
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(obj.project?.description ?: "", Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(obj.project?.description ?: "")
            }
        bin.projectDetailOverviewLay.desLbl.visibility =
            if (bin.projectDetailOverviewLay.desVal.text.isNotEmpty()) View.VISIBLE else View.GONE

        bin.projectDetailOverviewLay.desVal.visibility =
            if (bin.projectDetailOverviewLay.desVal.text.isNotEmpty()) View.VISIBLE else View.GONE

        bin.projectDetailOverviewLay.lastUpdateVal.text = obj.project?.updated ?: ""
        bin.projectDetailOverviewLay.createdOnVal.text = obj.project?.created ?: ""

        obj.attributes?.let {
            val lst = ArrayList<AttributesItem?>()
            /*|| (attributesItem.title.equals(ApiConstants.Interactions) && packageID == ApiConstants.professionalID)*/
            it.forEach { attributesItem ->
                if (attributesItem?.count != 0) {
                    lst.add(attributesItem)
                }
            }
            bottomAdptr = BottomOptionsAdapter(this, lst, this, packageID).apply {
                bin.bottomOptnRv.layoutManager = LinearLayoutManager(this@ProjectDetailsAc)
                bin.bottomOptnRv.adapter = this
            }
        }

        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setOnClickListener {
            runOnUiThread {
                if (obj.favourite == true) {
                    removeFavPro(obj.project?.id.toString(), obj)
                } else {
                    makeFavPro(obj.project?.id.toString(), obj)
                }
            }
        }
        onBncVersionOnCrontrols(isBncVersion, obj)

    }

    fun onBncVersionOnCrontrols(isBncVersion: Boolean, obj: ProjectResById) {
        if (isBncVersion) {
            bin.companyDetailsGeneralDetailLay.lbl0.text = obj.project?.bncVersion?.name ?: ""
            bin.companyDetailsGeneralDetailLay.country.text =
                obj.project?.location?.plus("\n").plus(obj.project?.city ?: "").plus(" , ")
                    .plus(obj.project?.country ?: "")

            bin.projectDetailOverviewLay.projectSectorVal.text = obj.project?.bncVersion?.sector

            bin.projectDetailOverviewLay.projectIndustryVal.text =
                obj.project?.bncVersion?.newProjectType ?: ""

            bin.projectDetailOverviewLay.projectStageVal.text =
                obj.project?.bncVersion?.projectStage ?: ""

            bin.projectDetailOverviewLay.projectTypeVal.text =
                obj.project?.bncVersion?.newSubtype ?: ""
            bin.projectDetailOverviewLay.projectEstmateVal.text =
                "$ ".plus(
                    Uitls.getMillionBillionTrillion(
                        (obj.project?.bncVersion?.value ?: "0.0").toFloat()
                    ).replace(".0", "")
                )
            bin.projectDetailOverviewLay.desVal.text =
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    Html.fromHtml(
                        obj.project?.bncVersion?.description ?: "",
                        Html.FROM_HTML_MODE_COMPACT
                    )
                } else {
                    Html.fromHtml(obj.project?.bncVersion?.description ?: "")
                }

            bin.companyDetailsGeneralDetailLay.projIc.setBackgroundResource(
                if (obj.project?.bncVersion?.recordType?.equals(ApiConstants.LinkedProject) == true
                ) R.drawable.ic_linked_project
                else if (obj.project?.bncVersion?.recordType?.equals(
                        ApiConstants.BncProject
                    ) == true
                ) R.drawable.ic_b else R.drawable.ic_internal
            )
        }
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.reminderImg.isEnabled =
            !isBncVersion
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.isEnabled =
            !isBncVersion
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.tagImg.isEnabled =
            !isBncVersion
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.interactionImg.isEnabled =
            !isBncVersion
        // bottomAdptr?.disableRvView(!isBncVersion)
        bin.shareInfoChatViewAppbarLay.shareBtn.isEnabled = !isBncVersion
        bin.shareInfoChatViewAppbarLay.infoBtn.isEnabled = !isBncVersion
        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
            if (isLinked || isBncVersion) View.VISIBLE else View.GONE

        if (obj.project?.record_type?.equals(ApiConstants.LinkedProject) == true) {
            bin.companyDetailsGeneralDetailLay.projIc.updateLayoutParams<ConstraintLayout.LayoutParams> {
                height = 20.dpToPix(this@ProjectDetailsAc)
                width = 26.dpToPix(this@ProjectDetailsAc)
            }
        }


    }

    override fun onTagClick(name: String?) {

    }
    override fun onOptionSelect(pos: Int, id: String, count: String) {
        println("====isBncVersion:${isBncVersionView}==id:${bncVersionID}")
        startActivity(
            Intent(
                this,
                if (id == ApiConstants.Companies || id == ApiConstants.Bidders) CompaniesBildder::class.java
                else if (id == ApiConstants.Schedules) Schedules::class.java
                else if (id == ApiConstants.Reminders) ReminderAc::class.java
                else if (id == ApiConstants.UpdatesAndHighlights) UpdateAndHighlightsAc::class.java
                else if (id == ApiConstants.ProjectTree) ProjectTreeAc::class.java
                else if (id == ApiConstants.ProjectNews) ProjectNews::class.java
                else if (id == ApiConstants.ProjectReference) ProjectReferenceAc::class.java
                else if (id == ApiConstants.KeyContact) ContactAc::class.java
                else InteractionAc::class.java
            ).putExtra(Enums.ID.toString(), if (isBncVersionView) bncVersionID else projectID)
                .putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                .putExtra(Enums.RECORDTYPE.toString(), recordType)
                .putExtra(Enums.NAME.toString(), bin.companyDetailsGeneralDetailLay.lbl0.text)
        )

    }


    fun makeFavPro(id: String, obj: ProjectResById) {
        Uitls.showProgree(true, this)
        val call =
            mInterfce.makeFavForProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        obj.favourite = true
                        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
                            R.drawable.ic_favorite_active
                        )
                    } else {
                        Uitls.showToast(this@ProjectDetailsAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectDetailsAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                Uitls.handlerError(this@ProjectDetailsAc, t)
            }
        })
    }

    fun getInvitationCall() {
        Uitls.showProgree(true, this)
        val call = mInterfce.getInvitationUrl()
        call.enqueue(object : Callback<InvitationUrlRes> {
            override fun onResponse(
                call: Call<InvitationUrlRes>,
                response: Response<InvitationUrlRes>
            ) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showSharingDialog(
                            response.body()!!.message ?: "",
                            response.body()!!.shareUrl ?: "",
                            this@ProjectDetailsAc
                        )
                    } else {
                        Uitls.showToast(this@ProjectDetailsAc, "Not able make invitation request")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectDetailsAc)
                }
            }

            override fun onFailure(call: Call<InvitationUrlRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                Uitls.handlerError(this@ProjectDetailsAc, t)
            }
        })
    }

    fun removeFavPro(id: String, obj: ProjectResById) {
        Uitls.showProgree(true, this)
        val call =
            mInterfce.removeFavFromProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        obj.favourite = false
                        bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
                            R.drawable.ic_favorite_inactive
                        )
                    } else {
                        Uitls.showToast(this@ProjectDetailsAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectDetailsAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                Uitls.handlerError(this@ProjectDetailsAc, t)
            }
        })
    }

    fun updateViewCount() {
        Uitls.showProgree(true, this)
        val call = mInterfce.updateProjectView(projectID)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status != "1") {
                        Uitls.showToast(
                            this@ProjectDetailsAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectDetailsAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectDetailsAc)
                Uitls.handlerError(this@ProjectDetailsAc, t)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        println("====isBncVersion:${isBncVersionView}==id:${bncVersionID}")
        runOnUiThread {
            getProjectDetailsCall(
                if (isBncVersionView) bncVersionID else projectID,
                isBncVersion = isBncVersionView,
                isFromBtnEvent = false
            )
        }
    }
}