package com.bncairnetwork.ui.activities.homeConnections

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.RadioButton
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.*
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.*
import com.bncairnetwork.pojo.post.project.FavProPost
import com.bncairnetwork.pojo.response.InvitationUrlRes
import com.bncairnetwork.ui.activities.*
import com.bncairnetwork.ui.activities.projectConnections.ProjectDetailsAc
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.adapter.MultiSpinnerAdptr
import com.bncairnetwork.ui.adapter.company.CompanySearchAptr
import com.bncairnetwork.ui.adapter.project.ProjectSearchAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.nio.charset.Charset
import java.util.*
import kotlin.collections.ArrayList


class ProjectSearchAc : AppCompatActivity(), ProjectSeachListner, MultiSelectionListnr, TagListnr {
    lateinit var mInterfce: ApiInterface
    var comlstByKeyword = ArrayList<CompaniesItem?>()
    var comlst = ArrayList<CompaniesDataObj>()
    lateinit var comAdptr: CompanySearchAptr
    var offset = 0
    var recentUrl = ""
    var position = 0
    lateinit var dialog: Dialog
    lateinit var dialogView: MultiSelectionLayBinding
    private var isProjectTabSelected = true
    private var packageID = 0
    lateinit var mutliSpAptr: MultiSpinnerAdptr
    var proObj: ProjectDetailsDataObj? = null
    var myproject_url = ""
    var tobepass = ArrayList<String>()
    var mycompanies_url = ""
    var AdncSearchOffset = 0 /*advance project/company search offset value*/
    var SortSearchOffset = 0 /*advance project/company search offset value*/
    var FilterSearchOffset = 0
    var resType =
        0
    var sortUrl = ""
    var filterUrl = ""
    private var selectedRrcdTypeLst = ArrayList<String>()
    var sortPreviousRdId = 0
    var searchkeyword = ""
    var isFiltersApplied: Boolean = false
    var isSortFiltersApplied: Boolean = false
    lateinit var proAdptr: ProjectSearchAptr
    var prolst = ArrayList<ProjectDetailsDataObj>()
    lateinit var bin: ActivityProjectSearchBinding
    var isProjectActive = true
    private var projectRecordTypeLst = arrayListOf(
        "BNC Projects",
        "Linked Projects",
        "Internal Projects"
    )
    private var projectViewTypeLst = arrayListOf(
        "My projects",
        "All Projects"
    )

    private var companyRecordTypeLst = arrayListOf(
        "BNC Companies",
        "Linked Companies",
        "Internal Companies"
    )
    private var companyViewTypeLst = arrayListOf(
        "My Companies",
        "All Companies"
    )
    private var advanceResultContract =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { it ->
            if (it.resultCode == Activity.RESULT_OK) {
                isProjectActive =
                    it.data?.getStringExtra(Enums.TYPE.toString()) != ApiConstants.Company
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityProjectSearchBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {
        dialog = Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            dialogView =
                MultiSelectionLayBinding.inflate(LayoutInflater.from(this@ProjectSearchAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(dialogView.root)
        }
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        bin.filtr.visibility =
            if (packageID == ApiConstants.ibisID) View.VISIBLE else View.INVISIBLE
        searchkeyword = intent.getStringExtra(Enums.searchTxt.toString()) ?: ""
        mInterfce = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this@ProjectSearchAc)
        bin.projectBtn.submitBtn.text = "Projects (0)"
        bin.companiesBtn.submitBtn.text = "Companies (0)"
        bin.projectSearchCusAppbar.img.setOnClickListener {
            onBackPressed()
        }
        if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Companies && intent.getStringExtra(
                Enums.URL.toString()
            ) != null
        ) {
            mycompanies_url = intent.getStringExtra(Enums.URL.toString()) ?: ""
        } else if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project && intent.getStringExtra(
                Enums.URL.toString()
            ) != null
        ) {
            myproject_url = intent.getStringExtra(Enums.URL.toString()) ?: ""
        }


    }

    fun chngeBtn(isProjectSelect: Boolean) {
        offset = 0
        if (bin.searchBarProject.text.toString().isNotEmpty()) {
            searchkeyword = bin.searchBarProject.text.toString()
        }
        Uitls.hideKeyboard(this)
        when (isProjectSelect) {
            true -> {
                isProjectActive = true
                bin.companiesBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.black))
                bin.companiesBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)
                bin.projectBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.projectBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)

                proAdptr = ProjectSearchAptr(
                    prolst,
                    this@ProjectSearchAc, packageID = packageID, this@ProjectSearchAc
                ).apply {
                    bin.projectSearchRv.layoutManager =
                        LinearLayoutManager(this@ProjectSearchAc)
                    bin.projectSearchRv.adapter = this
                }

            }
            false -> {
//                comlst.clear()
//                if (!isFiltersApplied && !isSortFiltersApplied)
//                    comlstByKeyword.clear()
                isProjectActive = false
                bin.companiesBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.companiesBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)

                bin.projectBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.black))
                bin.projectBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)
                comAdptr = CompanySearchAptr(
                    this@ProjectSearchAc,
                    null,
                    comlstByKeyword,
                    this@ProjectSearchAc, packageID, this@ProjectSearchAc
                ).apply {
                    bin.projectSearchRv.layoutManager =
                        LinearLayoutManager(this@ProjectSearchAc)
                    bin.projectSearchRv.adapter = this
                }
            }
        }
    }

    fun listnr() {
        bin.projectSearchCusAppbar.map.setOnClickListener {
            // Uitls.showToast(this, "under development")
            if (isProjectActive && !prolst.isNullOrEmpty()) {
                startActivity(
                    Intent(
                        this,
                        MapViewAc::class.java
                    ).putExtra(Enums.mMapViewProjectLst.toString(), Gson().toJson(prolst))
                )
            }

        }
        bin.projectSearchRv.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !bin.progressbar.isVisible && !isFiltersApplied && !isSortFiltersApplied && filterUrl.isEmpty()) {
                    println("calledONe")
                    offset += 10
                    runOnUiThread {
                        if (isProjectActive) {
                            if (bin.searchBarProject.text.isEmpty()) {
                                getProjectLst()
                            } else {
                                getProjectByKeyword(bin.searchBarProject.text.toString())
                            }
                            return@runOnUiThread
                        } else {
                            if (bin.searchBarProject.text.isEmpty()) {
                                getCompanies()
                            } else {
                                getCompaniesByKeyword(bin.searchBarProject.text.toString())
                            }
                        }
                    }
                    return
                } else if (isFiltersApplied && !recyclerView.canScrollVertically(1)) {
                    println("calledTwo")
                    AdncSearchOffset += 10
                    runOnUiThread {
                        if (!isProjectActive) {
                            getCompanyResultsFromFilterApi()
                        } else {
                            getProjectResultsFromFilterApi()
                        }
                    }
                    return
                } else if (isSortFiltersApplied && !recyclerView.canScrollVertically(1)) {
                    println("calledThree")
                    SortSearchOffset += 10
                    runOnUiThread {
                        if (!isProjectActive) {
                            // getCompanyResultsFromFilterApi()
                            getCompaniesSortByResults(sortUrl)
                        } else {
//                            getProjectResultsFromFilterApi()
                            getProjectsSortByResults(sortUrl)
                        }
                    }
                    return
                } else if (filterUrl.isNotEmpty() && !recyclerView.canScrollVertically(1)) {
                    println("calledFour")
                    FilterSearchOffset += 10
                    runOnUiThread {
                        if (!isProjectActive) {
                            getCompaniesFilterByResults(filterUrl)
                        } else {
                            getProjectsFilterByResults(filterUrl)
                        }
                    }
                    return
                }
            }
        })
        bin.filtr.setOnClickListener {
//            startActivity(Intent(this, FilterAc::class.java))
            showFilterBottomSheetDialog()

        }

        bin.searchBarProject.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= bin.searchBarProject.right - bin.searchBarProject.compoundDrawables[2].bounds.width()
                ) {
                    // your action here
                    if (bin.searchBarProject.text.isNotEmpty()) {
                        Uitls.hideKeyboard(this)
                        isFiltersApplied = false
                        updateProComBtn(true)
                        runOnUiThread {
                            showNoDataLay(false)
                            when (isProjectActive) {
                                true -> {
                                    prolst.clear()
                                    getProjectByKeyword(bin.searchBarProject.text.toString())
                                }
                                false -> {
                                    comlstByKeyword.clear()
                                    comlst.clear()
                                    getCompaniesByKeyword(bin.searchBarProject.text.toString())
                                }
                            }
                        }
                    }
                    return@OnTouchListener true
                }
            }
            false
        })

//        bin.searchBarProject.setOnKeyListener { v, keyCode, event ->
//            if (event.action === KeyEvent.ACTION_DOWN) {
//                when (keyCode) {
//                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
//
//                        return@setOnKeyListener true
//                    }
//                }
//            }
//            return@setOnKeyListener false
//
//        }
        bin.moreSercOptnBtn.setOnClickListener {
            //comlstByKeyword.clear()
            // prolst.clear()
            offset = 0
            AdncSearchOffset = 0
            SortSearchOffset = 0
            FilterSearchOffset = 0
            filterUrl = ""

            bin.projectBtn.submitBtn.isEnabled = true
            bin.companiesBtn.submitBtn.isEnabled = true
            advanceResultContract.launch(
                Intent(this, AdvanceSearchAc::class.java).putExtra(
                    Enums.FROM.toString(),
                    Enums.PROJECT_SEARCH_AC.toString()
                )
            )
            ApiConstants.FROM = Enums.PROJECT_SEARCH_AC.toString()
//            startActivity(Intent(this, AdvanceSearchAc::class.java))
        }

        bin.projectSearchCusAppbar.chat.setOnClickListener {
            startActivity(Intent(this, ChatAc::class.java))
        }
        bin.projectBtn.submitBtn.setOnClickListener {
            showNoDataLay(false)
            chngeBtn(true)
            runOnUiThread {
                if (bin.searchBarProject.text.isEmpty()) {
                    getProjectLst()
                } else {
                    getProjectByKeyword(searchkeyword)
                }
            }
        }
        bin.companiesBtn.submitBtn.setOnClickListener {
            showNoDataLay(false)
            chngeBtn(false)
            runOnUiThread {
                if (bin.searchBarProject.text.isEmpty()) {
                    getCompanies()
                } else {
                    getCompaniesByKeyword(searchkeyword)
                }
            }
        }

        bin.projectSearchCusAppbar.sort.setOnClickListener {
            Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
                SortSearchOffset = 0
                requestWindowFeature(Window.FEATURE_NO_TITLE)
                window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                val view =
                    SortByProjectLayBinding.inflate(LayoutInflater.from(this@ProjectSearchAc))
                setContentView(view.root)
                view.grup1.visibility = if (isProjectActive) View.VISIBLE else View.INVISIBLE
                view.grup2.visibility = if (isProjectActive) View.INVISIBLE else View.VISIBLE

                if (sortPreviousRdId != 0) {
                    if (isProjectActive) {
                        view.grup1.check(sortPreviousRdId)
                    } else {
                        view.grup2.check(sortPreviousRdId)
                    }
                }
                if (isProjectActive) {
                    view.grup1.setOnCheckedChangeListener { group, checkedId ->
                        isSortFiltersApplied = true
                        sortPreviousRdId = checkedId
                        println(group.findViewById<RadioButton>(checkedId).text.toString())
                        dismiss()
                        if (group.findViewById<RadioButton>(checkedId).text.toString() == "Relevance") {
                            runOnUiThread {
                                getProjectByKeyword(bin.searchBarProject.text.toString())
                            }

                        } else {
                            getUrlForSortByForBasicSearch(group.findViewById<RadioButton>(checkedId).text.toString())
                        }

                    }
                } else {
                    view.grup2.setOnCheckedChangeListener { group, checkedId ->
                        sortPreviousRdId = checkedId
                        isSortFiltersApplied = true
                        println(group.findViewById<RadioButton>(checkedId).text.toString())
                        dismiss()
                        if (group.findViewById<RadioButton>(checkedId).text.toString() == "Relevance") {
                            runOnUiThread {
                                getCompaniesByKeyword(bin.searchBarProject.text.toString())
                            }

                        } else {
                            getUrlForSortByForBasicSearch(group.findViewById<RadioButton>(checkedId).text.toString())
                        }
                    }
                }
                view.cancel.setOnClickListener {
                    dismiss()
                }
                show()

            }
        }
    }

    fun getUrlForSortByForBasicSearch(sortBy: String) {
        var sort = ""
        var sort_criteria = ""
        when (sortBy) {
            "Stage Ascending" -> {
                sort = "project_stage"
                sort_criteria = "asc"
            }
            "Stage Descending" -> {
                sort = "project_stage"
                sort_criteria = "desc"
            }
            "Type Ascending" -> {
                sort = "type_filter"
                sort_criteria = "asc"
            }
            "Type Descending" -> {
                sort = "type_filter"
                sort_criteria = "desc"
            }
            "Value Ascending" -> {
                sort = "value"
                sort_criteria = "asc"
            }
            "Value Descending" -> {
                sort = "value"
                sort_criteria = "desc"
            }
            /*************************** CompanySortByParams ***********************/
            "Name Ascending" -> {
                sort = "name"
                sort_criteria = "asc"
            }
            "Name Descending" -> {
                sort = "name"
                sort_criteria = "desc"
            }

            "Country Ascending" -> {
                sort = "country"
                sort_criteria = "asc"
            }

            "Country Descending" -> {
                sort = "country"
                sort_criteria = "desc"
            }

            "City Ascending" -> {
                sort = "city"
                sort_criteria = "asc"
            }

            "City Descending" -> {
                sort = "city"
                sort_criteria = "desc"
            }

            "Company Type Ascending" -> {
                sort = "company_type"
                sort_criteria = "asc"
            }
            "Company Type Descending" -> {
                sort = "company_type"
                sort_criteria = "desc"
            }
        }
        if (!isFiltersApplied) {
            sortUrl =
                if (isProjectActive) ApiConstants.project_sortByBasicSearchUrl.toString().replace(
                    "project_name=&",
                    "project_name=${bin.searchBarProject.text.toString()}&"
                ).replace("sort=&", "sort=${sort}&")
                    .replace("sort_criteria=", "sort_criteria=$sort_criteria")
                else ApiConstants.company_sortByBasicSearchUrl.toString().replace(
                    "company_name=&",
                    "company_name=${bin.searchBarProject.text.toString()}&"
                ).replace("sort=&", "sort=${sort}&")
                    .replace("sort_criteria=", "sort_criteria=$sort_criteria")


        } else {
            sortUrl = ""
            sortUrl =
                if (isProjectActive) ApiConstants.advanceProjectUrl.plus("&sort=$sort")
                    .plus("&sort_criteria=$sort_criteria")
                else ApiConstants.advanceCompanyUrl.toString().plus("&sort=$sort")
                    .plus("&sort_criteria=$sort_criteria")

        }

        runOnUiThread {
            if (isProjectActive) {
                getProjectsSortByResults(sortUrl)
            } else {
                getCompaniesSortByResults(sortUrl)
            }

        }


    }

    fun showProjectClaimDialog(obj: ProjectClaimInfoRes, proID: String) {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            val view =
                ProjectAlreadyClaimedPopupBinding.inflate(LayoutInflater.from(this@ProjectSearchAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(view.root)
            view.defBtnLay.submitBtn.text = "View Projects"
            view.rightNotNowLbl.setOnClickListener {
                dismiss()
            }
            view.ownerslblCount.text = obj.project?.owners.toString()
            view.bidderCount.text = obj.project?.bidders.toString()
            view.consultantCount.text = obj.project?.consultants.toString()
            view.contractorCount.text = obj.project?.contractors.toString()
            view.lbl.text = obj.project?.name ?: ""
            view.defBtnLay.submitBtn.setOnClickListener {
                dismiss()
                obj.credits?.let {
                    if (it >= 5) {
                        runOnUiThread {
                            exhaustCredits(proID = proID, proObj = proObj)
                        }
                    } else {
                        /*in sufficient credits*/
                        showInAppOption()
                    }
                }
            }
            show()
        }
    }

    fun showInAppOption() {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val view_by_crd =
                ActivityAddCreditBinding.inflate(LayoutInflater.from(this@ProjectSearchAc))
            setContentView(view_by_crd.root)
            show()
            view_by_crd.addCrdtInviteLay.setOnClickListener {
                dismiss()
            }
            view_by_crd.addCrdtShareLay.setOnClickListener {
                dismiss()
                runOnUiThread {
                    getInvitationCall()
                }
            }
            view_by_crd.addCrdtBuyLay.setOnClickListener {
                dismiss()
                /*start follow for in app*/
            }
        }
    }

    fun showFilterBottomSheetDialog() {
        FilterSearchOffset = 0
        BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme).apply {
            val view =
                FilterBottomSheetBinding.inflate(LayoutInflater.from(this@ProjectSearchAc))
            setContentView(view.root)
            view.closeBtn.setOnClickListener {
                dismiss()
            }
            view.recordTypeSp.setOnClickListener {
                showMultiSelctionDialog(
                    dialogView,
                    if (isProjectActive) projectRecordTypeLst else companyRecordTypeLst
                )
                dialogView.okbtn.setOnClickListener {
                    view.recordTypeSp.text =
                        selectedRrcdTypeLst.toString().replace("[", "").replace("]", "")
                    dialog.dismiss()
                }
                dialog.show()
            }
            view.defBtnLay.submitBtn.text = "Apply"
            view.defBtnLay.submitBtn.setOnClickListener {
                if (selectedRrcdTypeLst.isNotEmpty() && view.ViewTypeSp.selectedItemPosition > 0) {
                    if (!isFiltersApplied) {
                        filterUrl =
                            if (isProjectActive) ApiConstants.project_filterByBasicSearchUrl.toString()
                                .replace(
                                    "project_name=&",
                                    "project_name=${bin.searchBarProject.text.toString()}&"
                                ).replace(
                                    "record_type_filter=&",
                                    "record_type_filter=${
                                        selectedRrcdTypeLst.toString().replace("[", "[\"")
                                            .replace("]", "\"]").replace(",", "\",\"")
                                    }&"
                                )
                                .replace(
                                    "quick_link=&",
                                    if (view.ViewTypeSp.selectedItem == "My projects") "quick_link=16&"
                                    else ""
                                )
                            else ApiConstants.company_filterByBasicSearchUrl.toString().replace(
                                "company_name=&",
                                "company_name=${bin.searchBarProject.text.toString()}&"
                            ).replace(
                                "record_type_filter=&",
                                "record_type_filter=${
                                    selectedRrcdTypeLst.toString().replace("[", "[\"")
                                        .replace("]", "\"]").replace(",", "\",\"")
                                }&"
                            )
                                .replace(
                                    "quick_link=&",
                                    if (view.ViewTypeSp.selectedItem == "My Companies") "quick_link=17&"
                                    else ""
                                )


                    } else {
//                    filterUrl = ""
//                    filterUrl =
//                        if (isProjectActive) ApiConstants.advanceProjectUrl.plus("&sort=$sort")
//                            .plus("&sort_criteria=$sort_criteria")
//                        else ApiConstants.advanceCompanyUrl.toString().plus("&sort=$sort")
//                            .plus("&sort_criteria=$sort_criteria")

                    }
                    runOnUiThread {
                        if (isProjectActive) {
                            getProjectsFilterByResults(filterUrl)
                        } else {
                            getCompaniesFilterByResults(filterUrl)
                        }

                    }
                }
                dismiss()
            }
//            ArrayAdapter(
//                this@ProjectSearchAc,
//                android.R.layout.simple_list_item_1,
//                if (isProjectActive)
//                    arrayListOf(
//                        "Select record type",
//                        "BNC Projects",
//                        "Linked Projects",
//                        "Internal Projects"
//                    ) else arrayListOf(
//                    "BNC Companies",
//                    "Linked Companies",
//                    "Internal Companies"
//                )
//            ).apply {
//                setDropDownViewResource(R.layout.sp_row_item_lay)
//                view.recordTypeSp.adapter = this
//            }
            ArrayAdapter(
                this@ProjectSearchAc,
                android.R.layout.simple_list_item_1,
                if (isProjectActive)
                    arrayListOf(
                        "Select type",
                        "My projects",
                        "All projects"
                    ) else arrayListOf(
                    "Select type",
                    "My Companies",
                    "All Companies"
                )
            ).apply {
                setDropDownViewResource(R.layout.sp_row_item_lay)
                view.ViewTypeSp.adapter = this
            }

            show()
        }
    }

    fun showMultiSelctionDialog(
        view: MultiSelectionLayBinding,
        lst: ArrayList<String>
    ) {
        view.noDataLbl.noDataLbl.visibility = if (lst.isEmpty()) View.VISIBLE else View.GONE
        //0-> record type, 1->viewType
        tobepass.clear()

        when (resType) {
            0 -> {
                view.lbl.setText(R.string.select_record_type)
                tobepass.addAll(selectedRrcdTypeLst)
            }
            1 -> {
                view.lbl.setText(R.string.select_type)
                //tobepass.addAll(selectedcity.keys)
            }

        }
        mutliSpAptr = MultiSpinnerAdptr(
            this@ProjectSearchAc,
            lst,
            tobepass,
            this
        ).apply {
            view.multiRv.layoutManager = LinearLayoutManager(this@ProjectSearchAc)
            view.multiRv.adapter = this
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onTagClick(name: String?) {
        println("----onTagClick--${name}")
//        println(
//            "-----${
//                recentUrl.plus(
//                    "&tag=[{\"id\"=\"${
//                        Base64.getEncoder()
//                            .encodeToString((name)?.toByteArray(Charset.defaultCharset()))
//                    }\"}]"
//                )
//            }"
//        )
        bin.progressbar.visibility = View.VISIBLE
        val call = mInterfce.getProjectByTags(
            ApiConstants.tag_project_url.plus(
                "tags=[{\"id\":\"${
                    Base64.getEncoder()
                        .encodeToString((name)?.toByteArray(Charset.defaultCharset()))
                }\"}]"
            )
        )
        println("hitUrl--->${call.request().url.toString()}")
        call.enqueue(object : Callback<ProjectSearchRes> {
            override fun onResponse(
                call: Call<ProjectSearchRes>,
                response: Response<ProjectSearchRes>
            ) {
                bin.progressbar.visibility = View.INVISIBLE
                if (response.body() != null && response.isSuccessful) {
                    bin.projectBtn.submitBtn.text =
                        "Projects (${response.body()!!.total_projects})"
                    bin.companiesBtn.submitBtn.text =
                        "Companies (${response.body()!!.total_companies})"
                    prolst.clear()
                    prolst.addAll(response.body()!!.projects)
                    proAdptr.notifyDataSetChanged()
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                    onApiFail()
                }

            }

            override fun onFailure(call: Call<ProjectSearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.INVISIBLE
                Uitls.showToast(this@ProjectSearchAc, t.message ?: "")
            }
        })


    }

    override fun onPrjctSrchClk(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        proObj: ProjectDetailsDataObj?,
        comObj: CompaniesDataObj?, comkeywordObj: CompaniesItem?
    ) {

        if (isTxt) {
            this.proObj = proObj
            if (isProjectActive) {
                when (packageID) {
                    ApiConstants.professionalID -> {
                        proObj?.authorised?.let { isAuthorised ->
                            if (isAuthorised) {
                                /*this project is in unlocked state*/
                                startActivity(
                                    Intent(
                                        this@ProjectSearchAc,
                                        ProjectDetailsAc::class.java
                                    ).putExtra(Enums.ID.toString(), id)
                                        .putExtra(
                                            Enums.DATA.toString(),
                                            Gson().toJson(
                                                proObj
                                            )
                                        )
                                )
//                                runOnUiThread {
//                                    exhaustCredits(proID = proID, proObj = proObj)
//                                }
                            } else {
                                /*this project is in locked state*/
                                runOnUiThread {
                                    getProjectInfo(id)
                                }
                            }
                        }
                    }
                    ApiConstants.ibisID -> {
                        startActivity(
                            Intent(
                                this@ProjectSearchAc,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), id)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
                    }
                    ApiConstants.bi_userID -> {
                        startActivity(
                            Intent(
                                this@ProjectSearchAc,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), id)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
                    }
                }

            } else {
                startActivity(
                    Intent(
                        this, CompanyDetailAc::class.java
                    ).putExtra(Enums.ID.toString(), id)
                        .putExtra(
                            Enums.DATA.toString(),
                            Gson().toJson(comObj ?: comkeywordObj)
                        )
                )
            }

            return
        }

        if (isTag) {
            startActivity(
                Intent(this, Tag::class.java).putExtra(Enums.ID.toString(), id).putExtra(
                    Enums.TYPE.toString(),
                    if (isProjectActive) ApiConstants.Project else ApiConstants.Company
                ).putExtra(
                    Enums.DATA.toString(),
                    Gson().toJson(if (isProjectActive) proObj else comObj ?: comkeywordObj)
                )
            )
            return
        }
        if (isAddInteraction) {
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.ID.toString(),
                    id
                ).putExtra(
                    Enums.TYPE.toString(),
                    if (isProjectActive) ApiConstants.Project else ApiConstants.Company
                ).putExtra(
                    Enums.ProjectOrCompanyName.toString(),
                    if (isProjectActive) proObj?.name else comkeywordObj?.name ?: ""
                )
                    .putExtra(
                        Enums.ProjectOrCompanyCountry.toString(),
                        if (isProjectActive) proObj?.country else comkeywordObj?.country ?: ""
                    )
                    .putExtra(
                        Enums.ProjectOrCompanyCity.toString(),
                        if (isProjectActive) proObj?.city else comkeywordObj?.city ?: ""
                    )
            )
            return

        }
        if (isReminder) {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(Enums.ID.toString(), id)
                    .putExtra(
                        Enums.TYPE.toString(),
                        if (isProjectActive) ApiConstants.Project else ApiConstants.Company
                    )
            )
            return
        }
        if (isFav) {
            if (isProjectActive) {
                runOnUiThread {
                    if (prolst[pos].tag_obj?.isFavourite != true) {
                        makeFavPro(id, pos)
                    } else {
                        removeFavPro(id, pos)
                    }
                }

            } else {
                runOnUiThread {
                    if (comObj != null) {
                        if (comlst[pos].tag_obj?.isFavourite != true) {
                            makeFavCom(id, pos, false)
                        } else {
                            removeFavCom(id, pos, false)
                        }
                    } else {
                        if (comlstByKeyword[pos]?.favourite != true) {
                            makeFavCom(id, pos, true)
                        } else {
                            removeFavCom(id, pos, true)
                        }
                    }

                }
            }
            return
        }
    }


    /******Apis********/

    fun exhaustCredits(proID: String, proObj: ProjectDetailsDataObj?) {
        Uitls.showProgree(true, this)
        val call = mInterfce.exhaustCredit(projectID = proID)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectSearchAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        startActivity(
                            Intent(
                                this@ProjectSearchAc,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), proID)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "Not able to update project view.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectSearchAc)
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getInvitationCall() {
        Uitls.showProgree(true, this)
        val call = mInterfce.getInvitationUrl()
        call.enqueue(object : Callback<InvitationUrlRes> {
            override fun onResponse(
                call: Call<InvitationUrlRes>,
                response: Response<InvitationUrlRes>
            ) {
                Uitls.showProgree(false, this@ProjectSearchAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showSharingDialog(
                            response.body()!!.message ?: "",
                            response.body()!!.shareUrl ?: "",
                            this@ProjectSearchAc
                        )
                    } else {
                        Uitls.showToast(
                            this@ProjectSearchAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<InvitationUrlRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectSearchAc)
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getProjectInfo(projectID: String) {
        Uitls.showProgree(true, this)
        val call = mInterfce.getProjectInfo(projectID)
        call.enqueue(object : Callback<ProjectClaimInfoRes> {
            override fun onResponse(
                call: Call<ProjectClaimInfoRes>,
                response: Response<ProjectClaimInfoRes>
            ) {
                Uitls.showProgree(false, this@ProjectSearchAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        showProjectClaimDialog(response.body()!!, projectID)

//                        response.body()!!.credits?.let {credit->
//                            if (credit >= 5) {
//                                runOnUiThread {
//                                    //   projectClaimCall()
//                                    updateProjectView(proID = projectID, proObj = proObj)
//                                }
//                            } else {
//                                /*in sufficient credits*/
//                                showInAppOption()
//                            }
//
//                        }

                    } else {
                        Uitls.showToast(
                            this@ProjectSearchAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ProjectClaimInfoRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectSearchAc)
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getProjectByKeyword(keyword: String) {
        //prolst.clear()
        bin.progressbar.visibility = View.VISIBLE
        val call = mInterfce.getprojectBykeyword(keyword = keyword, offsetval = offset.toString())
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<ProjectSearchRes> {
            override fun onResponse(
                call: Call<ProjectSearchRes>,
                response: Response<ProjectSearchRes>
            ) {

                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        bin.projectBtn.submitBtn.text =
                            "Projects (${response.body()!!.total_projects})"
                        bin.companiesBtn.submitBtn.text =
                            "Companies (${response.body()!!.total_companies})"
                        if (offset == 0) {
                            prolst.clear()
                        }
                        prolst.addAll(response.body()!!.projects)
                        proAdptr.notifyDataSetChanged()

                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "No Data Found")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ProjectSearchRes>, t: Throwable) {

                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getCompaniesByKeyword(keyword: String) {
//        comlst.clear()
        bin.progressbar.visibility = View.VISIBLE
        val call = mInterfce.getCompaniesBykeyword(keyword = keyword, offsetval = offset.toString())
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<GetCompaniesBySearchRes> {
            override fun onResponse(
                call: Call<GetCompaniesBySearchRes>,
                response: Response<GetCompaniesBySearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        try {
                            bin.companiesBtn.submitBtn.text =
                                "Companies (${response.body()!!.totalCompanies ?: ""})"
                            if (offset == 0) {
                                comlstByKeyword.clear()
                            }
                            response.body()!!.companies?.let {
                                comlst.clear()
                                comlstByKeyword.addAll(it)
                            }

                            comAdptr.notifyDataSetChanged()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }

                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "No Data Found")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }

            }

            override fun onFailure(call: Call<GetCompaniesBySearchRes>, t: Throwable) {

                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getProjectLst() {
        bin.progressbar.visibility = View.VISIBLE
        bin.companiesBtn.submitBtn.isEnabled = false
        val call = mInterfce.getProjects(url = myproject_url)
        call.enqueue(object : Callback<ProjectSearchRes> {
            override fun onResponse(
                call: Call<ProjectSearchRes>,
                response: Response<ProjectSearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        bin.projectBtn.submitBtn.text =
                            "Projects( ${response.body()!!.total_projects} )"
                        prolst.clear()
                        prolst.addAll(response.body()!!.projects)
                        proAdptr.notifyDataSetChanged()
                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "No Data Found")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ProjectSearchRes>, t: Throwable) {
                t.printStackTrace()
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })

    }

    fun getCompanies() {
        bin.progressbar.visibility = View.VISIBLE
        bin.projectBtn.submitBtn.isEnabled = false
        val call = mInterfce.getMyCompanies(url = mycompanies_url)
        call.enqueue(object : Callback<GetCompaniesBySearchRes> {
            override fun onResponse(
                call: Call<GetCompaniesBySearchRes>,
                response: Response<GetCompaniesBySearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.companiesBtn.submitBtn.text =
                            "Companies (${response.body()!!.totalCompanies ?: ""})"
                        comlstByKeyword.clear()
                        response.body()!!.companies?.let {
                            val pos = comlstByKeyword.size + 1
                            comlstByKeyword.addAll(
                                it
                            )
                            comAdptr.notifyItemRangeInserted(pos, it.size)
                            println("============myCompaniesSize============" + comAdptr.comlstkeyword?.size)

                        }

                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "No Data Found")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<GetCompaniesBySearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun makeFavPro(id: String, pos: Int) {
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.makeFavForProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {

                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        prolst[pos].tag_obj?.isFavourite = true
                        proAdptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun removeFavPro(id: String, pos: Int) {
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.removeFavFromProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        prolst[pos].tag_obj?.isFavourite = false
                        proAdptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {

                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun makeFavCom(id: String, pos: Int, isFiltrApply: Boolean) {
        bin.progressbar.visibility = View.VISIBLE
        val call = mInterfce.addFavForCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        if (isFiltrApply) {
                            comlstByKeyword[pos]?.favourite = true
                        } else {
                            comlst[pos].tag_obj?.isFavourite = true
                        }
                        comAdptr.notifyItemChanged(pos)

                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {

                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun removeFavCom(id: String, pos: Int, isFiltrApply: Boolean) {
        bin.progressbar.visibility = View.VISIBLE
        val call = mInterfce.removeFavFromCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        if (isFiltrApply) {
                            comlstByKeyword[pos]?.favourite = false
                        } else {
                            comlst[pos].tag_obj?.isFavourite = false
                        }
                        comAdptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@ProjectSearchAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getCompanyResultsFromFilterApi() {
        updateProComBtn(false)
        isProjectActive = false
        isFiltersApplied = true
        ApiConstants.advanceProjectUrl = ""
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.advanceSearchByCompanyByUrl(
                url = ApiConstants.advanceCompanyUrl.replace(
                    "https://staging.bncnetwork.net",
                    ""
                ).replace("offset=0", "offset=$AdncSearchOffset")
            )
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<GetCompaniesBySearchRes> {
            override fun onResponse(
                call: Call<GetCompaniesBySearchRes>,
                response: Response<GetCompaniesBySearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (AdncSearchOffset == 0) {
                        chngeBtn(false)
                    }

                    bin.projectBtn.submitBtn.text = "Projects"
                    if (response.body()!!.totalCompanies != 0) {
                        bin.companiesBtn.submitBtn.text =
                            "Companies (".plus(response.body()!!.totalCompanies).plus(")")
                    } else {
                        bin.companiesBtn.submitBtn.text = "Companies"

                    }
                    if (AdncSearchOffset == 0) {
                        comlstByKeyword.clear()
                    }
                    response.body()!!.companies?.let {
                        val pos = comlstByKeyword.size + 1
                        comlstByKeyword.addAll(it)
                        comAdptr.notifyItemRangeInserted(pos, it.size)
                        println("==========adapterSize====>" + comAdptr.comlstkeyword?.size)
                    }
                    // comAdptr.notifyItemInserted(comlstByKeyword.size)
                    showNoDataLay(comlstByKeyword.isNullOrEmpty())
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                    onApiFail()
                }

            }

            override fun onFailure(call: Call<GetCompaniesBySearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getProjectResultsFromFilterApi() {
        println("getProjectResultsFromFilterApi")
        updateProComBtn(false)
        isProjectActive = true
        isFiltersApplied = true
        ApiConstants.advanceCompanyUrl = ""
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.advanceSearchByProjectByUrl(
                url = ApiConstants.advanceProjectUrl.replace(
                    "https://staging.bncnetwork.net",
                    ""
                ).replace("offset=0", "offset=$AdncSearchOffset")
            )
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<ProjectSearchRes> {
            override fun onResponse(
                call: Call<ProjectSearchRes>,
                response: Response<ProjectSearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    if (AdncSearchOffset == 0) {
                        chngeBtn(true)
                    }

                    bin.companiesBtn.submitBtn.text = "Companies"
                    if (response.body()!!.total_projects?.equals("0") != true) {
                        bin.projectBtn.submitBtn.text =
                            "Projects (".plus(response.body()!!.total_projects).plus(")")
                    } else {
                        bin.projectBtn.submitBtn.text =
                            "Project"
                        bin.companiesBtn.submitBtn.text = "Company"

                    }
                    if (AdncSearchOffset == 0) {
                        prolst.clear()
                    }
                    response.body()!!.projects.let {
                        position = prolst.size + 1
                        prolst.addAll(
                            it
                        )
                        // proAdptr.notifyDataSetChanged()
                        proAdptr.notifyItemRangeInserted(
                            position,
                            it.size
                        )
                    }
                    println("====> size" + prolst.size)
//                    proAdptr = ProjectSearchAptr(
//                        this@ProjectSearchAc,
//                        proLst = prolst,
//                        this@ProjectSearchAc,
//                        packageID = packageID
//                    ).apply {
//                        bin.projectSearchRv.layoutManager =
//                            LinearLayoutManager(this@ProjectSearchAc)
//                        bin.projectSearchRv.adapter = this
//                    }


                    showNoDataLay(prolst.isNullOrEmpty())
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                    onApiFail()
                }
            }

            override fun onFailure(call: Call<ProjectSearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getCompaniesFilterByResults(url: String) {
        bin.projectBtn.submitBtn.isEnabled = false
        isProjectActive = false
        isFiltersApplied = false
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.filterByCompaniesByUrl(
                url = url.replace("&offset=0", "&offset=$FilterSearchOffset")
            )
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<GetCompaniesBySearchRes> {
            override fun onResponse(
                call: Call<GetCompaniesBySearchRes>,
                response: Response<GetCompaniesBySearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    chngeBtn(false)
                    bin.projectBtn.submitBtn.text = "Projects"
                    if (response.body()!!.totalCompanies != 0) {
                        bin.companiesBtn.submitBtn.text =
                            "Companies (".plus(response.body()!!.totalCompanies).plus(")")
                    } else {
                        bin.companiesBtn.submitBtn.text = "Companies"

                    }
                    response.body()!!.companies?.let {
                        if (FilterSearchOffset == 0) {
                            comlstByKeyword.clear()
                        }
                        val pos = comlstByKeyword.size + 1
                        comlstByKeyword.addAll(
                            it
                        )
                        comAdptr.notifyItemRangeInserted(pos, it.size)
                        println("========================" + comAdptr.comlstkeyword?.size)

                    }
                    // comAdptr.notifyItemInserted(comlstByKeyword.size)


                    showNoDataLay(comlstByKeyword.isNullOrEmpty())
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                    onApiFail()
                }

            }

            override fun onFailure(call: Call<GetCompaniesBySearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getProjectsFilterByResults(url: String) {
        bin.companiesBtn.submitBtn.isEnabled = false
        isProjectActive = true
        isFiltersApplied = false
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.filterByProjectByUrl(
                url = url.replace("&offset=0", "&offset=$FilterSearchOffset")
            )
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<ProjectSearchRes> {
            override fun onResponse(
                call: Call<ProjectSearchRes>,
                response: Response<ProjectSearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    chngeBtn(true)
                    bin.companiesBtn.submitBtn.text = "Companies"
                    if (response.body()!!.total_projects?.equals("0") != true) {
                        bin.projectBtn.submitBtn.text =
                            "Projects (".plus(response.body()!!.total_projects).plus(")")
                    } else {
                        bin.projectBtn.submitBtn.text =
                            "Project"
                        bin.companiesBtn.submitBtn.text = "Company"

                    }
                    if (FilterSearchOffset == 0) {
                        prolst.clear()
                    }

                    response.body()!!.projects?.let {
                        prolst.addAll(
                            it
                        )
                    }

                    proAdptr = ProjectSearchAptr(
                        proLst = prolst,
                        this@ProjectSearchAc,
                        packageID = packageID, this@ProjectSearchAc
                    ).apply {
                        bin.projectSearchRv.layoutManager =
                            LinearLayoutManager(this@ProjectSearchAc)
                        bin.projectSearchRv.adapter = this
                    }
                    showNoDataLay(prolst.isNullOrEmpty())
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                    onApiFail()
                }

            }

            override fun onFailure(call: Call<ProjectSearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }


    fun getProjectsSortByResults(url: String) {
        bin.companiesBtn.submitBtn.isEnabled = false
        isProjectActive = true
        isFiltersApplied = false
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.sortByProjectByUrl(
                url = url.replace("&offset=0", "&offset=$SortSearchOffset")
            )
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<ProjectSearchRes> {
            override fun onResponse(
                call: Call<ProjectSearchRes>,
                response: Response<ProjectSearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    chngeBtn(true)
                    bin.companiesBtn.submitBtn.text = "Companies"
                    if (response.body()!!.total_projects?.equals("0") != true) {
                        bin.projectBtn.submitBtn.text =
                            "Projects (".plus(response.body()!!.total_projects).plus(")")
                    } else {
                        bin.projectBtn.submitBtn.text =
                            "Project"
                        bin.companiesBtn.submitBtn.text = "Company"

                    }
                    if (SortSearchOffset == 0) {
                        prolst.clear()
                    }

                    response.body()!!.projects?.let {
                        prolst.addAll(
                            it
                        )
                    }

                    proAdptr = ProjectSearchAptr(

                        proLst = prolst,
                        this@ProjectSearchAc,
                        packageID = packageID, this@ProjectSearchAc
                    ).apply {
                        bin.projectSearchRv.layoutManager =
                            LinearLayoutManager(this@ProjectSearchAc)
                        bin.projectSearchRv.adapter = this
                    }
                    showNoDataLay(prolst.isNullOrEmpty())
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                    onApiFail()
                }

            }

            override fun onFailure(call: Call<ProjectSearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    fun getCompaniesSortByResults(url: String) {
        bin.projectBtn.submitBtn.isEnabled = false
        isProjectActive = false
        isFiltersApplied = false
        bin.progressbar.visibility = View.VISIBLE
        val call =
            mInterfce.sortByCompaniesByUrl(
                url = url.replace("&offset=0", "&offset=$SortSearchOffset")
            )
        recentUrl = call.request().url.toString()
        call.enqueue(object : Callback<GetCompaniesBySearchRes> {
            override fun onResponse(
                call: Call<GetCompaniesBySearchRes>,
                response: Response<GetCompaniesBySearchRes>
            ) {
                bin.progressbar.visibility = View.GONE
                if (response.body() != null && response.isSuccessful) {
                    chngeBtn(false)
                    bin.projectBtn.submitBtn.text = "Projects"
                    if (response.body()!!.totalCompanies != 0) {
                        bin.companiesBtn.submitBtn.text =
                            "Companies (".plus(response.body()!!.totalCompanies).plus(")")
                    } else {
                        bin.companiesBtn.submitBtn.text = "Companies"

                    }
                    response.body()!!.companies?.let {
                        if (SortSearchOffset == 0) {
                            comlstByKeyword.clear()
                        }
                        val pos = comlstByKeyword.size + 1
                        comlstByKeyword.addAll(
                            it
                        )
                        comAdptr.notifyItemRangeInserted(pos, it.size)
                        println("========================" + comAdptr.comlstkeyword?.size)

                    }
                    // comAdptr.notifyItemInserted(comlstByKeyword.size)


                    showNoDataLay(comlstByKeyword.isNullOrEmpty())
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectSearchAc)
                    onApiFail()
                }

            }

            override fun onFailure(call: Call<GetCompaniesBySearchRes>, t: Throwable) {
                bin.progressbar.visibility = View.GONE
                Uitls.handlerError(this@ProjectSearchAc, t)
            }
        })
    }

    /***invoke this method for advance project/company api fails*/
    fun onApiFail() {
        bin.projectSearchRv.visibility = View.INVISIBLE
        bin.projectBtn.submitBtn.text = "Projects"
        bin.companiesBtn.submitBtn.text = "Companies"
    }

    fun showNoDataLay(flag: Boolean) {
        bin.noDataLbl.noDataLbl.text =
            "Your search did not match any records suggestions:\n- Make sure all words are spelled correctly.\n- Try different keywords.\n- Try fewer keywords."
        bin.noDataLbl.noDataLbl.visibility = if (flag) View.VISIBLE else View.GONE
        bin.projectSearchRv.visibility = if (flag) View.INVISIBLE else View.VISIBLE

    }

    fun updateProComBtn(flag: Boolean) {
        if (!flag) {
            bin.searchBarProject.text = null
        }
        bin.projectBtn.submitBtn.isEnabled = flag
        bin.companiesBtn.submitBtn.isEnabled = flag
    }


    override fun onResume() {
        super.onResume()
        if (intent.getBooleanExtra(
                Enums.isFilterApplied.toString(),
                false
            ) && ApiConstants.FROM == Enums.HOME_SCREEN_AC.toString()
        ) {
            try {
                runOnUiThread {
                    if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Company) {
                        getCompanyResultsFromFilterApi()
                    } else {
                        getProjectResultsFromFilterApi()
                    }

                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return
        } else if (!isFiltersApplied && !isSortFiltersApplied && ApiConstants.FROM != Enums.PROJECT_SEARCH_AC.toString() && filterUrl.isEmpty()) {
            println("---------------->" + ApiConstants.FROM)
            if (intent.getStringExtra(Enums.TYPE.toString()) != null) {
                runOnUiThread {
                    if (intent.getStringExtra(Enums.TYPE.toString())
                            .equals(ApiConstants.Companies) && !isProjectActive
                    ) {
                        chngeBtn(false)
                        if (searchkeyword.isEmpty()) {
                            println("=============getCompanyLstFromQuickLink===============")
                            getCompanies()
                        } else {
                            bin.searchBarProject.setText(
                                searchkeyword
                            )
                            getCompaniesByKeyword(
                                searchkeyword
                            )

                        }
                    } else {
                        chngeBtn(true)
                        if (searchkeyword.isEmpty()) {
                            println("=============getProjectLstFromQuickLink===============")
                            if (myproject_url.isNotEmpty()) {
                                getProjectLst()
                            } else {
                                println("=============getCompanyLstFromQuickLink===============")
                                chngeBtn(isProjectSelect = false)
                                getCompanies()
                            }

                        } else {
                            bin.searchBarProject.setText(
                                searchkeyword
                            )
                            getProjectByKeyword(searchkeyword)

                        }
                    }

                }
                return
            } else {
                runOnUiThread {
                    if (isProjectActive) {
                        if (bin.searchBarProject.text.isEmpty()) {
                            getProjectLst()
                        } else {
                            getProjectByKeyword(bin.searchBarProject.text.toString())
                        }

                    } else {
                        if (bin.searchBarProject.text.isEmpty()) {
                            getCompanies()
                        } else {
                            getCompaniesByKeyword(bin.searchBarProject.text.toString())
                        }
                    }
                }
                return
            }

        } else if (isProjectActive && ApiConstants.advanceProjectUrl.isNotEmpty()) {
            AdncSearchOffset = 0
            runOnUiThread {
                println("onResume1")
                getProjectResultsFromFilterApi()

            }
            return
        } else if (!isProjectActive && ApiConstants.advanceCompanyUrl.isNotEmpty()) {
            AdncSearchOffset = 0
            runOnUiThread {
                println("onResume2")
                getCompanyResultsFromFilterApi()
            }

            return
        }

    }

    override fun onMakeSelection(pos: Int, name: String, flag: Boolean) {
        if (selectedRrcdTypeLst.contains(name)) selectedRrcdTypeLst.remove(name) else selectedRrcdTypeLst.add(
            name
        )
    }


}