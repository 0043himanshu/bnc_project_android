package com.bncairnetwork.ui.activities

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.Color
import android.graphics.Rect
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewTreeObserver.OnGlobalLayoutListener
import android.view.Window
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.cardview.widget.CardView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.GravityCompat
import androidx.core.view.isVisible
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.adapter.UserInvitationAdapter
import com.bncairnetwork.databinding.*
import com.bncairnetwork.helper.*
import com.bncairnetwork.helper.GPSPermissionHelper.startLocation
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProjectDetailsDataObj
import com.bncairnetwork.pojo.UserInvitationEmailPojo
import com.bncairnetwork.pojo.post.InvitationByEmailPost
import com.bncairnetwork.pojo.post.notification.PushNotificationPost
import com.bncairnetwork.pojo.response.*
import com.bncairnetwork.pojo.response.Dashboard.NotificationCountRes
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.ui.activities.auth.LoginAc
import com.bncairnetwork.ui.activities.homeConnections.*
import com.bncairnetwork.ui.activities.password.ChangePasswordAc
import com.bncairnetwork.ui.activities.projectConnections.ProjectNews
import com.bncairnetwork.ui.activities.reminder.ReminderAc
import com.bncairnetwork.ui.adapter.project.ProjectNewsHighlightsAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.shape.CornerFamily
import com.google.android.material.shape.MaterialShapeDrawable
import com.google.android.material.snackbar.Snackbar
import com.google.gson.Gson
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.concurrent.CompletableFuture

@RequiresApi(Build.VERSION_CODES.N)
class MainActivity : AppCompatActivity(), GPSPermissionHelper.onLocationListner,
    ProNewsNdHighlightListnr, UserEmailInvitationListnr {
    lateinit var bin: ActivityMainBinding
    lateinit var mInterface: ApiInterface
    lateinit var addCrditView: View
    private var packageID = 0
    var prolst = ArrayList<ProjectDetailsDataObj?>()
    lateinit var userEmailAdptr: UserInvitationAdapter
    private var userEmailLst = ArrayList<UserInvitationEmailPojo>()
    private var userEmailForInvitationLst = ArrayList<String>()
    private var selectedUserLst = ArrayList<UserInvitationEmailPojo>()
    lateinit var projectNdHighlightView: View
    lateinit var searchView: View
    lateinit var quickLinkView: View
    lateinit var projectNearMeView: View
    private var onResult =
        registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
            if (it.resultCode == AppCompatActivity.RESULT_OK) {
//                showLoader(requireActivity())
                GPSPermissionHelper.loadCurrentLoc()
            } else {
                Uitls.showToast(this, "Please turn on Gps")
            }
        }
    private var onPermissionLaucher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            var permission = true
            permissions.entries.forEach {
                if (!it.value) {
                    permission = it.value
                }
            }
            when (permission) {
                true -> {
                    startLocation(this, onResultLaucher = onResult, null, this)
                }
                else -> {
                    startLocation(
                        this@MainActivity,
                        onResult,
                        onPermissionLaucher = snackViewPermissionLaucher,
                        this@MainActivity
                    )
                }
            }
        }
    private var snackViewPermissionLaucher =
        registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
            var permission = true
            permissions.entries.forEach {
                if (!it.value) {
                    permission = it.value
                }
            }
            when (permission) {
                true -> {
                    startLocation(this, onResultLaucher = onResult, null, this)
                }
                else -> {
                    bin.root.snack(
                        getString(R.string.turn_on_gps),
                        Snackbar.LENGTH_LONG
                    ) { Uitls.showToast(this@MainActivity, "Please try again later") }
                }
            }
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityMainBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {
        PrefUtils.with(this).apply {
            println("---TOken--" + getString(Enums.Token.toString(), ""))
            packageID = getInt(Enums.USER_TYPE.toString(), 0)
            println("==========packageID======>$packageID")
        }
        addCrditView = LayoutInflater.from(this).inflate(R.layout.add_credit_btn_lay, null)
        quickLinkView = LayoutInflater.from(this).inflate(R.layout.quick_crd_home, null)
        projectNdHighlightView =
            LayoutInflater.from(this).inflate(R.layout.project_and_highligh, null)
        projectNearMeView = LayoutInflater.from(this).inflate(R.layout.project_crd_home, null)
        searchView = LayoutInflater.from(this).inflate(R.layout.search_crd_home, null)
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.homeMainView.homeCusAppBar.notiCountTxt.visibility = View.INVISIBLE
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            val view = UpdateLayBinding.inflate(LayoutInflater.from(this@MainActivity))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(view.root)
            view.defBtnLay.submitBtn.text = "Rate Now"
            view.defBtnLay.submitBtn.setOnClickListener {
                try {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                        )
                    )
                } catch (e: ActivityNotFoundException) {
                    startActivity(
                        Intent(
                            Intent.ACTION_VIEW,
                            Uri.parse("https://play.google.com/store/apps/details?id=$packageName")
                        )
                    )
                }
            }
            view.later.setOnClickListener {
                dismiss()
            }
            show()
        }
        siderNavigationBinding()
        bin.homeMainView.homeCusAppBar.userName.text =
            "Hi ${PrefUtils.with(this).getString(ApiConstants.username, "")}"
        UserRestrictionRepo.dashboardRestriction(bin, packageID)
        /**2. You can disable add button on dashboard for now as we are planning to add some more fields to it ---> Done
         * remove --->&& bin.homeMainView.extendedFaMabBottom.visibility==View.VISIBLE ---> for rollback*/
        if (packageID == ApiConstants.ibisID && bin.homeMainView.extendedFaMabBottom.visibility == View.VISIBLE) {
            bin.drawableLayout.viewTreeObserver.addOnGlobalLayoutListener(OnGlobalLayoutListener {
                val r = Rect()
                bin.drawableLayout.getWindowVisibleDisplayFrame(r)
                if (bin.drawableLayout.rootView.height - (r.bottom - r.top) > 500) {
                    //Log.d("keyboardStatus","opened");
                    bin.homeMainView.extendedFaMabBottom.visibility = View.GONE
                } else {
                    // Log.d("keyboardStatus","closed");
                    bin.homeMainView.extendedFaMabBottom.visibility = View.VISIBLE
                }
            })
        }


//        runOnUiThread {
//            showPopup()
//        }

//        runOnUiThread {
//            getMasterData()
//            getAppVrn()
//
//        }


    }


    fun siderNavigationBinding() {
        bin.navView1.inflateHeaderView(R.layout.navigationhead).apply {
            if (packageID == ApiConstants.professionalID) {
                findViewById<TextView>(R.id.mangeWidgt).visibility = View.GONE
            }
            PrefUtils.with(this@MainActivity).apply {
                findViewById<TextView>(R.id.userName).text = getString(ApiConstants.username, "")
                findViewById<TextView>(R.id.userEmail).text = getString(Enums.EMAIL.toString(), "")
                findViewById<TextView>(R.id.userDesignation).text =
                    getString(Enums.UserDesignation.toString(), "")
                findViewById<TextView>(R.id.userphone).text =
                    getString(Enums.UserPhone.toString(), "")
                findViewById<SwitchCompat>(R.id.pushNotiBtn).isChecked =
                    getString(Enums.isPushNotificationEnabled.toString(), "").equals("true")
            }

            findViewById<TextView>(R.id.buyCredit).setOnClickListener {
                BottomSheetDialog(this@MainActivity, R.style.CustomBottomSheetDialogTheme).apply {
                    val view_by_crd =
                        ActivityBuyCreditBinding.inflate(LayoutInflater.from(this@MainActivity))
                    setContentView(view_by_crd.root)
                    show()

                }
            }
            findViewById<TextView>(R.id.mangeWidgt).setOnClickListener {
                startActivity(Intent(this@MainActivity, ManageWidgetAc::class.java))
                bin.drawableLayout.closeDrawers()
            }
            findViewById<TextView>(R.id.mangeNoti).setOnClickListener {
                findViewById<SwitchCompat>(R.id.pushNotiBtn).visibility =
                    if (findViewById<SwitchCompat>(R.id.pushNotiBtn).isVisible) View.GONE else View.VISIBLE

            }

            findViewById<SwitchCompat>(R.id.pushNotiBtn).setOnClickListener {
                println(findViewById<SwitchCompat>(R.id.pushNotiBtn).isChecked)
                runOnUiThread {
                    updatePushNotification(
                        findViewById<SwitchCompat>(R.id.pushNotiBtn).isChecked,
                        this
                    )
                }
            }
            findViewById<TextView>(R.id.help).setOnClickListener {
                startActivity(Intent(this@MainActivity, HelpAc::class.java))
            }

            findViewById<TextView>(R.id.chngePwd).setOnClickListener {
                startActivity(Intent(this@MainActivity, ChangePasswordAc::class.java))
                bin.drawableLayout.closeDrawers()
            }
            findViewById<TextView>(R.id.logout).setOnClickListener {
                PrefUtils.with(this@MainActivity).apply {
                    logout(
                        username = getString(Enums.EMAIL.toString(), "").toString(),
                        password = getString(Enums.PASSWORD.toString(), "").toString()
                    )
                }
            }
        }
        val navViewBackground = bin.navView1.background as MaterialShapeDrawable
        navViewBackground.shapeAppearanceModel = navViewBackground.shapeAppearanceModel
            .toBuilder()
            .setTopRightCorner(CornerFamily.ROUNDED, 280f)
            .build()
    }

    fun listnr() {
        bin.homeMainView.homeCusAppBar.navMangeWidget.setOnClickListener {
            startActivity(Intent(this, ChatAc::class.java))
        }
        searchView.findViewById<EditText>(R.id.serchpro_bar)
            .setOnEditorActionListener { _, actionId, _ ->
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    if (searchView.findViewById<EditText>(R.id.serchpro_bar).text.isNotEmpty()) {
                        // Hide Keyboard
                        Uitls.hideKeyboard(this)
                        ApiConstants.FROM = Enums.HOME_SCREEN_AC.toString()
                        startActivity(
                            Intent(
                                this,
                                ProjectSearchAc::class.java
                            ).putExtra(
                                Enums.searchTxt.toString(),
                                searchView.findViewById<EditText>(R.id.serchpro_bar).text.toString()
                            ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                                .putExtra(Enums.FROM.toString(), Enums.HOME_SCREEN_AC.toString())
                        )
                        searchView.findViewById<EditText>(R.id.serchpro_bar).text = null
                    }
                }
                false

            }
        searchView.findViewById<ImageView>(R.id.home_search_img).setOnClickListener {
            if (searchView.findViewById<EditText>(R.id.serchpro_bar).text.isNotEmpty()) {
                ApiConstants.FROM = Enums.HOME_SCREEN_AC.toString()
                startActivity(
                    Intent(
                        this,
                        ProjectSearchAc::class.java
                    ).putExtra(
                        Enums.searchTxt.toString(),
                        searchView.findViewById<EditText>(R.id.serchpro_bar).text.toString()
                    ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                        .putExtra(Enums.FROM.toString(), Enums.HOME_SCREEN_AC.toString())
                )
            }

        }
        bin.homeMainView.homeCusAppBar.navNoti.setOnClickListener {
            startActivity(Intent(this, NotificationAc::class.java))
        }
        bin.homeMainView.reminderCrdHome.viewReminders.setOnClickListener {
            startActivity(Intent(this, ReminderAc::class.java))
        }
        searchView.findViewById<ImageView>(R.id.filterbtn).setOnClickListener {
            startActivity(
                Intent(this, AdvanceSearchAc::class.java).putExtra(
                    Enums.FROM.toString(),
                    Enums.HOME_SCREEN_AC.toString()
                )
            )
            ApiConstants.FROM = Enums.HOME_SCREEN_AC.toString()
        }
        bin.homeMainView.homeCusAppBar.navDwrBtn.setOnClickListener {
            bin.drawableLayout.openDrawer(GravityCompat.START)
        }

        bin.homeMainView.extendedFaMabBottom.setOnClickListener {
            BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme).apply {
                val view =
                    AddCompanyOrContractBottomSheetLayBinding.inflate(LayoutInflater.from(this@MainActivity))
                setContentView(view.root)
                show()
                view.addCompanyBtn.submitBtn.text = "Add Company"
                view.addContractBtn.submitBtn.text = "Add Contact"
                view.cancelTv.setOnClickListener {
                    dismiss()
                }
                view.addCompanyBtn.submitBtn.setOnClickListener {
                    dismiss()
                    startActivity(Intent(this@MainActivity, AddCompanyAc::class.java))
                }
                view.addContractBtn.submitBtn.setOnClickListener {
                    dismiss()
                    startActivity(Intent(this@MainActivity, AddContactAc::class.java))
                }
            }
        }
    }

    fun showPopup() {
        Uitls.showProgree(true, this)
        val call = mInterface.popup()
        call.enqueue(object : Callback<PopupRes> {
            override fun onResponse(call: Call<PopupRes>, response: Response<PopupRes>) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {

                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }

            }

            override fun onFailure(call: Call<PopupRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }

    fun getAppVrn() {
        Uitls.showProgree(true, this)
        val call = mInterface.getappVrsn()
        call.enqueue(object : Callback<AppVersionRes> {
            override fun onResponse(call: Call<AppVersionRes>, response: Response<AppVersionRes>) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    // need to do
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<AppVersionRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }

    fun sendInvitationCall() {
        Uitls.showProgree(true, this)
        val call =
            mInterface.sendInvitationByEmail(InvitationByEmailPost(emails = userEmailForInvitationLst))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(
                            this@MainActivity,
                            response.body()!!.message ?: "Invitation send"
                        )
                    } else {
                        Uitls.showToast(
                            this@MainActivity,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.handlerError(this@MainActivity, t)
                Uitls.showProgree(false, this@MainActivity)
            }
        })
    }

    fun getMasterData() {
        Uitls.showProgree(true, this)
        val call = mInterface.getMasterData()
        call.enqueue(object : Callback<GetMasterDataRes> {
            override fun onResponse(
                call: Call<GetMasterDataRes>,
                response: Response<GetMasterDataRes>
            ) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        PrefUtils.with(this@MainActivity)
                            .save(Enums.MASTERDATA.toString(), Gson().toJson(response.body()!!))
                    } else {
                        Uitls.showToast(this@MainActivity, "")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
                // getAppVrn()
                getNotiCount()
            }

            override fun onFailure(call: Call<GetMasterDataRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
                // getAppVrn()
                getNotiCount()
            }
        })
    }

    fun logout(username: String, password: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.logout(username, password)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        PrefUtils(this@MainActivity).clear()
                        startActivity(
                            Intent(
                                this@MainActivity,
                                LoginAc::class.java
                            ).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                        )

                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }

    fun updatePushNotification(flag: Boolean, con: View) {
        Uitls.showProgree(true, this)
        val call = mInterface.updatePushNotification(
            PushNotificationPost(
                mobile = true,
                activate = flag,
                notification = "all"
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status != "1") {
                        con.findViewById<SwitchCompat>(R.id.pushNotiBtn).isChecked = !flag
                        PrefUtils.with(this@MainActivity)
                            .save(Enums.isPushNotificationEnabled.toString(), (!flag).toString())
                        Uitls.showToast(
                            this@MainActivity,
                            "Not able to update notification. Please try after sometime"
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }

    fun getNotiCount() {
        Uitls.showProgree(true, this)
        val call = mInterface.getNotificationCount()
        call.enqueue(object : Callback<NotificationCountRes> {
            override fun onResponse(
                call: Call<NotificationCountRes>,
                response: Response<NotificationCountRes>
            ) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    runOnUiThread {
                        getDashboardData()
                    }
                    if (response.body()!!.status == "1" && response.body()!!.notifications?.equals("0") != true) {
                        bin.homeMainView.homeCusAppBar.notiCountTxt.visibility = View.VISIBLE
                        bin.homeMainView.homeCusAppBar.notiCountTxt.text =
                            response.body()!!.notifications ?: ""

                    } else {
                        bin.homeMainView.homeCusAppBar.notiCountTxt.visibility = View.INVISIBLE
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<NotificationCountRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }

    fun getInvitationCall() {
        Uitls.showProgree(true, this)
        val call = mInterface.getInvitationUrl()
        call.enqueue(object : Callback<InvitationUrlRes> {
            override fun onResponse(
                call: Call<InvitationUrlRes>,
                response: Response<InvitationUrlRes>
            ) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showSharingDialog(
                            response.body()!!.message ?: "",
                            response.body()!!.shareUrl ?: "",
                            this@MainActivity
                        )
                    } else {
                        Uitls.showToast(
                            this@MainActivity,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<InvitationUrlRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }

    fun getProjectNearMe(widgetID: String) {
        /* latitude = PrefUtils.with(this).getString(ApiConstants.currentLat, "") ?: "",
            longitude = PrefUtils.with(this).getString(ApiConstants.currentLong, "") ?: "",*/
        prolst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectNearMe(
            latitude = PrefUtils.with(this).getString(Enums.CurrentLat.toString(), "") ?: "",
            longitude = PrefUtils.with(this).getString(Enums.CurrentLong.toString(), "") ?: "",
            widgetID = widgetID
        )
        call.enqueue(object : Callback<ProjectNearMeRes> {
            override fun onResponse(
                call: Call<ProjectNearMeRes>,
                response: Response<ProjectNearMeRes>
            ) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.widgetData?.content?.get(0)?.data?.isNullOrEmpty() != true) {
                        try {
                            prolst.addAll(
                                response.body()!!.widgetData?.content?.get(0)?.data!! as? ArrayList<ProjectDetailsDataObj?>
                                    ?: arrayListOf()
                            )
                            if (response.body()!!.widgetData?.content?.get(0)?.data!!.size != 0) {
                                startActivity(
                                    Intent(
                                        this@MainActivity,
                                        MapViewAc::class.java
                                    ).putExtra(
                                        Enums.mMapViewProjectLst.toString(),
                                        Gson().toJson(prolst)
                                    )
                                )
                            }
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        Uitls.showToast(this@MainActivity, "No Project Found Near You!")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<ProjectNearMeRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }

    fun getDashboardData() {
        Uitls.showProgree(isShow = true, this)
        val call = mInterface.getDashboardWidgetData(
            latitude = PrefUtils.with(this).getString(Enums.CurrentLat.toString(), "") ?: "",
            longitude = PrefUtils.with(this).getString(Enums.CurrentLong.toString(), "") ?: "",
        )
        call.enqueue(object : Callback<DashboardWidgetDataRes> {
            override fun onResponse(
                call: Call<DashboardWidgetDataRes>,
                response: Response<DashboardWidgetDataRes>
            ) {
                Uitls.showProgree(false, this@MainActivity)
                if (response.body() != null && response.isSuccessful) {
                    bin.homeMainView.dashboardContainerLay.removeAllViews()
                    bin.homeMainView.dashboardContainerLay.addView(searchView)
                    if (response.body()!!.activeWidgetData?.isNullOrEmpty() != true) {
                        bin.homeMainView.noDataLayout.noDataLbl.visibility = View.INVISIBLE
                        response.body()!!.activeWidgetData?.forEach {
                            when (it?.widgetData?.title) {
                                ApiConstants.QuickLinks -> {
                                    bindQuickLinks(it.widgetData)
                                }
                                ApiConstants.ProjNearMe -> {
                                    bindProjectNearMe(it.widgetData, it.widgetId.toString())
                                }
                                ApiConstants.ShareErnCrdt -> {
                                    binEarnCrdit(it.widgetData)
                                }
                                ApiConstants.ProjNewsNdHiglight -> {
                                    bindProjNewsNdHighlight(it.widgetData)
                                }
//                                ApiConstants.FavWatchlist -> {
//                                    bindFavWatchList(it.widgetData)
//                                }
                            }
                        }
                    } else {
                        bin.homeMainView.noDataLayout.noDataLbl.text = "No active widgets"
                        bin.homeMainView.noDataLayout.noDataLbl.visibility = View.VISIBLE
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MainActivity)
                }
            }

            override fun onFailure(call: Call<DashboardWidgetDataRes>, t: Throwable) {
                Uitls.showProgree(false, this@MainActivity)
                Uitls.handlerError(this@MainActivity, t)
            }
        })
    }


    private fun bindProjNewsNdHighlight(data: WidgetData?) {
        //  bin.homeMainView.projectAndHighlighCrdHome.root.visibility = View.VISIBLE
        bin.homeMainView.dashboardContainerLay.addView(projectNdHighlightView)
        val view = projectNdHighlightView.findViewById<RecyclerView>(R.id.project_news_rv)
        projectNdHighlightView.findViewById<TextView>(R.id.project_news_highlght_viewmore_lbl)
            .setOnClickListener {
                startActivity(
                    Intent(
                        this@MainActivity,
                        ProjectNews::class.java
                    ).putExtra(Enums.TYPE.toString(), Enums.ViewAll.toString())
                )
            }
        ProjectNewsHighlightsAptr(this, data?.content?.get(0)?.data, this).apply {
            view.layoutManager =
                LinearLayoutManager(this@MainActivity, LinearLayoutManager.HORIZONTAL, false)
            view.adapter = this
        }
    }


    private fun binEarnCrdit(data: WidgetData?) {
//       bin.homeMainView.addCrdtBtn.visibility = View.VISIBLE
//      bin.homeMainView.projectClaimLay.visibility = View.VISIBLE
        bin.homeMainView.dashboardContainerLay.addView(addCrditView)
        addCrditView.findViewById<TextView>(R.id.projectClamCount).text =
            (data?.content?.get(0)?.claimedProjects ?: 0).toString()
        addCrditView.findViewById<ConstraintLayout>(R.id.proClaimCrd).setOnClickListener {
            if (data?.content?.get(0)?.claimedProjects != 0) {
                startActivity(
                    Intent(
                        this@MainActivity,
                        ProjectSearchAc::class.java
                    ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                        .putExtra(Enums.searchTxt.toString(), "")
                        .putExtra(Enums.FROM.toString(), Enums.HOME_SCREEN_AC.toString())
                        .putExtra(
                            Enums.URL.toString(),
                            data?.content?.get(0)?.projectListUrl ?: ""
                        )
                )
            }

        }
        PrefUtils.with(this).save(
            Enums.AvailableCredit.toString(),
            (data?.content?.get(0)?.creditsAvailable ?: 0).toString()
        )
        addCrditView.findViewById<TextView>(R.id.crdt_available_count).text =
            (data?.content?.get(0)?.creditsAvailable ?: 0).toString()
        addCrditView.findViewById<TextView>(R.id.newClamCount).text =
            (data?.content?.get(0)?.claimedNews ?: 0).toString()
        addCrditView.findViewById<ConstraintLayout>(R.id.newClaimCrd).setOnClickListener {
            if (data?.content?.get(0)?.creditsAvailable != 0) {

            }
            startActivity(
                Intent(
                    this,
                    ProjectNews::class.java
                ).putExtra(Enums.TYPE.toString(), Enums.URL.toString())
                    .putExtra(
                        Enums.URL.toString(),
                        data?.content?.get(0)?.newsListUrl ?: ""
                    )
            )
        }
        addCrditView.findViewById<TextView>(R.id.add_crd_txt).setOnClickListener {
            showEarnCreditDialog()
        }
    }

    private fun showEarnCreditDialog() {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val view_by_crd =
                ActivityAddCreditBinding.inflate(LayoutInflater.from(this@MainActivity))
            setContentView(view_by_crd.root)
            show()
            view_by_crd.addCrdNtRgtNwTxt.setOnClickListener {
                dismiss()
            }
            view_by_crd.addCrdtInviteLay.setOnClickListener {
                dismiss()
                if (userEmailLst.isNullOrEmpty()) {
                    Uitls.showProgree(true, this@MainActivity)
                    CompletableFuture.supplyAsync {
                        Uitls.getContactsLst(this@MainActivity)
                    }.thenAccept {
                        userEmailLst.addAll(it)
                        Uitls.showProgree(false, this@MainActivity)
                        runOnUiThread {
                            showContactLsDialog(it)
                        }
                    }
                } else {
                    showContactLsDialog(userEmailLst = userEmailLst)
                }
            }
            view_by_crd.addCrdtShareLay.setOnClickListener {
                dismiss()
                runOnUiThread {
                    getInvitationCall()
                }
            }
            view_by_crd.addCrdtBuyLay.setOnClickListener {
                dismiss()
            }
        }
    }

    private fun showContactLsDialog(userEmailLst: ArrayList<UserInvitationEmailPojo>) {
        selectedUserLst.addAll(userEmailLst)
        Dialog(this@MainActivity, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val view = InviteUserEmailDialogBinding.inflate(LayoutInflater.from(this@MainActivity))
            setCanceledOnTouchOutside(false)
            setContentView(view.root)
            view.selectAllBtn.submitBtn.text = getString(R.string.unselectAll)
            view.sendInvite.submitBtn.text = "Send Invite"
            view.selectAllBtn.submitBtn.background =
                ContextCompat.getDrawable(this@MainActivity, R.drawable.dark_grey_crd_bg)
            userEmailAdptr = UserInvitationAdapter(
                this@MainActivity,
                selectEmailsLst = selectedUserLst,
                lst = userEmailLst,
                this@MainActivity
            ).apply {
                view.rv.layoutManager = GridLayoutManager(this@MainActivity, 2)
                view.rv.adapter = this

            }
            view.cancel.setOnClickListener {
                dismiss()
            }
            view.sendInvite.submitBtn.setOnClickListener {
                if (userEmailForInvitationLst.isNotEmpty()) {
                    dismiss()
                    runOnUiThread {
                        sendInvitationCall()
                    }
                } else {
                    Uitls.showToast(this@MainActivity, "Please select email")
                }

            }
            view.selectAllBtn.submitBtn.setOnClickListener {
                if (view.selectAllBtn.submitBtn.text == getString(R.string.unselectAll)) {
                    selectedUserLst.clear()
                    view.selectAllBtn.submitBtn.text = getString(R.string.selectAll)
                } else {
                    selectedUserLst.addAll(userEmailLst)
                    view.selectAllBtn.submitBtn.text = getString(R.string.unselectAll)
                }
                userEmailAdptr.notifyDataSetChanged()
            }
            view.emailIDEdxt.afterTextChanged {
                userEmailAdptr.filter.filter(it)
            }
            show()
        }
    }

    private fun bindProjectNearMe(data: WidgetData?, widgetID: String?) {
        // bin.homeMainView.projectCrdHome.root.visibility = View.VISIBLE
        bin.homeMainView.dashboardContainerLay.addView(projectNearMeView)
        projectNearMeView.findViewById<ImageView>(R.id.imageView5).setOnClickListener {
            runOnUiThread {
                getProjectNearMe(widgetID ?: "0")
            }
//            startActivity(
//                Intent(
//                    this@MainActivity,
//                    ProjectNearMeAc::class.java
//                ).putExtra(Enums.DATA.toString(), widgetID ?: "0")
//            )
        }


    }

    private fun bindQuickLinks(data: WidgetData?) {
        bin.homeMainView.dashboardContainerLay.addView(quickLinkView)
        // bin.homeMainView.quickLinkCrdHome.root.visibility = View.VISIBLE
        data?.content?.forEach { content ->
            when (content?.title!!) {
                "My Projects" -> {
                    quickLinkView.findViewById<TextView>(R.id.myproject_count).text =
                        content.count.toString()
                    quickLinkView.findViewById<CardView>(R.id.crd1).setOnClickListener {
                        if (quickLinkView.findViewById<TextView>(R.id.myproject_count).text.toString() != "0" && packageID == ApiConstants.ibisID) {
                            startActivity(
                                Intent(
                                    this@MainActivity,
                                    ProjectSearchAc::class.java
                                ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                                    .putExtra(Enums.searchTxt.toString(), "")
                                    .putExtra(
                                        Enums.FROM.toString(),
                                        Enums.HOME_SCREEN_AC.toString()
                                    )
                                    .putExtra(
                                        Enums.URL.toString(),
                                        "/api/".plus(content.url ?: "")
                                    )
                            )
                        }
                    }

                }
                "My Companies" -> {
                    quickLinkView.findViewById<TextView>(R.id.mycompanies_count).text =
                        content.count.toString()
                    quickLinkView.findViewById<CardView>(R.id.crd2).setOnClickListener {
                        if (quickLinkView.findViewById<TextView>(R.id.mycompanies_count).text.toString() != "0" && packageID == ApiConstants.ibisID) {
                            startActivity(
                                Intent(
                                    this@MainActivity,
                                    ProjectSearchAc::class.java
                                ).putExtra(Enums.TYPE.toString(), ApiConstants.Companies)
                                    .putExtra(Enums.searchTxt.toString(), "")
                                    .putExtra(
                                        Enums.FROM.toString(),
                                        Enums.HOME_SCREEN_AC.toString()
                                    )
                                    .putExtra(
                                        Enums.URL.toString(),
                                        "/api/".plus(content.url ?: "")
                                    )
                            )
                        }
                    }
                }
                "Favourite Updates" -> {
                    quickLinkView.findViewById<TextView>(R.id.fav_update_count).text =
                        content.count.toString()
                    quickLinkView.findViewById<CardView>(R.id.crd3).setOnClickListener {
                        if (quickLinkView.findViewById<TextView>(R.id.fav_update_count).text.toString() != "0") {
                            startActivity(
                                Intent(
                                    this@MainActivity,
                                    ProjectSearchAc::class.java
                                ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                                    .putExtra(Enums.searchTxt.toString(), "")
                                    .putExtra(
                                        Enums.FROM.toString(),
                                        Enums.HOME_SCREEN_AC.toString()
                                    )
                                    .putExtra(
                                        Enums.URL.toString(),
                                        "/api/".plus(content.url ?: "")
                                    )
                            )
                        }
                    }
                }
                "Saved Searches" -> {
                    quickLinkView.findViewById<TextView>(R.id.savedSrch_count).text =
                        content.count.toString()
                    quickLinkView.findViewById<CardView>(R.id.crd4).setOnClickListener {
                        if (quickLinkView.findViewById<TextView>(R.id.savedSrch_count).text.toString() != "0") {
                            startActivity(
                                Intent(
                                    this@MainActivity,
                                    SavedSearchAc::class.java
                                ).putExtra(
                                    Enums.URL.toString(),
                                    "/api/".plus(content.url ?: "")
                                )
                            )
                        }
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        ApiConstants.advanceProjectUrl = ""
        ApiConstants.advanceCompanyUrl = ""
        startLocation(this, onResult, onPermissionLaucher, this)
        if (PrefUtils.with(this).getString(Enums.MASTERDATA.toString(), "")
                ?.isEmpty() == true
        ) {
            runOnUiThread {
                getMasterData()
            }
        } else {
            runOnUiThread {
                getNotiCount()
            }
        }
    }

    override fun onBackPressed() {
        this.finishAffinity()
    }

    override fun onLocation(location: Location?) {
        PrefUtils.with(this).apply {
            save(Enums.CurrentLong.toString(), location?.longitude.toString())
            save(Enums.CurrentLat.toString(), location?.latitude.toString())
        }


    }

    override fun onProNewsNdHighlightClk(pos: Int, obj: DataItem?) {
//        if (PrefUtils.with(this).getString(Enums.AvailableCredit.toString(), "0") != "0") {
//            startActivity(
//                Intent(this@MainActivity, NewsDetails::class.java).putExtra(
//                    Enums.DATA.toString(),
//                    Gson().toJson(obj)
//                ).putExtra(Enums.NewsRelatedProjectCount.toString(), "")
//            )
//        } else {
//            showEarnCreditDialog()
//        }
        startActivity(
            Intent(this@MainActivity, NewsDetails::class.java).putExtra(
                Enums.DATA.toString(),
                Gson().toJson(obj)
            ).putExtra(Enums.NewsRelatedProjectCount.toString(), "")
                .putExtra(Enums.ID.toString(), obj?.projectID)
        )

    }

    override fun onUserClick(pos: Int, obj: UserInvitationEmailPojo, ischecked: Boolean) {
        if (ischecked)
            userEmailForInvitationLst.add(obj.email ?: "")
        else userEmailForInvitationLst.remove(obj.email ?: "")
    }
}