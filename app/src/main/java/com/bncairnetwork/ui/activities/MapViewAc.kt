package com.bncairnetwork.ui.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Window
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityAddCreditBinding
import com.bncairnetwork.databinding.ActivityMapViewBinding
import com.bncairnetwork.databinding.MapViewBotomSheetKayBinding
import com.bncairnetwork.databinding.ProjectAlreadyClaimedPopupBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.*
import com.bncairnetwork.pojo.post.project.FavProPost
import com.bncairnetwork.pojo.response.InvitationUrlRes
import com.bncairnetwork.ui.activities.projectConnections.ProjectDetailsAc
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.adapter.map.ClusterItemAdapter
import com.bncairnetwork.ui.adapter.mapViewAptr.BottomProjectSheetAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.algo.GridBasedAlgorithm
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MapViewAc : AppCompatActivity(), OnMapReadyCallback,
    ClusterManager.OnClusterItemClickListener<ClusterItemAdapter?>, ProjectSeachListner,TagListnr {
    private var startForFilterResult =
        registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == Activity.RESULT_OK) {
                showProjectOnMap(it.data?.getStringExtra(Enums.mMapViewProjectLst.toString()))
            }
        }
    lateinit var bin: ActivityMapViewBinding
    private lateinit var mMap: GoogleMap
    private var packageID = 0
    var proObj: ProjectDetailsDataObj? = null
    lateinit var mInterfce: ApiInterface
    lateinit var bottomSheetDialog: BottomSheetDialog
    lateinit var bottomSheetAdptr: BottomProjectSheetAptr
    private var bottomSheetLst = ArrayList<ProjectDetailsDataObj>()
    private var isBottomSheetIsViewing = false
    private var clusterItemManager: ClusterManager<ClusterItemAdapter?>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityMapViewBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initviews()
        bin.icBack.setOnClickListener {
            onBackPressed()
        }
        bin.filterbtn.setOnClickListener {
            startForFilterResult.launch(
                Intent(this, FilterAc::class.java)
            )

        }

    }

    fun initviews() {
        mInterfce = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        val mapFrg = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFrg.getMapAsync(this)
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        this.mMap = googleMap
        clusterItemManager = ClusterManager(this, mMap)
        mMap.setOnCameraIdleListener(clusterItemManager)  /*clustering animation in and out attach here*/
        clusterItemManager?.setOnClusterItemClickListener(this)
        mMap.setInfoWindowAdapter(clusterItemManager?.markerManager)
        mMap.setOnInfoWindowClickListener(clusterItemManager)
        showProjectOnMap(intent.getStringExtra(Enums.mMapViewProjectLst.toString()))

    }

    private fun showProjectOnMap(data: String?) {
        data?.let { dataObj ->
            try {
                clusterItemManager?.clearItems()
                val latLngBounds = LatLngBounds.Builder()
                val mMapViewLst =
                    Gson().fromJson<ArrayList<ProjectDetailsDataObj>>(
                        dataObj,
                        object : TypeToken<ArrayList<ProjectDetailsDataObj>>() {}.type
                    )

                mMapViewLst.forEachIndexed { index, obj ->
                    if (obj.latlongstring?.isNotEmpty() == true) {
                        latLngBounds.include(
                            LatLng(
                                obj?.latlongstring?.substring(
                                    0,
                                    obj?.latlongstring?.indexOf(",") ?: 0
                                )?.toDouble()
                                    ?: 0.0,
                                obj?.latlongstring?.substring(
                                    startIndex = (obj?.latlongstring?.indexOf(",")?.plus(1)) ?: 0,
                                    endIndex = obj?.latlongstring?.length ?: 0
                                )?.toDouble() ?: 0.0
                            )
                        )

                        clusterItemManager?.addItem(ClusterItemAdapter(obj))
                        mMap?.moveCamera(CameraUpdateFactory.newLatLng(latLngBounds.build().center))
                        mMap?.animateCamera(
                            CameraUpdateFactory.newLatLngZoom(
                                latLngBounds.build().center,
                                12f
                            )
                        )
                    }
                }
                clusterItemManager?.cluster()
                clusterItemManager?.algorithm = GridBasedAlgorithm<ClusterItemAdapter>()
                clusterItemManager?.setOnClusterClickListener {
//                    bottomSheetDialog.show()
                    bottomSheetLst.clear()
                    it.items.forEach { obj ->
                        bottomSheetLst.add(obj?.data!!)
                    }
                    showBottomSheet(flag = true)
                    bottomSheetAdptr.notifyDataSetChanged()
                    true
                }

            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        if (intent.getStringExtra(Enums.mMapViewProjectLst.toString()) != null) {

        }
    }

    override fun onClusterItemClick(item: ClusterItemAdapter?): Boolean {
        println("=======onClusterClick==========>${item?.data?.name}")
        if (!isBottomSheetIsViewing) {
//            isBottomSheetIsViewing = true
            /*firstTimeClick*/
            bottomSheetLst.clear()
            bottomSheetLst.add(item?.data!!)
            showBottomSheet(flag = true)

        }
        return true
    }

    fun showBottomSheet(flag: Boolean) {
        BottomSheetDialog(this, R.style.CustomBottomSheetDialogTheme).apply {
            val view =
                MapViewBotomSheetKayBinding.inflate(LayoutInflater.from(this@MapViewAc))
            setContentView(view.root)
            bottomSheetAdptr = BottomProjectSheetAptr(
                this@MapViewAc,
                bottomSheetLst,
                this@MapViewAc, packageID = packageID,this@MapViewAc
            ).apply {
                view.rv.layoutManager = LinearLayoutManager(this@MapViewAc)
                view.rv.adapter = this
            }
            if (flag) {
                show()
            } else {
                dismiss()
            }

        }
    }

    override fun onPrjctSrchClk(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        proObj: ProjectDetailsDataObj?,
        comObj: CompaniesDataObj?, comkeywordObj: CompaniesItem?
    ) {
        if (isTxt) {
            this.proObj = proObj
            when (packageID) {
                ApiConstants.professionalID -> {
                    proObj?.authorised?.let { isAuthorised ->
                        if (isAuthorised) {
                            /*this project is in unlocked state*/
                            startActivity(
                                Intent(
                                    this@MapViewAc,
                                    ProjectDetailsAc::class.java
                                ).putExtra(Enums.ID.toString(), id)
                                    .putExtra(
                                        Enums.DATA.toString(),
                                        Gson().toJson(
                                            proObj
                                        )
                                    )
                            )
                        } else {
                            /*this project is in locked state*/
                            runOnUiThread {
                                getProjectInfo(id)
                            }
                        }
                    }
                }
                ApiConstants.ibisID -> {
                    startActivity(
                        Intent(
                            this@MapViewAc,
                            ProjectDetailsAc::class.java
                        ).putExtra(Enums.ID.toString(), id)
                            .putExtra(
                                Enums.DATA.toString(),
                                Gson().toJson(
                                    proObj
                                )
                            )
                    )
                }
                ApiConstants.bi_userID -> {
                    startActivity(
                        Intent(
                            this@MapViewAc,
                            ProjectDetailsAc::class.java
                        ).putExtra(Enums.ID.toString(), id)
                            .putExtra(
                                Enums.DATA.toString(),
                                Gson().toJson(
                                    proObj
                                )
                            )
                    )
                }
            }

            return
        }
        if (isTag) {
            showBottomSheet(flag = false)
            startActivity(
                Intent(this, Tag::class.java).putExtra(Enums.ID.toString(), id).putExtra(
                    Enums.TYPE.toString(),
                    ApiConstants.Project
                ).putExtra(
                    Enums.DATA.toString(),
                    Gson().toJson(proObj)
                )
            )
            finish()
            return
        }
        if (isAddInteraction) {
            showBottomSheet(flag = false)
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.ID.toString(),
                    id
                ).putExtra(
                    Enums.TYPE.toString(),
                    ApiConstants.Project
                ).putExtra(
                    Enums.ProjectOrCompanyName.toString(),
                    proObj?.name
                )
                    .putExtra(
                        Enums.ProjectOrCompanyCountry.toString(),
                        proObj?.country
                    )
                    .putExtra(
                        Enums.ProjectOrCompanyCity.toString(),
                        proObj?.city
                    )
            )
            finish()
            return

        }
        if (isReminder) {
            showBottomSheet(flag = false)
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(Enums.ID.toString(), id)
                    .putExtra(
                        Enums.TYPE.toString(),
                        ApiConstants.Project
                    )
            )
            finish()
            return
        }

        if (isFav) {
            runOnUiThread {
                if (bottomSheetLst[pos].tag_obj?.isFavourite != true) {
                    makeFavPro(id, pos)
                } else {
                    removeFavPro(id, pos)
                }
            }
            return
        }
    }

    fun getProjectInfo(projectID: String) {
        Uitls.showProgree(true, this)
        val call = mInterfce.getProjectInfo(projectID)
        call.enqueue(object : Callback<ProjectClaimInfoRes> {
            override fun onResponse(
                call: Call<ProjectClaimInfoRes>,
                response: Response<ProjectClaimInfoRes>
            ) {
                Uitls.showProgree(false, this@MapViewAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        showProjectClaimDialog(response.body()!!, projectID)
                    } else {
                        Uitls.showToast(
                            this@MapViewAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MapViewAc)
                }
            }

            override fun onFailure(call: Call<ProjectClaimInfoRes>, t: Throwable) {
                Uitls.showProgree(false, this@MapViewAc)
                Uitls.handlerError(this@MapViewAc, t)
            }
        })
    }

    fun showProjectClaimDialog(obj: ProjectClaimInfoRes, proID: String) {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            val view =
                ProjectAlreadyClaimedPopupBinding.inflate(LayoutInflater.from(this@MapViewAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(view.root)
            view.defBtnLay.submitBtn.text = "View Projects"
            view.rightNotNowLbl.setOnClickListener {
                dismiss()
            }
            view.ownerslblCount.text = obj.project?.owners.toString()
            view.bidderCount.text = obj.project?.bidders.toString()
            view.consultantCount.text = obj.project?.consultants.toString()
            view.contractorCount.text = obj.project?.contractors.toString()
            view.lbl.text = obj.project?.name ?: ""
            view.defBtnLay.submitBtn.setOnClickListener {
                dismiss()
                obj.credits?.let {
                    if (it >= 5) {
                        runOnUiThread {
                            exhaustCredits(proID = proID, proObj = proObj)
                        }
                    } else {
                        /*in sufficient credits*/
                        showInAppOption()
                    }
                }
            }
            show()
        }
    }

    fun showInAppOption() {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            val view_by_crd =
                ActivityAddCreditBinding.inflate(LayoutInflater.from(this@MapViewAc))
            setContentView(view_by_crd.root)
            show()
            view_by_crd.addCrdtInviteLay.setOnClickListener {
                dismiss()
            }
            view_by_crd.addCrdtShareLay.setOnClickListener {
                dismiss()
                runOnUiThread {
                    getInvitationCall()
                }
            }
            view_by_crd.addCrdtBuyLay.setOnClickListener {
                dismiss()
                /*start follow for in app*/
            }
        }
    }

    override fun onTagClick(name: String?) {

    }

    fun makeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call =
            mInterfce.makeFavForProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {

                Uitls.showProgree(false, this@MapViewAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        bottomSheetLst[pos].tag_obj?.isFavourite = true
                        bottomSheetAdptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@MapViewAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MapViewAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@MapViewAc)
                Uitls.handlerError(this@MapViewAc, t)
            }
        })
    }

    fun removeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call =
            mInterfce.removeFavFromProject(FavProPost(project_id = id, add_child_projects = false))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@MapViewAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        bottomSheetLst[pos].tag_obj?.isFavourite = false
                        bottomSheetAdptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@MapViewAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MapViewAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@MapViewAc)
                Uitls.handlerError(this@MapViewAc, t)
            }
        })
    }

    fun getInvitationCall() {
        Uitls.showProgree(true, this)
        val call = mInterfce.getInvitationUrl()
        call.enqueue(object : Callback<InvitationUrlRes> {
            override fun onResponse(
                call: Call<InvitationUrlRes>,
                response: Response<InvitationUrlRes>
            ) {
                Uitls.showProgree(false, this@MapViewAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showSharingDialog(
                            response.body()!!.message ?: "",
                            response.body()!!.shareUrl ?: "",
                            this@MapViewAc
                        )
                    } else {
                        Uitls.showToast(
                            this@MapViewAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MapViewAc)
                }
            }

            override fun onFailure(call: Call<InvitationUrlRes>, t: Throwable) {
                Uitls.showProgree(false, this@MapViewAc)
                Uitls.handlerError(this@MapViewAc, t)
            }
        })
    }

    fun exhaustCredits(proID: String, proObj: ProjectDetailsDataObj?) {
        Uitls.showProgree(true, this)
        val call = mInterfce.exhaustCredit(projectID = proID)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@MapViewAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        startActivity(
                            Intent(
                                this@MapViewAc,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), proID)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
                    } else {
                        Uitls.showToast(this@MapViewAc, "Not able to update project view.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@MapViewAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@MapViewAc)
                Uitls.handlerError(this@MapViewAc, t)
            }
        })
    }
}