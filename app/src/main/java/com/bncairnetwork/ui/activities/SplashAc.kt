package com.bncairnetwork.ui.activities

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowInsets
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivitySplashBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.ui.activities.auth.LoginAc

class SplashAc : AppCompatActivity() {
    lateinit var bin: ActivitySplashBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivitySplashBinding.inflate(layoutInflater)
        setContentView(bin.root)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            window.insetsController?.hide(WindowInsets.Type.statusBars())
        } else {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN
            )
        }
        // we used the postDelayed(Runnable, time) method
        // to send a message with a delayed time.
        Handler(
            Looper.getMainLooper()
        ).postDelayed({
            val islogin = PrefUtils.with(this).getString(Enums.isLogin.toString(), "false")
            if (islogin.equals("true", false)) {
                startActivity(Intent(this, MainActivity::class.java))
            } else {
                startActivity(Intent(this, LoginAc::class.java))
            }

            finish()
        }, 4000) // 5000 is the delayed time in milliseconds.

    }
}