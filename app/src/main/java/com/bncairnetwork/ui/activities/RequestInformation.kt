package com.bncairnetwork.ui.activities

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityRequestInformationBinding
import com.bncairnetwork.databinding.RqtInfoBottomSheetBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.ReqInfoPost
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RequestInformation : AppCompatActivity() {
    lateinit var bin: ActivityRequestInformationBinding
    lateinit var mInterface: ApiInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityRequestInformationBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()

    }

    fun initView() {
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.crntStsSp.visibility =
            if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) View.VISIBLE else View.GONE
    }

    fun listnr() {
        bin.rqtInfoSbmtBtn.setOnClickListener {
            if (bin.rqtInfoEdxt.text.toString().trim().isNotEmpty()) {
                if (bin.crntStsSp.visibility == View.VISIBLE && bin.crntStsSp.selectedItemPosition > 0) {
                    runOnUiThread {
                        makeReqInfoCall()
                    }
                } else if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) {
                    Uitls.showToast(this, "Please select status of project")
                } else {
                    runOnUiThread {
                        makeReqInfoCall()
                    }
                }

            } else {
                bin.rqtInfoEdxt.error = "Please add some request information"
            }

        }
        bin.cusAppbarLay.appbarHeader.text = "Request Information"
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
//        bin.rqstInfor.msg.setOnClickListener {
//            startActivity(Intent(this,ChatAc::class.java))
//        }
    }

    fun makeReqInfoCall() {
        Uitls.showProgree(true, this)
        val call = mInterface.createReqInfo(
            ReqInfoPost(
                information = bin.rqtInfoEdxt.text.toString(),
                content_type = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) "project" else "company",
                information_value = bin.rqtInfoEdxt.text.toString(),
                object_id = intent.getStringExtra(Enums.ID.toString()) ?: "",
                request_type = if (intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Project) bin.crntStsSp.selectedItem.toString() else null
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@RequestInformation)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Dialog(this@RequestInformation).apply {
                            val view =
                                RqtInfoBottomSheetBinding.inflate(LayoutInflater.from(this@RequestInformation))
                            requestWindowFeature(Window.FEATURE_NO_TITLE)
                            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                            setCanceledOnTouchOutside(true)
                            setContentView(view.root)
                            show()
                            view.rqtInfoBtmShtBtn.submitBtn.text = "Ok"
                            view.rqtInfoBtmShtBtn.submitBtn.setOnClickListener {
                                dismiss()
                                this@RequestInformation.onBackPressed()
                            }
                        }
                    } else {
                        Uitls.showToast(
                            this@RequestInformation,
                            response.body()!!.errors
                                ?: "not able to create your request at this movement"
                        )
                    }

                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@RequestInformation)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@RequestInformation)
                Uitls.handlerError(this@RequestInformation,t)
            }
        })
    }
}