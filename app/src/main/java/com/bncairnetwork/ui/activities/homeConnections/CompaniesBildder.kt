package com.bncairnetwork.ui.activities.homeConnections

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.adapter.BiddersAdapter
import com.bncairnetwork.databinding.ActivityCompaniesBildderBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.response.company.CompaniesItem
import com.bncairnetwork.pojo.response.company.GetCompanyForCompnyBidderRes
import com.bncairnetwork.pojo.response.compnyAndBidders.BiddersItem
import com.bncairnetwork.pojo.response.compnyAndBidders.GetBiddersForComnyBiddrRes
import com.bncairnetwork.ui.activities.CompanyDetailAc
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.adapter.CompanyKeyContactAdptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompaniesBildder : AppCompatActivity(), CompanyBiddrsListner,TagListnr {
    lateinit var bin: ActivityCompaniesBildderBinding
    lateinit var mInterface: ApiInterface
    var id = ""
    private var packageID = 0
    var isCompanyTabSelected = true
    private var companyResLst = ArrayList<CompaniesItem>()
    private var biddrResLst = ArrayList<BiddersItem?>()
    lateinit var adptr: CompanyKeyContactAdptr
    lateinit var bdr: BiddersAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityCompaniesBildderBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun initView() {

        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        bin.compBldrLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.compBldrLay.appbarHeader.text = "Companies & Bidders"
        bin.cmpnyBtn.submitBtn.text = "Companies"
        bin.bldrbtn.submitBtn.text = "Bidders"
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.bldrbtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.grey))
        bin.bldrbtn.submitBtn.backgroundTintList =
            ContextCompat.getColorStateList(this, R.color.light_grey)

        bin.cmpnyBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
        bin.cmpnyBtn.submitBtn.backgroundTintList =
            ContextCompat.getColorStateList(this, R.color.app_blue)
        if (intent.getStringExtra(Enums.ID.toString())?.isNotEmpty() == true) {
            id = intent.getStringExtra(Enums.ID.toString()).toString()
            //intent.getStringExtra(Enums.TYPE.toString()) == ApiConstants.Company
            if (isCompanyTabSelected) {
                chngeBtn(false)
            } else {
                chngeBtn(true)
            }

        }

    }

    fun listnr() {
        bin.cmpnyBtn.submitBtn.setOnClickListener {
            chngeBtn(false)
        }
        bin.bldrbtn.submitBtn.setOnClickListener {
            if (bin.bldrbtn.submitBtn.text != "Bidders (0)") {
                chngeBtn(true)
            }

        }

    }

    fun chngeBtn(isProjectSelect: Boolean) {
        when (isProjectSelect) {
            true -> {
                isCompanyTabSelected = false

                bin.bldrbtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.bldrbtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)

                bin.cmpnyBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.black))
                bin.cmpnyBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)
                companyResLst.clear()
                adptr = CompanyKeyContactAdptr(
                    this@CompaniesBildder,
                    comlst = companyResLst,
                    biddrlst = biddrResLst,
                    this@CompaniesBildder, packageID,this
                ).apply {
                    bin.cmpyBldrRcy.layoutManager =
                        LinearLayoutManager(this@CompaniesBildder)
                    bin.cmpyBldrRcy.adapter = this
                }
                runOnUiThread {
                    /*bidders are companies linked with particular project*/
                    getBiddersForComnyBidderscall(id)
                }

            }
            false -> {
                isCompanyTabSelected = true
                bin.bldrbtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.black))
                bin.bldrbtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)

                bin.cmpnyBtn.submitBtn.setTextColor(ContextCompat.getColor(this, R.color.white))
                bin.cmpnyBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)
                biddrResLst.clear()
                adptr = CompanyKeyContactAdptr(
                    this@CompaniesBildder,
                    comlst = companyResLst,
                    biddrlst = biddrResLst,
                    this@CompaniesBildder, packageID,this@CompaniesBildder
                ).apply {
                    bin.cmpyBldrRcy.layoutManager =
                        LinearLayoutManager(this@CompaniesBildder)
                    bin.cmpyBldrRcy.adapter = this
                }
                runOnUiThread {
                    getComnyForComnyBiddercall(id)
                }
            }
        }
    }

    fun getComnyForComnyBiddercall(id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getComniesForComnyBidders(id)
        call.enqueue(object : Callback<GetCompanyForCompnyBidderRes> {
            override fun onResponse(
                call: Call<GetCompanyForCompnyBidderRes>,
                response: Response<GetCompanyForCompnyBidderRes>
            ) {
                Uitls.showProgree(false, this@CompaniesBildder)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.textView12.text = response.body()!!.project?.name ?: ""
                        bin.textView13.text = response.body()!!.project?.city ?: ""
                        bin.textView14.text = response.body()!!.project?.country ?: ""
                        if (!response.body()!!.companies.isNullOrEmpty()) {
                            biddrResLst.clear()
                            bin.cmpnyBtn.submitBtn.text =
                                "Companies (${response.body()!!.companiesCount})"
                            bin.bldrbtn.submitBtn.text =
                                "Bidders (${response.body()!!.biddersCount})"
                            bin.noDataLayout.noDataLbl.visibility = View.GONE
                            bin.cmpyBldrRcy.visibility = View.VISIBLE
                            companyResLst.clear()
                            response.body()!!.companies?.let { companyResLst.addAll(it) }
                            adptr.notifyDataSetChanged()
                        } else {
                            bin.cmpyBldrRcy.visibility = View.INVISIBLE
                            bin.noDataLayout.noDataLbl.visibility = View.VISIBLE
                        }
                    } else {
                        bin.cmpyBldrRcy.visibility = View.INVISIBLE
                        bin.noDataLayout.noDataLbl.visibility = View.VISIBLE
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompaniesBildder)
                }
            }

            override fun onFailure(call: Call<GetCompanyForCompnyBidderRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompaniesBildder)
                Uitls.handlerError(this@CompaniesBildder, t)
            }
        })
    }

    fun getBiddersForComnyBidderscall(id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getBiddersForComnyBidders(id)
        call.enqueue(object : Callback<GetBiddersForComnyBiddrRes> {
            override fun onResponse(
                call: Call<GetBiddersForComnyBiddrRes>,
                response: Response<GetBiddersForComnyBiddrRes>
            ) {
                Uitls.showProgree(false, this@CompaniesBildder)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.textView12.text = response.body()!!.project?.name ?: ""
                        bin.textView13.text = response.body()!!.project?.city ?: ""
                        bin.textView14.text = response.body()!!.project?.country ?: ""
                        if (response.body()!!.bidders?.isNullOrEmpty() != true) {
                            companyResLst.clear()
                            bin.cmpyBldrRcy.visibility = View.VISIBLE
                            bin.noDataLayout.noDataLbl.visibility = View.GONE
                            biddrResLst.clear()
                            response.body()!!.bidders?.let { biddrResLst.addAll(it) }
                            adptr.notifyDataSetChanged()
                        } else {
                            bin.cmpyBldrRcy.visibility = View.INVISIBLE
                            bin.noDataLayout.noDataLbl.visibility = View.VISIBLE
                        }
                    } else {
                        bin.cmpyBldrRcy.visibility = View.INVISIBLE
                        bin.noDataLayout.noDataLbl.visibility = View.VISIBLE
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompaniesBildder)
                }
            }

            override fun onFailure(call: Call<GetBiddersForComnyBiddrRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompaniesBildder)
                Uitls.handlerError(this@CompaniesBildder, t)
            }
        })
    }


    fun makeFavCom(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call = mInterface.addFavForCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@CompaniesBildder)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        companyResLst[pos].tag_obj?.isFavourite = true
                        adptr.notifyItemChanged(pos)

                    } else {
                        Uitls.showToast(this@CompaniesBildder, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompaniesBildder)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {

                Uitls.showProgree(false, this@CompaniesBildder)
                Uitls.handlerError(this@CompaniesBildder, t)
            }
        })
    }

    fun removeFavCom(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call = mInterface.removeFavFromCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@CompaniesBildder)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        companyResLst[pos].tag_obj?.isFavourite = false
                        adptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(
                            this@CompaniesBildder,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompaniesBildder)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompaniesBildder)
                Uitls.handlerError(this@CompaniesBildder, t)
            }
        })
    }

    fun makeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call = mInterface.addFavForCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@CompaniesBildder)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        biddrResLst[pos]?.tag_obj?.isFavourite = true
                        adptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(this@CompaniesBildder, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompaniesBildder)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompaniesBildder)
                Uitls.handlerError(this@CompaniesBildder, t)
            }
        })
    }

    fun removeFavPro(id: String, pos: Int) {
        Uitls.showProgree(true, this)
        val call = mInterface.removeFavFromCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@CompaniesBildder)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        biddrResLst[pos]?.tag_obj?.isFavourite = false
                        adptr.notifyItemChanged(pos)
                    } else {
                        Uitls.showToast(
                            this@CompaniesBildder,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompaniesBildder)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompaniesBildder)
                Uitls.handlerError(this@CompaniesBildder, t)
            }
        })
    }

    override fun onTagClick(name: String?) {

    }

    override fun onClk(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        compnyObj: CompaniesItem?,
        biddrObj: BiddersItem?
    ) {
        if (isTxt) {
            startActivity(
                Intent(
                    this, CompanyDetailAc::class.java
                ).putExtra(Enums.ID.toString(), id)
                    .putExtra(Enums.FROM.toString(), ApiConstants.CompanyNdBidders)
                    .putExtra(
                        Enums.DATA.toString(),
                        Gson().toJson(if (!companyResLst.isNullOrEmpty()) compnyObj else biddrObj)
                    )
            )
            return
        }

        if (isTag) {
//            startActivity(
//                Intent(this, Tag::class.java).putExtra(Enums.ID.toString(), id).putExtra(
//                    Enums.TYPE.toString(),
//                    ApiConstants.Company
//                ).putExtra(Enums.FROM.toString(), ApiConstants.CompanyNdBidders)
//                    .putExtra(
//                        Enums.DATA.toString(),
//                        Gson().toJson(if (!companyResLst.isNullOrEmpty()) compnyObj else biddrObj)
//                    )
//            )
            return
        }

        if (isReminder) {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(Enums.ID.toString(), id)
                    .putExtra(
                        Enums.TYPE.toString(),
                        ApiConstants.Company
                    )
            )
            return
        }

        if (isFav) {
//            if (!companyResLst.isNullOrEmpty()) {
//                runOnUiThread {
//                    if (companyResLst[pos].tag_obj?.isFavourite != true) {
//                        makeFavCom(id, pos)
//                    } else {
//                        removeFavCom(id, pos)
//                    }
//                }
//
//            } else {
//                runOnUiThread {
//                    if (biddrResLst[pos]?.tag_obj?.isFavourite != true) {
//                        makeFavPro(id, pos)
//                    } else {
//                        removeFavPro(id, pos)
//                    }
//
//                }
//            }
            return
        }
        if (isAddInteraction) {
//            startActivity(
//                Intent(this, AddInteractionAc::class.java).putExtra(
//                    Enums.ID.toString(),
//                    id
//                ).putExtra(
//                    Enums.TYPE.toString(),
//                    ApiConstants.Company
//                ).putExtra(
//                    Enums.ProjectOrCompanyName.toString(),
//                    if (!companyResLst.isNullOrEmpty()) compnyObj?.name ?: "" else biddrObj?.name
//                        ?: ""
//                )
//                    .putExtra(
//                        Enums.ProjectOrCompanyCountry.toString(),
//                        if (!companyResLst.isNullOrEmpty()) compnyObj?.country else biddrObj?.country
//                            ?: ""
//                    )
//                    .putExtra(
//                        Enums.ProjectOrCompanyCity.toString(),
//                        if (!companyResLst.isNullOrEmpty()) compnyObj?.city else biddrObj?.city
//                            ?: ""
//                    )
//            )
            return
        }


    }
}