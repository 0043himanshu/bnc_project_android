package com.bncairnetwork.ui.activities

import android.R
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivitySignUpBinding
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.FreeAccountPost
import com.bncairnetwork.pojo.response.GetCompaniesForReqDemo
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SignUp : AppCompatActivity() {
    lateinit var bin: ActivitySignUpBinding
    var compnyID = ""
    var companiesLst = ArrayList<String>()
    var companiesHashMap = HashMap<String, String>()
    lateinit var mInterface: ApiInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivitySignUpBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initview()
        listnr()

    }

    fun initview() {
        mInterface = ServiceBuilder.buildService(ApiInterface::class.java, this)
    }

    fun listnr() {
        bin.Companyedxt.setOnTouchListener(View.OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= bin.Companyedxt.right - bin.Companyedxt.compoundDrawables[2].bounds.width()
                ) {
                    // your action here
                    if (bin.Companyedxt.text.isNotEmpty()) {
                        runOnUiThread {
                            getcompanies_or_projects()
                        }
                    }

                    return@OnTouchListener true
                }
            }
            false
        })
        bin.submitBtn.setOnClickListener {
            if (validation()) {
                runOnUiThread {
                    makefreeAcApi()
                }
                //  startActivity(Intent(this, OnboardingTwo::class.java))
            } else {
                Uitls.showToast(this, "Some fields are missing")
            }

        }
    }

    private fun validation(): Boolean {
        return bin.userNameedxt.text.trim().toString().isNotEmpty() &&
                bin.userBussinessEmaildxt.text.trim().toString().isNotEmpty() &&
                bin.designationedxt.text.trim().toString().isNotEmpty() &&
                bin.countryCodeEdxt.text.trim().toString().isNotEmpty() &&
                bin.areaCodeEdxt.text.trim().toString().isNotEmpty() &&
                bin.MobileNoEdxt.text.trim().toString().isNotEmpty() &&
                bin.Companyedxt.text.trim().toString().isNotEmpty()
    }

    fun getcompanies_or_projects() {
        companiesHashMap.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getCompaniesLst4ReqDemo(bin.Companyedxt.text.toString())
        call.enqueue(object : Callback<GetCompaniesForReqDemo> {
            override fun onResponse(
                call: Call<GetCompaniesForReqDemo>,
                response: Response<GetCompaniesForReqDemo>
            ) {
                Uitls.showProgree(false, this@SignUp)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.Companyedxt.text = null
                        response.body()!!.suggestions?.forEach {
                            companiesHashMap[it?.name ?: ""] = it?.id.toString()
                            companiesLst.add(it?.name ?: "")
                        }
                        ArrayAdapter(
                            this@SignUp,
                            R.layout.simple_list_item_1, companiesLst
                        ).apply {
                            bin.Companyedxt.setAdapter(this)
                            setNotifyOnChange(true)
                            bin.Companyedxt.showDropDown()
                        }
                    } else {
                        Uitls.showToast(this@SignUp, "No Companies found.")
                    }
                } else {
                    Uitls.showToast(this@SignUp, "Something went wrong")
                }
            }

            override fun onFailure(call: Call<GetCompaniesForReqDemo>, t: Throwable) {
                Uitls.showProgree(false, this@SignUp)
                Uitls.handlerError(this@SignUp, t)
            }
        })
    }

    fun makefreeAcApi() {
        Uitls.showProgree(true, this)
        val call = mInterface.getFreeAccount(
            FreeAccountPost(
                identifier = "register",
                sourcePage = "AIR App",
                sourceDomain = "BNC",
                areaCode = bin.areaCodeEdxt.text.toString(),
                demo = bin.DemoRd.isChecked,
                countryCode = bin.countryCodeEdxt.text.toString(),
                phone = bin.countryCodeEdxt.text.toString().plus(bin.areaCodeEdxt.text.toString())
                    .plus(bin.MobileNoEdxt.text.toString()),
                companyName = bin.Companyedxt.text.toString(),
                name = bin.userNameedxt.text.toString(),
                phoneNumber = bin.MobileNoEdxt.text.toString(),
                company = companiesHashMap[bin.Companyedxt.text.toString()]?.toInt(),
                designation = bin.designationedxt.text.toString(),
                email = bin.userBussinessEmaildxt.text.toString()
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@SignUp)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(this@SignUp, "Request for get free account successfully created")
                        onBackPressed()
                    }
                } else {
                    Uitls.showToast(this@SignUp, response.body()!!.errors?:"Not able to process your request at this movement")
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@SignUp)
                Uitls.handlerError(this@SignUp, t)
            }
        })
    }
}