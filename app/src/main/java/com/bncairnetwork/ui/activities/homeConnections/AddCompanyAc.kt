package com.bncairnetwork.ui.activities.homeConnections

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityAddCompanyBinding
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.helper.Uitls.Companion.getSubTyeFromCompanyType
import com.bncairnetwork.helper.Uitls.Companion.getcityByCountry
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.company.CreateCompanyPost
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddCompanyAc : AppCompatActivity() {
    var cityHashMap = HashMap<String, String>()
    var cityLst = ArrayList<String>()
    var cityAptr: ArrayAdapter<String>? = null
    lateinit var mInterface: ApiInterface
    var obj: GetMasterDataRes? = null
    var countryHashMap = HashMap<String, String>()
    var countryLst = ArrayList<String>()
    var companyTypessHashMap = HashMap<String, String>()
    var companyTypessLst = ArrayList<String>()
    var SubcompanyTypessHashMap = HashMap<String, String>()
    var SubcompanyTypessLst = ArrayList<String>()
    var subComTyeAptr: ArrayAdapter<String>? = null
    lateinit var bin: ActivityAddCompanyBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityAddCompanyBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initview()
        listnr()
        runOnUiThread {
            getMasterData()
        }
    }


    fun initview() {
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.cusAppbarLay.appbarHeader.text = "Add Company"
        bin.defBtnLay.submitBtn.text = "Create Company"
        cityAptr = ArrayAdapter(
            this,
            android.R.layout.simple_spinner_item,
            cityLst
        ).apply {
            setDropDownViewResource(R.layout.sp_row_item_lay)
            bin.citySp.adapter = this
        }
        subComTyeAptr = ArrayAdapter(
            this@AddCompanyAc,
            android.R.layout.simple_spinner_item,
            SubcompanyTypessLst
        ).apply {
            setDropDownViewResource(R.layout.sp_row_item_lay)
            bin.SubcmnyTypeSp.adapter = this
        }

    }

    fun listnr() {
        bin.defBtnLay.submitBtn.setOnClickListener {
            if (checkValidation()) {
                runOnUiThread {
                    addCompany()
                }
            } else {
                Uitls.showToast(this, "Some fields are still not filled.")
            }

        }

        bin.cmnyTypeSp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position > 0) {
                    SubcompanyTypessLst = getSubTyeFromCompanyType(
                        position,
                        SubcompanyTypessLst = SubcompanyTypessLst,
                        SubcompanyTypessHashMap = SubcompanyTypessHashMap,
                        obj!!
                    )
                    subComTyeAptr?.notifyDataSetChanged()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }

        bin.contrySp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (position > 0) {
                    cityLst.addAll(
                        getcityByCountry(
                            obj = obj!!,
                            countryID = countryHashMap[bin.contrySp.selectedItem.toString()].toString(),
                            cityLst = cityLst,
                            cityHashMap = cityHashMap
                        ).keys
                    )
                    cityAptr?.notifyDataSetChanged()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
    }

    fun checkValidation(): Boolean {
        var flag = false
        if (bin.companyName.text.toString().isNotEmpty()) {
            if (bin.contrySp.selectedItem.toString() != ApiConstants.Select) {
                if (bin.citySp.selectedItem.toString() != ApiConstants.Select) {
                    if (bin.cmnyTypeSp.selectedItem.toString() != ApiConstants.Select) {
                        if (bin.websitetxt.text.toString().isNotEmpty()) {
                            if (bin.emialAdrsEdxt.text.toString().isNotEmpty()) {
                                if (bin.countryCodeEdxt.text.toString().isNotEmpty()) {
                                    if (bin.areaCodeEdxt.text.toString().isNotEmpty()) {
                                        if (bin.MobileNoEdxt.text.toString().isNotEmpty()) {
                                            flag = true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return flag
    }

    fun addCompany() {
        Uitls.showProgree(true, this)
        val call = mInterface.createCompany(
            CreateCompanyPost(
                phone1 = bin.MobileNoEdxt.text.toString(),
                mobile = true,
                categories = if (SubcompanyTypessHashMap.isNotEmpty()) arrayListOf(
                    SubcompanyTypessHashMap[bin.SubcmnyTypeSp.selectedItem.toString()].toString()
                        .toInt()
                ) else arrayListOf(),
                city = cityHashMap[bin.citySp.selectedItem.toString()].toString().toInt(),
                country = countryHashMap[bin.contrySp.selectedItem.toString()].toString().toInt(),
                email = bin.emialAdrsEdxt.text.toString(),
                name = bin.companyName.text.toString(),
                phoneAreaCode1 = bin.areaCodeEdxt.text.toString(),
                phoneCountryCode1 = bin.countryCodeEdxt.text.toString()
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@AddCompanyAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(this@AddCompanyAc, "Company Created Successfully.")
                        onBackPressed()
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddCompanyAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddCompanyAc)
                Uitls.handlerError(this@AddCompanyAc, t)
            }
        })
    }


    fun getMasterData() {
        Uitls.showProgree(true, this)
        val call = mInterface.getMasterData()
        call.enqueue(object : Callback<GetMasterDataRes> {
            override fun onResponse(
                call: Call<GetMasterDataRes>,
                response: Response<GetMasterDataRes>
            ) {
                Uitls.showProgree(false, this@AddCompanyAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        obj = response.body()!!
                        dropdownBinding(response.body()!!)
                    } else {
                        Uitls.showToast(this@AddCompanyAc, "")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddCompanyAc)
                }
            }

            override fun onFailure(call: Call<GetMasterDataRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddCompanyAc)
                Uitls.handlerError(this@AddCompanyAc, t)
            }
        })
    }

    fun dropdownBinding(obj: GetMasterDataRes) {
        cityLst.add("select")
        cityAptr?.notifyDataSetChanged()
        SubcompanyTypessLst.add("select")
        subComTyeAptr?.notifyDataSetChanged()
        /***country binding*/
        countryLst.add("Select")
        obj.usersCountries?.forEach {
            countryHashMap[it?.name!!] = it.id.toString()
            countryLst.add(it.name)
        }
        ArrayAdapter(
            this@AddCompanyAc,
            android.R.layout.simple_spinner_item,
            countryLst
        ).apply {
            setDropDownViewResource(R.layout.sp_row_item_lay)
            bin.contrySp.adapter = this
        }

        /***all company types binding*/
        companyTypessLst.add("Select")
        obj.allCompanyTypes?.forEach {
            companyTypessHashMap[it?.id.toString()] = it?.type!!
            companyTypessLst.add(it.type)
        }
        ArrayAdapter(
            this@AddCompanyAc,
            android.R.layout.simple_spinner_item,
            companyTypessLst
        ).apply {
            setDropDownViewResource(R.layout.sp_row_item_lay)
            bin.cmnyTypeSp.adapter = this
        }


    }

}