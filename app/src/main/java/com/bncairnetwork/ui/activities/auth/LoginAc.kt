package com.bncairnetwork.ui.activities.auth

import android.content.Intent
import android.os.Bundle
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.URLSpan
import android.util.Log
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityLoginBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.LoginRes
import com.bncairnetwork.ui.activities.MainActivity
import com.bncairnetwork.ui.activities.RequestDemo
import com.bncairnetwork.ui.activities.SignUp
import com.bncairnetwork.ui.activities.password.ChangePasswordAc
import com.bncairnetwork.ui.activities.password.ForgotPwdAc
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.firebase.messaging.FirebaseMessaging
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class LoginAc : AppCompatActivity() {
    lateinit var bin: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(bin.root)
        bin.signUpTxtv.removeLinksUnderline()
        listnr()

        FirebaseMessaging.getInstance().token.addOnCompleteListener {task->
            if (!task.isSuccessful) {
               println("OnException---->${task.exception}")
                return@addOnCompleteListener
            }
            println("OnRegistrationToken---->${task.result}")
        }
    }


    fun listnr() {
        bin.loginBtn.setOnClickListener {
            if (bin.userEmailEdxt.text.isNotBlank()) {
                if (bin.userPwdedxt.text?.isNotBlank() == true) {
                    runOnUiThread {
                        loginCall()
                    }
                } else {
                    Uitls.showToast(this, "Invalid Password")
                }
            } else {
                Uitls.showToast(this, "Invalid Username")
            }
        }
        bin.signUpTxtv.setOnClickListener {
            startActivity(Intent(this, SignUp::class.java))
        }
        bin.forgetPwd.setOnClickListener {
            startActivity(Intent(this, ForgotPwdAc::class.java))
        }
        bin.demoBtn.setOnClickListener {
            startActivity(Intent(this, RequestDemo::class.java))
        }
    }

    fun TextView.removeLinksUnderline() {
        val spannable = SpannableString(text)
        for (urlSpan in spannable.getSpans(0, spannable.length, URLSpan::class.java)) {
            spannable.setSpan(object : URLSpan(urlSpan.url) {
                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                }
            }, spannable.getSpanStart(urlSpan), spannable.getSpanEnd(urlSpan), 0)
        }
        text = spannable
    }

    fun loginCall() {
        Uitls.showProgree(true, this)
        val req = ServiceBuilder.buildService(ApiInterface::class.java, this)
        val call = req.login(
            bin.userEmailEdxt.text.trim().toString(),
            bin.userPwdedxt.text?.trim().toString()
        )
        call.enqueue(object : Callback<LoginRes> {
            override fun onResponse(call: Call<LoginRes>, response: Response<LoginRes>) {
                Uitls.showProgree(false, this@LoginAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        PrefUtils.with(this@LoginAc).apply {
                            save(
                                Enums.isLogin.toString(),
                                if (response.body()!!.policy_accepted == true) "true" else "false"
                            )
                            save(Enums.Token.toString(), response.body()!!.token ?: "")
                            save(ApiConstants.username, response.body()!!.first_name ?: "")
                            save(Enums.EMAIL.toString(), response.body()!!.username ?: "")
                            save(
                                Enums.isPushNotificationEnabled.toString(),
                                response.body()!!.push_notitification ?: "false"
                            )
                            save(Enums.USER_TYPE.toString(), response.body()!!.packageID ?: 0)
                            save(
                                Enums.UserDesignation.toString(),
                                response.body()!!.designation ?: ""
                            )
                            save(Enums.UserPhone.toString(), response.body()!!.phone ?: "")

                            save(Enums.PASSWORD.toString(), bin.userPwdedxt.text.toString())
                            save(
                                Enums.USER_COMPANY_ID.toString(),
                                response.body()!!.company?.id ?: ""
                            )
                        }
                        startActivity(
                            Intent(
                                this@LoginAc,
                                if (response.body()!!.policy_accepted == false) ChangePasswordAc::class.java else MainActivity::class.java
                            ).putExtra(ApiConstants.isUserWithTempPssd, true)
                                .putExtra(ApiConstants.TempPwd, bin.userPwdedxt.text.toString())
                        )
                        finish()
                    } else {
                        response.body()!!.error?.let {
                            Uitls.showToast(this@LoginAc, it)
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@LoginAc)
                }
            }

            override fun onFailure(call: Call<LoginRes>, t: Throwable) {
                Uitls.showProgree(false, this@LoginAc)
                Uitls.handlerError(this@LoginAc, t)

            }
        })
    }
}
