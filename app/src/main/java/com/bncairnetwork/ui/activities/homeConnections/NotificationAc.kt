package com.bncairnetwork.ui.activities.homeConnections

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ActivityNotificationBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.NotificationLisntr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProjectDetailsDataObj
import com.bncairnetwork.pojo.response.notification.GetNotificationLstRes
import com.bncairnetwork.pojo.response.notification.NotificationsItem
import com.bncairnetwork.pojo.response.notification.ReminderNotificationDataItem
import com.bncairnetwork.ui.activities.CompanyDetailAc
import com.bncairnetwork.ui.activities.projectConnections.ProjectDetailsAc
import com.bncairnetwork.ui.adapter.notification.NotificationAptr
import com.bncairnetwork.ui.adapter.notification.ReminderNotificationAptr
import com.bncairnetwork.webservices.ApiInterface
import com.google.gson.Gson
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NotificationAc : AppCompatActivity(), NotificationLisntr {
    lateinit var bin: ActivityNotificationBinding
    lateinit var noti_adp: NotificationAptr
    lateinit var remin_noti_adp: ReminderNotificationAptr
    private var proObj: ProjectDetailsDataObj? = null
    lateinit var mInterface: ApiInterface
    var notlst: ArrayList<NotificationsItem?>? = null
    var reminderNotlst: ArrayList<ReminderNotificationDataItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityNotificationBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
        runOnUiThread {
            getNotificationLst()
        }
    }

    fun getNotificationLst() {
        Uitls.showProgree(true, this)
        val mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        val call = mInterface.getnotificationLst()
        call.enqueue(object : Callback<GetNotificationLstRes> {
            override fun onResponse(
                call: Call<GetNotificationLstRes>,
                response: Response<GetNotificationLstRes>
            ) {
                Uitls.showProgree(false, this@NotificationAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLayout.noDataLbl.visibility =
                        if (response.body()!!.status == 1) View.GONE else View.VISIBLE
                    if (response.body()!!.status == 1) {
                        response.body()!!.notifications?.let {
                            notlst?.addAll(it)
                            noti_adp = NotificationAptr(
                                this@NotificationAc,
                                notlst,
                                this@NotificationAc
                            ).apply {
                                bin.notificaionRv.layoutManager =
                                    LinearLayoutManager(this@NotificationAc)
                                bin.notificaionRv.adapter = this
                            }
                        }
                        response.body()!!.reminder_notifications?.let {
                            reminderNotlst?.addAll(it)
                            remin_noti_adp = ReminderNotificationAptr(
                                this@NotificationAc,
                                reminderNotlst, this@NotificationAc
                            ).apply {
                                bin.reminderNotificaionRv.layoutManager =
                                    LinearLayoutManager(this@NotificationAc)
                                bin.reminderNotificaionRv.adapter = this
                            }
                        }


                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@NotificationAc)
                }
            }

            override fun onFailure(call: Call<GetNotificationLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@NotificationAc)
                Uitls.handlerError(this@NotificationAc, t)
            }
        })
    }

    fun initView() {
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        notlst = ArrayList()
        reminderNotlst = ArrayList()
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.cusAppbarLay.appbarHeader.text = "Notifications"
        bin.defBtnLay.submitBtn.text = "Clear all"
        val itemTouchHelperCallback =
            object :
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {
                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    when (direction) {
                        ItemTouchHelper.LEFT -> {
                            runOnUiThread {
                                clearNotificationByID(
                                    notlst?.get(viewHolder.adapterPosition)?.id.toString(),
                                    viewHolder.adapterPosition,
                                    isReminderNoti = false
                                )
                            }
                        }
                    }
                }

            }

        val reminderNotificationItemTouchCallback =
            object :
                ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
                override fun onMove(
                    recyclerView: RecyclerView,
                    viewHolder: RecyclerView.ViewHolder,
                    target: RecyclerView.ViewHolder
                ): Boolean {

                    return false
                }

                override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                    when (direction) {
                        ItemTouchHelper.LEFT -> {
                            runOnUiThread {
                                clearNotificationByID(
                                    reminderNotlst?.get(viewHolder.adapterPosition)?.id,
                                    viewHolder.adapterPosition,
                                    isReminderNoti = true
                                )
                            }
                        }
                    }
                }

            }

        ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(bin.notificaionRv)
        ItemTouchHelper(reminderNotificationItemTouchCallback).attachToRecyclerView(bin.reminderNotificaionRv)
    }

    fun listnr() {
        bin.defBtnLay.submitBtn.setOnClickListener {
            if (notlst?.isNotEmpty() == true) {
                runOnUiThread {
                    clearAllNotification()
                }
            }
        }
    }

    fun clearNotificationByID(id: String?, pos: Int, isReminderNoti: Boolean) {
        Uitls.showProgree(true, this)
        val call = mInterface.clearNotificationByID(id ?: "0")
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@NotificationAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        when (isReminderNoti) {
                            true -> {
                                reminderNotlst?.removeAt(pos)
                                remin_noti_adp.notifyItemRemoved(pos)
                                remin_noti_adp.notifyItemRangeChanged(
                                    pos,
                                    reminderNotlst?.size ?: 0
                                )
                            }
                            false -> {
                                notlst?.removeAt(pos)
                                noti_adp.notifyItemRemoved(pos)
                                noti_adp.notifyItemRangeChanged(
                                    pos,
                                    notlst?.size ?: 0
                                )
                            }
                        }

                    }

                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@NotificationAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@NotificationAc)
                Uitls.handlerError(this@NotificationAc, t)
            }
        })
    }

    fun clearAllNotification() {
        Uitls.showProgree(true, this)
        val call = mInterface.clear_all_notification()
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@NotificationAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        notlst?.clear()
                        reminderNotlst?.clear()
                        remin_noti_adp.notifyDataSetChanged()
                        noti_adp.notifyDataSetChanged()
                        bin.noDataLayout.noDataLbl.visibility = View.VISIBLE
                        bin.noDataLayout.noDataLbl.text = "No Notification"
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@NotificationAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@NotificationAc)
                Uitls.handlerError(this@NotificationAc, t)
            }
        })
    }

    override fun onNotificationClick(
        pos: Int,
        nonReminderObj: NotificationsItem?,
        reminderNotificationObj: ReminderNotificationDataItem?
    ) {
        if (nonReminderObj != null) {
            proObj = ProjectDetailsDataObj(
                authorised = null,
                id = nonReminderObj.objectId.toString(),
                name = "",
                industry = "",
                updated = "",
                record_type = nonReminderObj.record_type,
                value = "",
                tag_obj = null,
                reminder = null,
                description = "",
                country = "",
                city = "",
                stage = "",
                views_left = "",
                image = "",
                identfierObj = null
            )

            startActivity(
                Intent(
                    this,
                    ProjectDetailsAc::class.java
                ).putExtra(Enums.ID.toString(), nonReminderObj.objectId.toString())
                    .putExtra(
                        Enums.DATA.toString(),
                        Gson().toJson(proObj)
                    )
            )
            return
        } else if (reminderNotificationObj != null) {
            proObj = ProjectDetailsDataObj(
                authorised = null,
                id = Uitls.extractInt(reminderNotificationObj.url?:""),
                name = "",
                industry = "",
                updated = "",
                record_type = reminderNotificationObj.record_type,
                value = "",
                tag_obj = null,
                reminder = null,
                description = "",
                country = "",
                city = "",
                stage = "",
                views_left = "",
                image = "",
                identfierObj = null
            )
            startActivity(
                Intent(
                    this,
                    if (reminderNotificationObj.project_name != null) ProjectDetailsAc::class.java else CompanyDetailAc::class.java
                ).putExtra(Enums.ID.toString(), Uitls.extractInt(reminderNotificationObj.url?:""))
                    .putExtra(
                        Enums.DATA.toString(),
                        Gson().toJson(proObj)
                    )
            )

            return
        }

    }


}