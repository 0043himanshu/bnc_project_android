package com.bncairnetwork.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityHelpBinding
import com.denzcoskun.imageslider.constants.ScaleTypes
import com.denzcoskun.imageslider.models.SlideModel

class HelpAc : AppCompatActivity() {
    lateinit var bin: ActivityHelpBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityHelpBinding.inflate(layoutInflater)
        setContentView(bin.root)
        val imageList = ArrayList<SlideModel>()
        imageList.add(
            SlideModel(
                R.drawable.dashboard_help,
                "", ScaleTypes.FIT
            )
        )
        imageList.add(
            SlideModel(
                R.drawable.project_search_help,
                "", ScaleTypes.FIT
            )
        )
        imageList.add(
            SlideModel(
                R.drawable.companies_search_help,
                "", ScaleTypes.FIT
            )
        )
        imageList.add(
            SlideModel(
                R.drawable.project_detail_1_help,
                "", ScaleTypes.FIT
            )
        )

        imageList.add(
            SlideModel(
                R.drawable.map_view_help,
                "", ScaleTypes.FIT
            )
        )


        imageList.add(
            SlideModel(
                R.drawable.schedules_help,
                "", ScaleTypes.FIT
            )
        )


        imageList.add(
            SlideModel(
                R.drawable.companies_img_help_6,
                "", ScaleTypes.FIT
            )
        )

        imageList.add(
            SlideModel(
                R.drawable.bidders_help,
                "", ScaleTypes.FIT
            )

        )

        imageList.add(
            SlideModel(
                R.drawable.updates_nd_highlights_help,
                "", ScaleTypes.FIT
            )
        )
        imageList.add(
            SlideModel(
                R.drawable.project_tree_help,
                "", ScaleTypes.FIT
            )
        )

        bin.imageSlider.setImageList(imageList)
        bin.skip.setOnClickListener {
            onBackPressed()
        }
    }
}