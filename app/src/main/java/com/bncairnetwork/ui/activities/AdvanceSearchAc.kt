package com.bncairnetwork.ui.activities

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.view.forEachIndexed
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityAdvanceSearchBinding
import com.bncairnetwork.databinding.MultiSelectionLayBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.AddCondition
import com.bncairnetwork.pojo.post.company.*
import com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem.CompaniesDataItemForProjectAdSearch
import com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem.DateRangeDataItem
import com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem.DatesDataItemForProjectAdSearch
import com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem.RolesDataItemProjectAdPost
import com.bncairnetwork.pojo.response.GetAllTagsRes
import com.bncairnetwork.pojo.response.GetCitiesByCountryRes
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.ui.activities.homeConnections.ProjectSearchAc
import com.bncairnetwork.ui.adapter.MultiSpinnerAdptr
import com.bncairnetwork.ui.adapter.SingleSpinnerAdptr
import com.bncairnetwork.ui.adapter.project_advance_search.AddConditionsUnderCompaniesSectionAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.nio.charset.Charset
import java.util.*
import java.util.concurrent.CompletableFuture
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class AdvanceSearchAc : AppCompatActivity(), AddConditionListnr, MultiSelectionListnr {
    lateinit var mInterface: ApiInterface
    var obj: GetMasterDataRes? = null
    var tagLst = ArrayList<String>()
    var recordTypeLst = ArrayList<String>()
    var selectedrecordTypeLst = ArrayList<String>()
    private var packageID = 0
    var rolesLst = ArrayList<String>()
    var selectedRolesLst = ArrayList<String>()
    lateinit var SubTypeSpAdptr: ArrayAdapter<String>
    var assigneesLst = HashMap<String, String>()
    var cityHashMap = HashMap<String, String>()
    var cityLst = ArrayList<String>()
    var selectedcountry = HashMap<String, String>()
    var selectedcity = HashMap<String, String>()
    var selectedtag = ArrayList<TagsItem>()
    var selectedComtypes = HashMap<String, String>()
    var selectedSubComtypes = HashMap<SubtypeItem, String>()
    var selectedAssignees = HashMap<String, String>()
    var selectedProSector = HashMap<String, String>()
    var selectedProIndustrytyes = HashMap<String, String>()
    var selectedProSubtyes = HashMap<String, String>() //under project sector 3rd sp
    var subTypesUnderDateSectionHashMap = HashMap<String, String>()
    var selectedSubTypeUnderDateSection = ArrayList<String>()
    var taglst_toview = ArrayList<String>()
    var tobepass = ArrayList<String>()
    var resType =
        0

    /***
     *0-> country, 1->city, 2->tag,3->company sub type,4-> CompanySubTypeSp,5->assignees,6->project stage
     *  7->projectRoles,8->ProjectIndustry,9->projectSubType,10->projectStage,11->recrdType */
    var proStagesHashMap = HashMap<String, String>()
    var countryHashMap = HashMap<String, String>()
    var proSectorsHashMap = HashMap<String, String>()
    var proIndustrytyesHashMap = HashMap<String, String>()
    var proSubtyesHashMap = HashMap<String, String>()
    var selectedproStages = HashMap<String, String>()
    var countryLst = ArrayList<String>()
    var companyTypessHashMap = HashMap<String, String>()
    var companyTypessLst = ArrayList<String>()
    var SubcompanyTypessHashMap = HashMap<String, String>()
    var SubcompanyTypessLst = ArrayList<String>()
    lateinit var bin: ActivityAdvanceSearchBinding
    var addConditionsForCompniesLst = ArrayList<AddCondition>()
    var addConditionsUnderDateSection =
        ArrayList<DatesDataItemForProjectAdSearch>() //under date section
    lateinit var conditnsForCompniesAdptr: AddConditionsUnderCompaniesSectionAptr
    lateinit var conditnsUnderDateSectionAdptr: SingleSpinnerAdptr
    lateinit var dialog: Dialog
    lateinit var dialogView: MultiSelectionLayBinding
    lateinit var mutliSpAptr: MultiSpinnerAdptr
    var companiesLst =
        ArrayList<CompaniesDataItemForProjectAdSearch>() //used under companies section
    var datesLst =
        ArrayList<DatesDataItemForProjectAdSearch>() //used under date section


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityAdvanceSearchBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()

    }

    fun initView() {
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        ApiConstants.advanceCompanyUrl = ""
        ApiConstants.advanceProjectUrl = ""
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        dialog = Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            dialogView =
                MultiSelectionLayBinding.inflate(LayoutInflater.from(this@AdvanceSearchAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(dialogView.root)
        }
        bin.cusAppbarLay.appbarHeader.text = "Advanced Search"
        bin.projectBtn.submitBtn.text = "Projects"
        bin.companiesBtn.submitBtn.text = "Companies"
        bin.resetBtn.submitBtn.text = "Reset"
        bin.resetBtn.submitBtn.backgroundTintList =
            ContextCompat.getColorStateList(this, R.color.lightBlack)
        bin.searchBtn.submitBtn.text = "Search"


    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun listnr() {
        bin.resetBtn.submitBtn.setOnClickListener {
            clearAllSelectionsAndData()

        }
        bin.advanceSearchLay.dateSearchLay.addNewBtn.setOnClickListener {
            if (bin.advanceSearchLay.dateSearchLay.addconditionRv.childCount != 0 && bin.advanceSearchLay.dateSearchLay.addconditionRv.getChildAt(
                    addConditionsUnderDateSection.size - 1
                ).findViewById<Spinner>(R.id.typeSp).selectedItemPosition > 0
            ) {
                addDemoConditionUnderDateSection()
            } else if (bin.advanceSearchLay.dateSearchLay.typeSp.selectedItemPosition > 0 && bin.advanceSearchLay.dateSearchLay.addconditionRv.childCount == 0
            ) {
                addDemoConditionUnderDateSection()
            } else {
                Uitls.showToast(
                    this@AdvanceSearchAc,
                    "Please select type before adding condition"
                )
            }

        }
        bin.advanceSearchLay.companySearchLay.addNewBtn.setOnClickListener(object :
            View.OnClickListener {
            override fun onClick(v: View?) {
                if (bin.advanceSearchLay.companySearchLay.addconditionRv.childCount != 0 && bin.advanceSearchLay.companySearchLay.addconditionRv.getChildAt(
                        addConditionsForCompniesLst.size - 1
                    ).findViewById<EditText>(R.id.comnyExdt).text.toString().isNotEmpty()
                ) {
                    addDemoConditionToCompnies()
                } else if (bin.advanceSearchLay.companySearchLay.comnyExdt.text.toString()
                        .trim()
                        .isNotEmpty() && bin.advanceSearchLay.companySearchLay.addconditionRv.childCount == 0
                ) {
                    addDemoConditionToCompnies()
                } else {
                    Uitls.showToast(
                        this@AdvanceSearchAc,
                        "Please add company before adding condition"
                    )
                }
            }
        })


        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.projectBtn.submitBtn.setOnClickListener {
            chngeBtn(true)
        }

        bin.companiesBtn.submitBtn.setOnClickListener {
            chngeBtn(false)
        }

        bin.searchBtn.submitBtn.setOnClickListener {
            runOnUiThread {
                //Uitls.showProgree(true, this)
                if (bin.companiesBtn.submitBtn.textColors == ColorStateList.valueOf(
                        ContextCompat.getColor(
                            this,
                            R.color.white
                        )
                    )
                ) {
                    CompletableFuture.supplyAsync { getEncodedTags(selectedtag) }
                        .thenAccept { encodeLst ->
                            makeAdvanceSearchCallForCompany(encodeLst)
                        }
                    return@runOnUiThread
                } else {
                    CompletableFuture.supplyAsync { getEncodedTags(selectedtag) }
                        .thenAccept { encodeLst ->
                            makeAdvanceSearchCallForProject(encodeLst)
                        }
                    return@runOnUiThread
                }


            }
        }
        /*****************company bindings**************/
        companyViewBindings()

        /**********project bindings**************/
        projectViewBindings()

    }


    fun projectViewBindings() {
        bin.advanceSearchLay.geographicalLocaSearchLay.countrySp.setOnClickListener {
            resType = 0
            countryLst.sort()
            showMultiSelctionDialog(dialogView, countryLst)
            dialogView.okbtn.setOnClickListener {
//                cityLst.clear()
//                selectedcountry.keys.forEach {
//                    cityLst.addAll(
//                        Uitls.getcityByCountry(
//                            obj = obj!!, countryID = countryHashMap[it] ?: "",
//                            cityLst = arrayListOf(), cityHashMap = hashMapOf()
//                        ).keys
//                    )
//                    cityHashMap.putAll(
//                        Uitls.getcityByCountry(
//                            obj = obj!!, countryID = countryHashMap[it] ?: "",
//                            cityLst = arrayListOf(), cityHashMap = hashMapOf()
//                        )
//                    )
//                }
//                println("FilteredCities: $cityLst")
                bin.advanceSearchLay.geographicalLocaSearchLay.countrySp.setText(
                    selectedcountry.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )

                dialog.dismiss()
            }
            dialog.show()
        }

        bin.advanceSearchLay.geographicalLocaSearchLay.citySp.setOnClickListener {
            if (selectedcountry.isNotEmpty()) {
                resType = 1
                runOnUiThread {
                    getCitiesByCountry()
                }
            } else {
                Uitls.showToast(this, resources.getString(R.string.pleaseSelectCountry))
            }
        }

        bin.advanceSearchLay.generalSearchLay.tagSp.setOnClickListener {
            resType = 2
            showMultiSelctionDialog(dialogView, if (tagLst.isNotEmpty()) tagLst else arrayListOf())
            dialogView.okbtn.setOnClickListener {
                taglst_toview.clear()
                selectedtag.forEach {
                    taglst_toview.add(it.id ?: "")
                }
                bin.advanceSearchLay.generalSearchLay.tagSp.setText(
                    taglst_toview.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
                dialog.dismiss()
            }
            dialog.show()
        }




        bin.advanceSearchLay.generalSearchLay.assigneesSp.setOnClickListener {
            resType = 5
            showMultiSelctionDialog(
                dialogView,
                if (assigneesLst.isNotEmpty()) assigneesLst.keys.toList() as ArrayList<String> else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                bin.advanceSearchLay.generalSearchLay.assigneesSp.setText(
                    selectedAssignees.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )

                dialog.dismiss()
            }
            dialog.show()
        }

        bin.advanceSearchLay.projectSearchLay.proSectorSp.setOnClickListener {
            resType = 6
            showMultiSelctionDialog(
                dialogView,
                if (proSectorsHashMap.isNotEmpty()) proSectorsHashMap.keys.toList() as ArrayList<String> else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                bin.advanceSearchLay.projectSearchLay.proSectorSp.setText(
                    selectedProSector.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
                obj?.types?.forEach {
                    if (selectedProSector.containsValue(it?.sector.toString())) {
                        proIndustrytyesHashMap[it?.name ?: ""] = it?.id.toString()
                    }
                }
                println(proIndustrytyesHashMap)
                print(selectedProSector)
                dialog.dismiss()
            }
            dialog.show()
        }

        bin.advanceSearchLay.projectSearchLay.proIndustrySp.setOnClickListener {
            if (selectedProSector.isNotEmpty()) {
                resType = 8
                showMultiSelctionDialog(
                    dialogView,
                    if (proIndustrytyesHashMap.isNotEmpty()) proIndustrytyesHashMap.keys.toList() as ArrayList<String> else arrayListOf()
                )
                dialogView.okbtn.setOnClickListener {
                    bin.advanceSearchLay.projectSearchLay.proIndustrySp.setText(
                        selectedProIndustrytyes.keys.toString().replace("[", "").replace(
                            "]", ""
                        )
                    )
                    println(selectedProIndustrytyes)
                    obj?.subtypes?.forEach {
                        if (selectedProIndustrytyes.containsValue(it?.projectType.toString())) {
                            proSubtyesHashMap[it?.name ?: ""] = it?.id.toString()
                        }
                    }
                    println(proSubtyesHashMap)
//                println(proIndustrytyesHashMap)
//                print(selectedProSector)
                    dialog.dismiss()
                }
                dialog.show()
            } else {
                Uitls.showToast(this, "Please select project sector first")

            }


        }


        bin.advanceSearchLay.projectSearchLay.proTypeSp.setOnClickListener {
            if (selectedProIndustrytyes.isNotEmpty()) {
                resType = 9
                val lst = ArrayList<String>()
                proSubtyesHashMap.keys.forEach {
                    lst.add(it)
                }
                showMultiSelctionDialog(
                    dialogView,
                    if (lst.isNotEmpty()) lst else arrayListOf()
                )
                dialogView.okbtn.setOnClickListener {
                    bin.advanceSearchLay.projectSearchLay.proTypeSp.setText(
                        selectedProSubtyes.keys.toString().replace("[", "").replace(
                            "]", ""
                        )
                    )
                    println(selectedProSubtyes)
                    dialog.dismiss()
                }
                dialog.show()
            } else {
                Uitls.showToast(this, "Please select project industry first")
            }


        }

        bin.advanceSearchLay.dateSearchLay.typeSp.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    bin.advanceSearchLay.dateSearchLay.SubtypeSp.isEnabled = position > 0
                    subTypesUnderDateSectionHashMap.clear()
                    selectedSubTypeUnderDateSection.clear()
                    selectedSubTypeUnderDateSection.add("Select subtype")
                    when (bin.advanceSearchLay.dateSearchLay.typeSp.selectedItem.toString()) {
                        ApiConstants.Schedule -> {
                            obj?.schedules?.forEach {
                                subTypesUnderDateSectionHashMap[it?.name ?: ""] = it?.id.toString()
                                selectedSubTypeUnderDateSection.add(it?.name ?: "")
                            }
                        }
                        ApiConstants.contract_awarded -> {
                            obj?.contractAwarded?.forEach {
                                subTypesUnderDateSectionHashMap[it?.name ?: ""] = it?.id.toString()
                                selectedSubTypeUnderDateSection.add(it?.name ?: "")
                            }
                        }
                        ApiConstants.stages_changed -> {

                            obj?.stagesChanged?.forEach {
                                subTypesUnderDateSectionHashMap[it?.name ?: ""] = it?.id.toString()
                                selectedSubTypeUnderDateSection.add(it?.name ?: "")
                            }
                        }
                    }

                    SubTypeSpAdptr.notifyDataSetChanged()
                    println(subTypesUnderDateSectionHashMap)

                }

                override fun onNothingSelected(parent: AdapterView<*>?) {

                }
            }

//
//        bin.advanceSearchLay.dateSearchLay.SubtypeSp.setOnTouchListener { _, _ ->
//            if (bin.advanceSearchLay.dateSearchLay.typeSp.selectedItemPosition == 0) {
//                Uitls.showToast(this@AdvanceSearchAc, "Please select type first")
//            }
//            true
//        }


        bin.advanceSearchLay.companySearchLay.roleSp.setOnClickListener {
            resType = 7
            showMultiSelctionDialog(
                dialogView,
                if (rolesLst.isNotEmpty()) rolesLst else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                bin.advanceSearchLay.companySearchLay.roleSp.setText(
                    selectedRolesLst.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
                dialog.dismiss()
            }
            dialog.show()
        }


        bin.advanceSearchLay.generalSearchLay.proStageSp.setOnClickListener {
            resType = 10
            showMultiSelctionDialog(
                dialogView,
                if (proStagesHashMap.isNotEmpty()) proStagesHashMap.keys.toList() as ArrayList<String> else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                bin.advanceSearchLay.generalSearchLay.proStageSp.setText(
                    selectedproStages.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
                dialog.dismiss()
            }
            dialog.show()
        }

        bin.advanceSearchLay.generalSearchLay.recordSp.setOnClickListener {
            resType = 11
            showMultiSelctionDialog(dialogView, recordTypeLst)
            dialogView.okbtn.setOnClickListener {
                bin.advanceSearchLay.generalSearchLay.recordSp.setText(
                    selectedrecordTypeLst.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
                dialog.dismiss()
            }
            dialog.show()
        }

        bin.advanceSearchCompanyLay.adSrchRcrdTypeSp.setOnClickListener {
            resType = 11
            showMultiSelctionDialog(dialogView, recordTypeLst)
            dialogView.okbtn.setOnClickListener {
                bin.advanceSearchCompanyLay.adSrchRcrdTypeSp.setText(
                    selectedrecordTypeLst.toString().replace("[", "").replace(
                        "]", ""
                    )
                )
                dialog.dismiss()
            }
            dialog.show()
        }



        bin.advanceSearchLay.generalSearchLay.createdfromEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchLay.generalSearchLay.createdfromEdxt
            )
        }
        bin.advanceSearchLay.generalSearchLay.createdtoEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchLay.generalSearchLay.createdtoEdxt
            )
        }
        bin.advanceSearchLay.generalSearchLay.updatefromEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchLay.generalSearchLay.updatefromEdxt
            )
        }
        bin.advanceSearchLay.generalSearchLay.updatetoEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchLay.generalSearchLay.updatetoEdxt
            )
        }

        bin.advanceSearchLay.generalArrowBtn.setOnClickListener {
            Uitls.hideUnhideView(
                bin.advanceSearchLay.generalSearchLay.root,
                bin.advanceSearchLay.generalArrowBtn
            )
        }

        bin.advanceSearchLay.companiesArrowBtn.setOnClickListener {
            Uitls.hideUnhideView(
                bin.advanceSearchLay.companySearchLay.root,
                bin.advanceSearchLay.companiesArrowBtn
            )
        }

        bin.advanceSearchLay.dateArrowBtn.setOnClickListener {
            Uitls.hideUnhideView(
                bin.advanceSearchLay.dateSearchLay.root,
                bin.advanceSearchLay.dateArrowBtn
            )
        }

        bin.advanceSearchLay.geoLocArrowBtn.setOnClickListener {
            Uitls.hideUnhideView(
                bin.advanceSearchLay.geographicalLocaSearchLay.root,
                bin.advanceSearchLay.geoLocArrowBtn
            )
        }
        bin.advanceSearchLay.gprojectArrowBtn.setOnClickListener {
            Uitls.hideUnhideView(
                bin.advanceSearchLay.projectSearchLay.root,
                bin.advanceSearchLay.gprojectArrowBtn
            )
        }


    }

    fun companyViewBindings() {
        bin.advanceSearchCompanyLay.contrySp.setOnClickListener {
            resType = 0
            countryLst.sort()
            showMultiSelctionDialog(
                dialogView,
                if (countryLst.isNotEmpty()) countryLst else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                cityLst.clear()
                selectedcountry.keys.forEach {
                    cityLst.addAll(
                        Uitls.getcityByCountry(
                            obj = obj!!, countryID = countryHashMap[it] ?: "",
                            cityLst = arrayListOf(), cityHashMap = hashMapOf()
                        ).keys
                    )
                    cityHashMap.putAll(
                        Uitls.getcityByCountry(
                            obj = obj!!, countryID = countryHashMap[it] ?: "",
                            cityLst = arrayListOf(), cityHashMap = hashMapOf()
                        )
                    )
                }
                println("FilteredCities: $cityLst")
                bin.advanceSearchCompanyLay.contrySp.setText(
                    selectedcountry.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )

                dialog.dismiss()
            }
            dialog.show()
        }
        bin.advanceSearchCompanyLay.citySp.setOnClickListener {
            if (selectedcountry.isNotEmpty()) {
                resType = 1
                runOnUiThread {
                    getCitiesByCountry()
                }
            } else {
                Uitls.showToast(this, resources.getString(R.string.pleaseSelectCountry))
            }
        }
        bin.advanceSearchCompanyLay.createdfromEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchCompanyLay.createdfromEdxt
            )
        }
        bin.advanceSearchCompanyLay.createdtoEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchCompanyLay.createdtoEdxt
            )
        }
        bin.advanceSearchCompanyLay.updatefromEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchCompanyLay.updatefromEdxt
            )
        }
        bin.advanceSearchCompanyLay.updatetoEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchCompanyLay.updatetoEdxt
            )
        }
        bin.advanceSearchLay.dateSearchLay.createdfromEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchLay.dateSearchLay.createdfromEdxt
            )
        }
        bin.advanceSearchLay.dateSearchLay.createdtoEdxt.setOnClickListener {
            Uitls.getDatePicker(
                this,
                supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                bin.advanceSearchLay.dateSearchLay.createdtoEdxt
            )
        }

        bin.advanceSearchCompanyLay.tagSp.setOnClickListener {
            resType = 2
            showMultiSelctionDialog(dialogView, if (tagLst.isNotEmpty()) tagLst else arrayListOf())
            dialogView.okbtn.setOnClickListener {
                taglst_toview.clear()
                selectedtag.forEach {
                    taglst_toview.add(it.id ?: "")
                }
                bin.advanceSearchCompanyLay.tagSp.setText(
                    taglst_toview.toString().replace("[", "").replace(
                        "]", ""
                    )
                )

                dialog.dismiss()
            }
            dialog.show()
        }

        bin.advanceSearchCompanyLay.adSrchCmpnyTypeSp.setOnClickListener {
            resType = 3
            showMultiSelctionDialog(
                dialogView,
                if (companyTypessLst.isNotEmpty()) companyTypessLst else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                SubcompanyTypessLst.clear()
                selectedComtypes.values.forEach {
                    SubcompanyTypessLst.addAll(
                        Uitls.getSubTyeFromCompanyType(
                            it.toInt() + 1,
                            SubcompanyTypessLst,
                            SubcompanyTypessHashMap,
                            obj!!
                        )
                    )
                }
                println("FilteredSubCompnyType: $SubcompanyTypessLst")
                bin.advanceSearchCompanyLay.adSrchCmpnyTypeSp.setText(
                    selectedComtypes.keys.toList().toString().replace("[", "").replace("]", "")
                )
                dialog.dismiss()
            }
            dialog.show()
        }

        bin.advanceSearchCompanyLay.adSrchCmpnySbTypeSp.setOnClickListener {
            if (selectedComtypes.isNotEmpty()) {
                resType = 4
                showMultiSelctionDialog(
                    dialogView,
                    if (SubcompanyTypessLst.isNotEmpty()) SubcompanyTypessLst else arrayListOf()
                )
                dialogView.okbtn.setOnClickListener {
                    bin.advanceSearchCompanyLay.adSrchCmpnySbTypeSp.setText(
                        selectedSubComtypes.values.toString().replace("[", "").replace(
                            "]", ""
                        )
                    )
                    dialog.dismiss()
                }
                dialog.show()
            } else {
                Uitls.showToast(this, "Please select company type first")
            }

        }

        bin.advanceSearchCompanyLay.adSrchAssgnSp.setOnClickListener {
            resType = 5
            showMultiSelctionDialog(
                dialogView,
                if (assigneesLst.isNotEmpty()) assigneesLst.keys.toList() as ArrayList<String> else arrayListOf()
            )
            dialogView.okbtn.setOnClickListener {
                bin.advanceSearchCompanyLay.adSrchAssgnSp.setText(
                    selectedAssignees.keys.toString().replace("[", "").replace(
                        "]", ""
                    )
                )

                dialog.dismiss()
            }
            dialog.show()
        }

    }

    fun showMultiSelctionDialog(
        view: MultiSelectionLayBinding,
        lst: ArrayList<String>
    ) {
        view.noDataLbl.noDataLbl.visibility = if (lst.isEmpty()) View.VISIBLE else View.GONE
        //0-> country, 1->city, 2->tag,3->company sub type,4-> CompanySubTypeSp,5->assignees
        tobepass.clear()

        when (resType) {
            0 -> {
                view.lbl.setText(R.string.select_country)
                tobepass.addAll(selectedcountry.keys)
            }
            1 -> {
                view.lbl.setText(R.string.select_city)
                tobepass.addAll(selectedcity.keys)
            }
            2 -> {
                view.lbl.setText(R.string.select_tags)
                selectedtag.forEach {
                    tobepass.add(it.id ?: "")
                }

            }
            3 -> {
                view.lbl.setText(R.string.select_type)
                tobepass.addAll(selectedComtypes.keys)
            }
            4 -> {
                view.lbl.setText(R.string.select_subtype)
                selectedSubComtypes.values.forEach {
                    tobepass.add(it)
                }

            }
            5 -> {
                view.lbl.setText(R.string.select_assignees)
                selectedAssignees.keys.forEach {
                    tobepass.add(it)
                }
            }
            6 -> {
                view.lbl.setText(R.string.select_sector)
                selectedProSector.keys.forEach {
                    tobepass.add(it)
                }
            }
            7 -> {
                view.lbl.setText(R.string.select_role)
                tobepass.addAll(selectedRolesLst)
            }
            8 -> {
                view.lbl.setText(R.string.select_industry)
                selectedProIndustrytyes.keys.forEach {
                    tobepass.add(it)
                }
            }

            9 -> {
                view.lbl.setText(R.string.select_subtype)
                selectedProSubtyes.keys.forEach {
                    tobepass.add(it)
                }
            }
            10 -> {
                view.lbl.setText(R.string.select_stage)
                selectedproStages.keys.forEach {
                    tobepass.add(it)
                }
            }
            11 -> {
                view.lbl.setText(R.string.select_record_type)
                tobepass.addAll(selectedrecordTypeLst)
            }
        }
        mutliSpAptr = MultiSpinnerAdptr(
            this@AdvanceSearchAc,
            lst,
            tobepass,
            this
        ).apply {
            view.multiRv.layoutManager = LinearLayoutManager(this@AdvanceSearchAc)
            view.multiRv.adapter = this
        }
    }


    fun chngeBtn(isProjectSelect: Boolean) {
        clearAllSelectionsAndData()
        resType = 0
        recordTypeLst.clear()
        bin.advanceSearchLay.projectViewRoot.visibility =
            if (isProjectSelect) View.VISIBLE else View.GONE
        bin.advanceSearchCompanyLay.mainAdvncSrchPrjctLay.visibility =
            if (!isProjectSelect) View.VISIBLE else View.GONE
        when (isProjectSelect) {
            true -> {
                recordTypeLst.addAll(
                    arrayListOf(
                        "BNC Projects",
                        "Linked Projects",
                        "Internal Projects"
                    )
                )
                bin.companiesBtn.submitBtn.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )
                bin.companiesBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)

                bin.projectBtn.submitBtn.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.white
                    )
                )
                bin.projectBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)
            }
            false -> {
                recordTypeLst.addAll(
                    arrayListOf(
                        "BNC Companies",
                        "Linked Companies",
                        "Internal Companies"
                    )
                )
                bin.companiesBtn.submitBtn.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.white
                    )
                )
                bin.companiesBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.app_blue)

                bin.projectBtn.submitBtn.setTextColor(
                    ContextCompat.getColor(
                        this,
                        R.color.black
                    )
                )
                bin.projectBtn.submitBtn.backgroundTintList =
                    ContextCompat.getColorStateList(this, R.color.light_grey)
            }
        }
    }

    fun gettags() {
        Uitls.showProgree(true, this)
        val call = mInterface.gettags()
        call.enqueue(object : Callback<GetAllTagsRes> {
            override fun onResponse(call: Call<GetAllTagsRes>, response: Response<GetAllTagsRes>) {
                Uitls.showProgree(false, this@AdvanceSearchAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        response.body()!!.tags?.forEach {
                            tagLst.add(it?.name ?: "")
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AdvanceSearchAc)
                }
            }

            override fun onFailure(call: Call<GetAllTagsRes>, t: Throwable) {
                Uitls.showProgree(false, this@AdvanceSearchAc)
                Uitls.handlerError(this@AdvanceSearchAc, t)
            }
        })
    }

    override fun onMakeSelection(pos: Int, name: String, flag: Boolean) {
        when (resType) {
            0 -> if (!selectedcountry.contains(name)) {
                selectedcountry[name] = countryHashMap[name].toString()
                //countryLst[pos].flag = flag
            } else {
                selectedcountry.remove(name)
                selectedcity.clear()
                cityHashMap.clear()
                bin.advanceSearchLay.geographicalLocaSearchLay.citySp.text = null
            }
            1 -> if (!selectedcity.contains(name)) {
                selectedcity[name] = cityHashMap[name].toString()
            } else {
                selectedcity.remove(name)
            }
            2 -> if (!selectedtag.contains(TagsItem(id = name))) {
                selectedtag.add(TagsItem(id = name))
            } else {
                selectedtag.remove(TagsItem(id = name))
            }
            3 -> if (!selectedComtypes.contains(name)) {
                selectedComtypes[name] = companyTypessHashMap[name].toString()
            } else {
                selectedComtypes.remove(name)
            }
            4 -> {
                if (!selectedSubComtypes.contains(SubtypeItem(id = SubcompanyTypessHashMap[name]?.toInt()))) {
                    selectedSubComtypes[SubtypeItem(id = SubcompanyTypessHashMap[name]?.toInt())] =
                        name
                } else {
                    selectedSubComtypes.remove(SubtypeItem(id = SubcompanyTypessHashMap[name]?.toInt()))
                }
            }
            5 -> {
                if (!selectedAssignees.contains(name)) {
                    selectedAssignees[name] = assigneesLst[name].toString()
                    println(selectedAssignees.toString())
                } else {
                    selectedAssignees.remove(name)
                }
            }
            6 -> {
                if (!selectedProSector.containsKey(name)) {
                    selectedProSector[name] = proSectorsHashMap[name].toString()
                } else {
                    selectedProSector.remove(name)
                }
                if (selectedProSector.isEmpty()) {
                    selectedProIndustrytyes.clear()
                    selectedProSubtyes.clear()
                    bin.advanceSearchLay.projectSearchLay.proIndustrySp.text = null
                    bin.advanceSearchLay.projectSearchLay.proTypeSp.text = null

                }
            }
            7 -> {
                if (!selectedRolesLst.contains(name)) selectedRolesLst.add(name) else selectedRolesLst.remove(
                    name
                )
            }
            8 -> {
                if (!selectedProIndustrytyes.containsKey(name)) {
                    selectedProIndustrytyes[name] = proIndustrytyesHashMap[name].toString()
                } else {
                    selectedProIndustrytyes.remove(name)
                }
            }
            9 -> {
                if (!selectedProSubtyes.containsKey(name)) {
                    selectedProSubtyes[name] = proSubtyesHashMap[name].toString()
                } else {
                    selectedProSubtyes.remove(name)
                }
            }
            10 -> {
                if (!selectedproStages.containsKey(name)) {
                    selectedproStages[name] = proStagesHashMap[name].toString()
                } else {

                    selectedproStages.remove(
                        name
                    )
                }
            }
            11 -> {
                if (!selectedrecordTypeLst.contains(name)) selectedrecordTypeLst.add(name) else selectedrecordTypeLst.remove(
                    name
                )
            }
        }

    }

    override fun onAddCondition(
        pos: Int,
        obj1: AddCondition?,
        obj2: DatesDataItemForProjectAdSearch?,
        isUnderCompanies: Boolean,
        isUnderDates: Boolean
    ) {
        if (isUnderCompanies) {
            addConditionsForCompniesLst.removeAt(pos)
            conditnsForCompniesAdptr.notifyItemRemoved(pos)
            bin.advanceSearchLay.companySearchLay.addconditionRv.removeViewAt(pos)
        } else {
            addConditionsUnderDateSection.removeAt(pos)
            conditnsUnderDateSectionAdptr.notifyItemRemoved(pos)
            bin.advanceSearchLay.dateSearchLay.addconditionRv.removeViewAt(pos)
        }


    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun makeAdvanceSearchCallForCompany(encodedtagLst: ArrayList<TagsItem>) {
        try {
            //filterProject_SectorIndustryType_param()
            val interactionDataItem = ArrayList<InteractDateTypesItem>()
            if (bin.advanceSearchCompanyLay.createdfromEdxt.text.isNotEmpty() || bin.advanceSearchCompanyLay.createdtoEdxt.text.isNotEmpty() || bin.advanceSearchCompanyLay.updatefromEdxt.text.isNotEmpty() || bin.advanceSearchCompanyLay.updatetoEdxt.text.isNotEmpty() || selectedAssignees.isNotEmpty()) {
                for (i in 0..2) {
                    when (i) {
                        0 -> {
                            if (selectedAssignees.isNotEmpty())
                                interactionDataItem.add(
                                    InteractDateTypesItem(
                                        date_range = null,
                                        record = ApiConstants.Company,
                                        action = "Assigned",
                                        users = selectedAssignees.values.toString()
                                    )
                                )
                        }
                        1 -> {
                            if (bin.advanceSearchCompanyLay.createdfromEdxt.text.toString()
                                    .isNotEmpty() || bin.advanceSearchCompanyLay.createdtoEdxt.text.toString()
                                    .isNotEmpty()
                            )
                                interactionDataItem.add(
                                    InteractDateTypesItem(
                                        date_range = DateRange(
                                            date_range = ApiConstants.nine,
                                            start_date = bin.advanceSearchCompanyLay.createdfromEdxt.text.toString(),
                                            end_date = bin.advanceSearchCompanyLay.createdtoEdxt.text.toString()
                                        ),
                                        record = ApiConstants.Company,
                                        action = ApiConstants.Created,
                                        users = null
                                    )
                                )
                        }
                        2 -> {
                            if (bin.advanceSearchCompanyLay.updatefromEdxt.text.toString()
                                    .isNotEmpty() || bin.advanceSearchCompanyLay.updatetoEdxt.text.toString()
                                    .isNotEmpty()
                            )
                                interactionDataItem.add(
                                    InteractDateTypesItem(
                                        date_range = DateRange(
                                            date_range = ApiConstants.nine,
                                            start_date = bin.advanceSearchCompanyLay.updatefromEdxt.text.toString(),
                                            end_date = bin.advanceSearchCompanyLay.updatetoEdxt.text.toString()
                                        ),
                                        record = ApiConstants.Company,
                                        action = ApiConstants.Updated,
                                        users = null
                                    )
                                )
                        }
                    }
                }
            }

            val call =
                mInterface.advanceSearchByCompany(
                    companyName = if (bin.advanceSearchCompanyLay.companyEdxt.text.trim()
                            .isNotEmpty()
                    ) bin.advanceSearchCompanyLay.companyEdxt.text.toString() else null,
                    companyCountry = if (selectedcountry.isNotEmpty()) arrayListOf(selectedcountry.values) as ArrayList<Int?> else null,
                    compnyCity = if (selectedcity.isNotEmpty()) arrayListOf(selectedcity.values) as ArrayList<Int?> else null,
                    tags = if (encodedtagLst.isNotEmpty()) "[" + encodedtagLst.toString()
                        .replace("TagsItem(", "").replace(")", "")
                        .replace("[", "{\"").replace("]", "\"}")
                        .replace(",", "\"},{\"").replace(":\"\"", "").replace("=", "\":\"")
                        .replace("\":\"\"", "=\"").replace("\" ", "\"")
                        .replace(":\"\"", "") + "]" else null,
                    compnyTypeFilter = if (selectedComtypes.isNotEmpty()) arrayListOf(
                        selectedComtypes.values
                    ) as ArrayList<Int?> else null,
                    subType = if (selectedSubComtypes.isNotEmpty()) selectedSubComtypes.keys.toString()
                        .replace("SubtypeItem", "")
                        .replace("(", "{\"")
                        .replace(")", "\"}").replace("=", "\":\"") else null,
                    interactDataTypes = if (interactionDataItem.isNotEmpty()) interactionDataItem.toString()
                        .replace("InteractDateTypesItem", "")
                        .replace("(", "{\"")
                        .replace("DateRange", "")
                        .replace("=", "\":\"")
                        .replace(")", "\"}").replace(",", "\",\"")
                        .replace("}\",", "},")
                        .replace("\"[", "[\"")
                        .replace("]\"", "\"]")
                        .replace(",\" {", ",{")
                        .replace(":\"{", ":{")
                        .replace(",\" ", ",\"").replace(",\"users\":\"null\"", "")
                        .replace(",\"end_date\":\"\"", "")
                        .replace(",\"start_date\":\"\"", "")
                        .replace("\"date_range\":\"null\",", "") else null,
                    recordTypeFilter = if (bin.advanceSearchCompanyLay.adSrchRcrdTypeSp.text.toString()
                            .isNotBlank()
                    ) arrayListOf(
                        bin.advanceSearchCompanyLay.adSrchRcrdTypeSp.text.toString()
                    ).toString()
                        .replace("[", "[\"").replace("]", "\"]") else null,
                    projectStage = if (bin.advanceSearchCompanyLay.companyWithActiveProCb.isChecked) "[10,20,30,40]" else null,
                    favComanies = if (bin.advanceSearchCompanyLay.favComCb.isChecked) true else null,
                    offsetval = "0"
                )
            println("--makeAdvanceSearchCallForCompany----->" + call.request().url)

            ApiConstants.advanceProjectUrl = ""
            ApiConstants.advanceCompanyUrl = call.request().url.toString()
            when (intent.getStringExtra(Enums.FROM.toString())) {
                Enums.PROJECT_SEARCH_AC.toString() -> {
                    setResult(
                        Activity.RESULT_OK,
                        Intent().putExtra(Enums.TYPE.toString(), ApiConstants.Company)
                            .putExtra(Enums.isFilterApplied.toString(), true)

                    )

                }
                Enums.HOME_SCREEN_AC.toString() -> {
                    startActivity(
                        Intent(
                            this@AdvanceSearchAc,
                            ProjectSearchAc::class.java
                        ).putExtra(Enums.TYPE.toString(), ApiConstants.Company)
                            .putExtra(Enums.isFilterApplied.toString(), true)

                    )
                }
            }
            this@AdvanceSearchAc.finish()
        } catch (e: Exception) {
            println("excep$e")
            Uitls.showProgree(false, this@AdvanceSearchAc)
            Uitls.onUnSuccessResponse(503, this@AdvanceSearchAc)
        }

    }

    fun getCitiesByCountry() {
        Uitls.showProgree(true, this)
        val call =
            mInterface.getCitiesByCountry(arryOfCountryIds = selectedcountry.values.toString())
        call.enqueue(object : Callback<GetCitiesByCountryRes> {
            @RequiresApi(Build.VERSION_CODES.N)
            override fun onResponse(
                call: Call<GetCitiesByCountryRes>,
                response: Response<GetCitiesByCountryRes>
            ) {
                Uitls.showProgree(false, this@AdvanceSearchAc)
                CompletableFuture.supplyAsync { Uitls.getCityMappingByCountry(response.body()!!.cities) }
                    .thenAccept {
                        cityHashMap.clear()
                        cityHashMap.putAll(it)
                        // Uitls.getCityLstByCountry(it)
                        showMultiSelctionDialog(
                            dialogView,
                            if (Uitls.getCityLstByCountry(it)
                                    .isNotEmpty()
                            ) Uitls.getCityLstByCountry(it) else arrayListOf()
                        )
                        dialogView.okbtn.setOnClickListener {
                            if (bin.companiesBtn.submitBtn.textColors == ColorStateList.valueOf(
                                    ContextCompat.getColor(
                                        this@AdvanceSearchAc,
                                        R.color.white
                                    )
                                )
                            ) {
                                bin.advanceSearchCompanyLay.citySp.setText(
                                    selectedcity.keys.toString().replace("[", "").replace(
                                        "]", ""
                                    )
                                )
                            } else {
                                bin.advanceSearchLay.geographicalLocaSearchLay.citySp.setText(
                                    selectedcity.keys.toString().replace("[", "").replace(
                                        "]", ""
                                    )
                                )
                            }
                            dialog.dismiss()
                        }

                    }

                dialog.show()


            }

            override fun onFailure(call: Call<GetCitiesByCountryRes>, t: Throwable) {
                Uitls.showProgree(false, this@AdvanceSearchAc)
                Uitls.handlerError(this@AdvanceSearchAc, t)
            }
        })
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun makeAdvanceSearchCallForProject(encodedtagLst: ArrayList<TagsItem>) {
        try {
            filterProject_SectorIndustryType_param()
            val roleLst = ArrayList<RolesDataItemProjectAdPost>()
            /***Roles under companies*/
            if (selectedRolesLst.isNotEmpty()) {
                selectedRolesLst.forEach {
                    roleLst.add(RolesDataItemProjectAdPost(identifier = it.replace(" ", "#")))
                }
            }
            /***binding companies list*/
            companiesLst.clear()
            if (bin.advanceSearchLay.companySearchLay.comnyExdt.text.toString()
                    .trim().isNotEmpty()
            ) {
                companiesLst.add(
                    CompaniesDataItemForProjectAdSearch(
                        name = if (bin.advanceSearchLay.companySearchLay.comnyExdt.text.toString()
                                .trim().isNotEmpty()
                        ) bin.advanceSearchLay.companySearchLay.comnyExdt.text.toString()
                            .trim() else null,
                        exact = if (bin.advanceSearchLay.companySearchLay.matchExactRd.isChecked) "true" else null,
                        bidders = if (bin.advanceSearchLay.companySearchLay.inclueBidderRd.isChecked) "true" else null,
                        contracts = if (bin.advanceSearchLay.companySearchLay.inclueCncledContrctRd.isChecked) "true" else null
                    )
                )
            }

            if (addConditionsForCompniesLst.isNotEmpty()) {
                addConditionsForCompniesLst.forEachIndexed { index, _ ->
                    companiesLst.add(
                        CompaniesDataItemForProjectAdSearch(
                            name = bin.advanceSearchLay.companySearchLay.addconditionRv.getChildAt(
                                index
                            )
                                .findViewById<EditText>(R.id.comnyExdt).text.toString(),
                            exact = bin.advanceSearchLay.companySearchLay.addconditionRv.getChildAt(
                                index
                            )
                                .findViewById<CheckBox>(R.id.match_exact_rd).isChecked.toString(),
                            bidders = bin.advanceSearchLay.companySearchLay.addconditionRv.getChildAt(
                                index
                            )
                                .findViewById<CheckBox>(R.id.inclue_bidder_rd).isChecked.toString(),
                            contracts = bin.advanceSearchLay.companySearchLay.addconditionRv.getChildAt(
                                index
                            )
                                .findViewById<CheckBox>(R.id.inclueCncledContrct_rd).isChecked.toString()
                        )
                    )
                }
            }
            /***binding dates list*/
            datesLst.clear()
            if (bin.advanceSearchLay.dateSearchLay.typeSp.selectedItemPosition > 0) {
                datesLst.add(
                    DatesDataItemForProjectAdSearch(
                        type = bin.advanceSearchLay.dateSearchLay.typeSp.selectedItem.toString(),
                        subtype = subTypesUnderDateSectionHashMap[bin.advanceSearchLay.dateSearchLay.SubtypeSp.selectedItem.toString()],
                        date_range = DateRangeDataItem(
                            date_range = "7",
                            start_date = if (bin.advanceSearchLay.dateSearchLay.createdfromEdxt.text.toString()
                                    .isNotEmpty()
                            ) bin.advanceSearchLay.dateSearchLay.createdfromEdxt.text.toString() else null,
                            end_date = if (bin.advanceSearchLay.dateSearchLay.createdtoEdxt.text.toString()
                                    .isNotEmpty()
                            ) bin.advanceSearchLay.dateSearchLay.createdtoEdxt.text.toString() else null
                        )
                    )
                )
            }

            if (addConditionsUnderDateSection.isNotEmpty()) {
                addConditionsUnderDateSection.forEachIndexed { index, _ ->
                    datesLst.add(
                        DatesDataItemForProjectAdSearch(
                            type = bin.advanceSearchLay.dateSearchLay.addconditionRv.getChildAt(
                                index
                            ).findViewById<Spinner>(R.id.typeSp).selectedItem.toString(),
                            subtype = Uitls.getSubTypeIDByTypeKey(
                                typeKey = bin.advanceSearchLay.dateSearchLay.addconditionRv.getChildAt(
                                    index
                                ).findViewById<Spinner>(R.id.typeSp).selectedItem.toString(),
                                obj = obj,
                                subtypeKey = bin.advanceSearchLay.dateSearchLay.addconditionRv.getChildAt(
                                    index
                                ).findViewById<Spinner>(R.id.SubtypeSp).selectedItem.toString()
                            ),
                            date_range = DateRangeDataItem(
                                date_range = "7",
                                start_date = bin.advanceSearchLay.dateSearchLay.addconditionRv.getChildAt(
                                    index
                                ).findViewById<EditText>(R.id.createdfromEdxt).text.trim()
                                    .toString(),
                                end_date = bin.advanceSearchLay.dateSearchLay.addconditionRv.getChildAt(
                                    index
                                ).findViewById<EditText>(R.id.createdtoEdxt).text.trim()
                                    .toString()
                            )
                        )
                    )
                }
            }

            val interactionDataItem = ArrayList<InteractDateTypesItem>()
            if (bin.advanceSearchLay.generalSearchLay.createdfromEdxt.text.isNotEmpty() || bin.advanceSearchLay.generalSearchLay.createdtoEdxt.text.isNotEmpty() || bin.advanceSearchLay.generalSearchLay.updatefromEdxt.text.isNotEmpty() || bin.advanceSearchLay.generalSearchLay.updatetoEdxt.text.isNotEmpty() || selectedAssignees.isNotEmpty()) {
                for (i in 0..2) {
                    when (i) {
                        0 -> {
                            if (selectedAssignees.isNotEmpty()) {
                                interactionDataItem.add(
                                    InteractDateTypesItem(
                                        date_range = null,
                                        record = ApiConstants.Project,
                                        action = "Assigned",
                                        users = selectedAssignees.values.toString()
                                    )
                                )
                            }

                        }
                        1 -> {
                            if (bin.advanceSearchLay.generalSearchLay.createdfromEdxt.text.toString()
                                    .isNotEmpty() || bin.advanceSearchLay.generalSearchLay.createdtoEdxt.text.toString()
                                    .isNotEmpty()
                            )
                                interactionDataItem.add(
                                    InteractDateTypesItem(
                                        date_range = DateRange(
                                            date_range = ApiConstants.nine,
                                            start_date = bin.advanceSearchLay.generalSearchLay.createdfromEdxt.text.toString(),
                                            end_date = bin.advanceSearchLay.generalSearchLay.createdtoEdxt.text.toString()
                                        ),
                                        record = ApiConstants.Project,
                                        action = ApiConstants.Created,
                                        users = null
                                    )
                                )
                        }
                        2 -> {
                            if (bin.advanceSearchLay.generalSearchLay.updatefromEdxt.text.toString()
                                    .isNotEmpty() || bin.advanceSearchLay.generalSearchLay.updatetoEdxt.text.toString()
                                    .isNotEmpty()
                            )
                                interactionDataItem.add(
                                    InteractDateTypesItem(
                                        date_range = DateRange(
                                            date_range = ApiConstants.nine,
                                            start_date = bin.advanceSearchLay.generalSearchLay.updatefromEdxt.text.toString(),
                                            end_date = bin.advanceSearchLay.generalSearchLay.updatetoEdxt.text.toString()
                                        ),
                                        record = ApiConstants.Project,
                                        action = ApiConstants.Updated,
                                        users = null
                                    )
                                )
                        }
                    }

                }
            }

            val call = mInterface.advanceSearchByProject(
                project_name = if (bin.advanceSearchLay.generalSearchLay.proNameEdxt.text.toString()
                        .trim().isNotEmpty()
                ) bin.advanceSearchLay.generalSearchLay.proNameEdxt.text.toString() else null,
                bnc_project_number = if (bin.advanceSearchLay.generalSearchLay.proNoEdxt.text.toString()
                        .trim().isNotEmpty()
                ) bin.advanceSearchLay.generalSearchLay.proNoEdxt.text.toString() else null,
                project_stage = if (selectedproStages.isNotEmpty()) selectedproStages.values.toList()
                    .toString() else null,
                value_from = if (bin.advanceSearchLay.generalSearchLay.EstimateValFromEdxt.text.toString()
                        .trim().isNotEmpty()
                ) bin.advanceSearchLay.generalSearchLay.EstimateValFromEdxt.text.toString() else null,
                value_to = if (bin.advanceSearchLay.generalSearchLay.EstimateValToEdxt.text.toString()
                        .trim().isNotEmpty()
                ) bin.advanceSearchLay.generalSearchLay.EstimateValToEdxt.text.toString() else null,
                tags = if (encodedtagLst.isNotEmpty()) "[" + encodedtagLst.toString()
                    .replace("TagsItem(", "").replace(")", "")
                    .replace("[", "{\"").replace("]", "\"}")
                    .replace(",", "\"},{\"").replace(":\"\"", "").replace("=", "\":\"")
                    .replace("\":\"\"", "=\"").replace("\" ", "\"")
                    .replace(":\"\"", "") + "]" else null,
                record_type_filter = if (selectedrecordTypeLst.isNotEmpty()) "[\"".plus(
                    bin.advanceSearchLay.generalSearchLay.recordSp.text.toString()
                        .replace(",", "\",\"")
                ).plus("\"]") else null,
                interactDataTypes = if (interactionDataItem.isNotEmpty()) interactionDataItem.toString()
                    .replace("InteractDateTypesItem", "")
                    .replace("(", "{\"")
                    .replace("DateRange", "")
                    .replace("=", "\":\"")
                    .replace(")", "\"}").replace(",", "\",\"")
                    .replace("}\",", "},")
                    .replace("\"[", "[\"")
                    .replace("]\"", "\"]")
                    .replace(",\" {", ",{")
                    .replace(":\"{", ":{")
                    .replace(",\" ", ",\"").replace(",\"users\":\"null\"", "")
                    .replace(",\"end_date\":\"\"", "")
                    .replace(",\"start_date\":\"\"", "")
                    .replace("\"date_range\": \"null\",", "")
                    .replace("\"date_range\":\"null\",", "") else null,
                location = if (bin.advanceSearchLay.geographicalLocaSearchLay.locedxt.text.toString()
                        .trim().isNotEmpty()
                ) bin.advanceSearchLay.geographicalLocaSearchLay.locedxt.text.toString() else null,
                country = if (selectedcountry.isNotEmpty()) selectedcountry.values.toString() else null,
                city = if (selectedcity.isNotEmpty()) selectedcity.values.toString() else null,
                sector = if (selectedProSector.isNotEmpty()) selectedProSector.values.toString() else null,
                new_project_type = if (selectedProIndustrytyes.isNotEmpty()) selectedProIndustrytyes.values.toList()
                    .toString() else null,
                project_subtype = if (selectedProSubtyes.isNotEmpty()) selectedProSubtyes.values.toList()
                    .toString() else null,
                companies = if (companiesLst.isNotEmpty()) companiesLst.toString()
                    .replace("CompaniesDataItemForProjectAdSearch(", "{\"")
                    .replace("=", "\":\"").replace(")", "\"}").replace(",", "\",\"")
                    .replace("}\",\" {", "},{") else null,
                date_types = if (datesLst.isNotEmpty()) datesLst.toString()
                    .replace("DatesDataItemForProjectAdSearch(", "{\"")
                    .replace("=", "\":\"")
                    .replace(",", "\",\"").replace("DateRangeDataItem(", "{\"")
                    .replace("))", "\"}}").replace(":\"{", ":{").replace(",\" {", ",{")
                    .replace("}\",{", "},{").replace(" ", "") else null,
                roles = if (roleLst.isNotEmpty()) roleLst.toString()
                    .replace("RolesDataItemProjectAdPost(", "{\"")
                    .replace("=", "\":\"").replace("), ", "\"},").replace(")]", "\"}]") else null
            )


            ApiConstants.advanceCompanyUrl = ""
            ApiConstants.advanceProjectUrl = call.request().url.toString()
            println("~~${call.request().url.toString()}~~~")
            when (intent.getStringExtra(Enums.FROM.toString())) {
                Enums.PROJECT_SEARCH_AC.toString() -> {
                    setResult(
                        Activity.RESULT_OK,
                        Intent().putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                            .putExtra(Enums.isFilterApplied.toString(), true)

                    )

                }
                Enums.HOME_SCREEN_AC.toString() -> {
                    startActivity(
                        Intent(
                            this@AdvanceSearchAc,
                            ProjectSearchAc::class.java
                        ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                            .putExtra(Enums.isFilterApplied.toString(), true)
                    )
                }
            }
            this@AdvanceSearchAc.finish()
        } catch (e: Exception) {
            Uitls.showProgree(false, this@AdvanceSearchAc)
            Uitls.onUnSuccessResponse(503, this@AdvanceSearchAc)
            println("Exception")
        }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun getEncodedTags(lst: ArrayList<TagsItem>): ArrayList<TagsItem> {
        val encodedLst = ArrayList<TagsItem>()
        lst.forEach {
            encodedLst.add(
                TagsItem(
                    id = Base64.getEncoder()
                        .encodeToString((it.id ?: "").toByteArray(Charset.defaultCharset()))
                )
            )
        }
        return encodedLst
    }

    /**this will remove value from parent if any its child is selected*/
    fun filterProject_SectorIndustryType_param() {
        obj?.searchTree?.forEach {
            if (selectedProSector.keys.contains(it?.name ?: "")) {
                it?.projectTypes?.forEach { industry ->
                    if (selectedProIndustrytyes.keys.contains(industry?.name ?: "")) {
                        selectedProSector.remove(
                            it.name
                                ?: ""
                        )
                    }
                    industry?.projectSubtypes?.forEach { type ->
                        if (selectedProSubtyes.keys.contains(type?.name ?: "")) {
                            selectedProIndustrytyes.remove(industry.name ?: "")
                        }
                    }
                }
            }
        }
        println("--projectSector-" + selectedProSector)
        println("--projectIndustry-" + selectedProIndustrytyes)
        println("--projectType-" + selectedProSubtyes)
    }

    fun loadProjectNdCompaniesDropDownsData() {
        runOnUiThread {
            if (countryHashMap.isEmpty()) {
                PrefUtils.with(this).apply {
                    obj = Gson().fromJson(
                        getString(Enums.MASTERDATA.toString(), ""),
                        GetMasterDataRes::class.java
                    )
                    obj?.usersCountries?.forEach {
                        countryHashMap[it?.name!!] = it.id.toString()
                        // countryLst.add(MultiSelectionPojo(name = it.name, flag = false))
                        countryLst.add(it.name ?: "")
                    }
                    obj?.allCompanyTypes?.forEach {
                        companyTypessHashMap[it?.type.toString()] = it?.id.toString()
                        companyTypessLst.add(it?.type.toString())
                    }
                    obj?.allCompanyUsers?.forEach {
                        assigneesLst[it?.name ?: ""] = it?.id ?: ""
                    }
                    obj?.sectors?.forEach {
                        proSectorsHashMap[it?.name ?: ""] = it?.id.toString()
                    }
                    obj?.stages?.forEach {
                        proStagesHashMap[it?.name ?: ""] = it?.id.toString()
                    }
                    obj?.companyRoles?.forEach {
                        rolesLst.add(it?.name ?: "")
                    }
                }

            }

        }


    }

    fun clearAllSelectionsAndData() {
        bin.advanceSearchLay.dateSearchLay.SubtypeSp.isEnabled = false
        addConditionsUnderDateSection.clear()
        addConditionsForCompniesLst.clear()
        selectedcountry.clear()
        selectedcity.clear()
        selectedrecordTypeLst.clear()
        selectedtag.clear()
        selectedComtypes.clear()
        selectedSubComtypes.clear()
        selectedAssignees.clear()
        selectedProSector.clear()
        conditnsUnderDateSectionAdptr.notifyDataSetChanged()
        conditnsForCompniesAdptr.notifyDataSetChanged()
        if (bin.companiesBtn.submitBtn.textColors == ColorStateList.valueOf(
                ContextCompat.getColor(
                    this,
                    R.color.white
                )
            )
        ) {
            bin.advanceSearchCompanyLay.companyRoot.forEachIndexed { index, view ->
                if (view is EditText) {
                    (bin.advanceSearchCompanyLay.companyRoot.getChildAt(index) as EditText).setText(
                        ""
                    )
                } else if (view is Spinner) {
                    (bin.advanceSearchCompanyLay.companyRoot.getChildAt(index) as Spinner).setSelection(
                        0
                    )
                } else if (view is CheckBox) {
                    (bin.advanceSearchCompanyLay.companyRoot.getChildAt(index) as CheckBox).isChecked =
                        false
                }
            }
        } else {
            resetAllProjectViews(
                arrayOf(
                    bin.advanceSearchLay.generalSearchLay.root,
                    bin.advanceSearchLay.geographicalLocaSearchLay.root,
                    bin.advanceSearchLay.companySearchLay.root,
                    bin.advanceSearchLay.projectSearchLay.root
                )
            )
        }
    }

    fun resetAllProjectViews(views: Array<ConstraintLayout>) {
        views.forEach {
            it.forEachIndexed { index, view ->
                if (view is EditText) {
                    (it.getChildAt(index) as EditText).setText(
                        ""
                    )
                } else if (view is Spinner) {
                    (it.getChildAt(index) as Spinner).setSelection(
                        0
                    )
                } else if (view is AppCompatCheckBox) {
                    (it.getChildAt(index) as CheckBox).isChecked =
                        false
                }
            }
        }
    }

    fun addDemoConditionToCompnies() {
        addConditionsForCompniesLst.add(
            AddCondition(
                companyName = "",
                isMatchExactPhase = false,
                isIncludeBidder = false,
                isIncludeCancellationContracts = false
            )
        )
        conditnsForCompniesAdptr.notifyItemInserted(addConditionsForCompniesLst.size)
    }

    fun addDemoConditionUnderDateSection() {
        addConditionsUnderDateSection.add(
            DatesDataItemForProjectAdSearch(
                type = "",
                subtype = "",
                date_range = DateRangeDataItem(
                    date_range = "",
                    start_date = "",
                    end_date = ""
                )
            )
        )
        conditnsUnderDateSectionAdptr.notifyItemInserted(addConditionsUnderDateSection.size)
    }


    override fun onResume() {
        super.onResume()
        loadProjectNdCompaniesDropDownsData()
        UserRestrictionRepo.advanceSearchRestriction(bin, packageID)
        ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            arrayListOf(
                "Select type",
                ApiConstants.Schedule,
                ApiConstants.contract_awarded,
                ApiConstants.stages_changed
            )
        ).apply {
            setDropDownViewResource(R.layout.sp_row_item_lay)
            bin.advanceSearchLay.dateSearchLay.typeSp.adapter = this
        }

        SubTypeSpAdptr = ArrayAdapter(
            this,
            android.R.layout.simple_list_item_1,
            selectedSubTypeUnderDateSection
        ).apply {
            setDropDownViewResource(R.layout.sp_row_item_lay)
            bin.advanceSearchLay.dateSearchLay.SubtypeSp.adapter = this
        }

        conditnsForCompniesAdptr =
            AddConditionsUnderCompaniesSectionAptr(this, addConditionsForCompniesLst, this).apply {
                bin.advanceSearchLay.companySearchLay.addconditionRv.layoutManager =
                    LinearLayoutManager(this@AdvanceSearchAc, LinearLayoutManager.VERTICAL, false)
                bin.advanceSearchLay.companySearchLay.addconditionRv.adapter = this
            }

        conditnsUnderDateSectionAdptr =
            SingleSpinnerAdptr(
                this,
                addConditionsUnderDateSection,
                masterdataobj = obj,
                this
            ).apply {
                bin.advanceSearchLay.dateSearchLay.addconditionRv.layoutManager =
                    LinearLayoutManager(this@AdvanceSearchAc, LinearLayoutManager.VERTICAL, false)
                bin.advanceSearchLay.dateSearchLay.addconditionRv.adapter = this
            }


        runOnUiThread { gettags() }
        chngeBtn(true)

    }

}