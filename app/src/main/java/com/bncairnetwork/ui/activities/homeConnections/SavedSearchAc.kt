package com.bncairnetwork.ui.activities.homeConnections

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.adapter.SavedSearchAdapter
import com.bncairnetwork.databinding.ActivitySavedSearchBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.SavedSearchListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.Dashboard.SavedSearchRes
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SavedSearchAc : AppCompatActivity(), SavedSearchListnr {
    lateinit var bin: ActivitySavedSearchBinding
    lateinit var mInterface: ApiInterface
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivitySavedSearchBinding.inflate(layoutInflater)
        setContentView(bin.root)
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.cusAppbarLay.appbarHeader.text = "Saved Search"
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
    }

    override fun onResume() {
        super.onResume()
        runOnUiThread {
            Uitls.showProgree(true, this)
            val call =
                mInterface.getSavedSearchResults(url = intent.getStringExtra(Enums.URL.toString()))
            call.enqueue(object : Callback<SavedSearchRes> {
                override fun onResponse(
                    call: Call<SavedSearchRes>,
                    response: Response<SavedSearchRes>
                ) {
                    Uitls.showProgree(false, this@SavedSearchAc)
                    if (response.body() != null && response.isSuccessful) {
                        if (response.body()!!.status == 1) {
                            bin.noDataLbl.noDataLbl.visibility = View.GONE
                            SavedSearchAdapter(
                                this@SavedSearchAc,
                                response.body()!!.widgetData?.content, this@SavedSearchAc
                            ).apply {
                                bin.rv.layoutManager = LinearLayoutManager(this@SavedSearchAc)
                                bin.rv.adapter = this
                            }
                        } else {
                            bin.noDataLbl.noDataLbl.visibility = View.VISIBLE
                        }
                    } else {
                        Uitls.onUnSuccessResponse(response.code(), this@SavedSearchAc)
                    }
                }

                override fun onFailure(call: Call<SavedSearchRes>, t: Throwable) {
                    Uitls.showProgree(false, this@SavedSearchAc)
                    Uitls.handlerError(this@SavedSearchAc, t)
                }
            })
        }
    }

    override fun onSaveSearchColumnClk(
        pos: Int,
        isAddedClick: Boolean,
        isUpdatedClick: Boolean,
        url: String?
    ) {
        startActivity(
            Intent(
                this@SavedSearchAc,
                ProjectSearchAc::class.java
            ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                .putExtra(Enums.searchTxt.toString(), "")
                .putExtra(Enums.URL.toString(), url ?: "")
        )
    }
}