package com.bncairnetwork.ui.activities.projectConnections

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
//import com.bncairnetwork.adapter.project_tree.ProjectChildTreeAdptr
import com.bncairnetwork.databinding.ActivityProjectTreeBinding
import com.bncairnetwork.databinding.ProjectAlreadyClaimedPopupBinding
import com.bncairnetwork.helper.ChildOneListnr
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProjectClaimInfoRes
import com.bncairnetwork.pojo.response.project.ChilderDataItem
import com.bncairnetwork.pojo.response.project.ProjectTreeByIDRes
import com.bncairnetwork.ui.adapter.project_tree.ProjectParetTreeAdptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.google.gson.Gson
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectTreeAc : AppCompatActivity(), ChildOneListnr {
    var lst = ArrayList<ChilderDataItem>()
    lateinit var view: View
    lateinit var mInterface: ApiInterface
    private var packageID = 0
    var proObj: ChilderDataItem? = null

    //    lateinit var adptr: ProjectChildTreeAdptr
    lateinit var adptrTest: ProjectParetTreeAdptr
    lateinit var bin: ActivityProjectTreeBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityProjectTreeBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
        runOnUiThread {
            getProjectreeCall(intent.getStringExtra(Enums.ID.toString()) ?: "0")
        }
    }

    fun initView() {
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        packageID = PrefUtils.with(this).getInt(Enums.USER_TYPE.toString(), 0)
        bin.projectLbl.visibility = View.INVISIBLE
        bin.bncBannerChat.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.bncBannerChat.appbarHeader.text = "Project Trees"
//        adptr = ProjectChildTreeAdptr(this, lst, this).apply {
//            bin.projectTreeRv.layoutManager = LinearLayoutManager(this@ProjectTreeAc)
//            bin.projectTreeRv.adapter = this
//        }

        adptrTest = ProjectParetTreeAdptr(this, lst, this).apply {
            bin.projectTreeRv.layoutManager = LinearLayoutManager(this@ProjectTreeAc)
            bin.projectTreeRv.adapter = this
        }

    }

    fun listnr() {
        bin.projectLbl.setOnClickListener {
            bin.projectLbl.setCompoundDrawablesWithIntrinsicBounds(
                if (bin.projectTreeRv.isVisible) R.drawable.ic_add_green else R.drawable.ic_minus_gray,
                0,
                0, 0
            )
            bin.projectTreeRv.visibility =
                if (bin.projectTreeRv.isVisible) View.INVISIBLE else View.VISIBLE
            }
    }

    /*override fun onProjectTreeParntClk(
        pos: Int,
        flag: Boolean,
        childlst: ArrayList<ChilderDataItem>, paddingStart: Int
    ) {
        if (!lst[pos].isChildViewed) {
            childrnlst.clear()
            childlst.forEach {
                // println(it.name.plus("----").plus(index++))
                childrnlst.add(
                    ChilderDataItem(
                        status = it.status,
                        assigned = it.assigned,
                        bnc_project = it.bnc_project,
                        name = it.name,
                        city = it.city,
                        country = it.country,
                        isChildViewed = it.isChildViewed,
                        is_unassigned = it.is_unassigned,
                        owners = it.owners,
                        paddingStart = if (paddingStart != 0) paddingStart + 60 else 80,
                        children = it.children,
                        is_favourite = it.is_favourite,
                        consultants = it.consultants,
                        authorized = it.authorized,
                        bidders = it.bidders,
                        contractors = it.contractors,
                        views_left = it.views_left,
                        current = it.current,
                        id = it.id,
                        childType = it.childType
                    )
                )
//                paddingHashMap[it.name] = it.paddingStart

            }
            lst.addAll(pos + 1, childrnlst)
            adptr.notifyItemRangeInserted(pos + 1, childrnlst.size)

        } else {
            println("===pos=>${pos}======padding==>${paddingStart}")
            childlst.forEach {
                childrnlst.add(
                    ChilderDataItem(
                        status = it.status,
                        assigned = it.assigned,
                        bnc_project = it.bnc_project,
                        name = it.name,
                        city = it.city,
                        country = it.country,
                        isChildViewed = it.isChildViewed,
                        is_unassigned = it.is_unassigned,
                        owners = it.owners,
                        paddingStart = paddingStart,
                        children = it.children,
                        is_favourite = it.is_favourite,
                        consultants = it.consultants,
                        authorized = it.authorized,
                        bidders = it.bidders,
                        contractors = it.contractors,
                        views_left = it.views_left,
                        current = it.current,
                        id = it.id,
                        childType = it.childType
                    )
                )
                it.children.forEach { innrchild ->
                    childrnlst.add(
                        ChilderDataItem(
                            status = innrchild.status,
                            assigned = innrchild.assigned,
                            bnc_project = innrchild.bnc_project,
                            name = innrchild.name,
                            city = innrchild.city,
                            country = innrchild.country,
                            isChildViewed = innrchild.isChildViewed,
                            is_unassigned = innrchild.is_unassigned,
                            owners = innrchild.owners,
                            paddingStart = paddingStart,
                            children = innrchild.children,
                            is_favourite = innrchild.is_favourite,
                            consultants = innrchild.consultants,
                            authorized = innrchild.authorized,
                            bidders = innrchild.bidders,
                            contractors = innrchild.contractors,
                            views_left = innrchild.views_left,
                            current = innrchild.current,
                            id = innrchild.id,
                            childType = innrchild.childType
                        )
                    )
                }
            }
            lst.removeAll(childrnlst)
            adptr.notifyItemRangeRemoved(pos + 1, childrnlst.size)
        }
        lst[pos].isChildViewed = !lst[pos].isChildViewed
        adptr.notifyItemChanged(pos)
    }*/
    fun showProjectClaimDialog(obj: ProjectClaimInfoRes, proID: String) {
        Dialog(this, R.style.Theme_AppCompat_Dialog_MinWidth).apply {
            val view =
                ProjectAlreadyClaimedPopupBinding.inflate(LayoutInflater.from(this@ProjectTreeAc))
            requestWindowFeature(Window.FEATURE_NO_TITLE)
            window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            setCanceledOnTouchOutside(true)
            setContentView(view.root)
            view.defBtnLay.submitBtn.text = "View Projects"
            view.rightNotNowLbl.setOnClickListener {
                dismiss()
            }
            view.ownerslblCount.text = obj.project?.owners.toString()
            view.bidderCount.text = obj.project?.bidders.toString()
            view.consultantCount.text = obj.project?.consultants.toString()
            view.contractorCount.text = obj.project?.contractors.toString()
            view.lbl.text = obj.project?.name ?: ""
            view.defBtnLay.submitBtn.setOnClickListener {
                dismiss()
                obj.credits?.let {
                    if (it >= 5) {
                        runOnUiThread {
                            exhaustCredits(proID = proID, proObj = proObj)
                        }
                    } else {
                        /*in sufficient credits*/
                        // showInAppOption()
                    }
                }
            }
            show()
        }
    }

    fun exhaustCredits(proID: String, proObj: ChilderDataItem?) {
        Uitls.showProgree(true, this)
        val call = mInterface.exhaustCredit(projectID = proID)
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ProjectTreeAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        startActivity(
                            Intent(
                                this@ProjectTreeAc,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), proID)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
                    } else {
                        Uitls.showToast(this@ProjectTreeAc, "Not able to update project view.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectTreeAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectTreeAc)
                Uitls.handlerError(this@ProjectTreeAc, t)
            }
        })
    }


    fun getProjectInfo(projectID: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectInfo(projectID)
        call.enqueue(object : Callback<ProjectClaimInfoRes> {
            override fun onResponse(
                call: Call<ProjectClaimInfoRes>,
                response: Response<ProjectClaimInfoRes>
            ) {
                Uitls.showProgree(false, this@ProjectTreeAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        showProjectClaimDialog(response.body()!!, projectID)
                    } else {
                        Uitls.showToast(
                            this@ProjectTreeAc,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectTreeAc)
                }
            }

            override fun onFailure(call: Call<ProjectClaimInfoRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectTreeAc)
                Uitls.handlerError(this@ProjectTreeAc, t)
            }
        })
    }

    fun getProjectreeCall(id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectTreeByID(id)
        call.enqueue(object : Callback<ProjectTreeByIDRes> {
            override fun onResponse(
                call: Call<ProjectTreeByIDRes>,
                response: Response<ProjectTreeByIDRes>
            ) {
                Uitls.showProgree(false, this@ProjectTreeAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.projectLbl.visibility =
                        if (response.body()!!.status == 1) View.VISIBLE else View.INVISIBLE
                    if (response.body()!!.status == 1) "Project Tree" else "No Project Tree"
                    if (response.body()!!.status == 1) {
                        bin.projectLbl.text =
                            response.body()!!.projectTreeDataItem?.name ?: ""
                        response.body()!!.projectTreeDataItem?.children?.let {
                            bin.projectLbl.setCompoundDrawablesWithIntrinsicBounds(
                                if (it.isEmpty()) 0 else R.drawable.ic_minus_gray,
                                0,
                                0, 0
                            )
                            lst.addAll(it)
                            //ApiConstants.arry.add(lst.size-1)
                            //  adptr.notifyItemRangeInserted(0, lst.size)
                            adptrTest.notifyItemRangeInserted(0, lst.size)

                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectTreeAc)
                }
            }

            override fun onFailure(call: Call<ProjectTreeByIDRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectTreeAc)
                Uitls.handlerError(this@ProjectTreeAc, t)
            }
        })
    }

    override fun onChildOneClick(pos: Int, proObj: ChilderDataItem) {
        this.proObj = proObj
        when (packageID) {
            ApiConstants.professionalID -> {
                proObj.authorized.let { isAuthorised ->
                    if (isAuthorised) {
                        /*this project is in unlocked state*/
                        startActivity(
                            Intent(
                                this@ProjectTreeAc,
                                ProjectDetailsAc::class.java
                            ).putExtra(Enums.ID.toString(), proObj.id)
                                .putExtra(
                                    Enums.DATA.toString(),
                                    Gson().toJson(
                                        proObj
                                    )
                                )
                        )
//                                runOnUiThread {
//                                    exhaustCredits(proID = proID, proObj = proObj)
//                                }
                    } else {
                        /*this project is in locked state*/
                        runOnUiThread {
                            getProjectInfo(projectID = proObj.id)
                        }
                    }
                }
            }
            ApiConstants.ibisID -> {
                startActivity(
                    Intent(
                        this@ProjectTreeAc,
                        ProjectDetailsAc::class.java
                    ).putExtra(Enums.ID.toString(), proObj.id)
                        .putExtra(
                            Enums.DATA.toString(),
                            Gson().toJson(
                                proObj
                            )
                        )
                )
            }
            ApiConstants.bi_userID -> {
                startActivity(
                    Intent(
                        this@ProjectTreeAc,
                        ProjectDetailsAc::class.java
                    ).putExtra(Enums.ID.toString(), proObj.id)
                        .putExtra(
                            Enums.DATA.toString(),
                            Gson().toJson(
                                proObj
                            )
                        )
                )
            }
        }

    }
}