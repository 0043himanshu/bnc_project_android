package com.bncairnetwork.ui.activities.homeConnections

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityCharBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.webservices.ApiConstants
import com.livechatinc.inappchat.ChatWindowConfiguration
import com.livechatinc.inappchat.ChatWindowErrorType
import com.livechatinc.inappchat.ChatWindowView
import com.livechatinc.inappchat.models.NewMessageModel


class ChatAc : AppCompatActivity(), ChatWindowView.ChatWindowEventsListener {
    lateinit var configuration: ChatWindowConfiguration
    lateinit var chatWindowView: ChatWindowView
    lateinit var bin: ActivityCharBinding
    var userID = ""
    var userName = ""
    var userEmail = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityCharBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    /*groupid 10,3 for corporate users
      group id 5 for professional users(3)
      There is one more group id 0 where all the users are part of it.License id : 1345652 */

    fun initView() {
        PrefUtils.with(this).apply {
            userID = getInt(Enums.USER_TYPE.toString(), 0).toString()
            userName = getString(ApiConstants.username, "")?:"User"
            userEmail = getString(Enums.EMAIL.toString(), "")?:"User"
        }

        configuration = ChatWindowConfiguration(
            "1345652",
            if (userID == "3") "5" else "3",
            userName,
            userEmail, null
        )
        chatWindowView = ChatWindowView.createAndAttachChatWindowInstance(this);
        chatWindowView.setUpWindow(configuration);
        chatWindowView.setUpListener(this);
        chatWindowView.initialize();
        chatWindowView.showChatWindow();
    }

    fun listnr() {

    }

    override fun onChatWindowVisibilityChanged(visible: Boolean) {
        if(!visible){
            onBackPressed()
        }
    }

    override fun onError(
        errorType: ChatWindowErrorType?,
        errorCode: Int,
        errorDescription: String?
    ): Boolean {
        if (errorType == ChatWindowErrorType.WebViewClient && errorCode == -2 && chatWindowView.isChatLoaded) {
            //Chat window can handle reconnection. You might want to delegate this to chat window
            return false;
        } else {
            // reloadChatBtn.setVisibility(View.VISIBLE);
        }
        Uitls.showToast(this, errorDescription ?: "")
        return true
    }

    override fun handleUri(uri: Uri?): Boolean {
// Handle uri here...
        return true // Return true to disable default behavior.
    }

    override fun onNewMessage(message: NewMessageModel?, windowVisible: Boolean) {
        println("-----new Message here---------" + message?.text)
    }

    override fun onStartFilePickerActivity(intent: Intent?, requestCode: Int) {

    }

}