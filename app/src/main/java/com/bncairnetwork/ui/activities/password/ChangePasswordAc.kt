package com.bncairnetwork.ui.activities.password

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityChangePasswordBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.PrefUtils
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.post.password.ChnagePssdPost
import com.bncairnetwork.ui.activities.onboarding.OnboardingTwo
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ChangePasswordAc : AppCompatActivity() {
    lateinit var bin: ActivityChangePasswordBinding
    var userType = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityChangePasswordBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
    }

    fun listnr() {
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.chngePsd.submitBtn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (bin.newPssdEdxt.text.isNotEmpty() && Uitls.isValidPassword(
                        bin.newPssdEdxt.text.toString(),
                        userType
                    )
                ) {
                    if (bin.confirmPssdEdxt.text.isNotEmpty() && Uitls.isValidPassword(
                            bin.confirmPssdEdxt.text.toString(),
                            userType
                        )
                    ) {
                        if (bin.newPssdEdxt.text.toString() == bin.confirmPssdEdxt.text.toString()) {
                            if (bin.oldPssdEdxt.visibility == View.VISIBLE) {
                                if (bin.oldPssdEdxt.text.isNotEmpty()) {
                                    runOnUiThread {
                                        changePssCall()
                                    }
                                } else {
                                    bin.oldPssdEdxt.error = "please enter"
                                }

                            } else {
                                runOnUiThread {
                                    changePssCall()
                                }
                            }

                        } else {
                            Uitls.showToast(
                                this@ChangePasswordAc,
                                "New password and confirm password is not same"
                            )

                        }

                    } else {
                        bin.confirmPssdEdxt.error =
                            "Password should contain\nUppercase letters: A-Z\nLowercase letters: a-z\nNumbers: 0-9\nSymbols: ~`!@#\$%^&*()_-+={[}]|\\:;\"'<,>.?/"
                    }
                } else {
                    bin.newPssdEdxt.error =
                        "Password should contain\nUppercase letters: A-Z\nLowercase letters: a-z\nNumbers: 0-9\nSymbols: ~`!@#\$%^&*()_-+={[}]|\\:;\"'<,>.?/"

                }
            }
        })
    }

    fun initView() {
        PrefUtils.with(this).apply {
            userType = getInt(Enums.USER_TYPE.toString(), 0)
        }
        bin.cusAppbarLay.appbarHeader.text = "Change Password"
        bin.chngePsd.submitBtn.text = "Change Password"
        if (intent.getBooleanExtra(ApiConstants.isUserWithTempPssd, false)) {
            bin.oldPssdEdxt.visibility = View.GONE
            bin.oldlockIcon.visibility = View.GONE
        }
    }

    fun changePssCall() {
        Uitls.showProgree(true, this)
        val mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        val call = mInterface.chnagePssd(
            ChnagePssdPost(
                old_password = if (bin.oldPssdEdxt.visibility == View.VISIBLE) bin.oldPssdEdxt.text.toString() else intent.getStringExtra(
                    ApiConstants.TempPwd
                ) ?: "",
                confirm_password = bin.confirmPssdEdxt.text.toString(),
                new_password = bin.newPssdEdxt.text.toString()
            )
        )
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@ChangePasswordAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(this@ChangePasswordAc, "Password changed")
                        if (bin.oldPssdEdxt.visibility == View.VISIBLE) {
                            onBackPressed()
                        } else {
                            startActivity(Intent(this@ChangePasswordAc, OnboardingTwo::class.java))
                            this@ChangePasswordAc.finish()
                        }

                    } else {
                        Uitls.showToast(this@ChangePasswordAc, response.body()!!.errors ?: "Not able to make your request")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ChangePasswordAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@ChangePasswordAc)
                Uitls.handlerError(this@ChangePasswordAc, t)
            }
        })
    }
}