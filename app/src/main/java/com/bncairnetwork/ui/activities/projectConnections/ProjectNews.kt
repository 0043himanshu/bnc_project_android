package com.bncairnetwork.ui.activities.projectConnections

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ActivityProjectNewsBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.ProjectNewsLisntr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.Dashboard.NewsItem
import com.bncairnetwork.pojo.response.Dashboard.NewsNdHighlightsLstRes
import com.bncairnetwork.pojo.response.project.ProjectNewsDataItem
import com.bncairnetwork.pojo.response.project.ProjectNewsItem
import com.bncairnetwork.pojo.response.project.ProjectNewsLstRes
import com.bncairnetwork.ui.activities.AdvanceSearchAc
import com.bncairnetwork.ui.activities.NewsDetails
import com.bncairnetwork.ui.activities.homeConnections.ProjectSearchAc
import com.bncairnetwork.ui.adapter.NewsNdHighlights_and_claimedNews.ProjectNewsAdapter
import com.bncairnetwork.ui.adapter.NewsNdHighlights_and_claimedNews.ProjectNewsViewAllAdapter
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ProjectNews : AppCompatActivity(), ProjectNewsLisntr {
    lateinit var mInterface: ApiInterface
    var project_data: ProjectNewsDataItem? = null
    private var count: String = "0"
    lateinit var bin: ActivityProjectNewsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityProjectNewsBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
        if (intent.getStringExtra(Enums.ID.toString()) != null) {
            runOnUiThread {
                getProjectNewsCall(intent.getStringExtra(Enums.ID.toString())!!)
//                getProjectNewsCall("151297")
            }
            return
        } else if (intent.getStringExtra(Enums.TYPE.toString()) == Enums.ViewAll.toString()) {
            /***user is navigating from view all option on home screen*/
            runOnUiThread {
                getNewsNdHighlightLstCall()
            }
            return
        } else if (intent.getStringExtra(Enums.TYPE.toString()) == Enums.URL.toString()) {
            /*user from dashboard news claim*/
            runOnUiThread {
                getClaimedNews(intent.getStringExtra(Enums.URL.toString()))
            }
            return
        }

    }

    fun initView() {
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.prjctNws.appbarHeader.text = "Project News"
        bin.adncIC.setOnClickListener {
            startActivity(Intent(this, AdvanceSearchAc::class.java))
        }
    }

    fun listnr() {
        bin.prjctNws.backIcImgv.setOnClickListener {
            onBackPressed()
        }

        bin.adncIC.setOnClickListener {
            startActivity(
                Intent(this, AdvanceSearchAc::class.java).putExtra(
                    Enums.FROM.toString(),
                    Enums.HOME_SCREEN_AC.toString()
                )
            )
            ApiConstants.FROM = Enums.HOME_SCREEN_AC.toString()
        }

        bin.searchbar.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (bin.searchbar.text.isNotEmpty()) {
                    // Hide Keyboard
                    Uitls.hideKeyboard(this)
                    startActivity(
                        Intent(
                            this,
                            ProjectSearchAc::class.java
                        ).putExtra(
                            Enums.searchTxt.toString(),
                            bin.searchbar.text.toString()
                        ).putExtra(Enums.TYPE.toString(), ApiConstants.Project)
                            .putExtra(Enums.FROM.toString(), Enums.HOME_SCREEN_AC.toString())
                    )
                    bin.searchbar.text = null
                }
            }
            false

        }

    }

    fun getClaimedNews(url: String?) {
        Uitls.showProgree(true, this)
        val call = mInterface.getClaimedNewsLst(url)
        call.enqueue(object : Callback<ProjectNewsLstRes> {
            override fun onResponse(
                call: Call<ProjectNewsLstRes>,
                response: Response<ProjectNewsLstRes>
            ) {
                Uitls.showProgree(false, this@ProjectNews)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.noDataLbl.noDataLbl.visibility =
                            if (response.body()!!.news?.isNotEmpty() == true) View.GONE else View.VISIBLE
                        if (response.body()!!.news?.isNotEmpty() == true) {
                            count = response.body()!!.project?.related_projects_count ?: "0"
                            ProjectNewsAdapter(
                                this@ProjectNews,
                                response.body()!!.news,
                                this@ProjectNews
                            ).apply {
                                bin.prjtNwsRcy.layoutManager = LinearLayoutManager(this@ProjectNews)
                                bin.prjtNwsRcy.adapter = this
                            }
                            project_data = response.body()!!.project
                        }
                    } else {
                        bin.noDataLbl.noDataLbl.visibility = View.VISIBLE
                        Uitls.showToast(
                            this@ProjectNews,
                            getString(R.string.no_able_to_process_api)
                        )
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectNews)
                }
            }

            override fun onFailure(call: Call<ProjectNewsLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectNews)
                Uitls.handlerError(this@ProjectNews, t)
            }
        })

    }

    fun getProjectNewsCall(id: String) {
        Uitls.showProgree(true, this)
        val call = mInterface.getProjectNewsLst(id) //137504
        call.enqueue(object : Callback<ProjectNewsLstRes> {
            override fun onResponse(
                call: Call<ProjectNewsLstRes>,
                response: Response<ProjectNewsLstRes>
            ) {
                Uitls.showProgree(false, this@ProjectNews)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.noDataLbl.noDataLbl.visibility =
                            if (response.body()!!.news?.isNotEmpty() == true) View.GONE else View.VISIBLE
                        if (response.body()!!.news?.isNotEmpty() == true) {
                            count = response.body()!!.project?.related_projects_count ?: "0"
                            ProjectNewsAdapter(
                                this@ProjectNews,
                                response.body()!!.news,
                                this@ProjectNews
                            ).apply {
                                bin.prjtNwsRcy.layoutManager = LinearLayoutManager(this@ProjectNews)
                                bin.prjtNwsRcy.adapter = this
                            }
                            project_data = response.body()!!.project
                        }
                    } else {
                        bin.noDataLbl.noDataLbl.visibility = View.VISIBLE
                        Uitls.showToast(this@ProjectNews, "Please try after sometime ")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectNews)
                }
            }

            override fun onFailure(call: Call<ProjectNewsLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectNews)
                Uitls.handlerError(this@ProjectNews, t)
            }
        })
    }

    fun getNewsNdHighlightLstCall() {
        Uitls.showProgree(true, this)
        val call = mInterface.getNewsNdHighlightLst()
        call.enqueue(object : Callback<NewsNdHighlightsLstRes> {
            override fun onResponse(
                call: Call<NewsNdHighlightsLstRes>,
                response: Response<NewsNdHighlightsLstRes>
            ) {
                Uitls.showProgree(false, this@ProjectNews)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLbl.noDataLbl.visibility =
                        if (response.body()!!.status == 1) View.GONE else View.VISIBLE
                    if (response.body()!!.status == 1) {
                        ProjectNewsViewAllAdapter(
                            this@ProjectNews,
                            response.body()!!.news,
                            this@ProjectNews
                        ).apply {
                            bin.prjtNwsRcy.layoutManager = LinearLayoutManager(this@ProjectNews)
                            bin.prjtNwsRcy.adapter = this
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@ProjectNews)
                }
            }

            override fun onFailure(call: Call<NewsNdHighlightsLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@ProjectNews)
                Uitls.handlerError(this@ProjectNews, t)
            }
        })
    }


    override fun onNewsClk(pos: Int, obj: NewsItem?, obj2: ProjectNewsItem?) {
        println("----------->${obj?.id}------>${obj2?.id}")
        startActivity(
            Intent(this@ProjectNews, NewsDetails::class.java).putExtra(
                Enums.DATA.toString(),
                Gson().toJson(obj ?: obj2)
            ).putExtra(Enums.NewsRelatedProjectCount.toString(), count)
                .putExtra(
                    Enums.ID.toString(),
                    if (obj?.id != null) obj.id.toString() else obj2?.id.toString()
                )
        )
    }
}