package com.bncairnetwork.ui.activities.onboarding

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityOnboardingOneBinding

class OnboardingOne : AppCompatActivity() {
    lateinit var bin: ActivityOnboardingOneBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityOnboardingOneBinding.inflate(layoutInflater)
        setContentView(bin.root)

        bin.btn.submitBtn.text="Let's Get Started"
        bin.btn.submitBtn.setOnClickListener {
            startActivity(Intent(this,OnboardingTwo::class.java))
        }
    }
}