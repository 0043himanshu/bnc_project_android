package com.bncairnetwork.ui.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.SpannableString
import android.text.TextPaint
import android.text.style.URLSpan
import android.widget.TextView
import com.bncairnetwork.databinding.ActivityAddCreditBinding

class AddCredit : AppCompatActivity() {
    lateinit var bin:ActivityAddCreditBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin= ActivityAddCreditBinding.inflate(layoutInflater)
        setContentView(bin.root)
        bin.addCrdNtRgtNwTxt.removeLinksUnderline()

        listnr()
    }
    fun listnr(){

    }
    fun TextView.removeLinksUnderline() {
        val spannable = SpannableString(text)
        for (urlSpan in spannable.getSpans(0, spannable.length, URLSpan::class.java)) {
            spannable.setSpan(object : URLSpan(urlSpan.url) {
                override fun updateDrawState(ds: TextPaint) {
                    super.updateDrawState(ds)
                    ds.isUnderlineText = false
                }
            }, spannable.getSpanStart(urlSpan), spannable.getSpanEnd(urlSpan), 0)
        }
        text = spannable
    }
}