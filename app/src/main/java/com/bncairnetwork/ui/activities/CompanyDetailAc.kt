package com.bncairnetwork.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.R
import com.bncairnetwork.adapter.AssigneUserAptr
import com.bncairnetwork.adapter.BottomOptionsAdapter
import com.bncairnetwork.databinding.ActivityCompanyDetailBinding
import com.bncairnetwork.helper.*
import com.bncairnetwork.pojo.AttributesItem
import com.bncairnetwork.pojo.ComRes
import com.bncairnetwork.pojo.ProTagObj
import com.bncairnetwork.pojo.ProjectDetailsDataObj
import com.bncairnetwork.pojo.response.InvitationUrlRes
import com.bncairnetwork.pojo.response.company.CompaniesItem
import com.bncairnetwork.pojo.response.company.CompanyDetailByIDRes
import com.bncairnetwork.ui.activities.homeConnections.CompaniesBildder
import com.bncairnetwork.ui.activities.homeConnections.UpdateAndHighlightsAc
import com.bncairnetwork.ui.activities.projectConnections.LiveProjectsAc
import com.bncairnetwork.ui.activities.projectConnections.ProjectNews
import com.bncairnetwork.ui.activities.projectConnections.ProjectReferenceAc
import com.bncairnetwork.ui.activities.projectConnections.ProjectTreeAc
import com.bncairnetwork.ui.activities.reminder.AddReminderAc
import com.bncairnetwork.ui.activities.reminder.ReminderAc
import com.bncairnetwork.ui.adapter.EmailAdptr
import com.bncairnetwork.ui.adapter.PhoneNumberAdptr
import com.bncairnetwork.ui.adapter.project.ProjectTagAptr
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class CompanyDetailAc : AppCompatActivity(), BottomOptionListnr, TagListnr {
    lateinit var bin: ActivityCompanyDetailBinding
    lateinit var mInterface: ApiInterface
    var comID = "0"
    private var isBncVersionView = false
    private var proObj: ProjectDetailsDataObj? = null
    private var packageID = 0
    var bncVersionID = ""
    lateinit var bottomAdptr: BottomOptionsAdapter
    var isLinked = false
    var recordtype = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityCompanyDetailBinding.inflate(layoutInflater)
        setContentView(bin.root)
        listnr()
        initView()

    }

    fun initView() {
        PrefUtils.with(this).apply {
            UserRestrictionRepo.companyDetailRestriction(
                bin,
                getInt(Enums.USER_TYPE.toString(), 0)
            )
            packageID = getInt(Enums.USER_TYPE.toString(), 0)
        }

        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.shareInfoChatViewAppbarLay.backBtn.setOnClickListener {
            onBackPressed()
        }
        bin.compnyDetailAssignedUserLay.assignedUserRootLay.visibility = View.GONE
        bin.root.visibility = View.INVISIBLE
        if (intent.getStringExtra(Enums.ID.toString())?.isNotEmpty() == true) {
            comID = intent.getStringExtra(Enums.ID.toString()).toString()
            println("================>" + intent.getStringExtra(Enums.FROM.toString()))
            isLinked = if (intent.getStringExtra(Enums.FROM.toString()) == null) {
                Gson().fromJson(
                    intent.getStringExtra(Enums.DATA.toString()),
                    ProjectDetailsDataObj::class.java
                ).record_type?.linked ?: false

            } else {
                Gson().fromJson(
                    intent.getStringExtra(Enums.DATA.toString()),
                    CompaniesItem::class.java
                ).linked ?: false
            }
            proObj = Gson().fromJson(
                intent.getStringExtra(Enums.DATA.toString()),
                ProjectDetailsDataObj::class.java
            )
        }
    }

    fun listnr() {
//        bin.companyDetailCrdLay.phoneVal.setOnClickListener {
//            Uitls.makeDialIntent(this, bin.companyDetailCrdLay.phoneVal.text.toString())
//        }
        bin.shareInfoChatViewAppbarLay.infoBtn.setOnClickListener {
            startActivity(
                Intent(this, RequestInformation::class.java).putExtra(
                    Enums.ID.toString(),
                    comID
                ).putExtra(Enums.TYPE.toString(), ApiConstants.Company)
            )
        }
        bin.shareInfoChatViewAppbarLay.shareBtn.setOnClickListener {
            runOnUiThread { getInvitationCall() }
        }
        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.interactionImg.setOnClickListener {
            startActivity(
                Intent(this, AddInteractionAc::class.java).putExtra(
                    Enums.TYPE.toString(),
                    ApiConstants.Company
                )
            )
        }

        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.tagImg.setOnClickListener {
            startActivity(
                Intent(this, Tag::class.java).putExtra(
                    Enums.ID.toString(), intent.getStringExtra(Enums.ID.toString())!!
                ).putExtra(Enums.TYPE.toString(), ApiConstants.Company)
                    .putExtra(
                        Enums.DATA.toString(), Gson().toJson(
                            proObj
                        )
                    )
            )
        }

        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.reminderImg.setOnClickListener {
            startActivity(
                Intent(this, AddReminderAc::class.java).putExtra(
                    Enums.ID.toString(),
                    intent.getStringExtra(Enums.ID.toString())
                ).putExtra(Enums.TYPE.toString(), ApiConstants.Company)
            )
        }

        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.setOnClickListener {
            if (!isBncVersionView) {
                runOnUiThread {
                    getCompanyDetailCall(
                        id = bncVersionID,
                        isBncVersion = true,
                        isFromBtnEvent = true
                    )
                }
            } else {
                runOnUiThread {
                    getCompanyDetailCall(id = comID, isBncVersion = false, isFromBtnEvent = true)
                }
            }

        }

    }

    fun getCompanyDetailCall(id: String, isBncVersion: Boolean, isFromBtnEvent: Boolean) {
        Uitls.showProgree(true, this)
        val call = mInterface.getCompanyDetailsByID(
            id,
            if (isLinked) "true" else null
        )
        call.enqueue(object : Callback<CompanyDetailByIDRes> {
            override fun onResponse(
                call: Call<CompanyDetailByIDRes>,
                response: Response<CompanyDetailByIDRes>
            ) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                if (response.body() != null && response.isSuccessful) {
                    bin.noDataLayout.noDataLbl.visibility =
                        if (response.body()!!.status == 1) View.GONE else View.VISIBLE
                    bin.root.visibility =
                        if (bin.noDataLayout.noDataLbl.visibility == View.VISIBLE) View.INVISIBLE else View.VISIBLE
                    if (response.body()!!.status == 1) {
                        isBncVersionView = isBncVersion
                        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.setImageResource(
                            if (isBncVersion) R.drawable.ic_linked_project else R.drawable.ic_bnc_icon
                        )

                        if (!isBncVersion) {
                            bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.setPadding(
                                12,
                                12,
                                12,
                                12
                            )
                        } else {
                            bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.setPadding(
                                0,
                                0,
                                0,
                                0
                            )
                        }

                        if (isFromBtnEvent)
                            Uitls.createCustomeToast(this@CompanyDetailAc, isBncVersion)
                        databinding(response.body()!!, isBncVersion)
                    }

                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompanyDetailAc)
                }
            }

            override fun onFailure(call: Call<CompanyDetailByIDRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                Uitls.handlerError(this@CompanyDetailAc, t)
            }
        })


    }

    fun databinding(obj: CompanyDetailByIDRes, isBncVersion: Boolean) {
        proObj = ProjectDetailsDataObj(
            authorised = null,
            id = proObj?.id ?: "",
            name = obj.company?.name ?: "",
            industry = "",
            updated = "",
            record_type = proObj?.record_type,
            value = "",
            tag_obj = ProTagObj(
                selectedTags = obj.tags,
                isTypeIcon = false,
                isFavourite = false,
                isProjects = null
            ),
            reminder = null,
            description = "",
            country = obj.company?.country ?: "",
            city = obj.company?.city ?: "",
            stage = "",
            views_left = "",
            image = "",
            identfierObj = null
        )

        bncVersionID = obj.company?.bncVersion?.id.toString()
        PrefUtils.with(this).save(Enums.OBJ.toString(), Gson().toJson(obj))
        recordtype = obj.company?.record_type ?: ""
        ProjectTagAptr(obj.tags ?: arrayListOf(), this).apply {
            bin.comapanyDetailsGeneralLay.tagRv.layoutManager =
                LinearLayoutManager(this@CompanyDetailAc, LinearLayoutManager.HORIZONTAL, false)
            bin.comapanyDetailsGeneralLay.tagRv.adapter = this
        }
        bin.comapanyDetailsGeneralLay.projIc.setBackgroundResource(
            if (obj.company?.record_type?.equals(ApiConstants.LinkedCompany) == true
            ) R.drawable.ic_linked_project else if (obj.company?.record_type?.equals(
                    ApiConstants.BncCompany
                ) == true
            ) R.drawable.ic_b else R.drawable.ic_internal

        )
        obj.company?.assignees_obj?.let { ob ->
            if (ob.isNotEmpty()) {
                bin.compnyDetailAssignedUserLay.assignedUserRootLay.visibility = View.VISIBLE
                AssigneUserAptr(this, ob).apply {
                    bin.compnyDetailAssignedUserLay.assinedUserRv.layoutManager =
                        LinearLayoutManager(
                            this@CompanyDetailAc,
                            LinearLayoutManager.HORIZONTAL,
                            false
                        )
                    bin.compnyDetailAssignedUserLay.assinedUserRv.adapter = this
                }
            }
        }

        bin.comapanyDetailsGeneralLay.textView47.text = obj.company?.name

        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
            if (obj.favourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive
        )

        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.tagImg.setImageResource(
            if ((obj.tags
                    ?: arrayListOf()).isEmpty()
            ) R.drawable.ic_tag_inactive else R.drawable.ic_tag_active
        )

        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.reminderImg.setImageResource(
            if (obj.has_active_reminder == true) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive
        )

        // bin.comapanyDetailsGeneralLay.cityLbl.text = obj.company?.city.plus(", ").plus(obj.company?.country?:"")
        bin.comapanyDetailsGeneralLay.addresslbl.text =
            "".plus(obj.company?.city ?: "").plus(" , ")
                .plus(obj.company?.country ?: "")


        bin.companyDetailCrdLay.compnyTypeVal.text = obj.company?.companyType


//        bin.companyDetailCrdLay.webVal.text =
//            obj.company?.web.toString().replace("[", "").replace("]", "")

        obj.company?.web?.let {
            EmailAdptr(this, it).apply {
                bin.companyDetailCrdLay.webVal.layoutManager =
                    LinearLayoutManager(this@CompanyDetailAc, LinearLayoutManager.VERTICAL, true)
                bin.companyDetailCrdLay.webVal.adapter = this
            }

        }

        obj.company?.fax?.let {
            PhoneNumberAdptr(this, it).apply {
                bin.companyDetailCrdLay.faxVal.layoutManager =
                    LinearLayoutManager(this@CompanyDetailAc, LinearLayoutManager.VERTICAL, true)
                bin.companyDetailCrdLay.faxVal.adapter = this
            }

        }

        obj.company?.phone?.let {
            PhoneNumberAdptr(this, it).apply {
                bin.companyDetailCrdLay.phoneVal.layoutManager =
                    LinearLayoutManager(this@CompanyDetailAc, LinearLayoutManager.VERTICAL, true)
                bin.companyDetailCrdLay.phoneVal.adapter = this
            }

        }


//        bin.companyDetailCrdLay.phoneVal.text =
//            obj.company?.phone.toString().replace("[", "").replace("]", "")
//

        bin.companyDetailCrdLay.categoyVal.text =
            obj.company?.categoryList.toString().replace("[", "").replace("]", "")
        bin.companyDetailCrdLay.categoylbl.visibility =
            if (bin.companyDetailCrdLay.categoyVal.text.isNotEmpty()) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.compnyTypelbl.visibility =
            if (bin.companyDetailCrdLay.compnyTypeVal.text.isNotEmpty()) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.phonelbl.visibility =
            if (bin.companyDetailCrdLay.phoneVal.adapter?.itemCount != 0) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.faxlbl.visibility =
            if (bin.companyDetailCrdLay.faxVal.adapter?.itemCount != 0) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.weblbl.visibility =
            if (bin.companyDetailCrdLay.webVal.adapter?.itemCount != 0) View.VISIBLE else View.GONE

        bin.companyDetailCrdLay.categoyVal.visibility =
            if (bin.companyDetailCrdLay.categoyVal.text.isNotEmpty()) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.compnyTypeVal.visibility =
            if (bin.companyDetailCrdLay.compnyTypeVal.text.isNotEmpty()) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.phoneVal.visibility =
            if (bin.companyDetailCrdLay.phoneVal.adapter?.itemCount != 0) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.faxVal.visibility =
            if (bin.companyDetailCrdLay.faxVal.adapter?.itemCount != 0) View.VISIBLE else View.GONE
        bin.companyDetailCrdLay.webVal.visibility =
            if (bin.companyDetailCrdLay.webVal.adapter?.itemCount != 0) View.VISIBLE else View.GONE

        obj.attributes?.let {
            val lst = ArrayList<AttributesItem?>()
            /*|| (attributesItem.title.equals(ApiConstants.Interactions) && packageID != ApiConstants.professionalID)*/
            it.forEach { attributesItem ->
                if (attributesItem?.count != 0) {
                    lst.add(attributesItem)
                }
            }
            bottomAdptr = BottomOptionsAdapter(this, lst, this, packageID).apply {
                bin.bottomOptnRv.layoutManager = LinearLayoutManager(this@CompanyDetailAc)
                bin.bottomOptnRv.adapter = this
            }
        }

        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setOnClickListener {
            runOnUiThread {
                if (obj.favourite == true) {
                    removeFavCom(obj.company?.id.toString(), obj)
                } else {
                    makeFavCom(obj.company?.id.toString(), obj)
                }
            }
        }
        onBncVersionOnCrontrols(isBncVersion, obj)

    }

    fun onBncVersionOnCrontrols(isBncVersion: Boolean, obj: CompanyDetailByIDRes) {

        if (isBncVersion) {
            bin.comapanyDetailsGeneralLay.textView47.text = obj.company?.bncVersion?.name ?: ""
            bin.comapanyDetailsGeneralLay.addresslbl.text =
                obj.company?.bncVersion?.location?.plus(", ")
                    .plus(obj.company?.bncVersion?.city?.name ?: "").plus(" & ")
                    .plus(obj.company?.bncVersion?.projectCountry?.name ?: "")

//            bin.projectDetailOverviewLay.projectSectorVal.text = obj.company?.bncVersion?.sector
//            bin.projectDetailOverviewLay.projectIndustryVal.text =
//                obj.company?.bncVersion?.newProjectType ?: ""
//            bin.projectDetailOverviewLay.projectTypeVal.text =
//                obj.company?.bncVersion?.newSubtype ?: ""
//            bin.projectDetailOverviewLay.projectEstmateVal.text =
//                "$ ".plus(
//                    Uitls.getMillionBillionTrillion(
//                        (obj.company?.bncVersion?.value ?: "0.0").toFloat()
//                    ).replace(".0", "")
//                )
//            bin.projectDetailOverviewLay.desVal.text = obj.company?.bncVersion?.description

            bin.comapanyDetailsGeneralLay.projIc.setBackgroundResource(
                if (obj.company?.bncVersion?.recordType?.equals(ApiConstants.LinkedCompany) == true
                ) R.drawable.ic_linked_project
                else if (obj.company?.bncVersion?.recordType?.equals(ApiConstants.BncCompany) == true) R.drawable.ic_b else R.drawable.ic_internal
            )

        }
        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.reminderImg.isEnabled =
            !isBncVersion
        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.isEnabled =
            !isBncVersion
        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.tagImg.isEnabled =
            !isBncVersion
        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.interactionImg.isEnabled =
            !isBncVersion
        //  bottomAdptr.disableRvView(!isBncVersion)
        bin.shareInfoChatViewAppbarLay.shareBtn.isEnabled = !isBncVersion
        bin.shareInfoChatViewAppbarLay.infoBtn.isEnabled = !isBncVersion
        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
            if (isLinked || isBncVersion) View.VISIBLE else View.GONE

    }

    override fun onTagClick(name: String?) {

    }

    override fun onOptionSelect(pos: Int, id: String, count: String) {
        startActivity(
            Intent(
                this,
                if (id == ApiConstants.Companies || id == ApiConstants.Bidders) CompaniesBildder::class.java
                else if (id == ApiConstants.Schedules) Schedules::class.java
                else if (id == ApiConstants.Reminders) ReminderAc::class.java
                else if (id == ApiConstants.UpdatesAndHighlights) UpdateAndHighlightsAc::class.java
                else if (id == ApiConstants.ProjectTree) ProjectTreeAc::class.java
                else if (id == ApiConstants.ProjectNews) ProjectNews::class.java
                else if (id == ApiConstants.ProjectReference) ProjectReferenceAc::class.java
                else if (id == ApiConstants.KeyContact) ContactAc::class.java
                else if (id == ApiConstants.LiveProject) LiveProjectsAc::class.java
                else InteractionAc::class.java
            ).putExtra(Enums.ID.toString(), intent.getStringExtra(Enums.ID.toString()))
                .putExtra(Enums.TYPE.toString(), ApiConstants.Company)
                .putExtra(Enums.RECORDTYPE.toString(), recordtype)
                .putExtra(Enums.isLinked.toString(), isLinked)
                .putExtra(Enums.NAME.toString(), bin.comapanyDetailsGeneralLay.textView47.text)
        )
    }

    fun makeFavCom(id: String, obj: CompanyDetailByIDRes) {
        Uitls.showProgree(true, this)
        val call =
            mInterface.addFavForCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        obj.favourite = true
                        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
                            R.drawable.ic_favorite_active
                        )
                    } else {
                        Uitls.showToast(this@CompanyDetailAc, "Not able to mark as favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompanyDetailAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                Uitls.handlerError(this@CompanyDetailAc, t)
            }
        })
    }

    fun getInvitationCall() {
        Uitls.showProgree(true, this)
        val call = mInterface.getInvitationUrl()
        call.enqueue(object : Callback<InvitationUrlRes> {
            override fun onResponse(
                call: Call<InvitationUrlRes>,
                response: Response<InvitationUrlRes>
            ) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showSharingDialog(
                            (response.body()!!.message
                                ?: "").plus(" ".plus(response.body()!!.invitation)),
                            response.body()!!.shareUrl ?: "",
                            this@CompanyDetailAc
                        )
                    } else {
                        Uitls.showToast(this@CompanyDetailAc, "Not able make invitation request")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompanyDetailAc)
                }
            }

            override fun onFailure(call: Call<InvitationUrlRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                Uitls.handlerError(this@CompanyDetailAc, t)
            }
        })
    }

    fun removeFavCom(id: String, obj: CompanyDetailByIDRes) {
        Uitls.showProgree(true, this)
        val call = mInterface.removeFavFromCompany(arrayListOf(id.toInt()))
        call.enqueue(object : Callback<ComRes> {
            override fun onResponse(call: Call<ComRes>, response: Response<ComRes>) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        obj.favourite = false
                        bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
                            R.drawable.ic_favorite_inactive
                        )
                    } else {
                        /*carrers@randstad.in*/
                        Uitls.showToast(this@CompanyDetailAc, "Not able to mark as un-favourite")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@CompanyDetailAc)
                }
            }

            override fun onFailure(call: Call<ComRes>, t: Throwable) {
                Uitls.showProgree(false, this@CompanyDetailAc)
                Uitls.handlerError(this@CompanyDetailAc, t)
            }
        })
    }

    override fun onResume() {
        super.onResume()
        runOnUiThread {
            getCompanyDetailCall(comID, isBncVersion = isBncVersionView, isFromBtnEvent = false)
        }
    }
}