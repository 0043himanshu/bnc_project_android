package com.bncairnetwork.ui.activities.homeConnections

import android.R
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityAddContactBinding
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.post.contact.CreateContactPost
import com.bncairnetwork.pojo.response.company.GetAutoComCompaniesRes
import com.bncairnetwork.pojo.response.contact.AddContactRes
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class AddContactAc : AppCompatActivity() {
    lateinit var mInterface: ApiInterface
    lateinit var handler: Handler
    var flag: Boolean = false
    var companiesLst = ArrayList<String>()
    var companiesHashMap = HashMap<String, String>()
    var designationHashMap = HashMap<String, Int>()
    var designationLst = ArrayList<String>()
    lateinit var bin: ActivityAddContactBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityAddContactBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initview()
        listnr()
        runOnUiThread {
            getMasterData()
        }

    }

    fun initview() {
        handler = Handler(Looper.myLooper()!!)
        mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        bin.cusAppbarLay.backIcImgv.setOnClickListener {
            onBackPressed()
        }
        bin.cusAppbarLay.appbarHeader.text = "Add Contact"
        bin.defBtnLay.submitBtn.text = "Create Contact"
    }

    fun listnr() {
        bin.defBtnLay.submitBtn.setOnClickListener {
            if (validation()) {
                runOnUiThread {
                    addContact()
                }
            } else {
                Uitls.showToast(this, "Some fields are empty!")
            }
        }
        bin.comnyNameEdtxt.setOnTouchListener(OnTouchListener { _, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= bin.comnyNameEdtxt.right - bin.comnyNameEdtxt.compoundDrawables[2].bounds.width()
                ) {
                    // your action here
                    if (bin.comnyNameEdtxt.text.isNotEmpty()) {
                        runOnUiThread {
                            getcompanies_or_projects()
                        }
                    }

                    return@OnTouchListener true
                }
            }
            false
        })

        bin.comnyNameEdtxt.setOnKeyListener { v, keyCode, event ->
            if (event.action === KeyEvent.ACTION_DOWN) {
                when (keyCode) {
                    KeyEvent.KEYCODE_DPAD_CENTER, KeyEvent.KEYCODE_ENTER -> {
                        if (bin.comnyNameEdtxt.text.isNotEmpty()) {
                            runOnUiThread {
                                getcompanies_or_projects()
                            }
                        }
                        return@setOnKeyListener true
                    }
                }
            }
            return@setOnKeyListener false

        }
    }

    fun getMasterData() {
        Uitls.showProgree(true, this)
        val call = mInterface.getMasterData()
        call.enqueue(object : Callback<GetMasterDataRes> {
            override fun onResponse(
                call: Call<GetMasterDataRes>,
                response: Response<GetMasterDataRes>
            ) {
                Uitls.showProgree(false, this@AddContactAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        dropdownBinding(response.body()!!)
                    } else {
                        Uitls.showToast(this@AddContactAc, "")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddContactAc)
                }
            }

            override fun onFailure(call: Call<GetMasterDataRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddContactAc)
                Uitls.handlerError(this@AddContactAc, t)
            }
        })
    }

    fun validation(): Boolean {
        if (bin.comnyNameEdtxt.text.toString().isNotEmpty()) {
            if (bin.SalutationTxt.selectedItemPosition > 0) {
                if (bin.FrstNameTxt.text.toString().isNotEmpty()) {
                    if (bin.lstnameExt.text.toString().isNotEmpty()) {
                        if (bin.desigtnSp.selectedItemPosition > 0) {
                            if (bin.emialAdrsEdxt.text.toString().isNotEmpty()) {
                                if (bin.countryCodeEdxt.text.toString().isNotEmpty()) {
                                    if (bin.areaCodeEdxt.text.toString().isNotEmpty()) {
                                        if (bin.MobileNoEdxt.text.toString().isNotEmpty()) {
                                            flag = true
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return flag
    }

    fun dropdownBinding(obj: GetMasterDataRes) {
        /***designation binding*/
        designationLst.add("Select")
        obj.designationList?.forEach {
            designationHashMap[it?.name!!] = it.id ?: 0
            designationLst.add(it.name)
        }
        ArrayAdapter(
            this@AddContactAc,
            R.layout.simple_spinner_item,
            designationLst
        ).apply {
            setDropDownViewResource(com.bncairnetwork.R.layout.sp_row_item_lay)
            bin.desigtnSp.adapter = this
        }
    }

    fun getcompanies_or_projects() {
        companiesHashMap.clear()
        companiesLst.clear()
        Uitls.showProgree(true, this)
        val call = mInterface.getcompanies_autocomp(bin.comnyNameEdtxt.text.toString())
        call.enqueue(object : Callback<GetAutoComCompaniesRes> {
            override fun onResponse(
                call: Call<GetAutoComCompaniesRes>,
                response: Response<GetAutoComCompaniesRes>
            ) {
                Uitls.showProgree(false, this@AddContactAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        bin.comnyNameEdtxt.text = null
                        response.body()!!.suggestions?.forEach {
                            companiesHashMap[it?.name ?: ""] = it?.id.toString()
                            companiesLst.add(it?.name ?: "")
                        }
                        ArrayAdapter(
                            this@AddContactAc,
                            android.R.layout.simple_list_item_1, companiesLst
                        ).apply {
                            bin.comnyNameEdtxt.setAdapter(this)
                            setNotifyOnChange(true)
                            bin.comnyNameEdtxt.showDropDown()
                        }
                    } else {
                        Uitls.showToast(this@AddContactAc, "No Companies found.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddContactAc)
                }
            }

            override fun onFailure(call: Call<GetAutoComCompaniesRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddContactAc)
                Uitls.handlerError(this@AddContactAc, t)
            }
        })
    }


    fun addContact() {
        Uitls.showProgree(true, this)
        val call = mInterface.addContact(
            id = companiesHashMap[bin.comnyNameEdtxt.text.toString()].toString(),
            CreateContactPost(
                phoneAreaCode1 = bin.areaCodeEdxt.text.toString(),
                phone1 = bin.MobileNoEdxt.text.toString(),
                title = bin.SalutationTxt.selectedItem.toString(),
                linkedin = bin.LnkedEdxt.text.toString(),
                lastName = bin.lstnameExt.text.toString(),
                firstName = bin.FrstNameTxt.text.toString(),
                email1 = bin.emialAdrsEdxt.text.toString(),
                designation = designationHashMap[bin.desigtnSp.selectedItem],
                department = bin.deprtmntTxt.text.toString(),
                mobile = true,
            )
        )
        call.enqueue(object : Callback<AddContactRes> {
            override fun onResponse(
                call: Call<AddContactRes>,
                response: Response<AddContactRes>
            ) {
                Uitls.showProgree(false, this@AddContactAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == 1) {
                        Uitls.showToast(this@AddContactAc, "Contact added.")
                        onBackPressed()
                    } else {
                        Uitls.showToast(this@AddContactAc, "No able to create contact.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(), this@AddContactAc)
                }
            }

            override fun onFailure(call: Call<AddContactRes>, t: Throwable) {
                Uitls.showProgree(false, this@AddContactAc)
                Uitls.handlerError(this@AddContactAc, t)
            }
        })
    }


}