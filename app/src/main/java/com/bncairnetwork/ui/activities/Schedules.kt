package com.bncairnetwork.ui.activities

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.bncairnetwork.adapter.ScheduleAptr
import com.bncairnetwork.databinding.ActivitySchedulesBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.schedules.GetSchedulesLstRes
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class Schedules : AppCompatActivity() {
    lateinit var bin: ActivitySchedulesBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivitySchedulesBinding.inflate(layoutInflater)
        setContentView(bin.root)
        initView()
        listnr()
        if (!intent.getStringExtra(Enums.ID.toString()).isNullOrEmpty()) {
            runOnUiThread { getSchedules(intent.getStringExtra(Enums.ID.toString())) }
        }
    }

    fun initView() {

    }

    fun listnr() {
        bin.schdksSearchCusAppbar.appbarHeader.text = "Schedules"
//        bin.schdksSearchCusAppbar.msg.setOnClickListener {
//            startActivity(Intent(this,ChatAc::class.java))
//        }

        bin.schdksSearchCusAppbar.backIcImgv.setOnClickListener {
            onBackPressed()
        }
    }

    fun getSchedules(id: String?) {
        Uitls.showProgree(true, this)
        val mInterface = ServiceBuilder.buildServiceToken(ApiInterface::class.java, this)
        val call = mInterface.getSchedules(id ?: "")
        call.enqueue(object : Callback<GetSchedulesLstRes> {
            override fun onResponse(
                call: Call<GetSchedulesLstRes>,
                response: Response<GetSchedulesLstRes>
            ) {
                Uitls.showProgree(false, this@Schedules)
                if (response.body() != null && response.isSuccessful) {
                    bin.schedulesRv.visibility =
                        if (response.body()!!.status == 1) View.VISIBLE else View.INVISIBLE
                    if (response.body()!!.status == 1) {
                        bin.textView12.text = response.body()!!.project?.name ?: ""
                        bin.textView13.text = response.body()!!.project?.city ?: ""
                        bin.textView14.text = response.body()!!.project?.country?:""
                        bin.textView16.text =
                            "Updated on ${response.body()!!.project?.updated ?: ""}"
                        ScheduleAptr(this@Schedules, response.body()!!.schedules).apply {
                            bin.schedulesRv.layoutManager = LinearLayoutManager(this@Schedules)
                            bin.schedulesRv.adapter = this
                        }
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(),this@Schedules)
                }
            }

            override fun onFailure(call: Call<GetSchedulesLstRes>, t: Throwable) {
                Uitls.showProgree(false, this@Schedules)
                Uitls.handlerError(this@Schedules, t)
            }
        })
    }

}