package com.bncairnetwork.ui.activities.password

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bncairnetwork.databinding.ActivityForgotPwdBinding
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.post.password.ForgotPasswordPost
import com.bncairnetwork.pojo.response.ForgotPasswordRes
import com.bncairnetwork.webservices.ApiInterface
import com.bncairnetwork.webservices.ServiceBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPwdAc : AppCompatActivity() {
    lateinit var bin: ActivityForgotPwdBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        bin = ActivityForgotPwdBinding.inflate(layoutInflater)
        setContentView(bin.root)
        bin.bckBtn.setOnClickListener {
            onBackPressed()
        }
        bin.loginBtn.setOnClickListener {
            if (bin.userEmailEdxt.text.isNotEmpty()) {
                runOnUiThread {
                    forgotCall()
                }
            } else {
                Uitls.showToast(this, "Please enter email")
            }

        }
    }

    fun forgotCall() {
        Uitls.showProgree(true, this)
        val mInterface = ServiceBuilder.buildService(ApiInterface::class.java, this)
        val call = mInterface.forgotPassword(ForgotPasswordPost(bin.userEmailEdxt.text.toString()))
        call.enqueue(object : Callback<ForgotPasswordRes> {
            override fun onResponse(call: Call<ForgotPasswordRes>, response: Response<ForgotPasswordRes>) {
                Uitls.showProgree(false, this@ForgotPwdAc)
                if (response.body() != null && response.isSuccessful) {
                    if (response.body()!!.status == "1") {
                        Uitls.showToast(this@ForgotPwdAc, response.body()!!.message ?: "")
                        onBackPressed()
                    } else {
                        Uitls.showToast(this@ForgotPwdAc, "Enter a valid email address.")
                    }
                } else {
                    Uitls.onUnSuccessResponse(response.code(),this@ForgotPwdAc)
                }
            }
            override fun onFailure(call: Call<ForgotPasswordRes>, t: Throwable) {
                Uitls.showProgree(false, this@ForgotPwdAc)
                Uitls.handlerError(this@ForgotPwdAc, t)
            }
        })
    }
}