package com.bncairnetwork.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.NumberAdptrDataItemLayBinding
import com.bncairnetwork.helper.Uitls

class PhoneNumberAdptr(val con: Context, private val numberLst: ArrayList<String?>) :
    RecyclerView.Adapter<PhoneNumberAdptr.MViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MViewHolder {
        return MViewHolder(
            NumberAdptrDataItemLayBinding.inflate(
                LayoutInflater.from(con)
            )
        )
    }

    override fun getItemCount(): Int {
        return numberLst.size
    }

    inner class MViewHolder(val item: NumberAdptrDataItemLayBinding) :
        RecyclerView.ViewHolder(item.root)

    override fun onBindViewHolder(holder: MViewHolder, position: Int) {
        holder.item.phoneNolbl.text =
            numberLst[position]

        holder.itemView.setOnClickListener {
            Uitls.makeDialIntent(con, number = numberLst[position].toString())
        }
    }

}