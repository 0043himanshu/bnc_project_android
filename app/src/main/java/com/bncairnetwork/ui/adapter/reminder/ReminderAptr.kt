package com.bncairnetwork.ui.adapter.reminder

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ReminderCusItemLayBinding
import com.bncairnetwork.helper.ReminderListnr
import com.bncairnetwork.pojo.response.reminder.RemindersItem
import com.bncairnetwork.webservices.ApiConstants
import java.text.SimpleDateFormat
import java.util.*

class ReminderAptr(
    var con: Context,
    var lst: ArrayList<RemindersItem>,
    var lstnr: ReminderListnr
) :
    RecyclerView.Adapter<ReminderAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ReminderCusItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.ReminderTxtLbl.text = lst[position].reminder ?: ""
        holder.item.compnyLbl.text = lst[position].project?.name ?: ""

        holder.item.stsLbl.text = lst[position].status ?: ""
        holder.item.mrkAsCompelteBtn.visibility =
            if (lst[position].status.equals(ApiConstants.Pending)) View.VISIBLE else View.INVISIBLE
        holder.item.mrkAsCompelteBtn.text =
            if (lst[position].status.equals(ApiConstants.Pending)) "Mark as Complete" else "Completed"
        holder.item.reminderLbl.text = lst[position].remindOn ?: ""
        holder.item.deletebtn.setOnClickListener {
            lstnr.onRrminderClk(
                position,
                "",
                isDel = true,
                isEdit = false,
                isShare = false,
                isMarkCom = false,
                data = lst[position]
            )
        }
        holder.item.editbtn.setOnClickListener {
            lstnr.onRrminderClk(
                position,
                "",
                isDel = false,
                isEdit = true,
                isShare = false,
                isMarkCom = false,
                data = lst[position]
            )
        }
        holder.item.sharebtn.setOnClickListener {
            lstnr.onRrminderClk(
                position,
                "",
                isDel = false,
                isEdit = false,
                isShare = true,
                isMarkCom = false,
                data = lst[position]
            )
        }
        if (lst[position].status.equals(ApiConstants.Pending))
            holder.item.mrkAsCompelteBtn.setOnClickListener {
                lstnr.onRrminderClk(
                    position,
                    "",
                    isDel = false,
                    isEdit = false,
                    isShare = false,
                    isMarkCom = true,
                    data = lst[position]
                )

            }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    class ViewHolder(val item: ReminderCusItemLayBinding) : RecyclerView.ViewHolder(item.root)
}