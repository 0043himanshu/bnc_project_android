package com.bncairnetwork.ui.adapter.company

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.CompanySearchDataItemLayBinding
import com.bncairnetwork.helper.ProjectSeachListner
import com.bncairnetwork.helper.TagListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.helper.UserRestrictionRepo
import com.bncairnetwork.pojo.CompaniesDataObj
import com.bncairnetwork.pojo.CompaniesItem
import com.bncairnetwork.ui.adapter.project.ProjectTagAptr

class CompanySearchAptr(
    var con: Context,
    var comLst: ArrayList<CompaniesDataObj>? = null,
    var comlstkeyword: ArrayList<CompaniesItem?>? = null,
    var lstnr: ProjectSeachListner,
    var packageID: Int, var tagListnr: TagListnr
) :
    RecyclerView.Adapter<CompanySearchAptr.ViewHolder>() {
    private val viewPool = RecyclerView.RecycledViewPool()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CompanySearchDataItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val childLayoutManager =
            LinearLayoutManager(holder.item.tagRv.context, LinearLayoutManager.HORIZONTAL, false)
        // childLayoutManager.initialPrefetchItemCount = 4
        holder.item.tagRv.apply {
            layoutManager = childLayoutManager
            adapter =
                ProjectTagAptr(
                    if (comLst != null) comLst?.get(position)?.tag_obj?.selectedTags else comlstkeyword?.get(
                        position
                    )?.tags,tagListnr
                )
            setRecycledViewPool(viewPool)
        }
        comlstkeyword?.let {
            holder.item.projIc.setImageResource(
                if (comlstkeyword!![position]?.recordType?.linked == true && comlstkeyword!![position]?.recordType?.type == "IBIS") R.drawable.ic_linked_project
                else if (comlstkeyword!![position]?.recordType?.type == "IBIS" && comlstkeyword!![position]?.recordType?.linked == false) R.drawable.ic_internal
                else if (comlstkeyword!![position]?.recordType?.type == "BNC") R.drawable.ic_b else 0
            )
            holder.item.updateDate.text = comlstkeyword!![position]?.updated ?: ""
            holder.item.proEmailval.visibility =
                if (comlstkeyword!![position]?.email != null) View.VISIBLE else View.GONE
            holder.item.proEmailval.text = comlstkeyword!![position]?.email ?: ""
holder.item.proEmailval.setOnClickListener {
    Uitls.makeEmailIntent(con,email = holder.item.proEmailval.text.toString())
}
//            holder.item.proEmail.visibility =
//                if (comlstkeyword!![position]?.email != null && comlstkeyword!![position]?.email?.isNotEmpty() == true) View.VISIBLE else View.GONE

            holder.item.proPhoneval.text = comlstkeyword!![position]?.contactNumber ?: ""
            holder.item.proPhoneval.setOnClickListener {
                Uitls.makeDialIntent(con = con, holder.item.proPhoneval.text.toString())
            }
//            holder.item.proPhone.visibility =
//                if (!comlstkeyword!![position]?.contactNumber.isNullOrEmpty()) View.VISIBLE else View.GONE

            holder.item.projName.text = comlstkeyword!![position]?.name ?: ""
            holder.item.projCountry.text = comlstkeyword!![position]?.city.plus(", ")
                .plus(comlstkeyword!![position]?.country ?: "")
            holder.item.projtype.text = comlstkeyword!![position]?.companyType ?: ""
            holder.item.img3.setImageResource(if (comlstkeyword!![position]?.reminder != null) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive)
            holder.item.im2.setImageResource(if (comlstkeyword!![position]?.tags?.isEmpty() == true) R.drawable.ic_tag_inactive else R.drawable.ic_tag_active)
            holder.item.im4.setImageResource(if (comlstkeyword!![position]?.favourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)
            // holder.item.updateDate.text = comLst!![position].updated
            //Glide.with(con).load(comLst!![position].image).into(holder.item.proImg)
            holder.itemView.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = comlstkeyword!![position]?.id ?: "",
                    isTxt = true,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = false,
                    comObj = null,
                    proObj = null,
                    comkeywordObj = comlstkeyword!![position]

                )
            }
            holder.item.im2.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = comlstkeyword!![position]?.id ?: "",
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = true,
                    isReminder = false,
                    isFav = false,
                    comObj = null,
                    proObj = null,
                    comkeywordObj = comlstkeyword!![position]
                )
            }
            holder.item.img1.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = comlstkeyword!![position]?.id ?: "",
                    isTxt = false,
                    isAddInteraction = true,
                    isTag = false,
                    isReminder = false,
                    isFav = false,
                    comObj = null,
                    proObj = null,
                    comkeywordObj = comlstkeyword!![position]
                )
            }
            holder.item.img3.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = comlstkeyword!![position]?.id ?: "",
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = true,
                    isFav = false,
                    comObj = null,
                    proObj = null,
                    comkeywordObj = comlstkeyword!![position]
                )
            }
            holder.item.im4.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = comlstkeyword!![position]?.id ?: "",
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = true,
                    comObj = null,
                    proObj = null,
                    comkeywordObj = comlstkeyword!![position]
                )
            }

            UserRestrictionRepo.interactionTagReminderFavoriteSwitchRestriction(
                interactionView = holder.item.img1,
                TagView = holder.item.im2,
                ReminderView = holder.item.img3,
                FavoriteView = holder.item.im4,
                SwitchView = null,
                packageID = packageID,
                isCompanyView = true,
                isProjectView = false
            )

        }

//        comLst?.let {
//            holder.item.projIc.setImageResource(
//                if (comLst!![position].record_type?.linked == true && comLst!![position].record_type?.type == "IBIS") R.drawable.ic_linked_project
//                else if (comLst!![position].record_type?.type == "IBIS" && comLst!![position].record_type?.linked == false) R.drawable.ic_internal
//                else if (comLst!![position].record_type?.type == "BNC") R.drawable.ic_b else 0
//            )
//            holder.item.updateDate.text  =comLst!![position].updated?:""
//            holder.item.proEmailval.text = comLst!![position].email ?: ""
//            holder.item.proPhoneval.text = comLst!![position].phone ?: ""
//            holder.item.projName.text = comLst!![position].name
//            holder.item.projCountry.text =
//                comLst!![position].city.plus(", ").plus(comLst!![position].country)
//            holder.item.projtype.text = comLst!![position].company_type ?: ""
//            holder.item.img3.setImageResource(if (comLst!![position].reminder != null) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive)
//            holder.item.im2.setImageResource(if (comLst!![position].tag_obj?.selectedTags?.isEmpty() == true) R.drawable.ic_tag_inactive else R.drawable.ic_tag_active)
//            holder.item.im4.setImageResource(if (comLst!![position].tag_obj?.isFavourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)
////            holder.item.updateDate.text = comLst!![position].updated
////            Glide.with(con).load(comLst!![position].image).into(holder.item.proImg)
//            holder.item.projName.setOnClickListener {
//                lstnr.onPrjctSrchClk(
//                    position,
//                    id = comLst!![position].id,
//                    isTxt = true,
//                    isAddInteraction = false,
//                    isTag = false,
//                    isReminder = false,
//                    isFav = false, comObj = comLst!![position], proObj = null, comkeywordObj = null
//
//                )
//            }
//            holder.item.im2.setOnClickListener {
//                lstnr.onPrjctSrchClk(
//                    position,
//                    id = comLst!![position].id,
//                    isTxt = false,
//                    isAddInteraction = false,
//                    isTag = true,
//                    isReminder = false,
//                    isFav = false, comObj = comLst!![position], proObj = null, comkeywordObj = null
//                )
//            }
//            holder.item.img1.setOnClickListener {
//                lstnr.onPrjctSrchClk(
//                    position,
//                    id = comLst!![position].id,
//                    isTxt = false,
//                    isAddInteraction = true,
//                    isTag = false,
//                    isReminder = false,
//                    isFav = false, comObj = comLst!![position], proObj = null, comkeywordObj = null
//                )
//            }
//            holder.item.img3.setOnClickListener {
//                lstnr.onPrjctSrchClk(
//                    position,
//                    id = comLst!![position].id,
//                    isTxt = false,
//                    isAddInteraction = false,
//                    isTag = false,
//                    isReminder = true,
//                    isFav = false, comObj = comLst!![position], proObj = null, comkeywordObj = null
//                )
//            }
//            holder.item.im4.setOnClickListener {
//                lstnr.onPrjctSrchClk(
//                    position,
//                    id = comLst!![position].id,
//                    isTxt = false,
//                    isAddInteraction = false,
//                    isTag = false,
//                    isReminder = false,
//                    isFav = true, comObj = comLst!![position], proObj = null, comkeywordObj = null
//                )
//            }
//        }

    }

    override fun getItemCount(): Int {
        return if (comLst != null) comLst?.size ?: 0 else comlstkeyword?.size ?: 0
    }

    class ViewHolder(val item: CompanySearchDataItemLayBinding) : RecyclerView.ViewHolder(item.root)
}