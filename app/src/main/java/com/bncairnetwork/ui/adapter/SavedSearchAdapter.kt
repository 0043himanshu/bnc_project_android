package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.SavedSearchDataItemBinding
import com.bncairnetwork.helper.SavedSearchListnr
import com.bncairnetwork.pojo.response.Dashboard.ContentItem

class SavedSearchAdapter(
    var bdt_con: Context,
    var lst: ArrayList<ContentItem?>? = null,
    var listnr: SavedSearchListnr
) :
    RecyclerView.Adapter<SavedSearchAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            SavedSearchDataItemBinding.inflate(LayoutInflater.from(bdt_con), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        lst?.let { obj ->
            holder.item_bldr.lbl1Val.text = obj[position]?.name ?: ""
            holder.item_bldr.lbl2Val.text = (obj[position]?.addedCount ?: 0).toString()
            holder.item_bldr.lbl3Val.text = (obj[position]?.updatedCount ?: 0).toString()
            holder.item_bldr.lbl2Val.setOnClickListener {
                if (obj[position]?.addedCount != 0) {
                    listnr.onSaveSearchColumnClk(
                        position,
                        isAddedClick = true,
                        isUpdatedClick = false,
                        obj[position]?.addedUrl
                    )
                }

            }
            holder.item_bldr.lbl3Val.setOnClickListener {
                if (obj[position]?.updatedCount != 0) {
                    listnr.onSaveSearchColumnClk(
                        position,
                        isAddedClick = false,
                        isUpdatedClick = true,
                        obj[position]?.updatedUrl
                    )
                }

            }
        }


    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item_bldr: SavedSearchDataItemBinding) :
        RecyclerView.ViewHolder(item_bldr.root) {

    }
}