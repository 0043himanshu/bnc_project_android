package com.bncairnetwork.ui.adapter.mapViewAptr

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ProjectSearchItemLayBinding
import com.bncairnetwork.helper.ProjectSeachListner
import com.bncairnetwork.helper.TagListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.helper.UserRestrictionRepo
import com.bncairnetwork.pojo.ProjectDetailsDataObj
import com.bncairnetwork.ui.adapter.project.ProjectTagAptr
import com.bumptech.glide.Glide

class BottomProjectSheetAptr(
    var con: Context,
    var proLst: ArrayList<ProjectDetailsDataObj>?,
    var lstnr: ProjectSeachListner,
    var packageID: Int,var tagListnr: TagListnr
) :
    RecyclerView.Adapter<BottomProjectSheetAptr.ViewHolder>() {
    private val viewPool = RecyclerView.RecycledViewPool()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProjectSearchItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val childLayoutManager =
            LinearLayoutManager(holder.item.tagRv.context, LinearLayoutManager.HORIZONTAL, false)
        // childLayoutManager.initialPrefetchItemCount = 4
        holder.item.projIc.setImageResource(
            if (proLst!![position].record_type?.linked == true && proLst!![position].record_type?.type == "IBIS") R.drawable.ic_linked_project
            else if (proLst!![position].record_type?.type == "IBIS" && proLst!![position].record_type?.linked == false) R.drawable.ic_internal
            else if (proLst!![position].record_type?.type == "BNC") R.drawable.ic_b else 0
        )
        holder.item.underConstrLbl.text = proLst!![position].stage ?: ""
        holder.item.tagRv.apply {
            layoutManager = childLayoutManager
            adapter =
                ProjectTagAptr(
                    proLst!![position].tag_obj?.selectedTags,tagListnr
                )
            setRecycledViewPool(viewPool)
        }
        proLst?.let {
            holder.item.projName.text = proLst!![position].name
            holder.item.projCountry.text = proLst!![position].city.plus(", ")
            holder.item.projState.text = proLst!![position].country
            holder.item.updateDate.text = proLst!![position].updated
            holder.item.updateDate.visibility =
                if (proLst!![position].authorised == true) View.VISIBLE else View.GONE
            holder.item.updateDatelbl.visibility =
                if (proLst!![position].authorised == true) View.VISIBLE else View.GONE

            holder.item.im4.setImageResource(if (proLst!![position].tag_obj?.isFavourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)

            holder.item.proAmt.text =
                "$".plus(
                    Uitls.getMillionBillionTrillion(proLst!![position].value.toFloat())
                        .replace(".0", "")
                )
            holder.item.proAmt.visibility =
                if (proLst!![position].authorised == true) View.VISIBLE else View.GONE


            Glide.with(con).load(proLst!![position].image).placeholder(Uitls.GliderPlaceHolder(con))
                .into(holder.item.proImg)
            holder.item.im2.setImageResource(if (proLst!![position].tag_obj?.selectedTags?.isNotEmpty() == true) R.drawable.ic_tag_active else R.drawable.ic_tag_inactive)
            holder.item.img3.setImageResource(if (proLst!![position].identfierObj?.has_reminder == true) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive)
            holder.itemView.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = proLst!![position].id,
                    isTxt = true,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = false,
                    comObj = null,
                    proObj = proLst!![position],
                    comkeywordObj = null

                )

            }
            holder.item.im2.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = proLst!![position].id,
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = proLst!![position].authorised == true,
                    isReminder = false,
                    isFav = false, comObj = null, proObj = proLst!![position], comkeywordObj = null
                )
            }
            holder.item.img1.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = proLst!![position].id,
                    isTxt = false,
                    isAddInteraction = proLst!![position].authorised == true,
                    isTag = false,
                    isReminder = false,
                    isFav = false, comObj = null, proObj = proLst!![position], comkeywordObj = null
                )
            }
            holder.item.img3.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = proLst!![position].id,
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = proLst!![position].authorised == true,
                    isFav = false, comObj = null, proObj = proLst!![position], comkeywordObj = null
                )
            }
            holder.item.im4.setOnClickListener {
                lstnr.onPrjctSrchClk(
                    position,
                    id = proLst!![position].id,
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = proLst!![position].authorised == true,
                    comObj = null,
                    proObj = proLst!![position],
                    comkeywordObj = null
                )
            }

            UserRestrictionRepo.interactionTagReminderFavoriteSwitchRestriction(
                interactionView = holder.item.img1,
                TagView = holder.item.im2,
                ReminderView = holder.item.img3,
                FavoriteView = holder.item.im4,
                SwitchView = null,
                packageID = packageID,
                isCompanyView = false,
                isProjectView = true
            )
        }


    }

    override fun getItemCount(): Int {
        return proLst?.size ?: 0
    }

    class ViewHolder(val item: ProjectSearchItemLayBinding) : RecyclerView.ViewHolder(item.root)
}