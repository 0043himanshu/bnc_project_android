package com.bncairnetwork.ui.adapter

import android.content.Context
import android.util.Patterns.EMAIL_ADDRESS
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.EmailAdptrDataItemLayBinding
import com.bncairnetwork.helper.Uitls

class EmailAdptr(val con: Context, private val emailLst: ArrayList<String?>) :
    RecyclerView.Adapter<EmailAdptr.M_ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): M_ViewHolder {
        return M_ViewHolder(
            EmailAdptrDataItemLayBinding.inflate(
                LayoutInflater.from(con)
            )
        )
    }

    override fun getItemCount(): Int {
        return emailLst.size
    }

    inner class M_ViewHolder(val item: EmailAdptrDataItemLayBinding) :
        RecyclerView.ViewHolder(item.root)

    override fun onBindViewHolder(holder: M_ViewHolder, position: Int) {
        holder.item.emailNolbl.text =
            emailLst[position]

        holder.itemView.setOnClickListener {
            if (EMAIL_ADDRESS.matcher(emailLst[position].toString()).matches()) {
                Uitls.makeEmailIntent(con, email = emailLst[position].toString())
            }
        }
    }

}