package com.bncairnetwork.ui.adapter.reminder

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ReminderCusItemLayBinding
import com.bncairnetwork.helper.ReminderByIDListnr
import com.bncairnetwork.helper.formatTo
import com.bncairnetwork.helper.toDate
import com.bncairnetwork.pojo.response.reminder.project_and_company.RemindersItemByID
import com.bncairnetwork.webservices.ApiConstants
import java.util.*

class ReminderByIDAptr(
    var con: Context,
    var lst: ArrayList<RemindersItemByID>,
    var lstnr: ReminderByIDListnr
) :
    RecyclerView.Adapter<ReminderByIDAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ReminderCusItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.ReminderTxtLbl.text = lst[position].reminder ?: ""
        holder.item.compnyLbl.text = lst[position].company_name ?: lst[position].project_name ?: ""
        holder.item.lbl1.text =
            if (lst[position].company_name != null) "Company Name" else "Project Name"
        holder.item.stsLbl.text = lst[position].status ?: "Pending"
        holder.item.mrkAsCompelteBtn.text =
            if (lst[position].status.equals(ApiConstants.Pending)) "Mark as Complete" else "Completed"
        holder.item.reminderLbl.text = lst[position].reminderDate!!.toDate("dd MMM,yyyy hh:mm a")
            .formatTo("dd MMM,yyyy hh:mm a")
        println(
            "~~~~~~~~${
                lst[position].reminderDate!!.toDate("dd MMM,yyyy hh:mm a")
                    .formatTo("dd MMM,yyyy hh:mm a")
                    
            }"
        )

        holder.item.deletebtn.setOnClickListener {
            lstnr.onRrminderClkByID(
                position,
                "",
                isDel = true,
                isEdit = false,
                isShare = false,
                isMarkCom = false,
                data = lst[position]
            )
        }
        holder.item.editbtn.setOnClickListener {
            lstnr.onRrminderClkByID(
                position,
                "",
                isDel = false,
                isEdit = true,
                isShare = false,
                isMarkCom = false,
                data = lst[position]
            )
        }
        holder.item.sharebtn.setOnClickListener {
            lstnr.onRrminderClkByID(
                position,
                "",
                isDel = false,
                isEdit = false,
                isShare = true,
                isMarkCom = false,
                data = lst[position]
            )
        }
        if (lst[position].status.equals(ApiConstants.Pending))
            holder.item.mrkAsCompelteBtn.setOnClickListener {
                lstnr.onRrminderClkByID(
                    position,
                    "",
                    isDel = false,
                    isEdit = false,
                    isShare = false,
                    isMarkCom = true,
                    data = lst[position]
                )

            }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    class ViewHolder(val item: ReminderCusItemLayBinding) : RecyclerView.ViewHolder(item.root)
}