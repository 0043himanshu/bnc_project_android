package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ContactItemLayBinding
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.KeyContact.KeyContactsItem
import com.bncairnetwork.ui.adapter.EmailAdptr
import com.bncairnetwork.ui.adapter.PhoneNumberAdptr


class ContactAptr(
    var con: Context,
    var lst: List<KeyContactsItem?>?
) :
    RecyclerView.Adapter<ContactAptr.ViewHolder>() {
    private val viewPool = RecyclerView.RecycledViewPool()
    private val viewPool1 = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ContactItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.nameval.text = lst?.get(position)?.name
        holder.item.desigval.text = lst?.get(position)?.designation
        holder.item.email.visibility =
            if (lst?.get(position)?.email?.isNotEmpty() == true) View.VISIBLE else View.GONE
        holder.item.emailval.visibility =
            if (lst?.get(position)?.email?.isNotEmpty() == true) View.VISIBLE else View.GONE

//        holder.item.emailval.text =
//            lst?.get(position)?.email.toString().replace("[", "").replace("]", "")

        holder.item.nolblval.apply {
            layoutManager = LinearLayoutManager(
                holder.item.nolblval.context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = PhoneNumberAdptr(
                holder.item.nolblval.context,
                lst?.get(position)?.phone!!
            )
            setRecycledViewPool(viewPool)
        }

        holder.item.emailval.apply {
            layoutManager = LinearLayoutManager(
                holder.item.emailval.context,
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = EmailAdptr(
                holder.item.nolblval.context,
                lst?.get(position)?.email!!
            )
            setRecycledViewPool(viewPool1)
        }

    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item: ContactItemLayBinding) : RecyclerView.ViewHolder(item.root)
}