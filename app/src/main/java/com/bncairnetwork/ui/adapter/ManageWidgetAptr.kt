package com.bncairnetwork.adapter

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ManageWidgetDataItemLayBinding
import com.bncairnetwork.helper.ItemMoveCallback
import com.bncairnetwork.helper.UpdateWidget
import com.bncairnetwork.pojo.response.WidgetsItem
import java.util.*


class ManageWidgetAptr(
    var con: Context,
    var lst: ArrayList<WidgetsItem>,
    var listr: UpdateWidget
) :
    RecyclerView.Adapter<ManageWidgetAptr.ViewHolder>(), ItemMoveCallback.ItemTouchHelperContract {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ManageWidgetDataItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.widgetLbl.text = lst[position].title ?: ""
        holder.item.widgetBtn.setOnClickListener {
            listr.onUpdate(
                position,
                if (!holder.item.widgetBtn.isChecked) lst[position].userWidgetId.toString() else lst[position].id.toString(),
                !(lst[position].status ?: false),
                lst[position].priority.toString()
            )
        }
        holder.item.widgetBtn.isChecked = lst[position].status ?: false
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    class ViewHolder(val item: ManageWidgetDataItemLayBinding) : RecyclerView.ViewHolder(item.root)

    override fun onRowMoved(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(lst, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(lst, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
    }

    override fun onRowSelected(myViewHolder: ViewHolder?) {
        myViewHolder?.itemView?.setBackgroundColor(Color.GRAY)
    }

    override fun onRowClear(myViewHolder: ViewHolder?) {
        myViewHolder?.itemView?.setBackgroundColor(Color.WHITE)
    }
}