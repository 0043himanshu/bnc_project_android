package com.bncairnetwork.ui.adapter.project

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.TagItemCusLayBinding
import com.bncairnetwork.helper.TagListnr

class ProjectTagAptr(
    var tagLst: ArrayList<String>?, var listnr: TagListnr
) :
    RecyclerView.Adapter<ProjectTagAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            TagItemCusLayBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.textView9.text = tagLst?.get(position)
        holder.itemView.setOnClickListener {
            listnr.onTagClick(tagLst?.get(position))
        }

    }

    override fun getItemCount(): Int {
        return tagLst?.size ?: 0
    }

    class ViewHolder(val item: TagItemCusLayBinding) : RecyclerView.ViewHolder(item.root)
}