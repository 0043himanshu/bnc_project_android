package com.bncairnetwork.ui.adapter.project_advance_search

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.AddConditionsUnderDatesItemLayBinding
import com.bncairnetwork.helper.AddConditionListnr
import com.bncairnetwork.helper.TypeNdSubTypeSpListnr
import com.bncairnetwork.pojo.AddConditionDates
import com.google.android.material.dialog.MaterialAlertDialogBuilder

class AddConditionsUnderDatesSectionAptr(
    var con: Context,
    var lst: ArrayList<AddConditionDates>,
    var listnr: AddConditionListnr,
    var spLisntr: TypeNdSubTypeSpListnr
) :
    RecyclerView.Adapter<AddConditionsUnderDatesSectionAptr.ViewHolder>() {
    lateinit var materialDialogs: MaterialAlertDialogBuilder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        materialDialogs = MaterialAlertDialogBuilder(
            con,
            R.style.MyRounded_MaterialComponents_MaterialAlertDialog
        )
        return ViewHolder(
            AddConditionsUnderDatesItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        holder.item.comnyExdt.setText("")
//        holder.item.inclueBidderRd.isChecked = false
//        holder.item.inclueCncledContrctRd.isChecked = false
//        holder.item.matchExactRd.isChecked = false

        holder.item.crossAddCondtion.setOnClickListener {
            listnr.onAddCondition(
                position,
                null,obj2 = null, isUnderCompanies = false, isUnderDates = true
            )
        }
        holder.item.typeSp.setOnClickListener {
            spLisntr.onSpClck(
                position,
                isTypeSp = true,
                isSubTypeSp = false,
                TypeHashMap = lst[position].types ?: hashMapOf(),
                SubTypeHashMap = lst[position].subtypes ?: hashMapOf()
            )
        }
        holder.item.SubtypeSp.setOnClickListener {
            spLisntr.onSpClck(
                position,
                isTypeSp = false,
                isSubTypeSp = true,
                TypeHashMap = lst[position].types ?: hashMapOf(),
                SubTypeHashMap = lst[position].subtypes ?: hashMapOf()
            )
        }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    class ViewHolder(val item: AddConditionsUnderDatesItemLayBinding) :
        RecyclerView.ViewHolder(item.root) {
        init {
            setIsRecyclable(false)
        }
    }


}