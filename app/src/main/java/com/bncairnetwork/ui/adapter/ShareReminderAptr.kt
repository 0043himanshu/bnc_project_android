package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ShareReminderDataItemLayBinding
import com.bncairnetwork.helper.ShareWithUserListr
import com.bncairnetwork.pojo.response.FullUsersItem
import java.util.*
import kotlin.collections.ArrayList

class ShareReminderAptr(
    var con: Context,
    var lst: ArrayList<FullUsersItem?>,
    var listnr: ShareWithUserListr
) :
    RecyclerView.Adapter<ShareReminderAptr.ViewHolder>(), Filterable {
    var userFilterList = ArrayList<FullUsersItem?>()

    init {
        userFilterList = lst
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ShareReminderDataItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.name.text = userFilterList[position]?.name
        holder.item.chbox.setOnCheckedChangeListener { _, isChecked ->
            listnr.onSharedWithUserClick(position, isChecked, userFilterList[position])
        }
    }

    override fun getItemCount(): Int {
        return userFilterList.size
    }

    class ViewHolder(val item: ShareReminderDataItemLayBinding) : RecyclerView.ViewHolder(item.root)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                userFilterList = if (charSearch.isEmpty()) {
                    lst
                } else {
                    val resultList = ArrayList<FullUsersItem?>()
                    for (row in lst) {
                        if (row?.name?.lowercase(Locale.ROOT)
                                ?.contains(charSearch.lowercase(Locale.ROOT)) == true
                        ) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = userFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                userFilterList = results?.values as ArrayList<FullUsersItem?>
                notifyDataSetChanged()
            }

        }
    }
}