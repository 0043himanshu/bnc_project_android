package com.bncairnetwork.ui.adapter.map

import com.bncairnetwork.pojo.ProjectDetailsDataObj
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem

class ClusterItemAdapter(var data: ProjectDetailsDataObj?) : ClusterItem {
    override fun getPosition(): LatLng = LatLng(
        data?.latlongstring?.substring(0, data?.latlongstring?.indexOf(",") ?: 0)?.toDouble()
            ?: 0.0,
        data?.latlongstring?.substring(
            startIndex = (data?.latlongstring?.indexOf(",")?.plus(1))?:0,
            endIndex = data?.latlongstring?.length ?: 0
        )?.toDouble() ?: 0.0
    )

    override fun getTitle(): String? = data?.name
    override fun getSnippet(): String? = data?.name
}