package com.bncairnetwork.ui.adapter.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.NotificationCusLayBinding
import com.bncairnetwork.helper.NotificationLisntr
import com.bncairnetwork.pojo.response.notification.ReminderNotificationDataItem

class ReminderNotificationAptr(
    var con: Context,
    var lst: ArrayList<ReminderNotificationDataItem>?,
    var listnr: NotificationLisntr
) :
    RecyclerView.Adapter<ReminderNotificationAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            NotificationCusLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.notiLbl.text = lst?.get(position)?.reminder ?: ""
        holder.item.notiImg.setImageResource(R.drawable.ic_bell_notification)
        holder.item.notiLbl.setOnClickListener {
            listnr.onNotificationClick(
                pos = position,
                reminderNotificationObj = lst?.get(position),
                nonReminderObj = null
            )
        }
    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item: NotificationCusLayBinding) : RecyclerView.ViewHolder(item.root)
}