package com.bncairnetwork.ui.adapter.project

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ProjectNewsHihglightItemLayBinding
import com.bncairnetwork.helper.ProNewsNdHighlightListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.DataItem
import com.bumptech.glide.Glide

class ProjectNewsHighlightsAptr(
    var con: Context,
    var lst: List<DataItem?>? = null,
    var listnr: ProNewsNdHighlightListnr
) :
    RecyclerView.Adapter<ProjectNewsHighlightsAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProjectNewsHihglightItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.projNewsTitle.text = lst?.get(position)?.name ?: ""
        holder.item.projNewsDesc.text =
            lst?.get(position)?.city?.plus(", ").plus(lst?.get(position)?.country)
        Glide.with(con).load(lst?.get(position)?.projImageUrl ?: "")
            .placeholder(Uitls.GliderPlaceHolder(con)).into(holder.item.proImg)
        holder.itemView.setOnClickListener {
            listnr.onProNewsNdHighlightClk(position, lst?.get(position))
        }

    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item: ProjectNewsHihglightItemLayBinding) :
        RecyclerView.ViewHolder(item.root)
}