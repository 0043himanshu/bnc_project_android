package com.bncairnetwork.ui.adapter.notification

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.NotificationCusLayBinding
import com.bncairnetwork.helper.NotificationLisntr
import com.bncairnetwork.pojo.response.notification.NotificationsItem

class NotificationAptr(
    var con: Context,
    var lst: ArrayList<NotificationsItem?>?,
    var listnr: NotificationLisntr
) :
    RecyclerView.Adapter<NotificationAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            NotificationCusLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.notiImg.setImageResource(R.drawable.ic_non_reminder_notification)
        holder.item.notiLbl.text = lst?.get(position)?.notification?.android?.title ?: ""
        holder.item.notiLbl.setOnClickListener {
            listnr.onNotificationClick(
                pos = position,
                reminderNotificationObj = null,
                nonReminderObj = lst?.get(position)
            )
        }
    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item: NotificationCusLayBinding) : RecyclerView.ViewHolder(item.root)
}