package com.bncairnetwork.ui.adapter.interaction

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.IntractionCusItemLayBinding
import com.bncairnetwork.helper.InteractionLisntr
import com.bncairnetwork.helper.UserRestrictionRepo
import com.bncairnetwork.pojo.response.company.InteractionsItem
import com.bncairnetwork.pojo.response.project.InteractionDataItem

class IntractionListAdapter(
    var intc_con: Context,
    var projctlst: ArrayList<InteractionDataItem?>? = null, //interaction project list
    var cmylst: ArrayList<InteractionsItem?>? = null,//interaction company list,
    var lisnr: InteractionLisntr,
    var packageID: Int
) :
    RecyclerView.Adapter<IntractionListAdapter.ViewHolder>() {
    private val viewPool = RecyclerView.RecycledViewPool()


    class ViewHolder(val item: IntractionCusItemLayBinding) :
        RecyclerView.ViewHolder(item.root) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            IntractionCusItemLayBinding.inflate(
                LayoutInflater.from(intc_con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        UserRestrictionRepo.interactionListAdptrRestriction(holder.item, packageID = packageID)
        projctlst?.let { ob ->
            val childLayoutManager =
                LinearLayoutManager(
                    holder.item.compnyRv.context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            holder.item.inctnValLbl.text = ob[position]?.interaction ?: ""
            holder.item.inctnType.text = ob[position]?.interactionType ?: ""
            //   holder.item.compnyLbl.text = ob[position]?.company?.get(0)?.company ?: ""
            holder.item.compnyRv.apply {
                layoutManager = childLayoutManager
                adapter =
                    InteractionChildAdptr(holder.item.compnyRv.context, ob[position]?.company)
                setRecycledViewPool(viewPool)
            }
            if (ob[position]?.company_contact?.isNullOrEmpty() != true) {
                holder.item.lbl2.visibility = View.VISIBLE
                holder.item.cretdOnLbl.visibility = View.VISIBLE
                holder.item.cretdOnLbl.text = ob[position]?.company_contact!!.toString()
                    .replace("[", "").replace("]", "")
            } else {
                holder.item.lbl2.visibility = View.GONE
                holder.item.cretdOnLbl.visibility = View.GONE

            }
            holder.item.cretdOnLbl.visibility =
                if (ob[position]?.company_contact?.isNullOrEmpty() != true) View.VISIBLE else View.GONE
            holder.item.cretdOnLbl.text =
                if (ob[position]?.company_contact?.isNullOrEmpty() != true) ob[position]?.company_contact!!.toString()
                    .replace("[", "").replace("]", "") else ""
            ob[position]?.users?.forEachIndexed { index, it ->
                holder.item.shareWithD.append(
                    it?.name ?: "".plus(
                        if (index != ob[position]?.users?.size) ", " else " ,"
                    )
                )
            }
            holder.item.crtDate.text = ob[position]?.date ?: ""
            holder.item.reminderDateValLbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE
            holder.item.remindervallbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE
            holder.item.reminderdatelbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE
            holder.item.reminderlbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE


            holder.item.reminderDateValLbl.text = ob[position]?.reminder?.date ?: ""
            holder.item.remindervallbl.text = ob[position]?.reminder?.reminder ?: ""
        }
        cmylst?.let { ob ->
            val childLayoutManager =
                LinearLayoutManager(
                    holder.item.compnyRv.context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
            holder.item.inctnValLbl.text = ob[position]?.interaction ?: ""
            holder.item.inctnType.text = ob[position]?.interaction_type ?: ""
            holder.item.compnyRv.apply {
                layoutManager = childLayoutManager
                adapter =
                    InteractionChildAdptr(holder.item.compnyRv.context, ob[position]?.company)
                setRecycledViewPool(viewPool)
            }
            holder.item.lbl2.visibility = View.GONE
            holder.item.cretdOnLbl.visibility = View.GONE
            holder.item.shareWithD.text = ""
            holder.item.crtDate.text = ob[position]?.date ?: ""
            holder.item.reminderDateValLbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE
            holder.item.remindervallbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE
            holder.item.reminderdatelbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE
            holder.item.reminderlbl.visibility =
                if (ob[position]?.reminder != null) View.VISIBLE else View.GONE


            holder.item.reminderDateValLbl.text = ob[position]?.reminder?.date ?: ""
            holder.item.remindervallbl.text = ob[position]?.reminder?.reminder ?: ""
        }

        holder.item.deletebtn.setOnClickListener {
            lisnr.onInteractionLstnr(
                pos = position,
                obj1 = if (projctlst != null) projctlst?.get(position) else null,
                obj2 = if (cmylst != null) cmylst?.get(position) else null,
                isEdit = false,
                isDel = true,
                isShare = false
            )
        }
        holder.item.editbtn.setOnClickListener {
            lisnr.onInteractionLstnr(
                pos = position,
                obj1 = if (projctlst != null) projctlst?.get(position) else null,
                obj2 = if (cmylst != null) cmylst?.get(position) else null,
                isEdit = true,
                isDel = false,
                isShare = false
            )

        }
        holder.item.sharebtn.setOnClickListener {
            lisnr.onInteractionLstnr(
                pos = position,
                obj1 = if (projctlst != null) projctlst?.get(position) else null,
                obj2 = if (cmylst != null) cmylst?.get(position) else null,
                isEdit = false,
                isDel = false,
                isShare = true
            )
        }
//        holder.item.mrkAsCompelteBtn.setOnClickListener {
//            lstnr.newonRrminderClk(
//                position,
//                "",
//                isDel = false,
//                isEdit = false,
//                isShare = false,
//                isMarkCom = true
//
//            )
//        }


    }

    override fun getItemCount(): Int {
        return projctlst?.size ?: cmylst?.size ?: 0
    }
}