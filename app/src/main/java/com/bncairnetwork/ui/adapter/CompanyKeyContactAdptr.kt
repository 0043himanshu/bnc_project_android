package com.bncairnetwork.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.CompanySearchItemLayBinding
import com.bncairnetwork.helper.CompanyBiddrsListner
import com.bncairnetwork.helper.TagListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.helper.UserRestrictionRepo
import com.bncairnetwork.pojo.response.company.CompaniesItem
import com.bncairnetwork.pojo.response.compnyAndBidders.BiddersItem
import com.bncairnetwork.ui.adapter.project.ProjectTagAptr
import com.bumptech.glide.Glide


class CompanyKeyContactAdptr(
    var con: Context,
    var comlst: ArrayList<CompaniesItem>? = null,
    var biddrlst: ArrayList<BiddersItem?>? = null,
    var listnr: CompanyBiddrsListner,
    var packageID: Int, var tagListnr: TagListnr
) :
    RecyclerView.Adapter<CompanyKeyContactAdptr.ViewHolder>() {

    private val viewPool = RecyclerView.RecycledViewPool()
    private val viewPool2 = RecyclerView.RecycledViewPool()
    private val viewPool3 = RecyclerView.RecycledViewPool()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CompanySearchItemLayBinding.inflate(
                LayoutInflater.from(con), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val childLayoutManager =
            LinearLayoutManager(holder.item.tagRv.context, LinearLayoutManager.HORIZONTAL, false)

        UserRestrictionRepo.interactionTagReminderFavoriteSwitchRestriction(
            interactionView = holder.item.img1,
            TagView = holder.item.img2,
            ReminderView = holder.item.img3,
            FavoriteView = holder.item.img4,
            SwitchView = null,
            packageID = packageID,
            isCompanyView = true,
            isProjectView = false
        )
        holder.item.img2.setOnClickListener {
            listnr.onClk(
                position,
                id = if (!comlst.isNullOrEmpty()) comlst!![position].id.toString() else biddrlst!![position]?.id.toString(),
                isTxt = false,
                isAddInteraction = false,
                isTag = true,
                isReminder = false,
                isFav = false,
                compnyObj = if (!comlst.isNullOrEmpty()) comlst!![position] else null,
                biddrObj = if (biddrlst?.isNullOrEmpty() != true) biddrlst!![position] else null
            )
        }

        holder.itemView.setOnClickListener {
            listnr.onClk(
                position,
                id = if (!comlst.isNullOrEmpty()) comlst!![position].id.toString() else biddrlst!![position]?.id.toString(),
                isTxt = true,
                isAddInteraction = false,
                isTag = false,
                isReminder = false,
                isFav = false,
                compnyObj = if (!comlst.isNullOrEmpty()) comlst!![position] else null,
                biddrObj = if (biddrlst?.isNullOrEmpty() != true) biddrlst!![position] else null

            )
        }

        holder.item.img1.setOnClickListener {
            listnr.onClk(
                position,
                id = if (!comlst.isNullOrEmpty()) comlst!![position].id.toString() else biddrlst!![position]?.id.toString(),
                isTxt = false,
                isAddInteraction = true,
                isTag = false,
                isReminder = false,
                isFav = false,
                compnyObj = if (!comlst.isNullOrEmpty()) comlst!![position] else null,
                biddrObj = if (biddrlst?.isNullOrEmpty() != true) biddrlst!![position] else null
            )
        }
        holder.item.img3.setOnClickListener {
            listnr.onClk(
                position,
                id = if (!comlst.isNullOrEmpty()) comlst!![position].id.toString() else biddrlst!![position]?.id.toString(),
                isTxt = false,
                isAddInteraction = false,
                isTag = false,
                isReminder = true,
                isFav = false,
                compnyObj = if (!comlst.isNullOrEmpty()) comlst!![position] else null,
                biddrObj = if (biddrlst?.isNullOrEmpty() != true) biddrlst!![position] else null
            )
        }
        holder.item.img4.setOnClickListener {
            listnr.onClk(
                position,
                id = if (!comlst.isNullOrEmpty()) comlst!![position].id.toString() else biddrlst!![position]?.id.toString(),
                isTxt = false,
                isAddInteraction = false,
                isTag = false,
                isReminder = false,
                isFav = true,
                compnyObj = if (!comlst.isNullOrEmpty()) comlst!![position] else null,
                biddrObj = if (biddrlst?.isNullOrEmpty() != true) biddrlst!![position] else null
            )
        }

        holder.item.tagRv.apply {
            layoutManager = childLayoutManager
            adapter =
                ProjectTagAptr(
                    if (!comlst.isNullOrEmpty()) comlst!![position].tag_obj?.selectedTags else biddrlst!![position]?.tag_obj?.selectedTags,
                    tagListnr
                )
            setRecycledViewPool(viewPool)
        }
        if (!comlst.isNullOrEmpty()) {
            holder.item.cmpyTitle.text = comlst!![position].name ?: ""
            Glide.with(con).load(comlst!![position].countryFlag ?: "").into(holder.item.cmpyImgItem)
            holder.item.cmpyLocTxt.text = comlst!![position].country ?: ""
            holder.item.cmpyRolrTxt.text = comlst!![position].role ?: ""
            holder.item.cmpyTypeTxt.text = comlst!![position].companyType ?: ""
            try {
                holder.item.cmpyPhn1Txt.apply {
                    layoutManager = LinearLayoutManager(
                        holder.item.cmpyPhn1Txt.context,
                        LinearLayoutManager.VERTICAL,
                        false
                    )
                    adapter =
                        PhoneNumberAdptr(
                            holder.item.cmpyPhn1Txt.context,
                            comlst!![position].phone!!
                        )
                    setRecycledViewPool(viewPool2)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


            holder.item.cmpyEmlTxt.apply {
                layoutManager = LinearLayoutManager(
                    holder.item.cmpyEmlTxt.context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                adapter =
                    EmailAdptr(
                        holder.item.cmpyEmlTxt.context,
                        comlst!![position].email!!
                    )
                setRecycledViewPool(viewPool3)
            }
            if (comlst!![position].keyContactObj?.isNullOrEmpty() != true) {
                holder.item.constraintLayout4.visibility = View.VISIBLE
                holder.item.textView28.text =
                    comlst!![position].keyContactObj?.get(
                        0
                    )?.name ?: ""



                holder.item.cmpyKyphTxt.text =
                    comlst!![position].keyContactObj?.get(
                        0
                    )?.mobile?.number ?: ""

                holder.item.cmpyKyphTxt.setOnClickListener {
                    Uitls.makeDialIntent(
                        con = con,
                        number = holder.item.cmpyKyphTxt.text.toString()
                    )
                }

                holder.item.cmpyKycntemlTxt.text =
                    comlst!![position].keyContactObj?.get(
                        0
                    )?.email?.email ?: ""
                holder.item.cmpyKycntemlTxt.setOnClickListener {
                    Uitls.makeEmailIntent(
                        con = con,
                        email = holder.item.cmpyKycntemlTxt.text.toString()
                    )
                }

            } else {
                holder.item.constraintLayout4.visibility = View.GONE
            }

            holder.item.cmpyWebTxt.text =
                comlst!![position].web.toString().replace("[", "").replace("]", "")
            holder.item.img4.setImageResource(if (comlst!![position].tag_obj?.isFavourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)
            holder.item.img2.setImageResource(if (comlst!![position].tag_obj?.selectedTags?.isNotEmpty() == true) R.drawable.ic_tag_active else R.drawable.ic_tag_inactive)
            holder.item.img3.setImageResource(if (comlst!![position].identfierObj?.has_reminder == true) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive)

            holder.item.textView25.visibility =
                if (holder.item.cmpyEmlTxt.adapter?.itemCount != 0) View.VISIBLE else View.GONE

            holder.item.cmpyEmlTxt.visibility =
                if (holder.item.cmpyEmlTxt.adapter?.itemCount != 0) View.VISIBLE else View.GONE

            holder.item.textView26.visibility =
                if (holder.item.cmpyWebTxt.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.cmpyWebTxt.visibility =
                if (holder.item.cmpyWebTxt.text.isNotEmpty()) View.VISIBLE else View.GONE


            holder.item.textView30.visibility =
                if (holder.item.cmpyKycntemlTxt.text.isNotEmpty()) View.VISIBLE else View.GONE
            holder.item.cmpyKycntemlTxt.visibility =
                if (holder.item.cmpyKycntemlTxt.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.textView27.visibility =
                if (holder.item.textView28.text.isNotEmpty()) View.VISIBLE else View.GONE
            holder.item.textView28.visibility =
                if (holder.item.textView28.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.textView29.visibility =
                if (holder.item.cmpyKyphTxt.text.isNotEmpty()) View.VISIBLE else View.GONE
            holder.item.cmpyKyphTxt.visibility =
                if (holder.item.cmpyKyphTxt.text.isNotEmpty()) View.VISIBLE else View.GONE

            return
        }

        if (biddrlst?.isNullOrEmpty() != true) {
            holder.item.cmpyTitle.text = biddrlst!![position]?.name ?: ""

//        holder.item.cmpyImgItem.setImageResource(lst[position].cmyimg)
            Glide.with(con).load(biddrlst!![position]?.countryFlag ?: "")
                .into(holder.item.cmpyImgItem)
            holder.item.cmpyLocTxt.text = biddrlst!![position]?.country ?: ""
            holder.item.cmpyRolrTxt.text = biddrlst!![position]?.role ?: ""
            holder.item.cmpyTypeTxt.text = biddrlst!![position]?.companyType ?: ""
            try {
                holder.item.cmpyPhn1Txt.apply {
                    layoutManager = LinearLayoutManager(
                        holder.item.tagRv.context,
                        LinearLayoutManager.VERTICAL,
                        false
                    )
                    adapter =
                        PhoneNumberAdptr(
                            holder.item.cmpyPhn1Txt.context,
                            biddrlst!![position]?.phone!!
                        )
                    setRecycledViewPool(viewPool2)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


//            holder.item.cmpyPhn1Txt.text =
//                biddrlst!![position]?.phone.toString().replace("[", "").replace("]", "")


            if (biddrlst!![position]?.keyContactObj?.isNullOrEmpty() != true) {
                holder.item.constraintLayout4.visibility = View.VISIBLE
                holder.item.textView28.text =
                    biddrlst!![position]?.keyContactObj?.get(
                        0
                    )?.name ?: ""
                holder.item.cmpyKyphTxt.text =
                    biddrlst!![position]?.keyContactObj?.get(
                        0
                    )?.mobile?.number ?: ""

                holder.item.cmpyKyphTxt.setOnClickListener {
                    Uitls.makeDialIntent(
                        con = con,
                        number = holder.item.cmpyKyphTxt.text.toString()
                    )
                }

                holder.item.cmpyKycntemlTxt.text =
                    biddrlst!![position]?.keyContactObj?.get(
                        0
                    )?.email?.email ?: ""

                holder.item.cmpyKycntemlTxt.setOnClickListener {
                    Uitls.makeEmailIntent(
                        con = con,
                        email = holder.item.cmpyKycntemlTxt.text.toString()
                    )
                }
            } else {
                holder.item.constraintLayout4.visibility = View.GONE
            }
//            holder.item.cmpyEmlTxt.text =
//                biddrlst!![position]?.email.toString().replace("[", "").replace("]", "")

            holder.item.cmpyEmlTxt.apply {
                layoutManager = LinearLayoutManager(
                    holder.item.cmpyEmlTxt.context,
                    LinearLayoutManager.VERTICAL,
                    false
                )
                adapter =
                    EmailAdptr(
                        holder.item.cmpyEmlTxt.context,
                        biddrlst!![position]?.email!!
                    )
                setRecycledViewPool(viewPool3)
            }


            holder.item.cmpyWebTxt.text =
                biddrlst!![position]?.web.toString().replace("[", "").replace("]", "")

            holder.item.textView25.visibility =
                if (holder.item.cmpyEmlTxt.adapter?.itemCount != 0) View.VISIBLE else View.GONE

            holder.item.cmpyEmlTxt.visibility =
                if (holder.item.cmpyEmlTxt.adapter?.itemCount != 0) View.VISIBLE else View.GONE

            holder.item.textView26.visibility =
                if (holder.item.cmpyWebTxt.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.cmpyWebTxt.visibility =
                if (holder.item.cmpyWebTxt.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.textView30.visibility =
                if (holder.item.cmpyKycntemlTxt.text.isNotEmpty()) View.VISIBLE else View.GONE
            holder.item.cmpyKycntemlTxt.visibility =
                if (holder.item.cmpyKycntemlTxt.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.textView27.visibility =
                if (holder.item.textView28.text.isNotEmpty()) View.VISIBLE else View.GONE
            holder.item.textView28.visibility =
                if (holder.item.textView28.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.textView29.visibility =
                if (holder.item.cmpyKyphTxt.text.isNotEmpty()) View.VISIBLE else View.GONE
            holder.item.cmpyKyphTxt.visibility =
                if (holder.item.cmpyKyphTxt.text.isNotEmpty()) View.VISIBLE else View.GONE

            holder.item.img2.setImageResource(if (biddrlst!![position]?.tag_obj?.selectedTags?.isNotEmpty() == true) R.drawable.ic_tag_active else R.drawable.ic_tag_inactive)
            holder.item.img4.setImageResource(if (biddrlst!![position]?.tag_obj?.isFavourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)
            holder.item.img3.setImageResource(if (biddrlst!![position]?.identfierObj?.has_reminder == true) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive)

            return
        }


        UserRestrictionRepo.CompanyNDBidder_interactionTagReminderFavoriteSwitchRestriction(
            interactionView = holder.item.img1,
            TagView = holder.item.img2,
            ReminderView = holder.item.img3,
            FavoriteView = holder.item.img4,
            SwitchView = null,
            packageID = packageID,
            isCompanyView = false,
            isProjectView = true
        )


    }

    override fun getItemCount(): Int {
        return if (!comlst.isNullOrEmpty()) comlst!!.size else biddrlst!!.size
    }

    class ViewHolder(val item: CompanySearchItemLayBinding) : RecyclerView.ViewHolder(item.root)
}