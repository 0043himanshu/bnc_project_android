package com.bncairnetwork.ui.adapter

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.MultiSelectionSpLayBinding
import com.bncairnetwork.helper.ContactListnr
import com.bncairnetwork.pojo.response.ContactsItem

class InteractionContactMultiSpinnerAdptr(
    val con: Context,
    val lst: ArrayList<ContactsItem>,
    private val selectionLst: ArrayList<String>,
    val listnr: ContactListnr
) :
    RecyclerView.Adapter<InteractionContactMultiSpinnerAdptr.MViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InteractionContactMultiSpinnerAdptr.MViewHolder {
        return MViewHolder(MultiSelectionSpLayBinding.inflate(LayoutInflater.from(con)))
    }

    override fun onBindViewHolder(
        holder: InteractionContactMultiSpinnerAdptr.MViewHolder,
        position: Int
    ) {
        holder.item.chkbox.text = lst[position].name ?: ""
        holder.item.chkbox.isChecked = selectionLst.contains(lst[position].name ?: "")
        holder.item.chkbox.setTypeface(
            holder.item.chkbox.typeface,
            if (lst[position].isCompanyName == true) Typeface.BOLD else Typeface.NORMAL
        )

        if (lst[position].isCompanyName == true)
            holder.item.chkbox.buttonDrawable = null
        holder.itemView.isEnabled = lst[position].isCompanyName != true
        if (lst[position].isCompanyName == true) holder.item.chkbox.setPadding(0, 10, 0, 0)

        holder.item.chkbox.setOnClickListener {
            listnr.onContactSelect(pos = position, obj = lst[position])
        }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    inner class MViewHolder(val item: MultiSelectionSpLayBinding) :
        RecyclerView.ViewHolder(item.root) {
        init {
            setIsRecyclable(false)
        }
    }

}