package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.TagItemLayBinding
import com.bncairnetwork.helper.EditTagListnr
import com.bncairnetwork.helper.UpdateAddTagListnr
import com.bncairnetwork.pojo.response.TagsItem

class TagAdapter(
    var tag_con: Context,
    var tag_lst: ArrayList<TagsItem?>? = null,
    var active_lst: ArrayList<String>? = null,
    var listnr: UpdateAddTagListnr,
    var editlistnr: EditTagListnr
) :
    RecyclerView.Adapter<TagAdapter.ViewHolder>() {
    class ViewHolder(val item_tag: TagItemLayBinding) : RecyclerView.ViewHolder(item_tag.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(TagItemLayBinding.inflate(LayoutInflater.from(tag_con), parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item_tag.tagAddItemTxt.text = tag_lst?.get(position)?.name ?: ""
        holder.item_tag.imageView11.isChecked =
            active_lst?.contains(tag_lst?.get(position)?.name ?: "") ?: false
        holder.item_tag.imageView11.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                listnr.onUpdateOrAddTag(
                    pos = holder.adapterPosition,
                    tagName = tag_lst?.get(holder.adapterPosition)?.name ?: "",
                    flag = holder.item_tag.imageView11.isChecked,
                    isremove = false
                )
            }
        })
        holder.item_tag.tagAddItemTxt.setOnClickListener {
            editlistnr.onTagClick(tag_lst?.get(position)?.name ?: "", position)
        }

        holder.item_tag.removetagBtn.setOnClickListener {
            listnr.onUpdateOrAddTag(
                pos = position,
                tagName = tag_lst?.get(position)?.name ?: "",
                flag = false,
                isremove = true
            )

        }
    }

    override fun getItemCount(): Int {
        return tag_lst?.size ?: 0
    }
}