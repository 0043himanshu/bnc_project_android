package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.ScheduleDataItemLayBinding
import com.bncairnetwork.pojo.response.schedules.SchedulesItem

class ScheduleAptr(
    var con: Context, var lst: ArrayList<SchedulesItem>?
) :
    RecyclerView.Adapter<ScheduleAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ScheduleDataItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.lbl.text = lst?.get(position)?.scheduleName ?: ""
        holder.item.lblVal.text = lst?.get(position)?.date ?: ""
    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item: ScheduleDataItemLayBinding) : RecyclerView.ViewHolder(item.root)
}