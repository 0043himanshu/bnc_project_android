/*
package com.bncairnetwork.adapter.project_tree

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ProjectTreeChildDataItemLayBinding
import com.bncairnetwork.helper.Enums
import com.bncairnetwork.helper.ProjectTreeLstnr
import com.bncairnetwork.pojo.response.project.ChilderDataItem
import com.bncairnetwork.ui.activities.projectConnections.ProjectDetailsAc


class ProjectChildTreeAdptr(
    var con: Context,
    var lst: ArrayList<ChilderDataItem>,
    var listr: ProjectTreeLstnr
) :
    RecyclerView.Adapter<ProjectChildTreeAdptr.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProjectTreeChildDataItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.projectNameLbl.text = lst[position].name
//        val lp = ConstraintLayout.LayoutParams(
//            ConstraintLayout.LayoutParams.WRAP_CONTENT,
//            ConstraintLayout.LayoutParams.WRAP_CONTENT
//        )
//        lp.setMargins(lst[position].paddingStart,0,0,0)
//        holder.item.projectNameImg.layoutParams = lp


        holder.item.projectNameLbl.paintFlags =
            holder.item.projectNameLbl.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        holder.item.projectNameLbl.setTextColor(
            if (lst[position].children.isNotEmpty()) ContextCompat.getColor(
                con,
                R.color.black
            ) else ContextCompat.getColor(
                con,
                R.color.linkColor
            )
        )

        holder.item.projtypeimg.setOnClickListener {
            println("=======================<<<<${lst[position].isChildViewed}")
            listr.onProjectTreeParntClk(
                pos = position,
                flag = true,
                childlst = lst[position].children,
                paddingStart = if (!lst[position].isChildViewed) holder.item.root.paddingStart else lst[position + 1].paddingStart
            )
//            if (lst[position].children.isNotEmpty())

        }
        holder.item.projectNameLbl.setOnClickListener {
            con.startActivity(
                Intent(
                    con,
                    ProjectDetailsAc::class.java
                ).putExtra(Enums.ID.toString(), lst[position].id)
            )
        }

        if (lst[position].childType == "IBIS" && lst[position].children.isEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_linked_project)
        } else if (lst[position].childType == "BNC" && lst[position].children.isEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_b)
        } else if (!lst[position].isChildViewed && lst[position].children.isNotEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_add_green)
        } else if (lst[position].isChildViewed && lst[position].children.isNotEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_minus_gray)
        } else {
            holder.item.projtypeimg.setImageResource(0)
        }
        holder.item.root.setPadding(lst[position].paddingStart, 0, 0, 0)
    }


    override fun getItemCount(): Int {
        return lst.size
    }

    class ViewHolder(val item: ProjectTreeChildDataItemLayBinding) :
        RecyclerView.ViewHolder(item.root)
}*/
