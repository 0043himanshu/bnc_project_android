package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.BlddrItemLayBinding
import com.bncairnetwork.pojo.BiddersModels
import com.bncairnetwork.pojo.response.company.ContactsItem

class BiddersAdapter(var bdt_con:Context,var bdr_lst:ArrayList<ContactsItem>):
    RecyclerView.Adapter<BiddersAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(BlddrItemLayBinding.inflate(LayoutInflater.from(bdt_con),parent,false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//       holder.item_bldr.blddrLocTxt.text=bdr_lst[position].address?:""
//      holder.item_bldr.blddrRolrTxt.text=bdr_lst[position].contactDesignation?:""
//        holder.item_bldr.blddrOfcnoTxt.text=bdr_lst[position].ofcno
//        holder.item_bldr.blddrPhn1Txt.text=bdr_lst[position].cntcs
//        holder.item_bldr.blddrTitle.text = bdr_lst[position].contactName
//        holder.item_bldr.blddrWebTxt.text=bdr_lst[position].web
    }

    override fun getItemCount(): Int {
        return bdr_lst.size
    }
    class ViewHolder(val item_bldr: BlddrItemLayBinding):RecyclerView.ViewHolder(item_bldr.root) {

    }
}