package com.bncairnetwork.ui.adapter.project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ProjectSearchItemLayBinding
import com.bncairnetwork.helper.ProjectNearMeLisntr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.helper.UserRestrictionRepo
import com.bncairnetwork.pojo.response.ProjectNearDataItem
import com.bumptech.glide.Glide

class ProjectNearMeAptr(
    var con: Context,
    val data: List<ProjectNearDataItem?>? = null,
    var lstnr: ProjectNearMeLisntr,
    var packageID:Int
) :
    RecyclerView.Adapter<ProjectNearMeAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProjectSearchItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        data?.let { obj ->
            holder.item.projName.text = obj[position]?.name
            holder.item.projCountry.text = obj[position]?.city.plus(", ")
            holder.item.projState.text = obj[position]?.country
            holder.item.updateDate.text = obj[position]?.updated
            holder.item.updateDate.visibility =
                if (obj[position]?.authorized == true) View.VISIBLE else View.GONE
            holder.item.updateDatelbl.visibility =
                if (obj[position]?.authorized == true) View.VISIBLE else View.GONE

            holder.item.im4.setImageResource(if (obj[position]?.favourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)
            holder.item.proAmt.text =
                "$".plus(
                    Uitls.getMillionBillionTrillion(obj[position]?.value?.toFloat() ?: 0f)
                        .replace(".0", "")
                )
            holder.item.proAmt.visibility =
                if (obj[position]?.authorized == true) View.VISIBLE else View.GONE
            Glide.with(con).load(obj[position]?.image).into(holder.item.proImg)
            holder.item.im2.setImageResource(if (obj[position]?.tags?.isNotEmpty() == true) R.drawable.ic_tag_active else R.drawable.ic_tag_inactive)
            holder.item.img3.setImageResource(if (obj[position]?.reminder != null) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive)
            holder.itemView.setOnClickListener {
                lstnr.onProjectClick(
                    position,
                    id = obj[position]?.id.toString(),
                    isTxt = true,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = false, proObj = obj[position]

                )
            }
            holder.item.im2.setOnClickListener {
                lstnr.onProjectClick(
                    position,
                    id = obj[position]?.id.toString(),
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = true,
                    isReminder = false,
                    isFav = false, proObj = obj[position]
                )
            }
            holder.item.img1.setOnClickListener {
                lstnr.onProjectClick(
                    position,
                    id = obj[position]?.id.toString(),
                    isTxt = false,
                    isAddInteraction = true,
                    isTag = false,
                    isReminder = false,
                    isFav = false, proObj = obj[position]
                )
            }
            holder.item.img3.setOnClickListener {
                lstnr.onProjectClick(
                    position,
                    id = obj[position]?.id.toString(),
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = true,
                    isFav = false, proObj = obj[position]
                )
            }
            holder.item.im4.setOnClickListener {
                lstnr.onProjectClick(
                    position,
                    id = obj[position]?.id.toString(),
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = true,
                    proObj = obj[position]
                )
            }
        }
        UserRestrictionRepo.interactionTagReminderFavoriteSwitchRestriction(
            interactionView = holder.item.img1,
            TagView = holder.item.im2,
            ReminderView = holder.item.img3,
            FavoriteView = holder.item.im4,
            SwitchView = null,
            packageID = packageID,
            isCompanyView = false,
            isProjectView = true
        )
    }

    override fun getItemCount(): Int {
        return data?.size ?: 0
    }

    class ViewHolder(val item: ProjectSearchItemLayBinding) : RecyclerView.ViewHolder(item.root)
}