package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.BottomProjectCompaniesOptionDataItemBinding
import com.bncairnetwork.helper.BottomOptionListnr
import com.bncairnetwork.pojo.AttributesItem
import com.bncairnetwork.webservices.ApiConstants
import com.bncairnetwork.webservices.ApiConstants.Companies

class BottomOptionsAdapter(
    var con: Context,
    var lst: ArrayList<AttributesItem?>,
    var listnr: BottomOptionListnr,
    var packageID: Int
) :
    RecyclerView.Adapter<BottomOptionsAdapter.ViewHolder>() {
    var isClickable = true
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            BottomProjectCompaniesOptionDataItemBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.lbl1.text =
            if (lst?.get(position)?.title == Companies) "Companies & Bidders" else lst?.get(position)?.title

        holder.item.keyContractVal.text = lst?.get(position)?.count.toString()
        when (lst?.get(position)?.title) {
            Companies -> {
                holder.item.img1.setImageResource(R.drawable.ic_com_bidder)
            }
            ApiConstants.Schedules -> {
                holder.item.img1.setImageResource(R.drawable.ic_schedule)
            }
            ApiConstants.ProjectTree -> {
                holder.item.img1.setImageResource(R.drawable.ic_project_tree)
            }
            ApiConstants.UpdatesAndHighlights -> {
                holder.item.img1.setImageResource(R.drawable.ic_update_and_highlight)
            }
            ApiConstants.Interactions -> {
                holder.item.img1.setImageResource(R.drawable.ic_interaction)
            }
            ApiConstants.Reminders -> {
                holder.item.img1.setImageResource(R.drawable.ic_reminder_circular)
            }
            ApiConstants.Bidders -> {
                holder.item.img1.setImageResource(R.drawable.bidder_ic)
            }

            ApiConstants.KeyContact -> {
                holder.item.img1.setImageResource(R.drawable.key_contrast_ic)
            }
            ApiConstants.LiveProject -> {
                holder.item.img1.setImageResource(R.drawable.live_project_ic)
            }
            ApiConstants.ProjectNews -> {
                holder.item.img1.setImageResource(R.drawable.project_news_ic)
            }
            ApiConstants.ProjectReference -> {
                holder.item.img1.setImageResource(R.drawable.project_reference_ic)
            }

        }
        holder.item.keyContractVal.text = lst?.get(position)?.count.toString()
        holder.itemView.setOnClickListener {
            if (isClickable) {
                if (packageID != ApiConstants.professionalID || !lst[position]?.title.equals(
                        ApiConstants.KeyContact
                    )
                ) {
                    listnr.onOptionSelect(
                        position,
                        lst[position]?.title ?: "",
                        count = lst[position]?.count.toString()
                    )
                }

            }
        }

    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    fun disableRvView(flag: Boolean) {
        this.isClickable = flag

    }

    class ViewHolder(val item: BottomProjectCompaniesOptionDataItemBinding) :
        RecyclerView.ViewHolder(item.root)
}