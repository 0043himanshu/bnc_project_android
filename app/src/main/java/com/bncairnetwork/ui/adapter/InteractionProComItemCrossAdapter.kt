package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.InteractionProComItemCrossLayBinding
import com.bncairnetwork.helper.InteractionProComDelListnr

class InteractionProComItemCrossAdapter(
    var con: Context,
    var lst: ArrayList<String>,
    var listnr: InteractionProComDelListnr
) :
    RecyclerView.Adapter<InteractionProComItemCrossAdapter.ViewHolder>() {
    class ViewHolder(val item_tag: InteractionProComItemCrossLayBinding) :
        RecyclerView.ViewHolder(item_tag.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            InteractionProComItemCrossLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item_tag.lbl.text = lst[position]
        holder.item_tag.delbtn.setOnClickListener {
            listnr.onClick(position, lst[position])
        }
    }

    override fun getItemCount(): Int {
        return lst.size
    }
}