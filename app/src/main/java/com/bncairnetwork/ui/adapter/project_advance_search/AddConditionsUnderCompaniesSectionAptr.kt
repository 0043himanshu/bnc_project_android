package com.bncairnetwork.ui.adapter.project_advance_search

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.AddConditionItemLayBinding
import com.bncairnetwork.helper.AddConditionListnr
import com.bncairnetwork.pojo.AddCondition

class AddConditionsUnderCompaniesSectionAptr(
    var con: Context,
    var lst: ArrayList<AddCondition>,
    var listnr: AddConditionListnr
) :
    RecyclerView.Adapter<AddConditionsUnderCompaniesSectionAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            AddConditionItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.comnyExdt.setText("")
        holder.item.inclueBidderRd.isChecked = false
        holder.item.inclueCncledContrctRd.isChecked = false
        holder.item.matchExactRd.isChecked = false
        holder.item.crossAddCondtion.setOnClickListener {
            listnr.onAddCondition(
                position,
                AddCondition(
                    companyName = holder.item.comnyExdt.text.toString(),
                    isMatchExactPhase = holder.item.matchExactRd.isChecked,
                    isIncludeBidder = holder.item.inclueBidderRd.isChecked,
                    isIncludeCancellationContracts = holder.item.inclueCncledContrctRd.isChecked
                ),obj2 = null, isUnderCompanies = true, isUnderDates = false
            )
        }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    class ViewHolder(val item: AddConditionItemLayBinding) : RecyclerView.ViewHolder(item.root) {
        init {
            setIsRecyclable(false)
        }
    }
}