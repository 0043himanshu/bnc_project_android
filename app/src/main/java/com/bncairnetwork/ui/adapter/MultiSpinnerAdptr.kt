package com.bncairnetwork.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.MultiSelectionSpLayBinding
import com.bncairnetwork.helper.MultiSelectionListnr

class MultiSpinnerAdptr(
    val con: Context,
    val lst: ArrayList<String>,
    private val selectionLst: ArrayList<String>,
    val lisntr: MultiSelectionListnr
) :
    RecyclerView.Adapter<MultiSpinnerAdptr.MViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): MultiSpinnerAdptr.MViewHolder {
        return MViewHolder(MultiSelectionSpLayBinding.inflate(LayoutInflater.from(con)))
    }

    override fun onBindViewHolder(holder: MultiSpinnerAdptr.MViewHolder, position: Int) {
        holder.item.chkbox.text = lst[position]
        holder.item.chkbox.isChecked = selectionLst.contains(lst[position])
        holder.item.chkbox.setOnClickListener {
            println(lst[position])
            if (holder.item.chkbox.isChecked) selectionLst.add(lst[position]) else selectionLst.remove(
                lst[position]
            )
            lisntr.onMakeSelection(position, lst[position], holder.item.chkbox.isChecked)
        }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    inner class MViewHolder(val item: MultiSelectionSpLayBinding) :
        RecyclerView.ViewHolder(item.root) {
        init {
            setIsRecyclable(false)
        }
    }

}