package com.bncairnetwork.ui.adapter.project

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.UpdatesNdHighlightDataItemBinding
import com.bncairnetwork.helper.loadNormalPhoto
import com.bncairnetwork.pojo.response.project.NotesItem

class ProUpdateNdHighlightsAptr(
    var con: Context,
    var lst: ArrayList<NotesItem?>? = null
) :
    RecyclerView.Adapter<ProUpdateNdHighlightsAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            UpdatesNdHighlightDataItemBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        lst?.let { obj ->
            holder.item.prjctNwsImgItem.visibility =
                if (obj[position]?.s3ImageUrl?.isNotEmpty() == true) View.VISIBLE else View.GONE
            holder.item.prjctNwsImgItem.loadNormalPhoto(obj[position]?.s3ImageUrl ?: "")
            holder.item.textView49.text = obj[position]?.date ?: ""
            holder.item.textView50.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(obj[position]?.note ?: "", Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(obj[position]?.note ?: "")
            }
        }
    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item: UpdatesNdHighlightDataItemBinding) :
        RecyclerView.ViewHolder(item.root)
}