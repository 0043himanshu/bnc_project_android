package com.bncairnetwork.ui.adapter.interaction

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.MultiSelectionSpLayBinding
import com.bncairnetwork.helper.ShareWithUserListr
import com.bncairnetwork.pojo.response.FullUsersItem

class ShareWithMultiSpAdptr(
    val con: Context,
    val lst: ArrayList<FullUsersItem?>,
    private val selectionLst: ArrayList<FullUsersItem?>,
    val lisntr: ShareWithUserListr
) :
    RecyclerView.Adapter<ShareWithMultiSpAdptr.MViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ShareWithMultiSpAdptr.MViewHolder {
        return MViewHolder(MultiSelectionSpLayBinding.inflate(LayoutInflater.from(con)))
    }

    override fun onBindViewHolder(holder: ShareWithMultiSpAdptr.MViewHolder, position: Int) {
        holder.item.chkbox.text = lst[position]?.name ?: ""
        holder.item.chkbox.isChecked = selectionLst.contains(lst[position])
        holder.item.chkbox.setOnClickListener {
            println(lst[position])
            if (holder.item.chkbox.isChecked) selectionLst.add(
                lst[position]
            ) else selectionLst.remove(
                lst[position]
            )
            lisntr.onSharedWithUserClick(position, holder.item.chkbox.isChecked, lst[position])
        }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    inner class MViewHolder(val item: MultiSelectionSpLayBinding) :
        RecyclerView.ViewHolder(item.root) {
        init {
            setIsRecyclable(false)
        }
    }

}