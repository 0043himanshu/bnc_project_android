package com.bncairnetwork.ui.adapter.project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ProjectReferenceDataItemLayBinding
import com.bncairnetwork.helper.ProjectRefListnr
import com.bncairnetwork.helper.TagListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.project.ProjectsItem
import com.bumptech.glide.Glide

class ProjectReferenceAptr(
    var con: Context,
    var proLst: ArrayList<ProjectsItem>?,
    var lstnr: ProjectRefListnr,var tagListnr: TagListnr
) :
    RecyclerView.Adapter<ProjectReferenceAptr.ViewHolder>() {
    private val viewPool = RecyclerView.RecycledViewPool()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ProjectReferenceDataItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val childLayoutManager =
            LinearLayoutManager(holder.item.tagRv.context, LinearLayoutManager.HORIZONTAL, false)
        // childLayoutManager.initialPrefetchItemCount = 4
        holder.item.tagRv.apply {
            layoutManager = childLayoutManager
            adapter =
                proLst!![position].tags?.let {
                    ProjectTagAptr(
                        it,tagListnr
                    )
                }
            setRecycledViewPool(viewPool)
        }
        proLst?.let {
            holder.item.projName.text = proLst!![position].name
            holder.item.projCountry.text = proLst!![position].city
            holder.item.projState.text = proLst!![position].country
            holder.item.underConstrLbl.text = proLst!![position].stage ?: ""
             holder.item.updateDate.text = proLst!![position].updated
            holder.item.updateDate.visibility =
                if (proLst!![position].authorized == true) View.VISIBLE else View.GONE
            holder.item.updateDatelbl.visibility =
                if (proLst!![position].authorized == true) View.VISIBLE else View.GONE
            holder.item.im4.setImageResource(if (proLst!![position].favourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)
            holder.item.proAmt.text =
                "$".plus(
                    Uitls.getMillionBillionTrillion(proLst!![position].value.toFloat())
                        .replace(".0", "")
                )
            holder.item.proAmt.visibility =
                if (proLst!![position].authorized == true) View.VISIBLE else View.GONE

            Glide.with(con).load(proLst!![position].image).into(holder.item.proImg)
            holder.item.im2.setImageResource(if (proLst!![position].tags?.isEmpty() == true) R.drawable.ic_tag_inactive else R.drawable.ic_tag_active)
            holder.item.projName.setOnClickListener {
                lstnr.onProRefClk(
                    position,
                    id = proLst!![position].id.toString(),
                    isTxt = true,
                    isAddInteraction = false,
                    isTag = false,
                    isFav = false,
                    isReminder = false,
                    comObj = proLst!![position]

                )
            }
            holder.item.img1.setOnClickListener {
                lstnr.onProRefClk(
                    position,
                    id = proLst!![position].id.toString(),
                    isTxt = false,
                    isAddInteraction = true,
                    isTag = false,
                    isFav = false,
                    isReminder = false,
                    comObj = proLst!![position]

                )
            }

            holder.item.im2.setOnClickListener {
                lstnr.onProRefClk(
                    position,
                    id = proLst!![position].id.toString(),
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = true,
                    isFav = false,
                    isReminder = false,
                    comObj = proLst!![position]
                )
            }
            holder.item.img3.setOnClickListener {
                lstnr.onProRefClk(
                    position,
                    id = proLst!![position].id.toString(),
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isFav = false,
                    isReminder = proLst!![position].authorized == true,
                    comObj = proLst!![position]
                )
            }
            holder.item.im4.setOnClickListener {
                lstnr.onProRefClk(
                    position,
                    id = proLst!![position].id.toString(),
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isFav = true,
                    isReminder = false,
                    comObj = proLst!![position]
                )
            }
        }


    }

    override fun getItemCount(): Int {
        return proLst?.size ?: 0
    }

    class ViewHolder(val item: ProjectReferenceDataItemLayBinding) :
        RecyclerView.ViewHolder(item.root)
}