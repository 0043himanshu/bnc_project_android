package com.bncairnetwork.ui.adapter.project

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.LiveProjectItemLayBinding
import com.bncairnetwork.helper.LiveProjectListner
import com.bncairnetwork.helper.TagListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.project.LiveProjectsItem
import com.bumptech.glide.Glide
import com.bncairnetwork.webservices.ApiConstants

class ProjectLiveAptr(
    var con: Context,
    var proLst: ArrayList<LiveProjectsItem?>? = null,
    var lstnr: LiveProjectListner,
    var packageID:Int,var tagListnr: TagListnr
) :
    RecyclerView.Adapter<ProjectLiveAptr.ViewHolder>() {
    private val viewPool = RecyclerView.RecycledViewPool()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LiveProjectItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        proLst?.let {
            holder.item.img1.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            holder.item.im2.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            val childLayoutManager =
                LinearLayoutManager(holder.item.tagRv.context, LinearLayoutManager.HORIZONTAL, false)
            // childLayoutManager.initialPrefetchItemCount = 4
            holder.item.projIc.setImageResource(
                if (proLst!![position]?.recordType?.linked == true && proLst!![position]?.recordType?.type == "IBIS") R.drawable.ic_linked_project
                else if (proLst!![position]?.recordType?.type == "IBIS" && proLst!![position]?.recordType?.linked == false) R.drawable.ic_internal
                else if (proLst!![position]?.recordType?.type == "BNC") R.drawable.ic_b else 0
            )
            holder.item.underConstrLbl.text = proLst!![position]?.projectStage ?: ""
            holder.item.tagRv.apply {
                layoutManager = childLayoutManager
                adapter =
                    ProjectTagAptr(
                        proLst!![position]?.tagObj?.selectedTags,tagListnr
                    )
                setRecycledViewPool(viewPool)
            }
            holder.item.projName.text = proLst!![position]?.name
            holder.item.projCountry.text = proLst!![position]?.city.plus(", ")
            holder.item.projState.text = proLst!![position]?.country
            holder.item.updateDate.text = proLst!![position]?.updated
            holder.item.im4.setImageResource(if (proLst!![position]?.tagObj?.isFavourite == true) R.drawable.ic_favorite_active else R.drawable.ic_favorite_inactive)
            holder.item.proAmt.text =
                "USD ".plus(
                    Uitls.getMillionBillionTrillion(proLst!![position]?.value?.toFloat() ?: 0f)
                        .replace(".0", "")
                )
            Glide.with(con).load(proLst!![position]?.image).into(holder.item.proImg)
            holder.item.im2.setImageResource(if (proLst!![position]?.tagObj?.selectedTags?.isNotEmpty() == true) R.drawable.ic_tag_active else R.drawable.ic_tag_inactive)
            holder.item.img3.setImageResource(if (proLst!![position]?.identfierObj?.hasReminder == true) R.drawable.ic_reminder_active else R.drawable.ic_reminder_inactive)
            holder.itemView.setOnClickListener {
                lstnr.onLiveProClck(
                    position,
                    id = proLst!![position]?.id?:"",
                    isTxt = true,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = false, proObj = proLst!![position]

                )
            }
            holder.item.im2.setOnClickListener {
                lstnr.onLiveProClck(
                    position,
                    id = proLst!![position]?.id?:"",
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = true,
                    isReminder = false,
                    isFav = false, proObj = proLst!![position]
                )
            }
            holder.item.img1.setOnClickListener {
                lstnr.onLiveProClck(
                    position,
                    id = proLst!![position]?.id?:"",
                    isTxt = false,
                    isAddInteraction = true,
                    isTag = false,
                    isReminder = false,
                    isFav = false,  proObj = proLst!![position]
                )
            }
            holder.item.img3.setOnClickListener {
                lstnr.onLiveProClck(
                    position,
                    id = proLst!![position]?.id?:"",
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = true,
                    isFav = false, proObj = proLst!![position]
                )
            }
            holder.item.im4.setOnClickListener {
                lstnr.onLiveProClck(
                    position,
                    id = proLst!![position]?.id?:"",
                    isTxt = false,
                    isAddInteraction = false,
                    isTag = false,
                    isReminder = false,
                    isFav = true,  proObj = proLst!![position]
                )
            }
        }

    }

    override fun getItemCount(): Int {
        return proLst?.size?:0
    }

    class ViewHolder(val item: LiveProjectItemLayBinding) : RecyclerView.ViewHolder(item.root)
}