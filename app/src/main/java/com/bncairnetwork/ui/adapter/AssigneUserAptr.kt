package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.AssgnedUserDataItemLayBinding
import com.bncairnetwork.pojo.response.AssigneesDataItem
import com.bumptech.glide.Glide

class AssigneUserAptr(
    var con: Context,
    var lst: ArrayList<AssigneesDataItem>?
) :
    RecyclerView.Adapter<AssigneUserAptr.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            AssgnedUserDataItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.assignUserlbl.text = lst?.get(position)?.first_name
        Glide.with(con).load(lst?.get(position)?.profile_image ?: "").error(R.drawable.ic_assignnes)
            .into(holder.item.assignUserDpImg)
    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val item: AssgnedUserDataItemLayBinding) : RecyclerView.ViewHolder(item.root)
}