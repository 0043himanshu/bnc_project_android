package com.bncairnetwork.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.SingleSpinnerItemLayBinding
import com.bncairnetwork.helper.AddConditionListnr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem.DateRangeDataItem
import com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem.DatesDataItemForProjectAdSearch
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.ui.activities.AdvanceSearchAc
import com.bncairnetwork.webservices.ApiConstants

class SingleSpinnerAdptr(
    val con: Context,
    val lst: ArrayList<DatesDataItemForProjectAdSearch>,
    val masterdataobj: GetMasterDataRes?,
    val lisntr: AddConditionListnr
) :
    RecyclerView.Adapter<SingleSpinnerAdptr.MViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SingleSpinnerAdptr.MViewHolder {
        return MViewHolder(
            SingleSpinnerItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: SingleSpinnerAdptr.MViewHolder, position: Int) {
        holder.item.createdfromEdxt.setOnClickListener {
            Uitls.getDatePicker(
                con,
                (con as AdvanceSearchAc).supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                holder.item.createdfromEdxt
            )
        }
        holder.item.createdtoEdxt.setOnClickListener {
            Uitls.getDatePicker(
                con,
                (con as AdvanceSearchAc).supportFragmentManager,
                ApiConstants.YMD_PATTERN,
                holder.item.createdtoEdxt
            )
        }
        ArrayAdapter(
            con,
            android.R.layout.simple_spinner_item,
            arrayListOf(
                "Select type",
                ApiConstants.Schedule,
                ApiConstants.contract_awarded,
                ApiConstants.stages_changed
            )
        ).apply {
            setDropDownViewResource(R.layout.sp_row_item_lay)
            holder.item.typeSp.adapter = this
        }
        holder.item.typeSp.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                holder.item.lbl.visibility = if (position > 0) View.GONE else View.VISIBLE
                if (position > 0) {
                    println(
                        Uitls.getSubTypesFromTypeUnderDateSectionProjeAdncSearch(
                            masterdataobj,
                            holder.item.typeSp.selectedItem.toString()
                        )
                    )
                    ArrayAdapter(
                        con,
                        android.R.layout.simple_spinner_item,
                        if (Uitls.getSubTypesFromTypeUnderDateSectionProjeAdncSearch(
                                masterdataobj,
                                holder.item.typeSp.selectedItem.toString()
                            ).isNotEmpty()
                        )
                            Uitls.getSubTypesFromTypeUnderDateSectionProjeAdncSearch(
                                masterdataobj,
                                holder.item.typeSp.selectedItem.toString()
                            ).keys.toList() as ArrayList<String> else arrayListOf("Select")
                    ).apply {
                        setDropDownViewResource(R.layout.sp_row_item_lay)
                        holder.item.SubtypeSp.adapter = this
                    }
                }
                holder.item.SubtypeSp.isEnabled = position > 0

            }

            override fun onNothingSelected(parent: AdapterView<*>?) {

            }
        }
//        holder.item.chkbox.text = lst[position]
//       holder.item.chkbox.isChecked = selectionLst.contains(lst[position])
//        holder.item.chkbox.setOnClickListener {
//            println(lst[position])
//            if(holder.item.chkbox.isChecked) selectionLst.add(lst[position]) else selectionLst.remove(lst[position])
//            lisntr.onMakeSelection(position, lst[position], holder.item.chkbox.isChecked)
//        }
        holder.item.crossAddCondtion.setOnClickListener {
            lisntr.onAddCondition(
                pos = position,
                obj1 = null,
                obj2 = DatesDataItemForProjectAdSearch(
                    type = holder.item.typeSp.selectedItem.toString(),
                    subtype = "",
                    date_range = DateRangeDataItem(
                        date_range = ApiConstants.nine,
                        start_date = holder.item.createdfromEdxt.text.toString(),
                        end_date = holder.item.createdtoEdxt.text.toString()
                    )
                ),
                isUnderCompanies = false,
                isUnderDates = true
            )
        }
    }

    override fun getItemCount(): Int {
        return lst.size
    }

    inner class MViewHolder(val item: SingleSpinnerItemLayBinding) :
        RecyclerView.ViewHolder(item.root) {
        init {
            setIsRecyclable(false)
        }
    }

}