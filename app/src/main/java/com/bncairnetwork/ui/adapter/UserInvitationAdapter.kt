package com.bncairnetwork.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.UserEmailItemLayBinding
import com.bncairnetwork.helper.UserEmailInvitationListnr
import com.bncairnetwork.pojo.UserInvitationEmailPojo
import java.util.*
import kotlin.collections.ArrayList

class UserInvitationAdapter(
    var bdt_con: Context,
    var selectEmailsLst: ArrayList<UserInvitationEmailPojo>,
    var lst: ArrayList<UserInvitationEmailPojo>,
    var listnr: UserEmailInvitationListnr
) :
    RecyclerView.Adapter<UserInvitationAdapter.ViewHolder>(), Filterable {
    var userFilterList = ArrayList<UserInvitationEmailPojo>()

    init {
        userFilterList = lst
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            UserEmailItemLayBinding.inflate(LayoutInflater.from(bdt_con), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.userEmailLbl.text = userFilterList[position].email ?: ""
        holder.item.userNameLbl.text = userFilterList[position].name ?: ""
        holder.item.emailCb.setOnCheckedChangeListener { _, ischecked ->
            println("calledddddd")
            listnr.onUserClick(pos = position, userFilterList[position], ischecked = ischecked)
        }
        holder.item.emailCb.isChecked = selectEmailsLst.contains(userFilterList[position])

    }

    override fun getItemCount(): Int {
        return userFilterList.size
    }


    class ViewHolder(val item: UserEmailItemLayBinding) :
        RecyclerView.ViewHolder(item.root)

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                userFilterList = if (charSearch.isEmpty()) {
                    lst
                } else {
                    val resultList = ArrayList<UserInvitationEmailPojo>()
                    for (row in lst) {
                        if (row.email?.lowercase(Locale.ROOT)
                                ?.contains(charSearch.lowercase(Locale.ROOT)) == true
                        ) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = userFilterList
                return filterResults
            }

            @Suppress("UNCHECKED_CAST")
            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                userFilterList = results?.values as ArrayList<UserInvitationEmailPojo>
                notifyDataSetChanged()
            }

        }
    }
}