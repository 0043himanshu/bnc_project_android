package com.bncairnetwork.ui.adapter.project_tree

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.ChildOneLayBinding
import com.bncairnetwork.helper.ChildOneListnr
import com.bncairnetwork.helper.Uitls.Companion.getInnerItem
import com.bncairnetwork.pojo.response.project.ChilderDataItem


class ProjectChildOneAdptr(
    var con: Context,
    var lst: ArrayList<ChilderDataItem>,
    var listr: ChildOneListnr
) :
    RecyclerView.Adapter<ProjectChildOneAdptr.ViewHolder>() {
    private val viewPool = RecyclerView.RecycledViewPool()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ChildOneLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.item.nestedRv.visibility = View.GONE
        val childLayoutManager =
            LinearLayoutManager(holder.item.nestedRv.context, LinearLayoutManager.VERTICAL, false)
        holder.item.projectNameLbl.text = lst[position].name
        holder.item.projectNameLbl.paintFlags =
            holder.item.projectNameLbl.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        holder.item.projectNameLbl.setTextColor(
            if (lst[position].children.isNotEmpty()) ContextCompat.getColor(
                con,
                R.color.black
            ) else ContextCompat.getColor(
                con,
                R.color.linkColor
            )
        )
        holder.item.projectNameLbl.setOnClickListener {
            listr.onChildOneClick(pos = position, obj = lst[position])
//            con.startActivity(
//                Intent(
//                    con,
//                    ProjectDetailsAc::class.java
//                ).putExtra(Enums.ID.toString(), lst[position].id)
//            )
        }
        if (lst[position].childType == "IBIS" && lst[position].children.isEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_linked_project)
        } else if (lst[position].childType == "BNC" && lst[position].children.isEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_b)
        } else if (!lst[position].isChildViewed && lst[position].children.isNotEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_add_green)
        } else if (lst[position].isChildViewed && lst[position].children.isNotEmpty()) {
            holder.item.projtypeimg.setImageResource(R.drawable.ic_minus_gray)
        } else {
            holder.item.projtypeimg.setImageResource(0)
        }

        holder.item.projtypeimg.setOnClickListener {
            if (holder.item.nestedRv.visibility == View.GONE) {
                holder.item.nestedRv.apply {
                    layoutManager = childLayoutManager
                    adapter =
                        ProjectChildOneAdptr(
                            holder.item.nestedRv.context,
                            getInnerItem(lst, parentItem = lst[position].name),
                            listr = listr
                        )
                    setRecycledViewPool(viewPool)
                }
                holder.item.nestedRv.visibility = View.VISIBLE
                holder.item.projtypeimg.setImageResource(R.drawable.ic_minus_gray)
            } else {
                holder.item.projtypeimg.setImageResource(R.drawable.ic_add_green)
                holder.item.nestedRv.visibility = View.GONE
            }
        }

    }


    override fun getItemCount(): Int {
        return lst.size
    }


    class ViewHolder(val item: ChildOneLayBinding) :
        RecyclerView.ViewHolder(item.root)
}