package com.bncairnetwork.ui.adapter.interaction

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.InteractionChildItemLayBinding

import com.bncairnetwork.pojo.response.project.CompanyItem

class InteractionChildAdptr(val con: Context, val lst: List<CompanyItem?>?) :
    RecyclerView.Adapter<InteractionChildAdptr.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): InteractionChildAdptr.ViewHolder {
        return ViewHolder(
            InteractionChildItemLayBinding.inflate(
                LayoutInflater.from(con),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: InteractionChildAdptr.ViewHolder, position: Int) {
        holder.mView.lbl.text = lst?.get(position)?.company?:""
    }

    override fun getItemCount(): Int {
        return lst?.size ?: 0
    }

    class ViewHolder(val mView: InteractionChildItemLayBinding) :
        RecyclerView.ViewHolder(mView.root)
}