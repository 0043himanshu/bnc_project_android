package com.bncairnetwork.ui.adapter.NewsNdHighlights_and_claimedNews

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.databinding.PrjoctsNewsItemBinding
import com.bncairnetwork.helper.ProjectNewsLisntr
import com.bncairnetwork.helper.Uitls
import com.bncairnetwork.pojo.response.project.ProjectNewsItem
import com.bumptech.glide.Glide
import java.util.*

class ProjectNewsAdapter(
    var con_prjct_nws: Context,
    var lst_prjct_nws: ArrayList<ProjectNewsItem?>? = null,
    var listnr: ProjectNewsLisntr
) :
    RecyclerView.Adapter<ProjectNewsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        return ViewHolder(
            PrjoctsNewsItemBinding.inflate(
                LayoutInflater.from(con_prjct_nws),
                parent,
                false
            )
        )
    }


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        lst_prjct_nws?.let { obj ->
            holder.prj_nws.prjctNwsTltItem.text = obj[position]?.name ?: ""
            holder.prj_nws.prjctNwsCountry.text =
                if (!obj[position]?.city.isNullOrEmpty()) obj[position]?.city!!.plus(", ")
                    .plus(obj[position]?.country ?: "") else obj[position]?.country ?: ""
            holder.prj_nws.prjctNwsDes2.text = obj[position]?.description ?: ""
            Glide.with(con_prjct_nws).load(obj[position]?.image ?: "")
                .placeholder(Uitls.GliderPlaceHolder(con_prjct_nws))
                .into(holder.prj_nws.prjctNwsImgItem)
            Glide.with(con_prjct_nws).load(obj[position]?.countryFlag ?: "")
                .into(holder.prj_nws.prjctNwsImgLoc)
            holder.itemView.setOnClickListener {
                listnr.onNewsClk(position, obj = null, obj2 = obj[position])
            }
        }


    }

    override fun getItemCount(): Int {
        return lst_prjct_nws?.size ?: 0
    }

    class ViewHolder(val prj_nws: PrjoctsNewsItemBinding) : RecyclerView.ViewHolder(prj_nws.root)


}