package com.bncairnetwork.helper

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Context.LAYOUT_INFLATER_SERVICE
import android.content.DialogInterface
import android.content.Intent
import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.ContactsContract
import android.text.SpannableString
import android.text.TextPaint
import android.text.TextUtils
import android.text.style.URLSpan
import android.util.Log
import android.util.Patterns
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.inputmethod.InputMethodManager
import android.webkit.WebResourceError
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bncairnetwork.R
import com.bncairnetwork.pojo.UserInvitationEmailPojo
import com.bncairnetwork.pojo.response.CitiesItem
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.pojo.response.project.ChilderDataItem
import com.bncairnetwork.ui.activities.auth.LoginAc
import com.bncairnetwork.webservices.ApiConstants
import com.google.android.material.datepicker.CalendarConstraints
import com.google.android.material.datepicker.DateValidatorPointForward
import com.google.android.material.datepicker.MaterialDatePicker
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.net.ConnectException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Matcher
import java.util.regex.Pattern
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.math.roundToLong


class Uitls {
    companion object {
        private var circulrDrawabe: CircularProgressDrawable? = null
        private var progressDialog: Dialog? = null
        private var materialDialog: MaterialAlertDialogBuilder? = null
        private val DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY
        private val FILTER = "$DISPLAY_NAME NOT LIKE '%@%'"
        private val ORDER = String.format("%1\$s COLLATE NOCASE", DISPLAY_NAME)

        @SuppressLint("InlinedApi")
        private val PROJECTION = arrayOf(
            ContactsContract.Contacts._ID,
            DISPLAY_NAME,
            ContactsContract.Contacts.HAS_PHONE_NUMBER
        )

        /*project tree nested list*/
        fun getInnerItem(
            lst: ArrayList<ChilderDataItem>,
            parentItem: String
        ): ArrayList<ChilderDataItem> {
            val returnLst = ArrayList<ChilderDataItem>()
            lst.forEach {
                if (it.name == parentItem) {
                    returnLst.addAll(it.children)
                    return@forEach
                }
            }
            return returnLst
        }

        fun onUnSuccessResponse(code: Int, con: Context) {
            if (code == 401) {
                con.startActivity(
                    Intent(
                        con,
                        LoginAc::class.java
                    ).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
                )
                PrefUtils.with(con).clear()
            } else {
                Toast.makeText(con, "Something went wrong!!", Toast.LENGTH_SHORT).show()
            }
        }

        fun GliderPlaceHolder(con: Context): Drawable {
            return circulrDrawabe
                ?: CircularProgressDrawable(con).apply {
                    strokeWidth = 5f
                    centerRadius = 30f
                    start()
                }
//            return CircularProgressDrawable(con).apply {
//                strokeWidth = 5f
//                centerRadius = 30f
//                start()
//            }

        }

        fun isOnline(context: Context): Boolean {
            return try {
                val connectivityManager =
                    context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
                val mobile_info =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                val wifi_info = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                if (mobile_info != null) {
                    mobile_info.isConnectedOrConnecting || wifi_info!!.isConnectedOrConnecting
                } else {
                    wifi_info!!.isConnectedOrConnecting
                }
            } catch (e: Exception) {
                e.printStackTrace()
                println("" + e)
                false
            }
        }

        fun showSharingDialog(text: String, url: String, con: Context) {
            con.startActivity(Intent.createChooser(Intent().apply {
                action = Intent.ACTION_SEND
                type = "text/plain"
                putExtra(Intent.EXTRA_TEXT, "$text: $url")
            }, "Shared with:"))
        }

        fun showDialog(con: Context, title: String) {
            materialDialog = MaterialAlertDialogBuilder(
                con,
                R.style.MyRounded_MaterialComponents_MaterialAlertDialog
            ).apply {
                setMessage(title)
                setPositiveButton(
                    "Ok"
                ) { dialog, _ ->
                    dialog.dismiss()
                    (con as Activity).onBackPressed()
                }
                show()
            }
        }

        fun isValidEmail(target: CharSequence?): Boolean {
            return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        fun isValidPassword(password: String?, userType: Int): Boolean {
            val pattern: Pattern
            val passwordPattern =
                "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"
            pattern = Pattern.compile(passwordPattern)
            val matcher: Matcher = pattern.matcher(password)
            return if (userType == 1 || userType == 7) matcher.matches() && password?.length!! > 6 else true
        }


        fun getcityByCountry(
            obj: GetMasterDataRes,
            countryID: String,
            cityLst: ArrayList<String>,
            cityHashMap: HashMap<String, String>
        ): HashMap<String, String> {
            cityLst.clear()
            cityHashMap.clear()
            /***city binding*/
            if (!cityLst.contains("Select")) {
                cityLst.add("Select")
            }
            obj.userCities?.forEach {
                if (it?.country.toString() == countryID) {
                    // cityHashMap[it?.name!!] = it.id.toString()
                    cityHashMap[it?.name!!] = it.id.toString()
                }

            }
            return cityHashMap
        }

        fun getSubTypeIDByTypeKey(
            typeKey: String,
            subtypeKey: String,
            obj: GetMasterDataRes?
        ): String {
            var id = ""
            when (typeKey) {
                ApiConstants.Schedule -> {
                    obj?.schedules?.forEach {
                        if (it?.name!! == subtypeKey) {
                            id = it.id.toString()
                        }
                    }
                }
                ApiConstants.contract_awarded -> {
                    obj?.contractAwarded?.forEach {
                        if (it?.name!! == subtypeKey) {
                            id = it.id.toString()
                        }
                    }
                }
                ApiConstants.stages_changed -> {
                    obj?.stagesChanged?.forEach {
                        if (it?.name!! == subtypeKey) {
                            id = it.id.toString()
                        }
                    }
                }
            }
            return id
        }


        fun getCityMappingByCountry(cityLst: List<CitiesItem?>?): HashMap<String, String> {
            val mapping = HashMap<String, String>()
            cityLst?.forEach {
                mapping[it?.name.toString()] = it?.id.toString()
            }
            return mapping
        }

        fun getCityLstByCountry(cityMapping: HashMap<String, String>): ArrayList<String> {
            val lst = ArrayList<String>()
            cityMapping.forEach {
                lst.add(it.key)
            }

            return lst
        }

        fun hideUnhideView(rootLay: ConstraintLayout, arrowBtn: ImageView) {
            if (rootLay.visibility == View.VISIBLE) {
                rootLay.visibility = View.GONE
                arrowBtn.setImageResource(R.drawable.ic_arrow_up)
            } else {
                rootLay.visibility = View.VISIBLE
                arrowBtn.setImageResource(R.drawable.ic_arrow_down)
            }
        }

        fun makeDialIntent(con: Context, number: String) {
            con.startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", number, null)))
        }

        fun makeEmailIntent(con: Context, email: String) {
            val intent = Intent(Intent.ACTION_SENDTO)
            intent.putExtra(
                Intent.EXTRA_EMAIL,
                arrayOf<String>(email)
            )
            intent.data = Uri.parse("mailto:")
            if (intent.resolveActivity(con.packageManager) != null) {
                con.startActivity(intent)
            } else {
                println("==============No Activity found to handle Intent=================")
            }
        }

        fun getSubTypesFromTypeUnderDateSectionProjeAdncSearch(
            obj: GetMasterDataRes?,
            type: String
        ): HashMap<String, String> {
            val subTypesUnderDateSectionHashMap = HashMap<String, String>()
            //  subTypesUnderDateSectionHashMap["Select"] = ""
            when (type) {
                ApiConstants.Schedule -> {
                    obj?.schedules?.forEach {
                        subTypesUnderDateSectionHashMap[it?.name ?: ""] = it?.id.toString()
                        // selectedSubTypeUnderDateSection.add(it?.name ?: "")
                    }
                }
                ApiConstants.contract_awarded -> {
                    obj?.contractAwarded?.forEach {
                        subTypesUnderDateSectionHashMap[it?.name ?: ""] = it?.id.toString()
                        // selectedSubTypeUnderDateSection.add(it?.name ?: "")
                    }
                }
                ApiConstants.stages_changed -> {
                    obj?.stagesChanged?.forEach {
                        subTypesUnderDateSectionHashMap[it?.name ?: ""] = it?.id.toString()
                        //  selectedSubTypeUnderDateSection.add(it?.name ?: "")
                    }
                }
            }
            println(subTypesUnderDateSectionHashMap)
            return subTypesUnderDateSectionHashMap
        }

        fun getDatePicker(
            con: Context,
            supportFragmentManager: FragmentManager,
            format: String, view: EditText
        ) {
            MaterialDatePicker.Builder.datePicker().setSelection(Date().time)
                .build().apply {
                    show(supportFragmentManager, con.toString())
                    addOnPositiveButtonClickListener {
                        //dd MMM,yyyy
                        view.setText(
                            SimpleDateFormat(format, Locale.getDefault()).format(
                                Date(it)
                            )
                        )
                    }
                }
        }

        /***return future dates in picker only*/
        fun getFutureDatePicker(
            con: Context,
            supportFragmentManager: FragmentManager,
            format: String, view: EditText
        ) {
            MaterialDatePicker.Builder.datePicker().setSelection(Date().time)
                .setCalendarConstraints(
                    CalendarConstraints.Builder()
                        .setValidator(DateValidatorPointForward.from(Date(System.currentTimeMillis() - (1000 * 60 * 60 * 24)).time))
                        .build()
                )
                .build().apply {
                    show(supportFragmentManager, con.toString())
                    addOnPositiveButtonClickListener {
                        //dd MMM,yyyy
                        view.setText(
                            SimpleDateFormat(format, Locale.getDefault()).format(
                                Date(it)
                            )
                        )
                    }
                }
        }

        fun getSubTyeFromCompanyType(
            position: Int,
            SubcompanyTypessLst: ArrayList<String>,
            SubcompanyTypessHashMap: HashMap<String, String>,
            obj: GetMasterDataRes
        ): ArrayList<String> {
            // println("--------${position}---${obj?.allCompanyTypes?.get(position - 1)?.type}-----")
            /***Company Sub-type binding*/
            if (!SubcompanyTypessLst.contains("Select")) {
                SubcompanyTypessLst.add("Select")
            }
            SubcompanyTypessLst.clear()
            SubcompanyTypessHashMap.clear()
            obj.allCompanyTypes?.get(position - 1)?.subtypes?.forEach {
                SubcompanyTypessHashMap[it?.subtype ?: ""] = it?.id.toString()
                SubcompanyTypessLst.add(it?.subtype ?: "")
            }
            return SubcompanyTypessLst
        }


        fun getMillionBillionTrillion(floatNumber: Float): String {
            val million = 1000000L
            val billion = 1000000000L
            val trillion = 1000000000000L
            val number = floatNumber.roundToLong()

            if ((number > million) && number < billion) {
                val fraction = calculateFraction(number, million)
                return fraction.toString().plus(" million")
            } else if ((number >= billion) && (number < trillion)) {
                val fraction = calculateFraction(number, billion)
                return fraction.toString().plus(" billion")
            }
            return number.toString()

        }

        fun calculateFraction(number: Long, divisor: Long): Float {
            val truncate = (number * 10L + (divisor / 2L)) / divisor
            return truncate * 0.10F
        }

        fun showToast(con: Context, msg: String) {
            Toast.makeText(con, msg, Toast.LENGTH_SHORT).show()
        }

        fun handlerError(context: Context?, throwable: Throwable) {
            Log.w("errorHandler", throwable.message.toString())
            if (context == null)
                return
            else {
                when (throwable) {
                    is ConnectException -> Uitls.showToast(context, "Server Connection Error")
                    is SocketException -> Uitls.showToast(context, "Socket Time Out Exception")
                    is SocketTimeoutException -> Uitls.showToast(
                        context,
                        "Socket Time Out Exception"
                    )
                    is UnknownHostException -> Uitls.showToast(context, "No Internet Connection")
                    is InternalError -> Uitls.showToast(context, "Internal Server Error")
                    else -> Uitls.showToast(context, "Something went wrong!")
                }
            }
        }

        fun createCustomeToast(con: Context, isBncVersion: Boolean) {
            val inflater = con
                .getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val layout: View = inflater.inflate(R.layout.cus_top_toast_lay, null)
            val toast = Toast(con)
            toast.duration = Toast.LENGTH_LONG
            toast.setGravity(Gravity.TOP, 0, 0)
            toast.setView(layout)

            layout.findViewById<TextView>(R.id.lbl).text =
                if (isBncVersion) "Switched to BNC record view" else "Switched to Linked record view"
            //tvMessage.text=message
            toast.show()
        }

        // Function to return the modified string
        fun extractInt(str: String): String {
            // Replacing every non-digit number
            // with a space(" ")
            var str = str
            str = str.replace("[^\\d]".toRegex(), " ")

            // Remove extra spaces from the beginning
            // and the ending of the string
            str = str.trim { it <= ' ' }

            // Replace all the consecutive white
            // spaces with a single space
            str = str.replace(" +".toRegex(), " ")
            return if (str == "") "-1" else str
        }

        fun getContactsLst(con: Context): ArrayList<UserInvitationEmailPojo> {
            val contacts: ArrayList<UserInvitationEmailPojo> = ArrayList()
            val cr = con.contentResolver
            val cursor: Cursor? = cr.query(
                ContactsContract.Contacts.CONTENT_URI,
                PROJECTION,
                FILTER,
                null,
                ORDER
            )
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    // get the contact's information
                    val id: String =
                        cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID))
                    val name: String = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME))
                    val hasPhone: Int =
                        cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))
                    // get the user's email address
                    var email: String? = null
                    val ce: Cursor? = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI,
                        null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        arrayOf(id),
                        null
                    )
                    if (ce != null && ce.moveToFirst()) {
                        email =
                            ce.getString(ce.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))
                        ce.close()
                    }
//                get the user's phone number
                    var phone: String? = null
                    if (hasPhone > 0) {
                        val cp: Cursor? = cr.query(
                            ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                            arrayOf(id),
                            null
                        )
                        if (cp != null && cp.moveToFirst()) {
                            phone =
                                cp.getString(cp.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            cp.close()
                        }
                    }
                    // if the user user has an email or phone then add it to contacts
                    if ((!TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email)
                            .matches()
                                && !email.equals(
                            name,
                            ignoreCase = true
                        ) || (!TextUtils.isEmpty(phone)))
                    ) {
                        if (email != null) {
                            println("==========>" + name.plus(" ").plus(email))
                            contacts.add(UserInvitationEmailPojo(name = name, email = email))
                        }

                    }
                } while (cursor.moveToNext())
                // clean up cursor
                cursor.close()
            }
            return contacts
        }

        fun TextView.removeLinksUnderline() {
            val spannable = SpannableString(text)
            for (urlSpan in spannable.getSpans(0, spannable.length, URLSpan::class.java)) {
                spannable.setSpan(object : URLSpan(urlSpan.url) {
                    override fun updateDrawState(ds: TextPaint) {
                        super.updateDrawState(ds)
                        ds.isUnderlineText = false
                    }
                }, spannable.getSpanStart(urlSpan), spannable.getSpanEnd(urlSpan), 0)
            }
            text = spannable
        }

        fun showProgree(isShow: Boolean, con: Context) {
            try {
                if (isShow) {
                    getCustomDialog(con)
                } else {
                    dismissDialog()
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }


        }

        fun hideKeyboard(activity: Activity) {
            val imm: InputMethodManager =
                activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            var view = activity.currentFocus
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(activity)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }

        fun getCustomDialog(context: Context): Dialog {
            if (progressDialog != null && progressDialog!!.isShowing) {
                return progressDialog!!
            } else {
                progressDialog = Dialog(context)
                progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
                progressDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                progressDialog!!.setContentView(R.layout.layout_progress_bar)
                progressDialog!!.setCancelable(false)
                progressDialog!!.show()

            }
            return progressDialog!!
        }

        fun dismissDialog() {
            if (progressDialog != null && progressDialog!!.isShowing)
                progressDialog!!.dismiss()
        }


        fun LoadWebView(context: Context, app_url: String?, webView: WebView) {
            val progressDialog = ProgressDialog(context)
            progressDialog.setMessage("Please wait")
            progressDialog.show()
            progressDialog.setCanceledOnTouchOutside(false)
            progressDialog.setOnCancelListener { dialog: DialogInterface? -> (context as Activity).finish() }
            val webSettings = webView.settings
            webSettings.javaScriptEnabled = true
            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    return false
                }

                override fun onPageFinished(view: WebView, url: String) {
                    if (progressDialog.isShowing) {
                        progressDialog.dismiss()
                    }
                }

                override fun onReceivedError(
                    view: WebView,
                    request: WebResourceRequest,
                    error: WebResourceError
                ) {
                    (context as Activity).finish()
                    Toast.makeText(context, "Error$error", Toast.LENGTH_SHORT).show()
                }
            }
            webView.loadUrl(app_url!!)
        }

    }


}