package com.bncairnetwork.helper

import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

fun ImageView.loadNormalPhoto(url: Any?) {
    Glide.with(context).load(url)
        /*.apply(RequestOptions.overrideOf(Constants.width_300, Constants.height_300).fitCenter())*/
        .placeholder(Uitls.GliderPlaceHolder(context))
        .into(this)

}
