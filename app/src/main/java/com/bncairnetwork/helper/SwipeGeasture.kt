package com.bncairnetwork.ui.activities.homeConnections

import android.content.Context
import android.graphics.Canvas
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.bncairnetwork.R
import it.xabaras.android.recyclerview.swipedecorator.RecyclerViewSwipeDecorator

abstract class SwipeGeasture (context: Context):
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
    val deletecolor = ContextCompat.getColor(context, R.color.deletecolor)

    val deleteIcon = R.drawable.ic_reminder_delete
//    val archiveIcon = R.drawable.ic_add
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return  false
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean
    ) {

        RecyclerViewSwipeDecorator.Builder(c,recyclerView,viewHolder,dX, dY, actionState, isCurrentlyActive)
            .addSwipeLeftBackgroundColor(deletecolor)
            .addSwipeLeftActionIcon(deleteIcon)
//            .addSwipeRightBackgroundColor(archivecolor)
//            .addSwipeRightActionIcon(archiveIcon)
//             .addActionIcon(R.drawable.ic_com_b)
            .create()
            .decorate()
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive)
    }
}