package com.bncairnetwork.helper

import com.bncairnetwork.pojo.*
import com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem.DatesDataItemForProjectAdSearch
import com.bncairnetwork.pojo.response.ContactsItem
import com.bncairnetwork.pojo.response.Dashboard.NewsItem
import com.bncairnetwork.pojo.response.DataItem
import com.bncairnetwork.pojo.response.FullUsersItem
import com.bncairnetwork.pojo.response.ProjectNearDataItem
import com.bncairnetwork.pojo.response.company.InteractionsItem
import com.bncairnetwork.pojo.response.compnyAndBidders.BiddersItem
import com.bncairnetwork.pojo.response.notification.NotificationsItem
import com.bncairnetwork.pojo.response.notification.ReminderNotificationDataItem
import com.bncairnetwork.pojo.response.project.*
import com.bncairnetwork.pojo.response.reminder.RemindersItem
import com.bncairnetwork.pojo.response.reminder.project_and_company.RemindersItemByID

interface ProjectSeachListner {
    fun onPrjctSrchClk(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        proObj: ProjectDetailsDataObj?,
        comObj: CompaniesDataObj?,
        comkeywordObj: CompaniesItem?
    )
}

interface TagListnr {
    fun onTagClick(name: String?)
}


interface NotificationLisntr {
    fun onNotificationClick(
        pos: Int,
        nonReminderObj: NotificationsItem?,
        reminderNotificationObj: ReminderNotificationDataItem?
    )
}


interface CompanyBiddrsListner {
    fun onClk(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        compnyObj: com.bncairnetwork.pojo.response.company.CompaniesItem?,
        biddrObj: BiddersItem?
    )
}

interface UserEmailInvitationListnr {
    fun onUserClick(pos: Int, obj: UserInvitationEmailPojo, ischecked: Boolean)
}

interface SavedSearchListnr {
    fun onSaveSearchColumnClk(
        pos: Int,
        isAddedClick: Boolean,
        isUpdatedClick: Boolean,
        url: String?
    )
}

interface ProNewsNdHighlightListnr {
    fun onProNewsNdHighlightClk(pos: Int, obj: DataItem?)
}

interface ShareWithUserListr {
    fun onSharedWithUserClick(pos: Int, flag: Boolean, obj: FullUsersItem?)
}

interface ProjectNearMeLisntr {
    fun onProjectClick(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        proObj: ProjectNearDataItem?
    )
}

interface LiveProjectListner {
    fun onLiveProClck(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean,
        proObj: LiveProjectsItem?
    )
}

interface ProjectNewsLisntr {
    fun onNewsClk(pos: Int, obj: NewsItem?, obj2: ProjectNewsItem?)
}

interface EditTagListnr {
    fun onTagClick(id: String, pos: Int)
}

interface InteractionProComDelListnr {
    fun onClick(pos: Int, obj: String)
}

interface ProjectRefListnr {
    fun onProRefClk(
        pos: Int,
        id: String,
        isTxt: Boolean,
        isAddInteraction: Boolean,
        isTag: Boolean,
        isReminder: Boolean,
        isFav: Boolean, comObj: ProjectsItem?
    )
}

interface MultiSelectionListnr {
    fun onMakeSelection(pos: Int, name: String, flag: Boolean)
}

interface UpdateAddTagListnr {
    fun onUpdateOrAddTag(pos: Int, tagName: String, flag: Boolean, isremove: Boolean)
}

interface ReminderListnr {
    fun onRrminderClk(
        pos: Int,
        id: String,
        isDel: Boolean,
        isEdit: Boolean,
        isShare: Boolean,
        isMarkCom: Boolean,
        data: RemindersItem?
    )

}

interface ReminderByIDListnr {
    fun onRrminderClkByID(
        pos: Int,
        id: String,
        isDel: Boolean,
        isEdit: Boolean,
        isShare: Boolean,
        isMarkCom: Boolean,
        data: RemindersItemByID
    )

}

interface AddConditionListnr {
    fun onAddCondition(
        pos: Int,
        obj1: AddCondition?,
        obj2: DatesDataItemForProjectAdSearch?,
        isUnderCompanies: Boolean,
        isUnderDates: Boolean
    )
}

interface InteractionLisntr {
    fun onInteractionLstnr(
        pos: Int, obj1: InteractionDataItem?, obj2: InteractionsItem?, isDel: Boolean,
        isEdit: Boolean,
        isShare: Boolean
    )
}

interface ContactListnr {
    fun onContactSelect(pos: Int, obj: ContactsItem)
}


interface TypeNdSubTypeSpListnr {
    fun onSpClck(
        pos: Int,
        isTypeSp: Boolean,
        isSubTypeSp: Boolean,
        TypeHashMap: HashMap<String, String>,
        SubTypeHashMap: HashMap<String, String>
    )
}

interface RemoveTag {
    fun onremove(pos: Int, tag_name: String)
}

interface BottomOptionListnr {
    fun onOptionSelect(pos: Int, id: String, count: String)
}

interface UpdateWidget {
    fun onUpdate(pos: Int, id: String, flag: Boolean, priority: String)
}

interface ProjectTreeLstnr {
    fun onProjectTreeParntClk(
        pos: Int, flag: Boolean, childlst: ArrayList<ChilderDataItem>, paddingStart: Int
    )
}

interface ChildOneListnr {
    fun onChildOneClick(pos: Int, obj: ChilderDataItem)
}
