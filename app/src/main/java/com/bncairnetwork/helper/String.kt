package com.bncairnetwork.helper

import java.text.SimpleDateFormat
import java.util.*

fun String.toIst(dateFormat: String): String {
    var returnDate = ""
    SimpleDateFormat(dateFormat, Locale.getDefault()).parse(this).also {
        Calendar.getInstance().apply {
            time = it
            add(Calendar.HOUR, 5)
            add(Calendar.MINUTE, 30)
            returnDate = SimpleDateFormat(dateFormat, Locale.getDefault()).format(time)
        }
    }
    return returnDate


}

fun String.toDate(dateFormat: String = "yyyy-MM-dd HH:mm:ss", timeZone: TimeZone = TimeZone.getTimeZone("UTC")): Date {
    val parser = SimpleDateFormat(dateFormat, Locale.getDefault())
    parser.timeZone = timeZone
    return parser.parse(this)
}

fun Date.formatTo(dateFormat: String, timeZone: TimeZone = TimeZone.getDefault()): String {
    val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
    formatter.timeZone = timeZone
    return formatter.format(this)
}