package com.bncairnetwork.helper

import android.view.View
import android.widget.ImageView
import com.bncairnetwork.R
import com.bncairnetwork.databinding.*
import com.bncairnetwork.webservices.ApiConstants

class UserRestrictionRepo {
    companion object {
        fun dashboardRestriction(bin: ActivityMainBinding, packageID: Int) {
            /*2. You can disable add button on dashboard for now as we are planning to add some more fields to it ---> Done*/
            /* if (packageID != ApiConstants.ibisID) View.GONE else View.VISIBLE*/
            bin.homeMainView.extendedFaMabBottom.visibility = View.GONE
        }

        fun advanceSearchRestriction(bin: ActivityAdvanceSearchBinding, packageID: Int) {
            bin.advanceSearchLay.generalSearchLay.recordLbl.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE
            bin.advanceSearchLay.generalSearchLay.recordSp.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE
            bin.advanceSearchLay.generalSearchLay.assigneesLbl.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE
            bin.advanceSearchLay.generalSearchLay.assigneesSp.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE

            bin.advanceSearchLay.generalSearchLay.createdlbl.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            bin.advanceSearchLay.generalSearchLay.createdtoEdxt.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            bin.advanceSearchLay.generalSearchLay.createdfromEdxt.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            bin.advanceSearchLay.generalSearchLay.updatedlbl.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            bin.advanceSearchLay.generalSearchLay.updatefromEdxt.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            bin.advanceSearchLay.generalSearchLay.updatetoEdxt.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE


            bin.advanceSearchCompanyLay.adSrchRcrdType.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE
            bin.advanceSearchCompanyLay.adSrchRcrdTypeSp.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE
            bin.advanceSearchCompanyLay.adSrchAssgn.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE
            bin.advanceSearchCompanyLay.adSrchAssgnSp.visibility =
                if (packageID == ApiConstants.ibisID) View.VISIBLE else View.GONE


        }

        fun addInteractionRestriction(bin: ActivityAddInteractionBinding, packageID: Int) {
            bin.Addcompnylbl.visibility =
                if (packageID == ApiConstants.bi_userID) View.INVISIBLE else View.VISIBLE
            bin.Addcontactlbl.visibility =
                if (packageID == ApiConstants.bi_userID) View.INVISIBLE else View.VISIBLE
        }

        fun interactionListScreenRestriction(bin: ActivityInteractionBinding, packageID: Int) {
            when (packageID) {
                ApiConstants.professionalID -> {
                    bin.defBtnLay.submitBtn.setBackgroundResource(R.drawable.dark_grey_crd_bg)
                }
                ApiConstants.ibisID -> {


                }
                ApiConstants.bi_userID -> {

                }
            }

        }

        fun interactionListAdptrRestriction(bin: IntractionCusItemLayBinding, packageID: Int) {
            bin.deletebtn.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            bin.sharebtn.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE
            bin.editbtn.visibility =
                if (packageID == ApiConstants.professionalID) View.GONE else View.VISIBLE


        }

        fun projectDetailRestricition(bin: ActivityProjectDetailsBinding, packageID: Int) {
            when (packageID) {
                ApiConstants.professionalID -> {
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.interactionImg.setImageResource(
                        R.drawable.ic_interaction_inactive
                    )
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.interactionImg.isClickable =
                        false
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.tagImg.setImageResource(
                        R.drawable.ic_tag_inactive
                    )
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.tagImg.isClickable =
                        false
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
                        R.drawable.ic_favorite_inactive
                    )
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.isClickable =
                        false
                    bin.shareInfoChatViewAppbarLay.infoBtn.visibility = View.GONE
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
                        View.GONE
                }
                ApiConstants.bi_userID -> {
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
                        View.GONE
                    bin.shareInfoChatViewAppbarLay.infoBtn.visibility = View.VISIBLE
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.visibility =
                        View.VISIBLE
                }
                ApiConstants.ibisID -> {
                    bin.shareInfoChatViewAppbarLay.infoBtn.visibility = View.VISIBLE
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
                        View.VISIBLE
                    bin.companyDetailsGeneralDetailLay.interactionTagFavoriteReminderEtcLay.favoriteImg.visibility =
                        View.VISIBLE
                }
            }
        }

        fun companyDetailRestriction(bin: ActivityCompanyDetailBinding, packageID: Int) {
            when (packageID) {
                ApiConstants.professionalID -> {
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.interactionImg.setImageResource(
                        R.drawable.ic_interaction_inactive
                    )
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.interactionImg.isClickable =
                        false
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.tagImg.setImageResource(
                        R.drawable.ic_tag_inactive
                    )
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.tagImg.isClickable =
                        false
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.setImageResource(
                        R.drawable.ic_favorite_inactive
                    )
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.isClickable =
                        false
                    bin.shareInfoChatViewAppbarLay.infoBtn.visibility = View.GONE
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
                        View.GONE

                }
                ApiConstants.bi_userID -> {
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
                        View.GONE
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.visibility =
                        View.VISIBLE

                }
                ApiConstants.ibisID -> {
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.switchImg.visibility =
                        View.VISIBLE
                    bin.comapanyDetailsGeneralLay.interactionTagFavoriteReminderEtcLay.favoriteImg.visibility =
                        View.VISIBLE
                }
            }

        }

        fun CompanyNDBidder_interactionTagReminderFavoriteSwitchRestriction(
            interactionView: ImageView?,
            TagView: ImageView?,
            ReminderView: ImageView?,
            FavoriteView: ImageView?,
            SwitchView: ImageView?,
            packageID: Int,
            isCompanyView: Boolean,
            isProjectView: Boolean
        ) {
            when (packageID) {
                ApiConstants.professionalID -> {
                    interactionView?.setImageResource(R.drawable.ic_interaction_inactive)
                    interactionView?.isClickable = false
                    TagView?.setImageResource(R.drawable.ic_tag_inactive)
                    TagView?.isClickable = false
                    FavoriteView?.setImageResource(R.drawable.ic_favorite_inactive)
                    FavoriteView?.isClickable = false

                }
                ApiConstants.bi_userID -> {

                }
                ApiConstants.ibisID -> {

                }
            }

        }

        fun interactionTagReminderFavoriteSwitchRestriction(
            interactionView: ImageView?,
            TagView: ImageView?,
            ReminderView: ImageView?,
            FavoriteView: ImageView?,
            SwitchView: ImageView?,
            packageID: Int,
            isCompanyView: Boolean,
            isProjectView: Boolean
        ) {
            when (packageID) {
                ApiConstants.professionalID -> {
                    interactionView?.setImageResource(R.drawable.ic_interaction_inactive)
                    interactionView?.isClickable = false

                    TagView?.setImageResource(R.drawable.ic_tag_inactive)
                    TagView?.isClickable = false

                    FavoriteView?.visibility = View.VISIBLE
                    FavoriteView?.setBackgroundResource(R.drawable.ic_favorite_inactive)
                    FavoriteView?.isClickable = !isCompanyView
                    SwitchView?.visibility = View.GONE
                }
                ApiConstants.bi_userID -> {
                    SwitchView?.visibility = View.GONE
                }
                ApiConstants.ibisID -> {

                }
            }

        }

    }

}