package com.bncairnetwork.webservices

import android.app.NotificationChannel
import android.app.NotificationManager
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.bncairnetwork.R
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class MyFirebaseMessagingService : FirebaseMessagingService() {
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        if (remoteMessage.data.isNotEmpty()) sendNotification(
            remoteMessage.data["title"],
            remoteMessage.data["body"],
            remoteMessage.data
        )
        else sendNotification(
            remoteMessage.notification?.title,
            remoteMessage.notification?.body,
            null
        )
    }

    override fun onNewToken(token: String) {
        println("-onNewToken--->${token}")
    }

    private fun sendNotification(
        title: String?,
        messageBody: String?,
        dataPayload: Map<String?, String?>?
    ) {
//        var intent : Intent?=null
//        intent = when(dataPayload?.get("type")){
//            ORDER_ACCEPT , ORDER_READY_FOR_PICKUP -> { Intent(this, OrderHistoryActivity::class.java).putExtra("type","upcoming").putExtra("notification",true) }
//            ORDER_DELIVERED,ORDER_REJECTED -> { Intent(this, OrderHistoryActivity::class.java).putExtra("type","history").putExtra("notification",true) }
//            VISIT_APP -> { Intent(this, MainActivity::class.java) }
//            ITEM_ADDED -> { Intent(this, MenuActivity::class.java).putExtra("_id", dataPayload["data"]).putExtra("type",KeyConstants.RESTAURANT) }
//            WELCOME -> { Intent(this, MainActivity::class.java) }
//            CREDIT_BY_COUPON -> { Intent(this, RewardActivity::class.java) }
//            CREDIT_REWARD -> { Intent(this, RewardActivity::class.java) }
//            else ->{ Intent(this, MainActivity::class.java) }
//        }
//        intent?.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
//        val pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val channelId = getString(R.string.notification_channel_id)
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_bnc_icon)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.drawable.ic_bnc_icon))
            .setContentTitle(title ?: "BncAirNetwork")
            .setContentText(messageBody ?: "You have new messasge")
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setVibrate(longArrayOf(1000, 1000))
            .setColor(ContextCompat.getColor(this, R.color.app_blue))
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDefaults(NotificationCompat.DEFAULT_SOUND)
//            .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val sound = defaultSoundUri
            val attributes =
                AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_NOTIFICATION).build()
            val channel =
                NotificationChannel(channelId, "Notification", NotificationManager.IMPORTANCE_HIGH)
            channel.enableLights(true)
            channel.enableVibration(true)
            channel.vibrationPattern = longArrayOf(1000, 1000, 1000)
            channel.setShowBadge(true)
            channel.lockscreenVisibility = NotificationCompat.VISIBILITY_PUBLIC
            channel.setSound(sound, attributes);
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(430, notificationBuilder.build())
    }

}