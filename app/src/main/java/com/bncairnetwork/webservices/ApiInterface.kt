package com.bncairnetwork.webservices

import com.bncairnetwork.pojo.*
import com.bncairnetwork.pojo.post.*
import com.bncairnetwork.pojo.post.company.CreateCompanyPost
import com.bncairnetwork.pojo.post.contact.CreateContactPost
import com.bncairnetwork.pojo.post.interaction.AddInteractionProjectNdCompanyPost
import com.bncairnetwork.pojo.post.interaction.DeleteInteractionPost
import com.bncairnetwork.pojo.post.interaction.ShareInteractionWithUserPost
import com.bncairnetwork.pojo.post.notification.PushNotificationPost
import com.bncairnetwork.pojo.post.password.ChnagePssdPost
import com.bncairnetwork.pojo.post.password.ForgotPasswordPost
import com.bncairnetwork.pojo.post.project.FavProPost
import com.bncairnetwork.pojo.post.reminder.EditReminderPost
import com.bncairnetwork.pojo.post.reminder.MarkAsCompletePost
import com.bncairnetwork.pojo.post.reminder.ReminderForCompanyPost
import com.bncairnetwork.pojo.post.reminder.ReminderForProjectPost
import com.bncairnetwork.pojo.post.tag.EditTagPost
import com.bncairnetwork.pojo.post.tag.RemoveTagPost
import com.bncairnetwork.pojo.post.widget.ActivateWidgetPost
import com.bncairnetwork.pojo.post.widget.DeactivateWidgetPost
import com.bncairnetwork.pojo.post.widget.RearrangeWidgetPost
import com.bncairnetwork.pojo.response.*
import com.bncairnetwork.pojo.response.Dashboard.NewsNdHighlightsLstRes
import com.bncairnetwork.pojo.response.Dashboard.NotificationCountRes
import com.bncairnetwork.pojo.response.Dashboard.SavedSearchRes
import com.bncairnetwork.pojo.response.KeyContact.GetKEyContactRes
import com.bncairnetwork.pojo.response.company.CompanyDetailByIDRes
import com.bncairnetwork.pojo.response.company.GetAutoComCompaniesRes
import com.bncairnetwork.pojo.response.company.GetCompanyForCompnyBidderRes
import com.bncairnetwork.pojo.response.company.GetInteractionCompny
import com.bncairnetwork.pojo.response.compnyAndBidders.GetBiddersForComnyBiddrRes
import com.bncairnetwork.pojo.response.contact.AddContactRes
import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.bncairnetwork.pojo.response.notification.GetNotificationLstRes
import com.bncairnetwork.pojo.response.project.*
import com.bncairnetwork.pojo.response.reminder.EditReminderRes
import com.bncairnetwork.pojo.response.reminder.GetReminderLstRes
import com.bncairnetwork.pojo.response.reminder.ReminderRes
import com.bncairnetwork.pojo.response.reminder.project_and_company.GetReminderLstByIdRes
import com.bncairnetwork.pojo.response.requesDemo.GetCitiesForReqDemoRes
import com.bncairnetwork.pojo.response.requesDemo.GetCountriesForReqDemoRes
import com.bncairnetwork.pojo.response.schedules.GetSchedulesLstRes
import retrofit2.Call
import retrofit2.http.*


interface ApiInterface {


    @FormUrlEncoded
    @POST("auth/user/login_token/?mobile=true")
    fun login(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<LoginRes>

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "auth/user/login_token/?mobile=true", hasBody = true)
    fun logout(
        @Field("username") username: String,
        @Field("password") password: String
    ): Call<ComRes>


    @GET("projects/keyword_search/?call_frm=search&source=search_result&search_tab=project&search_st=keyword&mobile=true")
    fun getprojectBykeyword(
        @Query("keyword") keyword: String, @Query("offset") offsetval: String
    ): Call<ProjectSearchRes>

    @GET("companies/keyword_search/?call_frm=search&search_st=keyword&search_tab=company&user_tags=%5B%5D&mobile=true")
    fun getCompaniesBykeyword(
        @Query("keyword") keyword: String, @Query("offset") offsetval: String
    ): Call<GetCompaniesBySearchRes>

    @POST("/api/interact/reminders/?mobile=true")
    fun addReminderForCompany(@Body post: ReminderForCompanyPost): Call<ReminderRes>

    @POST("/api/interact/reminders/?mobile=true")
    fun addReminderForProject(@Body post: ReminderForProjectPost): Call<ReminderRes>

    @GET("/api/mobile/popup/")
    fun popup(): Call<PopupRes>

    @POST("/api/mobile/popup/{id}")
    fun upadate_poup(@Path("id") id: String): Call<PopupRes>

    @POST("/api/interact/interactions/")
    fun addInteraction(
        @Body post: AddInteractionProjectNdCompanyPost
    ): Call<ComRes>

    @POST("/api/interact/interactions/")
    fun deleteInteraction(@Body del_post: DeleteInteractionPost): Call<ComRes>

    @PUT("/api/interact/interactions/{Id}/")
    fun editInteraction(
        @Path("Id") id: String,
        @Body post: AddInteractionProjectNdCompanyPost
    ): Call<ComRes>

    @GET("/api/auth/countries/cities/")
    fun getCitiesByCountry(@Query("countries") arryOfCountryIds: String): Call<GetCitiesByCountryRes>

    @GET("/api/companies/multi/contacts/?ibis_view=true&add_pop_up=true")
    fun getContactsLstByMultiCompanyIDs(@Query("companies") ids: String): Call<GetConLstByMultiComIdsRes>

    @GET("/api//interact/reminders/?mobile=true")
    fun getReminderLst(): Call<GetReminderLstRes>

    @GET("/api/mobile/view/notifications/")
    fun clear_all_notification(): Call<ComRes>

    @GET("/api/mobile/notifications/count/")
    fun getNotificationCount(): Call<NotificationCountRes>

    @GET("/api/mobile/notifications/{id}/")
    fun clearNotificationByID(@Path("id") notificaitonID: String): Call<ComRes>

    @GET("/api/invite/social/code/")
    fun getInvitationUrl(): Call<InvitationUrlRes>

    @GET("/api//interact/{type}/{ID}/?interaction_type=[\"Reminders\"]&mobile=true")
    fun getReminderByID(
        @Path("type") type: String = "Project",
        @Path("ID") id: String,
        @Query("ibis_view") isIBIS: Boolean
    ): Call<GetReminderLstByIdRes>

    @PUT("/api/interact/reminders/{id}/")
    fun editReminder(
        @Body post: EditReminderPost,
        @Path("id") reminderID: String
    ): Call<EditReminderRes>

    @PUT("/api//interact/reminders/{id}/")
    fun markRemAsComp(@Path("id") id: String, @Body pos: MarkAsCompletePost): Call<ComRes>

    @DELETE("/api//interact/reminders/{id}/")
    fun deleteReminder(@Path("id") reminderID: String): Call<ComRes>

    /*** pass project---> for project tag else companies*/
    @POST("/api/users/{type}/update_tags/")
    fun addOrUpdateTag(
        @Body post: AddUpdateTagPost,
        @Path("type") type: String = "projects"
    ): Call<UpdateAddTagRes>

    @POST("/api/users/delete/tags/")
    fun removeTagByName(@Body posRemoveTagPost: RemoveTagPost): Call<ComRes>

    @GET("/api/auth/policy/?mobile=true")
    fun privacyPolicyAccept(): Call<ComRes>

    @POST("/api/mobile/widgets/manage/")
    fun DeactivateWidget(@Body post: DeactivateWidgetPost): Call<ComRes>

    @POST("/api/mobile/widgets/manage/")
    fun ActivateWidget(@Body post: ActivateWidgetPost): Call<ComRes>

    @POST("/api/mobile/widgets/manage/")
    fun RearrangeWidget(@Body post: RearrangeWidgetPost): Call<ComRes>

    @GET("/api/mobile/versions/?type=android")
    fun getappVrsn(): Call<AppVersionRes>

    @POST("/api/users/projects/favourite/")
    fun makeFavForProject(@Body post: FavProPost): Call<ComRes>

    @HTTP(method = "DELETE", path = "/api/users/projects/favourite/", hasBody = true)
    fun removeFavFromProject(@Body post: FavProPost): Call<ComRes>

    @GET("/api/mobile/widgets/{widgetID}/?elastic=true")
    fun getProjectNearMe(
        @Path("widgetID") widgetID: String,
        @Query("latitude") latitude: String,
        @Query("longitude") longitude: String
    ): Call<ProjectNearMeRes>

    @POST("/api/invite/bulk_invitation/")
    fun sendInvitationByEmail(@Body post: InvitationByEmailPost): Call<ComRes>

    @POST("/api/interact/interactions/{id}/share/")
    fun shareInteractionWithUser(
        @Body post: ShareInteractionWithUserPost,
        @Path("id") interactionID: String
    ): Call<ComRes>

    @POST("/api/interact/reminders/{id}/share/")
    fun shareReminderWithUser(
        @Body post: ShareInteractionWithUserPost,
        @Path("id") interactionID: String
    ): Call<ComRes>


    @GET("/api//projects/{id}/interactions/?mobile=true")
    fun getInteractionOnProjectByID(
        @Path("id") id: String,
        @Query("ibis_view") isIBIS: Boolean
    ): Call<GetInteractionProjectRes>

    @GET("/api/companies/{id}/interactions/?mobile=true")
    fun getInteractionOnCompnyByID(
        @Path("id") id: String,
        @Query("ibis_view") isIBIS: Boolean
    ): Call<GetInteractionCompny>

    @GET("projects/{id}/?mobile=true")
    fun getProjectDetailsByID(
        @Path("id") id: String,
        @Query("ibis_view") ibisView: String? = null
    ): Call<ProjectResById>


    @POST("/api/users/favourite_companies/remove/")
    fun removeFavFromCompany(@Body post: ArrayList<Int>): Call<ComRes>

    @POST("/api/users/favourite_company/")
    fun addFavForCompany(@Body post: ArrayList<Int>): Call<ComRes>

    @POST("/api/users/edit/tags/")
    fun editTag(@Body post: EditTagPost): Call<ComRes>

    @GET("/api//companies/{id}/contacts/?mobile=true")
    fun getkeycontactslst(
        @Path("id") id: String,
        @Query("ibis_view") ibisView: String? = null
    ): Call<GetKEyContactRes>

    @GET("/api/companies/{Id}/project_bidders/?mobile=true")
    fun getLiveProjects(@Path("Id") id: String): Call<LiveProjectRes>

    @GET("/api/mobile/widgets/active/?my_companies=True&quick_link=8&mobile=true&elastic=true")
    fun getDashboardWidgetData(
        @Query("latitude") latitude: String,
        @Query("longitude") longitude: String
    ): Call<DashboardWidgetDataRes>


    @GET("/api/news/get_news/{id}/?mobile=true")
    fun getNewsDetails(@Path("id") id: String): Call<NewsDetailRes>

    @GET("/api/companies/search/?call_frm=search&mobile=True&search_st=advanced_company&search_tab=company")
    fun advanceSearchByCompany(
        @Query("company_name") companyName: String? = null,
        @Query("company_country") companyCountry: ArrayList<Int?>? = null,
        @Query("company_city") compnyCity: ArrayList<Int?>? = null,
        @Query("tags") tags: String? = null,
        @Query("company_type_filter") compnyTypeFilter: ArrayList<Int?>? = null,
        @Query("subtype") subType: String? = null,
        @Query("favourate_companies") favComanies: Boolean? = null,
        @Query("project_stage") projectStage: String? = null,
        @Query("record_type_filter") recordTypeFilter: String? = null,
        @Query("interact_date_types") interactDataTypes: String? = null,
        @Query("offset") offsetval: String? = null
    ): Call<GetCompaniesBySearchRes>


    @GET("/api//interact/companies/users/")
    fun getShareWithUserLst(@Query("companies") id: String): Call<ShareWithUserLstRes>

    @GET("/api/projects/search/?call_frm=search&offset=0&mobile=True&search_st=advanced_project&search_tab=project")
    fun advanceSearchByProject(
        @Query("project_name") project_name: String? = null,
        @Query("bnc_project_number") bnc_project_number: String? = null,
        @Query("project_stage") project_stage: String? = null,
        @Query("value_from") value_from: String? = null,
        @Query("value_to") value_to: String? = null,
        @Query("tags") tags: String? = null,
        @Query("record_type_filter") record_type_filter: String? = null,
        @Query("interact_date_types") interactDataTypes: String? = null,
        @Query("location") location: String? = null,
        @Query("country") country: String? = null,
        @Query("city") city: String? = null,
        @Query("sector") sector: String? = null,
        @Query("new_project_type") new_project_type: String? = null,
        @Query("project_subtype") project_subtype: String? = null,
        @Query("companies") companies: String? = null,
        @Query("date_types") date_types: String? = null,
        @Query("roles") roles: String? = null,
        @Query("widget_identifier") widgetIdentifier: String? = null,
        @Query("latitude") latitude: String? = null,
        @Query("longitude") longitude: String? = null,
        @Query("nearby_stage") nearby_stage: String? = null


    ): Call<ProjectSearchRes>

    @GET
    fun advanceSearchByCompanyByUrl(
        @Url url: String? = null
    ): Call<GetCompaniesBySearchRes>

    @GET
    fun advanceSearchByProjectByUrl(
        @Url url: String? = null
    ): Call<ProjectSearchRes>

    @GET
    fun getProjectByTags(@Url url: String?): Call<ProjectSearchRes>

    @GET
    fun getSavedSearchResults(@Url url: String? = null): Call<SavedSearchRes>

    @GET
    fun getClaimedNewsLst(@Url url: String? = null): Call<ProjectNewsLstRes>

    @GET
    fun getMyCompanies(@Url url: String? = null): Call<GetCompaniesBySearchRes>

    @GET
    fun getProjects(@Url url: String? = null): Call<ProjectSearchRes>

    @GET
    fun sortByProjectByUrl(
        @Url url: String? = null
    ): Call<ProjectSearchRes>

    @GET
    fun filterByProjectByUrl(
        @Url url: String? = null
    ): Call<ProjectSearchRes>

    @GET
    fun filterByCompaniesByUrl(
        @Url url: String? = null
    ): Call<GetCompaniesBySearchRes>


    @GET
    fun sortByCompaniesByUrl(
        @Url url: String? = null
    ): Call<GetCompaniesBySearchRes>


    @POST("/api/marketing_api/register_user/")
    fun requestDemo(@Body post: RequestDemoPost): Call<ComRes>

    @POST("/api/marketing_api/lead_countries/")
    fun getCountriesForReqDemo(): Call<GetCountriesForReqDemoRes>

    @POST("/api/marketing_api/lead_countries/{ID}/cities/")
    fun getCitiesForReqDemo(@Path("ID") countryID: String): Call<GetCitiesForReqDemoRes>

    @POST("/api/marketing_api/register_user/")
    fun getFreeAccount(@Body post: FreeAccountPost): Call<ComRes>

    @FormUrlEncoded
    @POST("/api/marketing_api/company_auto_complete/")
    fun getCompaniesLst4ReqDemo(@Field("keyword") query: String): Call<GetCompaniesForReqDemo>

    @POST("/api/projects/log_view/{project_id}/")
    fun updateProjectView(
        @Path("project_id") id: String
    ): Call<ComRes>

    @GET("credits/projects/{project_id}/claim/")
    fun exhaustCredit(@Path("project_id") projectID: String): Call<ComRes>

    @GET("companies/{id}/detail/?mobile=true")
    fun getCompanyDetailsByID(
        @Path("id") id: String,
        @Query("ibis_view") ibisView: String? = null
    ): Call<CompanyDetailByIDRes>

    @GET("projects/{id}/tree/?mobile=true")
    fun getProjectTreeByID(@Path("id") id: String): Call<ProjectTreeByIDRes>

    @POST("/api/ibis/companies/{company_id}/contacts/")
    fun addContact(
        @Path("company_id") id: String,
        @Body post: CreateContactPost
    ): Call<AddContactRes>

    @GET("/api/users/get_tags/")
    fun gettags(): Call<GetAllTagsRes>

    @GET("mobile/widgets/")
    fun getWidgetsLst(): Call<WidgetsLstRes>

    @GET("/api/shopping/research_packs/{id}/")
    fun getProjectInfo(@Path("id") projectID: String): Call<ProjectClaimInfoRes>

    @POST("/api/users/information/")
    fun createReqInfo(@Body post: ReqInfoPost): Call<ComRes>

    @GET("/api/projects/{id}/companies/?mobile=true&ibis_view=true")
    fun getComniesForComnyBidders(@Path("id") id: String): Call<GetCompanyForCompnyBidderRes>

    @GET("/api/projects/{id}/bidders/?mobile=true&ibis_view=true")
    fun getBiddersForComnyBidders(@Path("id") id: String): Call<GetBiddersForComnyBiddrRes>

    @GET("/api/projects/{id}/schedules/?mobile=true")
    fun getSchedules(@Path("id") id: String): Call<GetSchedulesLstRes>

    @GET("/api/companies/{id}/references/?mobile=true")
    fun getProjectRefByID(@Path("id") id: String): Call<GetProjectRefLstRes>

    @GET("/api//auth/master_data/?mobile=true")
    fun getMasterData(): Call<GetMasterDataRes>

    @POST("/api/auth/password/")
    fun chnagePssd(@Body post: ChnagePssdPost): Call<ComRes>

    @PUT("/api/mobile/push_notifications/")
    fun updatePushNotification(@Body post: PushNotificationPost): Call<ComRes>

    @POST("/api/ibis/companies/")
    fun createCompany(@Body post: CreateCompanyPost): Call<ComRes>

    @GET("/api//projects/{id}/notes/?mobile=true")
    fun getUpdatesNdHighlights4Project(@Path("id") id: String): Call<GetProjectUpdatesNdHighlight>

    @GET("/api/companies/company_auto_complete/")
    fun getcompanies_autocomp(@Query("keyword") keyword: String): Call<GetAutoComCompaniesRes>


    @GET("/api/projects/auto_complete/")
    fun getprojects_autocomp(@Query("keyword") keyword: String): Call<GetAutoComCompaniesRes>


    @GET("/api/mobile/notifications/")
    fun getnotificationLst(): Call<GetNotificationLstRes>


    @POST("/api/auth/user/password/")
    fun forgotPassword(@Body post: ForgotPasswordPost): Call<ForgotPasswordRes>

    @GET("/api/projects/{id}/news/?mobile=True")
    fun getProjectNewsLst(@Path("id") id: String): Call<ProjectNewsLstRes>

    /***view all option on home screen*/
    @GET("/api//news/search/?widget_identifier=mobile_project_news_highlights&mobile=true")
    fun getNewsNdHighlightLst(): Call<NewsNdHighlightsLstRes>

}