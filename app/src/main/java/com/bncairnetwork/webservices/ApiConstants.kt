package com.bncairnetwork.webservices

object ApiConstants {
    var tag_project_url =
        "https://staging.bncnetwork.net/api/projects/search/?call_frm=search&offset=0&mobile=True&search_st=advanced_project&search_tab=project&"
    var tag_company_url =
        "https://staging.bncnetwork.net/api/companies/search/?call_frm=search&mobile=True&search_st=advanced_company&search_tab=company&offset=0"
    const val BASEURL = "https://staging.bncnetwork.net/api/"
    const val ProjectNews = "Project News"
    const val professionalID = 3
    const val bi_userID = 1
    var padding = 0
    const val ibisID = 7
    var FROM = "HOME_SCREEN_AC"
    const val Created = "Created"
    var arry = arrayListOf<String>()
    const val Updated = "Updated"
    const val nine = "9"
    const val project_sortByBasicSearchUrl =
        "/api/projects/search/?call_frm=search&mobile=True&search_st=advanced_project&search_tab=project&project_name=&sort=&sort_criteria=&offset=0"
    const val company_sortByBasicSearchUrl =
        "/api/companies/search/?call_frm=search&mobile=True&search_st=advanced_company&search_tab=company&company_name=&sort=&sort_criteria=&offset=0"
    const val project_filterByBasicSearchUrl =
        "/api/projects/search/?call_frm=search&mobile=True&search_st=advanced_project&search_tab=project&project_name=&record_type_filter=&quick_link=&offset=0"
    const val company_filterByBasicSearchUrl =
        "/api/companies/search/?call_frm=search&mobile=True&search_st=advanced_company&search_tab=company&company_name=&record_type_filter=&quick_link=&offset=0"
    const val Saved_Search = "Saved Searches"
    const val QuickLinks = "Quicklinks"
    const val ProjNearMe = "Projects Near Me"
    const val ShareErnCrdt = "Share AIR & Earn Credits"
    const val ProjNewsNdHiglight = "Project News & Highlights"
    const val FavWatchlist = "Favorite Watchlist"
    const val packageID = "package"
    const val YMD_PATTERN = "yyyy-MM-dd"
    const val DMY_PATTERN = "dd MMM,yyyy"
    const val ApplyChanges = "Apply Changes"
    var advanceCompanyUrl = ""
    var advanceProjectUrl = ""
    const val Companies = "Companies"
    const val KeyContact = "Key Contacts"
    const val activate = "activate"
    const val identifier = "register"
    const val isUserWithTempPssd = "isUserWithTempPssd"
    const val TempPwd = "TempPwd"
    const val Schedule = "Schedule"
    const val contract_awarded = "Contract Awarded To"
    const val stages_changed = "Stage Changed To"
    const val source_domain = "BNC"
    const val ComAdvnceSearchApiResults = "ComAdvnceSearchApiResults"
    const val ProjectAdvnceSearchApiResults = "ProjectAdvnceSearchApiResults"
    const val deactivate = "deactivate"
    const val previousAc = "previousAc"
    const val Bidders = "Bidders"
    const val CompanyNdBidders = "CompanyNdBidders"
    const val Project = "Project"
    const val LinkedProject = "Linked Project"
    const val BncProject = "BNC Project"
    const val InternalProject = "Internal Project"
    const val LinkedCompany = "Linked Company"
    const val BncCompany = "BNC Company"
    const val InternalCompany = "Internal Company"
    const val username = "username"
    const val LiveProject = "Live Projects"
    const val Schedules = "Schedules"
    const val Select = "Select"
    const val Pending = "Pending"
    const val UpdatesAndHighlights = "Updates & Highlights"
    const val ProjectTree = "Project Tree"
    const val Interactions = "Interactions"
    const val Reminders = "Reminders"
    const val Company = "Company"
    const val ProjectReference = "Project References"
    const val PHONE_TBALE_ORIENT = 1
    const val EMAIL_TBALE_ORIENT = 1

}