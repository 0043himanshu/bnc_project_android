package com.bncairnetwork.pojo.post.tag

data class RemoveTagPost(
    val tag: String
)
