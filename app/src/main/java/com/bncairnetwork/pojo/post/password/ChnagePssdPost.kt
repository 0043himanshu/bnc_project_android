package com.bncairnetwork.pojo.post.password

data class ChnagePssdPost(
    val old_password: String,
    val confirm_password: String,
    val new_password: String
)
