package com.bncairnetwork.pojo.post

import com.google.gson.annotations.SerializedName

data class FreeAccountPost(

	@field:SerializedName("identifier")
	val identifier: String? = null,

	@field:SerializedName("source_page")
	val sourcePage: String? = null,

	@field:SerializedName("source_domain")
	val sourceDomain: String? = null,

	@field:SerializedName("area_code")
	val areaCode: String? = null,

	@field:SerializedName("demo")
	val demo: Boolean? = null,

	@field:SerializedName("country_code")
	val countryCode: String? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("company_name")
	val companyName: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("phone_number")
	val phoneNumber: String? = null,

	@field:SerializedName("company")
	val company: Int? = null,

	@field:SerializedName("designation")
	val designation: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
