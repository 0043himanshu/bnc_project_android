package com.bncairnetwork.pojo.post.tag

data class EditTagPost(
    val new_tag:String,
    val old_tag:String
)
