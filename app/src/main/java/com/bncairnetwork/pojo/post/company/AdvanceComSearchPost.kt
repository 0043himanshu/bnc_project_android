package com.bncairnetwork.pojo.post.company

import com.google.gson.annotations.SerializedName

data class AdvanceComSearchPost(

	@field:SerializedName("company_type_filter")
	val companyTypeFilter: ArrayList<Int?>? = null,

	@field:SerializedName("interact_date_types")
	val interactDateTypes: ArrayList<InteractDateTypesItem>,

	@field:SerializedName("record_type_filter")
	val recordTypeFilter: ArrayList<String?>? = null,

	@field:SerializedName("subtype")
	val subtype: ArrayList<SubtypeItem>,

	@field:SerializedName("project_stage")
	val projectStage: ArrayList<Int?>? = null,

	@field:SerializedName("company_name")
	val companyName: String? = null,

	@field:SerializedName("favourate_companies")
	val favourateCompanies: Boolean? = null,

	@field:SerializedName("company_country")
	val companyCountry: ArrayList<Int?>? = null,

	@field:SerializedName("company_city")
	val companyCity: ArrayList<Int?>? = null,

	@field:SerializedName("tags")
	val tags: ArrayList<TagsItem>
)

data class InteractDateTypesItem(
	val date_range: Any? = null,

	@field:SerializedName("record")
	val record: String? = null,

	@field:SerializedName("action")
	val action: String? = null,

	@field:SerializedName("users")
	val users: String?=null
)

data class DateRange(
	val date_range: String? = null,

	val end_date: String? = null,
	val start_date: String? = null
)

data class TagsItem(

	@field:SerializedName("id")
	val id: String? = null
)

data class SubtypeItem(

	@field:SerializedName("id")
	val id: Int? = null
)
