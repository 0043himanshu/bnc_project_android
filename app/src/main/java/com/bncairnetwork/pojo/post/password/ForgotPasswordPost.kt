package com.bncairnetwork.pojo.post.password

data class ForgotPasswordPost(
    val email:String
)