package com.bncairnetwork.pojo.post.interaction

data class ShareInteractionWithUserPost(
    val email_notification: Boolean,
    val users: ArrayList<Int>
)
