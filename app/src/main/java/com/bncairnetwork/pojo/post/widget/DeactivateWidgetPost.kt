package com.bncairnetwork.pojo.post.widget

data class DeactivateWidgetPost(
    val action: String,
    val user_widget: Int

)
