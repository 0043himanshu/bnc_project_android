package com.bncairnetwork.pojo.post.widget

data class ActivateWidgetPost(
    val action: String,
    val widget: Int,
    val priority: Int
)
