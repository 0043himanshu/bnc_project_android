package com.bncairnetwork.pojo.post.reminder

import com.google.gson.annotations.SerializedName

data class ReminderForCompanyPost(

	@field:SerializedName("reminder_text")
	val reminderText: String? = null,
	val company_id: String,
	val selected_user:ArrayList<Int>,
	val content_type:String,
	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("note")
	val note: String? = null,

	@field:SerializedName("mobile")
	val mobile: Boolean? = null,

	@field:SerializedName("action")
	val action: String? = null,

	@field:SerializedName("time")
	val time: String? = null,

	@field:SerializedName("email_notification")
	val emailNotification: Boolean? = null
)
