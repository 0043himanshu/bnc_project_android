package com.bncairnetwork.pojo.post

data class InvitationByEmailPost(
    val emails: ArrayList<String>
)
