package com.bncairnetwork.pojo.post

import com.google.gson.annotations.SerializedName

data class RequestDemoPost(

    @field:SerializedName("company_phone")
    val companyPhone: String? = null,

    @field:SerializedName("country")
    val country: Int? = null,

    @field:SerializedName("source_page")
    val sourcePage: String? = null,

    @field:SerializedName("identifier")
    val identifier: String? = null,

    @field:SerializedName("extra_params")
    val extraParams: ExtraParams? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("source_domain")
    val sourceDomain: String? = null,

    @field:SerializedName("area_code")
    val areaCode: String? = null,

    @field:SerializedName("demo")
    val demo: Boolean? = null,

    @field:SerializedName("country_code")
    val countryCode: String? = null,

    @field:SerializedName("samplereport")
    val samplereport: Boolean? = null,

    @field:SerializedName("phone")
    val phone: String? = null,

    @field:SerializedName("company_name")
    val companyName: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("phone_number")
    val phoneNumber: String? = null,

    @field:SerializedName("company")
    val company: String? = null,

    @field:SerializedName("designation")
    val designation: String? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("bulletin")
    val bulletin: Boolean? = null
)

data class ExtraParams(
    @field:SerializedName("linkedin")
    val linkedin: String? = null
)
