package com.bncairnetwork.pojo.post

data class AddUpdateTagPost(
    val active_tags: ArrayList<String>,
    val inactive_tags: ArrayList<String>,
    val projects: ArrayList<String>,
    val companies: ArrayList<String>
)

