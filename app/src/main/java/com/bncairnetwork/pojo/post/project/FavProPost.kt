package com.bncairnetwork.pojo.post.project

data class FavProPost(
    val project_id:String,
    val add_child_projects:Boolean
)
