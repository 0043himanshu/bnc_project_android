package com.bncairnetwork.pojo.post.interaction

import com.bncairnetwork.pojo.response.FullUsersItem
import com.google.gson.annotations.SerializedName

data class AddInteractionProjectNdCompanyPost(

    @field:SerializedName("reminder_text")
    val reminderText: String? = null,

    @field:SerializedName("note")
    val note: String? = null,

    val company: Int? = null,

    @field:SerializedName("company_ids")
    val companyIds: List<Int?>? = null,

    @field:SerializedName("interaction")
    val interaction: String? = null,

    @field:SerializedName("reminder_date")
    val reminderDate: String? = null,

    @field:SerializedName("interaction_type")
    val interactionType: Int? = null,

    @field:SerializedName("project")
    val project: Int? = null,

    @field:SerializedName("from")
    val from: String? = null,

    @field:SerializedName("interaction_date")
    val interactionDate: String? = null,

    @field:SerializedName("selected_user")
    val selectedUser: ArrayList<FullUsersItem?>? = null,

    @field:SerializedName("email_notification")
    val emailNotification: Boolean? = null,

    @field:SerializedName("company_contacts")
    val companyContacts: ArrayList<String>? = null
)

data class SelectedUserItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("designation")
    val designation: String? = null,

    @field:SerializedName("status")
    val status: String? = null
)
