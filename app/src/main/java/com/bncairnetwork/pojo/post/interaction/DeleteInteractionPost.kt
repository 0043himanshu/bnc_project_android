package com.bncairnetwork.pojo.post.interaction

data class DeleteInteractionPost(
    val from: String = "delete",
    val ids: ArrayList<String>? = null
)
