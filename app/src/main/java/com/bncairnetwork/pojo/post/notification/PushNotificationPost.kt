package com.bncairnetwork.pojo.post.notification

data class PushNotificationPost(
    val mobile: Boolean,
    val activate: Boolean,
    val notification: String
)
