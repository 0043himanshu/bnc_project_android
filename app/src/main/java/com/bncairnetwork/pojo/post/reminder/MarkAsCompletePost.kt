package com.bncairnetwork.pojo.post.reminder

data class MarkAsCompletePost(
    val email_notification: String,
    val action: String,
    val mobile: Boolean = true
)
