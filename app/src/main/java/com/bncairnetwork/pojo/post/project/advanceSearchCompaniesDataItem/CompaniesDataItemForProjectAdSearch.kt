package com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem

data class CompaniesDataItemForProjectAdSearch(
    val name:String?,
    val exact:String?,
    val bidders:String?,
    val contracts:String?

)
