package com.bncairnetwork.pojo.post.company

import com.google.gson.annotations.SerializedName

data class CreateCompanyPost(

	@field:SerializedName("phone_area_code1")
	val phoneAreaCode1: String? = null,

	@field:SerializedName("country")
	val country: Int? = null,

	@field:SerializedName("city")
	val city: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("mobile")
	val mobile: Boolean? = null,

	@field:SerializedName("categories")
	val categories: List<Any?>? = null,

	@field:SerializedName("phone_country_code1")
	val phoneCountryCode1: String? = null,

	@field:SerializedName("email1")
	val email: String? = null,

	@field:SerializedName("phone1")
	val phone1: String? = null
)
