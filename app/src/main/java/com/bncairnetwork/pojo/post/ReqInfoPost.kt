package com.bncairnetwork.pojo.post

data class ReqInfoPost(
    val information:String,
    val content_type:String,
    val information_value:String,
    val object_id:String,
    val request_type:String?=null
)
