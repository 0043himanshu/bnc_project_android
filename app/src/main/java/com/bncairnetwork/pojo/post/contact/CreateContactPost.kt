package com.bncairnetwork.pojo.post.contact

import com.google.gson.annotations.SerializedName

data class CreateContactPost(

	@field:SerializedName("phone_area_code1")
	val phoneAreaCode1: String? = null,

	@field:SerializedName("email1")
	val email1: String? = null,

	@field:SerializedName("mobile")
	val mobile: Boolean? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("designation")
	val designation: Int? = null,

	@field:SerializedName("linkedin")
	val linkedin: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("department")
	val department: String? = null,

	@field:SerializedName("phone_country_code1")
	val phoneCountryCode1: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("phone1")
	val phone1: String? = null
)
