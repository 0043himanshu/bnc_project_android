package com.bncairnetwork.pojo.post.widget

import com.google.gson.annotations.SerializedName

data class RearrangeWidgetPost(

	@field:SerializedName("user_widgets")
	val userWidgets: ArrayList<UserWidgetsItem>,

	@field:SerializedName("action")
	val action: String? = null
)

data class UserWidgetsItem(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("priority")
	val priority: Int? = null
)
