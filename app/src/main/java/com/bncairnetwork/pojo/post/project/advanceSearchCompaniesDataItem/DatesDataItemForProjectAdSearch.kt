package com.bncairnetwork.pojo.post.project.advanceSearchCompaniesDataItem

import com.bncairnetwork.pojo.post.company.InteractDateTypesItem

data class DatesDataItemForProjectAdSearch(
    val type: String?,
    val subtype: String?,
    val date_range: DateRangeDataItem?
)

data class DateRangeDataItem(
    val date_range:String?,
    val start_date:String?,
    val end_date:String?
)
