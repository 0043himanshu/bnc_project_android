package com.bncairnetwork.pojo.response.requesDemo

import com.google.gson.annotations.SerializedName

data class GetCountriesForReqDemoRes(

	@field:SerializedName("countries")
	val countries: List<CountriesItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CountriesItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
