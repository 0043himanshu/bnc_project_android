package com.bncairnetwork.pojo.response.project

import com.google.gson.annotations.SerializedName

data class GetInteractionProjectRes(

    @field:SerializedName("display_title")
    val displayTitle: String? = null,

    @field:SerializedName("project")
    val project: Project? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("switch")
    val jsonMemberSwitch: Boolean? = null,

    @field:SerializedName("interactions")
    val interactions: ArrayList<InteractionDataItem?>? = null
)

data class IrisProjectDetails(

    @field:SerializedName("has_iris_project")
    val hasIrisProject: Boolean? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class Project(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("completion_percentage")
    val completionPercentage: Any? = null,

    @field:SerializedName("iris_project_details")
    val irisProjectDetails: IrisProjectDetails? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("assignees")
    val assignees: List<Any?>? = null,

    @field:SerializedName("views_left")
    val viewsLeft: Any? = null,

    @field:SerializedName("bnc_project_number")
    val bncProjectNumber: String? = null,

    @field:SerializedName("type_of_project")
    val typeOfProject: String? = null,

    @field:SerializedName("project_subtype")
    val projectSubtype: String? = null,

    @field:SerializedName("authorized")
    val authorized: Boolean? = null,

    @field:SerializedName("spent_value")
    val spentValue: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: Double? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("area")
    val area: Any? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("project_type")
    val projectType: String? = null,

    @field:SerializedName("estimated_completion")
    val estimatedCompletion: String? = null,

    @field:SerializedName("created")
    val created: String? = null,

    @field:SerializedName("bnc_version")
    val bncVersion: Any? = null,

    @field:SerializedName("record_type")
    val recordType: String? = null,

    @field:SerializedName("assignees_obj")
    val assigneesObj: List<Any?>? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("progress")
    val progress: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("latlongstring")
    val latlongstring: String? = null,

    @field:SerializedName("updated")
    val updated: String? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null
)

data class InteractionDataItem(

    @field:SerializedName("date")
    val date: String? = null,

    @field:SerializedName("reminder")
    val reminder: InteractionReminderDataItem? = null,

    @field:SerializedName("interaction")
    val interaction: String? = null,

    @field:SerializedName("interaction_type")
    val interactionType: String? = null,
//
//    val project: ProjectInteractionDataItem? = null, /*ProjectInteractionDataItem ----> model of project that user selected while adding interaction*/

    @field:SerializedName("company")
    val company: List<CompanyItem?>? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("users")
    val users: List<UsersItem?>? = null,

    val company_contact: ArrayList<String>? = null
)

data class ProjectInteractionDataItem(
    val project: String? = null,
    val favourite: Boolean? = null,
    val id: Int? = null,
    val tags: ArrayList<String>? = null
)

data class UsersItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class InteractionReminderDataItem(
    val date: String? = null,
    val reminder: String? = null,
    val id: String? = null
)


data class CompanyItem(

    @field:SerializedName("company")
    val company: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("favourite")
    val favourite: Boolean? = null,

    @field:SerializedName("tags")
    val tags: List<Any?>? = null
)
