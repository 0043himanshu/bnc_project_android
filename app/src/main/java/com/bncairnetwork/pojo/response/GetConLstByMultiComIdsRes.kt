package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class GetConLstByMultiComIdsRes(
    @field:SerializedName("contacts")
    val contacts: ArrayList<ContactsItem>? = null,
    @field:SerializedName("status")
    val status: Int? = null
)

data class ContactsItem(
    val isCompanyName: Boolean? = null,
    val id: String? = null,
    val designation: String? = null,
    val name: String? = null
)
