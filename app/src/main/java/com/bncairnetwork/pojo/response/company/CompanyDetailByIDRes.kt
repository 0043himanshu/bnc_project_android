package com.bncairnetwork.pojo.response.company

import com.bncairnetwork.pojo.AttributesItem
import com.bncairnetwork.pojo.response.AssigneesDataItem
import com.bncairnetwork.pojo.response.BncVersion
import com.google.gson.annotations.SerializedName

data class CompanyDetailByIDRes(

    @field:SerializedName("subscribed")
    val subscribed: Boolean? = null,

    val assignees: ArrayList<String>? = null,

    val has_active_reminder: Boolean?,
    @field:SerializedName("reminder")
    val reminder: Any? = null,

    @field:SerializedName("display_title")
    val displayTitle: String? = null,

    @field:SerializedName("attributes")
    val attributes: List<AttributesItem?>? = null,

    @field:SerializedName("company")
    val company: Company? = null,

    @field:SerializedName("favourite")
    var favourite: Boolean? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("tags")
    var tags: ArrayList<String>? = null
)

data class Company(
    @field:SerializedName("bnc_version")
    val bncVersion: BncVersion? = null,

    @field:SerializedName("established")
    val established: Any? = null,

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("company_type")
    val companyType: String? = null,

    val assignees_obj: ArrayList<AssigneesDataItem>?,

    @field:SerializedName("type_of_company")
    val typeOfCompany: String? = null,

    @field:SerializedName("address")
    val address: String? = null,

    val record_type: String?,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("year_started")
    val yearStarted: Any? = null,

    @field:SerializedName("categories_list")
    val categoriesList: List<Any?>? = null,

    @field:SerializedName("category_list")
    val categoryList: List<String?>? = null,

    @field:SerializedName("iris_company_details")
    val irisCompanyDetails: IrisCompanyDetails? = null,

    @field:SerializedName("phone")
    val phone: ArrayList<String?>? = null,

    @field:SerializedName("web")
    val web: ArrayList<String?>? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("fax")
    val fax: ArrayList<String?>? = null,

    @field:SerializedName("email")
    val email: List<Any?>? = null
)


data class IrisCompanyDetails(

    @field:SerializedName("has_iris_company")
    val hasIrisCompany: Boolean? = null
)
