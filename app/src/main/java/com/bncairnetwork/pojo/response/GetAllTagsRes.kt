package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class GetAllTagsRes(

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("tags")
    val tags: ArrayList<TagsItem?>? = null
)

data class TagsItem(
    @field:SerializedName("name")
    var name: String? = null
)
