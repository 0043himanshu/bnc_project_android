package com.bncairnetwork.pojo.response.Dashboard

data class NotificationCountRes(
    val status: String?,
    val notifications: String?
)
