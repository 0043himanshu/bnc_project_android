package com.bncairnetwork.pojo.response.reminder

import com.bncairnetwork.pojo.response.company.Company
import com.google.gson.annotations.SerializedName

data class GetReminderLstRes(

	@field:SerializedName("reminders")
	val reminders: ArrayList<RemindersItem>	,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Project(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class RemindersItem(
	@field:SerializedName("reminder")
	val reminder: String? = null,

	@field:SerializedName("remind_on")
	val remindOn: String? = null,

	@field:SerializedName("project")
	val project: Project? = null,

	@field:SerializedName("company")
	val company: Company? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val status: String? = null
)
