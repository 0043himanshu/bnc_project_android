package com.bncairnetwork.pojo.response.contact

import com.google.gson.annotations.SerializedName

data class AddContactRes(

	@field:SerializedName("contact")
	val contact: Contact? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Contact(

	@field:SerializedName("notes")
	val notes: Any? = null,

	@field:SerializedName("mep_consultant")
	val mepConsultant: Any? = null,

	@field:SerializedName("mobile_area_code1")
	val mobileAreaCode1: Any? = null,

	@field:SerializedName("phone2")
	val phone2: Any? = null,

	@field:SerializedName("phone3")
	val phone3: Any? = null,

	@field:SerializedName("phone1")
	val phone1: String? = null,

	@field:SerializedName("email3")
	val email3: Any? = null,

	@field:SerializedName("email2")
	val email2: Any? = null,

	@field:SerializedName("email1")
	val email1: String? = null,

	@field:SerializedName("mobile_area_code2")
	val mobileAreaCode2: Any? = null,

	@field:SerializedName("desigcode")
	val desigcode: Any? = null,

	@field:SerializedName("fax3")
	val fax3: Any? = null,

	@field:SerializedName("mobile_area_code3")
	val mobileAreaCode3: Any? = null,

	@field:SerializedName("fax2")
	val fax2: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("fax1")
	val fax1: Any? = null,

	@field:SerializedName("subscribed_company")
	val subscribedCompany: Int? = null,

	@field:SerializedName("phone_area_code1")
	val phoneAreaCode1: String? = null,

	@field:SerializedName("main_contractor")
	val mainContractor: Any? = null,

	@field:SerializedName("visible")
	val visible: Boolean? = null,

	@field:SerializedName("verified_general_contact")
	val verifiedGeneralContact: Boolean? = null,

	@field:SerializedName("phone_area_code3")
	val phoneAreaCode3: Any? = null,

	@field:SerializedName("phone_area_code2")
	val phoneAreaCode2: Any? = null,

	@field:SerializedName("general_contact")
	val generalContact: Boolean? = null,

	@field:SerializedName("main_consultant")
	val mainConsultant: Any? = null,

	@field:SerializedName("linked_bnc_contact_id")
	val linkedBncContactId: Any? = null,

	@field:SerializedName("designation")
	val designation: String? = null,

	@field:SerializedName("other_designation")
	val otherDesignation: Any? = null,

	@field:SerializedName("status")
	val status: String? = null,

	@field:SerializedName("extension")
	val extension: Any? = null,

	@field:SerializedName("bidder")
	val bidder: Any? = null,

	@field:SerializedName("mobile_country_code2")
	val mobileCountryCode2: Any? = null,

	@field:SerializedName("mobile_country_code1")
	val mobileCountryCode1: Any? = null,

	@field:SerializedName("display_order")
	val displayOrder: Any? = null,

	@field:SerializedName("mobile1")
	val mobile1: Any? = null,

	@field:SerializedName("phone_country_code3")
	val phoneCountryCode3: Any? = null,

	@field:SerializedName("linkedin")
	val linkedin: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("phone_country_code1")
	val phoneCountryCode1: String? = null,

	@field:SerializedName("phone_country_code2")
	val phoneCountryCode2: Any? = null,

	@field:SerializedName("mep_contractor")
	val mepContractor: Any? = null,

	@field:SerializedName("mobile3")
	val mobile3: Any? = null,

	@field:SerializedName("mobile2")
	val mobile2: Any? = null,

	@field:SerializedName("user_modified_on")
	val userModifiedOn: Any? = null,

	@field:SerializedName("company")
	val company: Int? = null,

	@field:SerializedName("department")
	val department: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("fax_area_code3")
	val faxAreaCode3: Any? = null,

	@field:SerializedName("added_on")
	val addedOn: String? = null,

	@field:SerializedName("can_list")
	val canList: Boolean? = null,

	@field:SerializedName("fax_area_code2")
	val faxAreaCode2: Any? = null,

	@field:SerializedName("fax_area_code1")
	val faxAreaCode1: Any? = null,

	@field:SerializedName("last_modified_on")
	val lastModifiedOn: String? = null,

	@field:SerializedName("address")
	val address: Any? = null,

	@field:SerializedName("fax_country_code3")
	val faxCountryCode3: Any? = null,

	@field:SerializedName("fax_country_code2")
	val faxCountryCode2: Any? = null,

	@field:SerializedName("fax_country_code1")
	val faxCountryCode1: Any? = null,

	@field:SerializedName("facebook")
	val facebook: Any? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("verified_on")
	val verifiedOn: Any? = null,

	@field:SerializedName("syn_id")
	val synId: Any? = null,

	@field:SerializedName("mobile_country_code3")
	val mobileCountryCode3: Any? = null
)
