package com.bncairnetwork.pojo.response.Dashboard

import com.google.gson.annotations.SerializedName

data class SavedSearchRes(

    @field:SerializedName("widget_data")
    val widgetData: WidgetData? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class ContentItem(

    @field:SerializedName("updated_count")
    val updatedCount: Int? = null,

    @field:SerializedName("updated_url")
    val updatedUrl: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("added_url")
    val addedUrl: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("added_count")
    val addedCount: Int? = null
)

data class WidgetData(

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("content")
    val content: ArrayList<ContentItem?>? = null
)
