package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class GetCompaniesForReqDemo(

	@field:SerializedName("suggestions")
	val suggestions: List<SuggestionsItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class SuggestionsItem(

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
