package com.bncairnetwork.pojo.response.notification

import com.bncairnetwork.pojo.LinkedProjectDataItem
import com.google.gson.annotations.SerializedName

data class GetNotificationLstRes(

    @field:SerializedName("notifications")
    val notifications: ArrayList<NotificationsItem?>? = null,

    val reminder_notifications: ArrayList<ReminderNotificationDataItem>? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class Android(

    @field:SerializedName("subtitle")
    val subtitle: String? = null,

    @field:SerializedName("title")
    val title: String? = null
)

data class ReminderNotificationDataItem(
    val id: String? = null,
    val reminder_date: String? = null,
    val reminder: String? = null,
    val status: String? = null,
    val project_name: String? = null,
    val company_name: String? = null,
    val url: String? = null,
    val record_type: RecordTypeDataItem? = null
)


data class Notification(

    @field:SerializedName("android")
    val android: Android? = null,

    @field:SerializedName("api")
    val api: Any? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("ios")
    val ios: String? = null
)

data class NotificationsItem(

    @field:SerializedName("notification")
    val notification: Notification? = null,

    @field:SerializedName("content_type")
    val contentType: String? = null,

    @field:SerializedName("notified_on")
    val notifiedOn: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("object_id")
    val objectId: Int? = null,

    @field:SerializedName("device")
    val device: String? = null,

    val record_type: RecordTypeDataItem? = null
)

data class RecordTypeDataItem(
    val type: String,
    val linked: Boolean,
    val linked_project: LinkedProjectDataItem
)

data class LinkedProjectDataItem(
    val id: String,
    val name: String
)
