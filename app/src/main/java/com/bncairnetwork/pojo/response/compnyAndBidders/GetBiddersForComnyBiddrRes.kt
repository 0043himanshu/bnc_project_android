package com.bncairnetwork.pojo.response.compnyAndBidders

import com.bncairnetwork.pojo.IdentfierObjDataItem
import com.bncairnetwork.pojo.LinkedProjectDataItem
import com.bncairnetwork.pojo.ProTagObj
import com.bncairnetwork.pojo.RecordTypeDataItem
import com.bncairnetwork.pojo.response.company.KeyContactobj
import com.google.gson.annotations.SerializedName

data class GetBiddersForComnyBiddrRes(

    @field:SerializedName("bidders")
    val bidders: ArrayList<BiddersItem?>? = null,

    @field:SerializedName("display_title")
    val displayTitle: String? = null,

    @field:SerializedName("project")
    val project: Project? = null,

    @field:SerializedName("bidders_count")
    val biddersCount: Int? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class BiddersItem(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("company_type")
    val companyType: String? = null,
    val tag_obj: ProTagObj?,
    val identfierObj: IdentfierObjDataItem?,
    @field:SerializedName("key_contact_obj")
    val keyContactObj: ArrayList<KeyContactobj>? = null,
    @field:SerializedName("note")
    val note: String? = null,

    @field:SerializedName("role")
    val role: String? = null,

    @field:SerializedName("reminder")
    val reminder: Any? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("favourite")
    var favourite: Boolean? = null,

    @field:SerializedName("record_type")
    val recordType: BiddrRecordTypeDataItem? = null,

    @field:SerializedName("tags")
    val tags: List<Any?>? = null,

    @field:SerializedName("phone")
    val phone: ArrayList<String?>? = null,

    @field:SerializedName("web")
    val web: List<String?>? = null,

    @field:SerializedName("bnc_record")
    val bncRecord: Boolean? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null,

    @field:SerializedName("email")
    val email: ArrayList<String?>? = null,

    @field:SerializedName("status")
    val status: String? = null
)

data class IrisProjectDetails(

    @field:SerializedName("has_iris_project")
    val hasIrisProject: Boolean? = null
)

data class BiddrRecordTypeDataItem(
    val type: String,
    val linked: Boolean,
    val linked_projectcompany: LinkedProjectDataItem
)

data class Project(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("completion_percentage")
    val completionPercentage: Any? = null,

    @field:SerializedName("iris_project_details")
    val irisProjectDetails: IrisProjectDetails? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("assignees")
    val assignees: List<Any?>? = null,

    @field:SerializedName("views_left")
    val viewsLeft: Any? = null,

    @field:SerializedName("bnc_project_number")
    val bncProjectNumber: String? = null,

    @field:SerializedName("type_of_project")
    val typeOfProject: String? = null,

    @field:SerializedName("project_subtype")
    val projectSubtype: String? = null,

    @field:SerializedName("authorized")
    val authorized: Boolean? = null,

    @field:SerializedName("spent_value")
    val spentValue: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: Double? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("area")
    val area: Any? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("project_type")
    val projectType: String? = null,

    @field:SerializedName("estimated_completion")
    val estimatedCompletion: String? = null,

    @field:SerializedName("created")
    val created: String? = null,

    @field:SerializedName("bnc_version")
    val bncVersion: Any? = null,

    @field:SerializedName("record_type")
    val recordType: String? = null,

    @field:SerializedName("assignees_obj")
    val assigneesObj: List<Any?>? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("progress")
    val progress: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("latlongstring")
    val latlongstring: Any? = null,

    @field:SerializedName("updated")
    val updated: String? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null
)
