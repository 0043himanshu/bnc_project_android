package com.bncairnetwork.pojo.response.project

import com.google.gson.annotations.SerializedName

data class GetProjectRefLstRes(

	@field:SerializedName("projects")
	val projects: ArrayList<ProjectsItem>? = null,

	@field:SerializedName("display_title")
	val displayTitle: String? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("company")
	val company: Company? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class RecordType(

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("linked_project")
	val linkedProject: LinkedProject? = null,

	@field:SerializedName("linked")
	val linked: Boolean? = null
)

data class ProjectsItem(

	@field:SerializedName("bidders")
	val bidders: Int? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("role")
	val role: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("views_left")
	val viewsLeft: Any? = null,

	@field:SerializedName("owners")
	val owners: Int? = null,

	@field:SerializedName("industry")
	val industry: String? = null,

	@field:SerializedName("linked_bnc_project_id")
	val linkedBncProjectId: Any? = null,

	@field:SerializedName("authorized")
	val authorized: Boolean? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("value")
	val value:String,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("project_type")
	val projectType: String? = null,

	@field:SerializedName("reminder")
	val reminder: Any? = null,

	@field:SerializedName("favourite")
	var favourite: Boolean? = null,

	@field:SerializedName("record_type")
	val recordType: RecordType? = null,

	@field:SerializedName("tags")
	val tags: ArrayList<String>? = null,

	@field:SerializedName("master")
	val master: Boolean? = null,

	@field:SerializedName("contractors")
	val contractors: Int? = null,

	@field:SerializedName("stage")
	val stage: String? = null,

	val updated: String?=null,

	@field:SerializedName("consultants")
	val consultants: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("country_flag")
	val countryFlag: String? = null
)

data class IrisCompanyDetails(

	@field:SerializedName("has_iris_company")
	val hasIrisCompany: Boolean? = null
)

data class LinkedProject(
	val any: Any? = null
)

data class Company(

	@field:SerializedName("established")
	val established: Any? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("company_type")
	val companyType: String? = null,

	@field:SerializedName("type_of_company")
	val typeOfCompany: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("year_started")
	val yearStarted: Any? = null,

	@field:SerializedName("bnc_version")
	val bncVersion: Any? = null,

	@field:SerializedName("categories_list")
	val categoriesList: List<Any?>? = null,

	@field:SerializedName("key_contacts_count")
	val keyContactsCount: Int? = null,

	@field:SerializedName("record_type")
	val recordType: String? = null,

	@field:SerializedName("category_list")
	val categoryList: List<Any?>? = null,

	@field:SerializedName("iris_company_details")
	val irisCompanyDetails: IrisCompanyDetails? = null,

	@field:SerializedName("phone")
	val phone: List<String?>? = null,

	@field:SerializedName("web")
	val web: List<Any?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("fax")
	val fax: List<Any?>? = null,

	@field:SerializedName("email")
	val email: List<Any?>? = null
)
