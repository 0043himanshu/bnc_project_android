package com.bncairnetwork.pojo.response.requesDemo

import com.google.gson.annotations.SerializedName

data class GetCitiesForReqDemoRes(

	@field:SerializedName("cities")
	val cities: ArrayList<CitiesItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CitiesItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
