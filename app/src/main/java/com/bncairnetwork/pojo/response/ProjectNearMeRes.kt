package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class ProjectNearMeRes(

	@field:SerializedName("widget_data")
	val widgetData: ProjectNearWidgetData? = null,

	@field:SerializedName("widget_id")
	val widgetId: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class ProjectNearContent(

	@field:SerializedName("data")
	val data: ArrayList<ProjectNearDataItem?>? = null,

	@field:SerializedName("updated_projects_url")
	val updatedProjectsUrl: String? = null,

	@field:SerializedName("updated_projects")
	val updatedProjects: Int? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("new_projects")
	val newProjects: Int? = null,

	@field:SerializedName("new_projects_url")
	val newProjectsUrl: String? = null
)

data class ProjectNearWidgetData(

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("content")
	val content: ArrayList<ProjectNearContent?>? = null
)

data class ProjectNearDataItem(

	@field:SerializedName("bidders")
	val bidders: Int? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("flag")
	val flag: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("latitude")
	val latitude: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("owners")
	val owners: Int? = null,

	@field:SerializedName("industry")
	val industry: String? = null,

	@field:SerializedName("views_left")
	val viewsLeft: Any? = null,

	@field:SerializedName("companies")
	val companies: Int? = null,

	@field:SerializedName("authorized")
	val authorized: Boolean? = null,

	@field:SerializedName("map_location")
	val mapLocation: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("sector")
	val sector: String? = null,

	@field:SerializedName("value")
	val value: Double? = null,

	@field:SerializedName("longitude")
	val longitude: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("reminder")
	val reminder: Any? = null,

	@field:SerializedName("favourite")
	var favourite: Boolean? = null,

	@field:SerializedName("url")
	val url: String? = null,

	@field:SerializedName("master")
	val master: Boolean? = null,

	@field:SerializedName("tags")
	val tags: List<String?>? = null,

	@field:SerializedName("contractors")
	val contractors: Int? = null,

	@field:SerializedName("consultants")
	val consultants: Int? = null,

	@field:SerializedName("stage")
	val stage: String? = null,

	@field:SerializedName("django_id")
	val djangoId: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("latlongstring")
	val latlongstring: String? = null,

	@field:SerializedName("updated")
	val updated: String? = null
)
