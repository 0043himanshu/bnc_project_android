package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class NewsDetailRes(

	@field:SerializedName("news")
	val news: News? = null,

	@field:SerializedName("display_title")
	val displayTitle: String? = null,

	@field:SerializedName("attributes")
	val attributes: List<AttributesItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class News(

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("authorized")
	val authorized: Boolean? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("views_left")
	val viewsLeft: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("published")
	val published: String? = null,

	@field:SerializedName("country_flag")
	val countryFlag: String? = null
)

data class AttributesItem(

	@field:SerializedName("bidders")
	val bidders: Int? = null,

	@field:SerializedName("contractors")
	val contractors: Int? = null,

	@field:SerializedName("consultants")
	val consultants: Int? = null,

	@field:SerializedName("authorized")
	val authorized: Boolean? = null,

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("owners")
	val owners: Int? = null,

	@field:SerializedName("views_left")
	val viewsLeft: Any? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)
