package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class DashboardWidgetDataRes(

    @field:SerializedName("active_widget_data")
    val activeWidgetData: List<ActiveWidgetDataItem?>? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class ContentItem(

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("url")
    val url: String? = null
)

data class DataItem(
//    @field:SerializedName("id")
//    val id:String?=null,

    @field:SerializedName("search_url")
    val searchUrl: String? = null,

    @field:SerializedName("id")
    val projectID: String? = null,

    @field:SerializedName("image")
    val projImageUrl: String? = null,

    @field:SerializedName("short_description")
    val projShortDesc: String? = null,

    val city: String? = null,
    val country: String? = null,

    @field:SerializedName("description")
    val projDesc: String? = null,


    @field:SerializedName("authorized")
    val authorized: Boolean? = null,


    @field:SerializedName("updated_projects_url")
    val updatedProjectsUrl: String? = null,

    @field:SerializedName("updated_projects")
    val updatedProjects: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("new_projects")
    val newProjects: Int? = null,

    @field:SerializedName("search_count")
    val searchCount: Int? = null,

    @field:SerializedName("new_projects_url")
    val newProjectsUrl: String? = null
)

data class Content(

    @field:SerializedName("updated_company_projects")
    val updatedCompanyProjects: Int? = null,

    @field:SerializedName("updated_projects_url")
    val updatedProjectsUrl: String? = null,

    @field:SerializedName("updated_projects")
    val updatedProjects: Int? = null,

    @field:SerializedName("favourite_companies")
    val favouriteCompanies: Int? = null,

    @field:SerializedName("favourite_projects")
    val favouriteProjects: Int? = null,

    @field:SerializedName("project_url")
    val projectUrl: String? = null,

    @field:SerializedName("company_url")
    val companyUrl: String? = null,

    @field:SerializedName("updated_company_projects_url")
    val updatedCompanyProjectsUrl: String? = null,

    @field:SerializedName("data")
    val data: List<DataItem?>? = null,

    @field:SerializedName("news_url")
    val newsUrl: String? = null,

    val url: String? = null,
    val title: String? = null,


    @field:SerializedName("credits_available")
    val creditsAvailable: Int? = null,

    @field:SerializedName("claimed_projects")
    val claimedProjects: Int? = null,

    @field:SerializedName("news_list_url")
    val newsListUrl: String? = null,

    @field:SerializedName("project_list_url")
    val projectListUrl: String? = null,

    @field:SerializedName("claimed_news")
    val claimedNews: Int? = null,

    @field:SerializedName("count")
    val count: Int? = null,

    @field:SerializedName("new_projects")
    val newProjects: Int? = null,

    @field:SerializedName("new_projects_url")
    val newProjectsUrl: String? = null,

    @field:SerializedName("completed_projects_url")
    val completedProjectsUrl: String? = null,

    @field:SerializedName("completed_projects")
    val completedProjects: Int? = null,

    @field:SerializedName("view_more_url")
    val viewMoreUrl: String? = null
)

data class WidgetData(

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("content")
    val content: ArrayList<Content?>? = null
)

data class ActiveWidgetDataItem(

    @field:SerializedName("widget_data")
    val widgetData: WidgetData? = null,

    @field:SerializedName("widget_id")
    val widgetId: Int? = null
)
