package com.bncairnetwork.pojo.response.schedules

import com.google.gson.annotations.SerializedName

data class GetSchedulesLstRes(

	@field:SerializedName("display_title")
	val displayTitle: String? = null,

	@field:SerializedName("schedules")
	val schedules: ArrayList<SchedulesItem>? = null,

	@field:SerializedName("project")
	val project: Project? = null,

	@field:SerializedName("schedules_count")
	val schedulesCount: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Project(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("completion_percentage")
	val completionPercentage: Any? = null,

	@field:SerializedName("iris_project_details")
	val irisProjectDetails: IrisProjectDetails? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("assignees")
	val assignees: List<Any?>? = null,

	@field:SerializedName("views_left")
	val viewsLeft: Any? = null,

	@field:SerializedName("bnc_project_number")
	val bncProjectNumber: String? = null,

	@field:SerializedName("type_of_project")
	val typeOfProject: String? = null,

	@field:SerializedName("project_subtype")
	val projectSubtype: String? = null,

	@field:SerializedName("authorized")
	val authorized: Boolean? = null,

	@field:SerializedName("spent_value")
	val spentValue: Any? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("value")
	val value: Double? = null,

	@field:SerializedName("sector")
	val sector: String? = null,

	@field:SerializedName("area")
	val area: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("project_type")
	val projectType: String? = null,

	@field:SerializedName("estimated_completion")
	val estimatedCompletion: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("bnc_version")
	val bncVersion: Any? = null,

	@field:SerializedName("record_type")
	val recordType: String? = null,

	@field:SerializedName("assignees_obj")
	val assigneesObj: List<Any?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("progress")
	val progress: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("latlongstring")
	val latlongstring: Any? = null,

	@field:SerializedName("updated")
	val updated: String? = null,

	@field:SerializedName("country_flag")
	val countryFlag: String? = null
)

data class IrisProjectDetails(

	@field:SerializedName("has_iris_project")
	val hasIrisProject: Boolean? = null
)

data class SchedulesItem(

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("datetime")
	val datetime: String? = null,

	@field:SerializedName("bnc_record")
	val bncRecord: Boolean? = null,

	@field:SerializedName("format")
	val format: String? = null,

	@field:SerializedName("show")
	val show: Boolean? = null,

	@field:SerializedName("schedule__name")
	val scheduleName: String? = null,

	@field:SerializedName("is_conflict")
	val isConflict: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("classification")
	val classification: String? = null,

	@field:SerializedName("schedule_id")
	val scheduleId: Int? = null,

	@field:SerializedName("record_type")
	val recordType: String? = null
)
