package com.bncairnetwork.pojo.response.company

import com.google.gson.annotations.SerializedName

data class GetAutoComCompaniesRes(

	@field:SerializedName("suggestions")
	val suggestions: List<SuggestionsItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class SuggestionsItem(

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
