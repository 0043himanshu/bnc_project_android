package com.bncairnetwork.pojo.response

data class ForgotPasswordRes(
    val status: String,
    val error: MError,
    val message:String
)

data class MError(
    val email: ArrayList<String>
)
