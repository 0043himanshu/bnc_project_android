package com.bncairnetwork.pojo.response

import com.bncairnetwork.pojo.AttributesItem
import com.google.gson.annotations.SerializedName

data class ProjectResById(

    @field:SerializedName("reminder")
    val reminder: ReminderDataItem? = null,

    val has_active_reminder: Boolean?,

    @field:SerializedName("display_title")
    val displayTitle: String? = null,

    val assignees: ArrayList<String>? = null,

    @field:SerializedName("signposts")
    val signposts: Signposts? = null,

    @field:SerializedName("project")
    val project: Project? = null,

    @field:SerializedName("attributes")
    val attributes: List<AttributesItem?>? = null,

    @field:SerializedName("favourite")
    var favourite: Boolean? = null,

    @field:SerializedName("message")
    val message: String? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("tags")
    var tags: ArrayList<String>? = null
)

data class ReminderDataItem(
    val date: String,
    val reminder_id: String,
    val reminder: String,
    val mobile_reminder_date: String
)

data class AssigneesDataItem(
    val first_name: String?,
    val last_name: String?,
    val id: String?,
    val profile_image: String?,
    val title: String?
)

data class Project(


    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("completion_percentage")
    val completionPercentage: String? = null,

    @field:SerializedName("iris_project_details")
    val irisProjectDetails: IrisProjectDetails? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("views_left")
    val viewsLeft: Any? = null,

    val record_type: String?,

    val assignees_obj: ArrayList<AssigneesDataItem>?,

    @field:SerializedName("bnc_project_number")
    val bncProjectNumber: String? = null,

    @field:SerializedName("type_of_project")
    val typeOfProject: String? = null,

    @field:SerializedName("project_subtype")
    val projectSubtype: String? = null,

    @field:SerializedName("authorized")
    val authorized: Boolean? = null,

    @field:SerializedName("spent_value")
    val spentValue: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: String? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("area")
    val area: Any? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("project_type")
    val projectType: String? = null,

    @field:SerializedName("estimated_completion")
    val estimatedCompletion: String? = null,

    @field:SerializedName("created")
    val created: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("progress")
    val progress: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("latlongstring")
    val latlongstring: String? = null,

    @field:SerializedName("project_stage")
    val project_stage: String? = null,

    @field:SerializedName("updated")
    val updated: String? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null,

    @field:SerializedName("bnc_version")
    val bncVersion: BncVersion? = null
)

data class Signposts(
    val any: Any? = null
)

data class ProjectCountry(

    @field:SerializedName("zone")
    val zone: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("zone_name")
    val zoneName: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class BncVersion(

    @field:SerializedName("plot_number")
    val plotNumber: String? = null,

    @field:SerializedName("city")
    val city: City? = null,

    @field:SerializedName("new_project_type")
    val newProjectType: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("record_type")
    val recordType: String? = null,

    @field:SerializedName("project_stage")
    val projectStage: String? = null,

    @field:SerializedName("new_subtype")
    val newSubtype: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("country")
    val projectCountry: CompanyCountry? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("value")
    val value: String? = null
)

data class CompanyCountry(
    @field:SerializedName("zone")
    val zone: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("zone_name")
    val zoneName: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class City(

    @field:SerializedName("country")
    val country: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("linked_bnc_city_id")
    val linkedBncCityId: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("syn_id")
    val synId: Int? = null,

    @field:SerializedName("state_code")
    val stateCode: String? = null,

    @field:SerializedName("subscribed_company")
    val subscribedCompany: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("phone_code")
    val phoneCode: String? = null
)


data class IrisProjectDetails(

    @field:SerializedName("has_iris_project")
    val hasIrisProject: Boolean? = null
)
