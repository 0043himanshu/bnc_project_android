package com.bncairnetwork.pojo.response.master

import com.google.gson.annotations.SerializedName

data class GetMasterDataRes(

    @field:SerializedName("project_search_optional_fields")
    val projectSearchOptionalFields: ProjectSearchOptionalFields? = null,

    @field:SerializedName("contract_awarded")
    val contractAwarded: List<ContractAwardedItem?>? = null,

    @field:SerializedName("search_company_subtypes")
    val searchCompanySubtypes: List<SearchCompanySubtypesItem?>? = null,

    @field:SerializedName("project_company_types")
    val projectCompanyTypes: List<ProjectCompanyTypesItem?>? = null,

    @field:SerializedName("search_tree")
    val searchTree: List<SearchTreeItem?>? = null,

    @field:SerializedName("future_dates")
    val futureDates: FutureDates? = null,

    @field:SerializedName("stages_changed")
    val stagesChanged: List<StagesChangedItem?>? = null,

    @field:SerializedName("usage_of_building")
    val usageOfBuilding: List<UsageOfBuildingItem?>? = null,

    @field:SerializedName("past_dates")
    val pastDates: PastDates? = null,

    @field:SerializedName("interaction_types")
    val interactionTypes: ArrayList<InteractionTypeDataItem?>? = null,

    @field:SerializedName("subtypes")
    val subtypes: List<SubtypesItem?>? = null,

    @field:SerializedName("company_subtypes")
    val companySubtypes: List<CompanySubtypesItem?>? = null,

    @field:SerializedName("eh_widget_countries")
    val ehWidgetCountries: List<EhWidgetCountriesItem?>? = null,

    @field:SerializedName("all_company_users")
    val allCompanyUsers: List<AllCompanyUsersItem?>? = null,

    @field:SerializedName("schedules")
    val schedules: List<SchedulesItem?>? = null,

    @field:SerializedName("contractor_schedules")
    val contractorSchedules: List<ContractorSchedulesItem?>? = null,

    @field:SerializedName("professional_stages")
    val professionalStages: List<ProfessionalStagesItem?>? = null,

    @field:SerializedName("user_cities")
    val userCities: List<UserCitiesItem?>? = null,

    @field:SerializedName("currency")
    val currency: List<String?>? = null,

    @field:SerializedName("company_types")
    val companyTypes: List<CompanyTypesItem?>? = null,

    @field:SerializedName("record_types")
    val recordTypes: List<RecordTypesItem?>? = null,

    @field:SerializedName("proj_cities")
    val projCities: List<ProjCitiesItem?>? = null,

    @field:SerializedName("star_ratings")
    val starRatings: List<StarRatingsItem?>? = null,

    @field:SerializedName("types")
    val types: List<TypesItem?>? = null,

    @field:SerializedName("sectors")
    val sectors: List<SectorsItem?>? = null,

    @field:SerializedName("all_countries")
    val allCountries: List<AllCountriesItem?>? = null,

    @field:SerializedName("all_cities")
    val allCities: List<AllCitiesItem?>? = null,

    @field:SerializedName("project_countries")
    val projectCountries: List<ProjectCountriesItem?>? = null,

    @field:SerializedName("all_company_types")
    val allCompanyTypes: List<AllCompanyTypesItem?>? = null,

    @field:SerializedName("users_countries")
    val usersCountries: List<UsersCountriesItem?>? = null,

    @field:SerializedName("user_stages")
    val userStages: List<UserStagesItem?>? = null,

    @field:SerializedName("user_tags")
    val userTags: List<String?>? = null,

    @field:SerializedName("ownership")
    val ownership: List<OwnershipItem?>? = null,

    @field:SerializedName("active_stages")
    val activeStages: List<ActiveStagesItem?>? = null,

    @field:SerializedName("designation_list")
    val designationList: List<DesignationListItem?>? = null,

    @field:SerializedName("company_roles")
    val companyRoles: List<CompanyRolesItem?>? = null,

    @field:SerializedName("users_friends")
    val usersFriends: List<UsersFriendsItem?>? = null,

    @field:SerializedName("stages")
    val stages: List<StagesItem?>? = null,

    @field:SerializedName("all_stages")
    val allStages: List<AllStagesItem?>? = null,

    @field:SerializedName("preference_tree")
    val preferenceTree: List<PreferenceTreeItem?>? = null,

    @field:SerializedName("city_list")
    val cityList: List<CityListItem?>? = null,

    @field:SerializedName("search_company_types")
    val searchCompanyTypes: List<SearchCompanyTypesItem?>? = null,

    @field:SerializedName("all_dates")
    val allDates: AllDates? = null,

    @field:SerializedName("project_export_fields")
    val projectExportFields: ProjectExportFields? = null,

    @field:SerializedName("trusted_companies")
    val trustedCompanies: List<TrustedCompaniesItem?>? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class InteractionTypeDataItem(
    val id: String,
    val name: String
)

data class DesignationListItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class SearchCompanySubtypesItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("company_subtype")
    val companySubtype: String? = null
)

data class PreferenceTreeItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("project_types")
    val projectTypes: List<ProjectTypesItem?>? = null
)

data class ContractAwardedItem(

    @field:SerializedName("category_name")
    val categoryName: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("category_type")
    val categoryType: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class StarRatingsItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: String? = null
)

data class AllCountriesItem(

    @field:SerializedName("zone")
    val zone: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("zone_name")
    val zoneName: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class ActiveStagesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class PastDates(

    @field:SerializedName("1")
    val jsonMember1: String? = null,

    @field:SerializedName("2")
    val jsonMember2: String? = null,

    @field:SerializedName("3")
    val jsonMember3: String? = null,

    @field:SerializedName("4")
    val jsonMember4: String? = null,

    @field:SerializedName("5")
    val jsonMember5: String? = null,

    @field:SerializedName("6")
    val jsonMember6: String? = null,

    @field:SerializedName("7")
    val jsonMember7: String? = null,

    @field:SerializedName("8")
    val jsonMember8: String? = null,

    @field:SerializedName("9")
    val jsonMember9: String? = null,

    @field:SerializedName("10")
    val jsonMember10: String? = null
)

data class EhWidgetCountriesItem(

    @field:SerializedName("zone")
    val zone: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("countries")
    val countries: List<CountriesItem?>? = null
)

data class AllCompanyTypesItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("subtypes")
    val subtypes: List<SubtypesItem?>? = null
)

data class RecordTypesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("classification")
    val classification: String? = null
)

data class SearchTreeItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("project_types")
    val projectTypes: List<ProjectTypesItem?>? = null
)


data class FutureDates(

    @field:SerializedName("1")
    val jsonMember1: String? = null,

    @field:SerializedName("2")
    val jsonMember2: String? = null,

    @field:SerializedName("3")
    val jsonMember3: String? = null,

    @field:SerializedName("4")
    val jsonMember4: String? = null,

    @field:SerializedName("5")
    val jsonMember5: String? = null,

    @field:SerializedName("6")
    val jsonMember6: String? = null,

    @field:SerializedName("7")
    val jsonMember7: String? = null
)

data class ProjCitiesItem(

    @field:SerializedName("country")
    val country: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("linked_bnc_city_id")
    val linkedBncCityId: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("syn_id")
    val synId: Int? = null,

    @field:SerializedName("state_code")
    val stateCode: String? = null,

    @field:SerializedName("subscribed_company")
    val subscribedCompany: Int? = null,

    @field:SerializedName("slug")
    val slug: Any? = null,

    @field:SerializedName("phone_code")
    val phoneCode: String? = null
)

data class UsersFriendsItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("last_name")
    val lastName: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("designation")
    val designation: String? = null,

    @field:SerializedName("first_name")
    val firstName: String? = null
)

data class UserCitiesItem(

    @field:SerializedName("country")
    val country: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("linked_bnc_city_id")
    val linkedBncCityId: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("syn_id")
    val synId: Int? = null,

    @field:SerializedName("state_code")
    val stateCode: String? = null,

    @field:SerializedName("subscribed_company")
    val subscribedCompany: Int? = null,

    @field:SerializedName("slug")
    val slug: Any? = null,

    @field:SerializedName("phone_code")
    val phoneCode: String? = null
)

data class UserStagesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class CompanySubtypesItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("company_subtype")
    val companySubtype: String? = null
)

data class AllDates(

    @field:SerializedName("1")
    val jsonMember1: String? = null,

    @field:SerializedName("2")
    val jsonMember2: String? = null,

    @field:SerializedName("3")
    val jsonMember3: String? = null,

    @field:SerializedName("4")
    val jsonMember4: String? = null,

    @field:SerializedName("5")
    val jsonMember5: String? = null,

    @field:SerializedName("6")
    val jsonMember6: String? = null,

    @field:SerializedName("7")
    val jsonMember7: String? = null
)

data class SearchCompanyTypesItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("subtypes")
    val subtypes: List<SubtypesItem?>? = null
)

data class CompanyRolesItem(

    @field:SerializedName("identifier")
    val identifier: String? = null,

    @field:SerializedName("name")
    val name: String? = null
)

data class TrustedCompaniesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class SchedulesItem(

    @field:SerializedName("stage")
    val stage: Stage? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class CityListItem(

    @field:SerializedName("country")
    val country: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("linked_bnc_city_id")
    val linkedBncCityId: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("syn_id")
    val synId: Int? = null,

    @field:SerializedName("state_code")
    val stateCode: String? = null,

    @field:SerializedName("subscribed_company")
    val subscribedCompany: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("phone_code")
    val phoneCode: String? = null
)

data class ProjectCountriesItem(

    @field:SerializedName("zone")
    val zone: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("zone_name")
    val zoneName: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class AllCitiesItem(

    @field:SerializedName("country")
    val country: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("linked_bnc_city_id")
    val linkedBncCityId: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("syn_id")
    val synId: Int? = null,

    @field:SerializedName("state_code")
    val stateCode: String? = null,

    @field:SerializedName("subscribed_company")
    val subscribedCompany: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null,

    @field:SerializedName("phone_code")
    val phoneCode: String? = null
)

data class TypesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("sector")
    val sector: Int? = null
)

data class OwnershipItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: String? = null
)

data class UsersCountriesItem(

    @field:SerializedName("zone")
    val zone: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("zone_name")
    val zoneName: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class ProjectCompanyTypesItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("subtypes")
    val subtypes: List<SubtypesItem?>? = null
)

data class Stage(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class AllCompanyUsersItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("last_name")
    val lastName: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("first_name")
    val firstName: String? = null
)

data class SectorsItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class CountriesItem(

    @field:SerializedName("zone")
    val zone: Zone? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class StagesChangedItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class ProjectExportFields(

    @field:SerializedName("completion_percentage")
    val completionPercentage: String? = null,

    @field:SerializedName("profile_type")
    val profileType: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("new_project_type")
    val newProjectType: String? = null,

    @field:SerializedName("mep_consultants")
    val mepConsultants: String? = null,

    @field:SerializedName("assignees")
    val assignees: String? = null,

    @field:SerializedName("owners")
    val owners: String? = null,

    @field:SerializedName("main_infra_epc_contractors")
    val mainInfraEpcContractors: String? = null,

    @field:SerializedName("mep_contractors")
    val mepContractors: String? = null,

    @field:SerializedName("bnc_project_number")
    val bncProjectNumber: String? = null,

    @field:SerializedName("project_stage")
    val projectStage: String? = null,

    @field:SerializedName("new_subtype")
    val newSubtype: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("completion_date")
    val completionDate: String? = null,

    @field:SerializedName("spent_value")
    val spentValue: String? = null,

    @field:SerializedName("project_country")
    val projectCountry: String? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("value")
    val value: String? = null,

    @field:SerializedName("updated")
    val updated: String? = null,

    @field:SerializedName("lead_infra_feed_consultants")
    val leadInfraFeedConsultants: String? = null
)

data class ProfessionalStagesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class ContractorSchedulesItem(

    @field:SerializedName("schedule")
    val schedule: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class Zone(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("slug")
    val slug: String? = null
)

data class UsageOfBuildingItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: String? = null
)

data class StagesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class SubtypesItem(

    @field:SerializedName("category_name")
    val categoryName: String? = null,

    @field:SerializedName("subtype")
    val subtype: String? = null,

    @field:SerializedName("type_id")
    val typeId: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("subtype_id")
    val subtypeId: Int? = null,

    @field:SerializedName("project_type")
    val projectType: Int? = null,

    @field:SerializedName("name")
    val name: String? = null
)

data class ProjectSubtypesItem(

    @field:SerializedName("project_type")
    val projectType: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class AllStagesItem(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class ProjectTypesItem(

    @field:SerializedName("project_subtypes")
    val projectSubtypes: List<ProjectSubtypesItem?>? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class CompanyTypesItem(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("subtypes")
    val subtypes: List<SubtypesItem?>? = null
)

data class ProjectSearchOptionalFields(

    @field:SerializedName("completion_percentage")
    val completionPercentage: String? = null,

    @field:SerializedName("profile_type")
    val profileType: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("new_project_type")
    val newProjectType: String? = null,

    @field:SerializedName("mep_consultants")
    val mepConsultants: String? = null,

    @field:SerializedName("assignees")
    val assignees: String? = null,

    @field:SerializedName("owners")
    val owners: String? = null,

    @field:SerializedName("main_infra_epc_contractors")
    val mainInfraEpcContractors: String? = null,

    @field:SerializedName("mep_contractors")
    val mepContractors: String? = null,

    @field:SerializedName("bnc_project_number")
    val bncProjectNumber: String? = null,

    @field:SerializedName("project_stage")
    val projectStage: String? = null,

    @field:SerializedName("new_subtype")
    val newSubtype: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("completion_date")
    val completionDate: String? = null,

    @field:SerializedName("spent_value")
    val spentValue: String? = null,

    @field:SerializedName("project_country")
    val projectCountry: String? = null,

    @field:SerializedName("value")
    val value: String? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("updated")
    val updated: String? = null,

    @field:SerializedName("lead_infra_feed_consultants")
    val leadInfraFeedConsultants: String? = null
)
