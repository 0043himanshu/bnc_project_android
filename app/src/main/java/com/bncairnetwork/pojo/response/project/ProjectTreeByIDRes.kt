package com.bncairnetwork.pojo.response.project

import com.google.gson.annotations.SerializedName

data class ProjectTreeByIDRes(

    @field:SerializedName("tree")
    val projectTreeDataItem: ProjectTreeDataItem? = null,

    @field:SerializedName("is_ibis_tree")
    val isIbisTree: Boolean? = null,

    @field:SerializedName("data_source")
    val dataSource: DataSource? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class ProjectTreeDataItem(

    @field:SerializedName("bnc_project")
    val bncProject: Boolean? = null,

    @field:SerializedName("bidders")
    val bidders: Int? = null,

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("is_unassigned")
    val isUnassigned: Boolean? = null,

    @field:SerializedName("owners")
    val owners: Int? = null,

    @field:SerializedName("views_left")
    val viewsLeft: Any? = null,

    @field:SerializedName("is_favourite")
    val isFavourite: Boolean? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("contractors")
    val contractors: Int? = null,

    @field:SerializedName("current")
    val current: Boolean? = null,

    @field:SerializedName("consultants")
    val consultants: Int? = null,

    @field:SerializedName("children")
    val children: ArrayList<ChilderDataItem>,

    @field:SerializedName("authorized")
    val authorized: Boolean? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("assigned")
    val assigned: Boolean? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("status")
    val status: String? = null
)

data class DataSource(

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("type")
    val type: String? = null
)

data class ChilderDataItem(
    val status: String,
    val assigned: Boolean,
    val bnc_project: Boolean,
    val name: String,
    val city: String,
    val country: String,
    var isChildViewed:Boolean = false,
    val is_unassigned: Boolean,
    val owners: String,
    val paddingStart:Int =0,
    val children: ArrayList<ChilderDataItem>,
    val is_favourite: Boolean,
    val consultants: String,
    val authorized: Boolean,
    val bidders: String,
    val contractors: String,
    val views_left: String?=null,
    val current: String,
    @SerializedName("type")
    val childType: String?=null,
    val id: String

)
