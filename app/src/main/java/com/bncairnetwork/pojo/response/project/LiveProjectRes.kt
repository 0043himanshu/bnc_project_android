package com.bncairnetwork.pojo.response.project

import com.google.gson.annotations.SerializedName

data class LiveProjectRes(

	@field:SerializedName("full_count")
	val fullCount: Int? = null,

	@field:SerializedName("projects")
	val projects: ArrayList<LiveProjectsItem?>? = null,

	@field:SerializedName("fields")
	val fields: List<FieldsItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null,

	@field:SerializedName("count_num_projs")
	val countNumProjs: Int? = null
)

data class LiveRecordType(

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("linked_project")
	val linkedProject: LiveLinkedProject? = null,

	@field:SerializedName("linked")
	val linked: Boolean? = null
)

data class FieldsItem(

	@field:SerializedName("filter")
	val filter: String? = null,

	@field:SerializedName("headerName")
	val headerName: String? = null,

	@field:SerializedName("field")
	val field: String? = null,

	@field:SerializedName("width")
	val width: Int? = null,

	@field:SerializedName("minWidth")
	val minWidth: Int? = null,

	@field:SerializedName("unSortIcon")
	val unSortIcon: Boolean? = null,

	@field:SerializedName("filterParams")
	val filterParams: FilterParams? = null,

	@field:SerializedName("suppressMenu")
	val suppressMenu: Boolean? = null,

	@field:SerializedName("suppressSizeToFit")
	val suppressSizeToFit: Boolean? = null,

	@field:SerializedName("cellClass")
	val cellClass: String? = null,

	@field:SerializedName("suppressSorting")
	val suppressSorting: Boolean? = null,

	@field:SerializedName("suppressResize")
	val suppressResize: Boolean? = null,

	@field:SerializedName("headerClass")
	val headerClass: String? = null
)

data class TagObj(

	@field:SerializedName("isProjects")
	val isProjects: Boolean? = null,

	@field:SerializedName("selectedTags")
	val selectedTags: ArrayList<String>? = null,

	@field:SerializedName("isFavourite")
	var isFavourite: Boolean? = null,

	@field:SerializedName("isTypeIcon")
	val isTypeIcon: Boolean? = null
)

data class LiveLinkedProject(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class FilterParams(

	@field:SerializedName("apply")
	val apply: Boolean? = null,

	@field:SerializedName("suppressRemoveEntries")
	val suppressRemoveEntries: Boolean? = null,

	@field:SerializedName("values")
	val values: List<Any?>? = null,

	@field:SerializedName("newRowsAction")
	val newRowsAction: String? = null
)

data class IdentfierObj(

	@field:SerializedName("date")
	val date: Any? = null,

	@field:SerializedName("reminder_text")
	val reminderText: String? = null,

	@field:SerializedName("content_type")
	val contentType: List<String?>? = null,

	@field:SerializedName("content_id")
	val contentId: Int? = null,

	@field:SerializedName("self_type")
	val selfType: String? = null,

	@field:SerializedName("self_id")
	val selfId: Int? = null,

	@field:SerializedName("has_reminder")
	val hasReminder: Boolean? = null,

	@field:SerializedName("reminder_text_display")
	val reminderTextDisplay: String? = null,

	@field:SerializedName("reminder_id")
	val reminderId: Any? = null
)

data class LiveProjectsItem(

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("completion_percentage")
	val completionPercentage: Double? = null,

	@field:SerializedName("project_type")
	val projectType: String? = null,

	@field:SerializedName("tag_obj")
	val tagObj: TagObj? = null,

	@field:SerializedName("role")
	val role: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	val authorized:Boolean? = null,

//	@field:SerializedName("authorized")
//	val isAuthorized: Boolean? = null,

	@field:SerializedName("new_project_type")
	val newProjectType: String? = null,

	@field:SerializedName("record_type")
	val recordType: LiveRecordType? = null,

	@field:SerializedName("project_location")
	val projectLocation: String? = null,

	@field:SerializedName("linked_bnc_project_id")
	val linkedBncProjectId: Int? = null,

	@field:SerializedName("project_stage")
	val projectStage: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("fav_tags")
	val favTags: List<Any?>? = null,

	@field:SerializedName("updated")
	val updated: String? = null,

	@field:SerializedName("value")
	val value: String? = null,

	@field:SerializedName("identfierObj")
	val identfierObj: IdentfierObj? = null
)
