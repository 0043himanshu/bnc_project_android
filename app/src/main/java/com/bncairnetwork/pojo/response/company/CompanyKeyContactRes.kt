package com.bncairnetwork.pojo.response.company

import com.google.gson.annotations.SerializedName

data class CompanyKeyContactRes(

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("contacts")
	val contacts: List<ContactsItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class KeyContactEmail(

	@field:SerializedName("last_verified_on")
	val lastVerifiedOn: String? = null,

	@field:SerializedName("tooltip")
	val tooltip: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class KeyContactPhone(

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("text")
	val text: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Email2(

	@field:SerializedName("last_verified_on")
	val lastVerifiedOn: String? = null,

	@field:SerializedName("tooltip")
	val tooltip: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class Email3(

	@field:SerializedName("last_verified_on")
	val lastVerifiedOn: String? = null,

	@field:SerializedName("tooltip")
	val tooltip: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)

data class ContactsItem(

	@field:SerializedName("bnc_contact")
	val bncContact: Boolean? = null,

	@field:SerializedName("contact_name")
	val contactName: String? = null,

	@field:SerializedName("contact_designation")
	val contactDesignation: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("bnc_version")
	val bncVersion: String? = null,

	@field:SerializedName("phone2")
	val phone2: String? = null,

	@field:SerializedName("verified_on")
	val verifiedOn: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("phone3")
	val phone3: String? = null,

	@field:SerializedName("linkedin")
	val linkedin: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("phone1")
	val phone1: String? = null,

	@field:SerializedName("email3")
	val email3: Email3? = null,

	@field:SerializedName("email2")
	val email2: Email2? = null,

	@field:SerializedName("email1")
	val email1: Email1? = null,

	@field:SerializedName("key_contact_phone")
	val keyContactPhone: KeyContactPhone? = null,

	@field:SerializedName("desigcode")
	val desigcode: Int? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("contact_record_type")
	val contactRecordType: String? = null,

	@field:SerializedName("key_contact_email")
	val keyContactEmail: KeyContactEmail? = null,

	@field:SerializedName("department")
	val department: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null
)

data class Email1(

	@field:SerializedName("last_verified_on")
	val lastVerifiedOn: String? = null,

	@field:SerializedName("tooltip")
	val tooltip: String? = null,

	@field:SerializedName("email")
	val email: String? = null
)
