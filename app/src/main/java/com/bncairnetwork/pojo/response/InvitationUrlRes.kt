package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class InvitationUrlRes(

	@field:SerializedName("share_url")
	val shareUrl: String? = null,

	@field:SerializedName("invitation")
	val invitation: Int? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
