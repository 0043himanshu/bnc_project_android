package com.bncairnetwork.pojo.response.company

import com.bncairnetwork.pojo.response.project.CompanyItem
import com.bncairnetwork.pojo.response.project.InteractionReminderDataItem
import com.bncairnetwork.pojo.response.project.ProjectInteractionDataItem

data class GetInteractionCompny(
	val displayTitle: String? = null,
	val company: InteractionCompanyDataItem? = null,
	val status: Int? = null,
	val jsonMemberSwitch: Boolean? = null,
	val interactions: ArrayList<InteractionsItem?>? = null
)

data class InteractionProjectDataItem(
	val project: String? = null,
	val id: Int? = null,
	val favourite: Boolean? = null,
	val tags: List<Any?>? = null
)

data class InteractionCompanyDataItem(
	val established: Any? = null,
	val country: String? = null,
	val companyType: String? = null,
	val typeOfCompany: String? = null,
	val address: String? = null,
	val city: String? = null,
	val yearStarted: Any? = null,
	val bncVersion: BncVersion? = null,
	val categoriesList: List<Any?>? = null,
	val keyContactsCount: Int? = null,
	val recordType: String? = null,
	val categoryList: List<Any?>? = null,
	val irisCompanyDetails: IrisCompanyDetailsDataItem? = null,
	val phone: List<String?>? = null,
	val web: List<String?>? = null,
	val name: String? = null,
	val id: Int? = null,
	val fax: List<Any?>? = null,
	val email: List<Any?>? = null
)

data class BncVersion(
	val country: Country? = null,
	val companyType: CompanyType? = null,
	val address: String? = null,
	val city: City? = null,
	val profile: String? = null,
	val categoriesList: List<Any?>? = null,
	val phone2: Any? = null,
	val phone3: Any? = null,
	val phone1: String? = null,
	val email3: String? = null,
	val email2: String? = null,
	val email1: String? = null,
	val website1: String? = null,
	val website2: String? = null,
	val name: String? = null,
	val website3: String? = null,
	val id: Int? = null
)

data class City(
	val country: Int? = null,
	val name: String? = null,
	val linkedBncCityId: Any? = null,
	val id: Int? = null,
	val synId: Int? = null,
	val stateCode: String? = null,
	val subscribedCompany: Int? = null,
	val slug: String? = null,
	val phoneCode: String? = null
)

data class UsersItem(
	val name: String? = null,
	val id: Int? = null
)

data class CompanyType(
	val name: String? = null,
	val id: Int? = null
)

data class IrisCompanyDetailsDataItem(
	val hasIrisCompany: Boolean? = null
)

data class Country(
	val zone: Int? = null,
	val name: String? = null,
	val zoneName: String? = null,
	val id: Int? = null,
	val slug: String? = null
)


data class InteractionsItem(
	val date: String? = null,
	val reminder: InteractionReminderDataItem? = null,
	val interaction: String? = null,
	val project: InteractionProjectDataItem? = null,
	val interaction_type: String? = null,
	val company: List<CompanyItem?>? = null,
	val id: String? = null,
	val users: List<UsersItem?>? = null
)

