package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class PopupRes(

	@field:SerializedName("popup")
	val popup: Popup? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class Popup(

	@field:SerializedName("popup")
	val popup: String? = null,

	@field:SerializedName("popup_id")
	val popupId: Int? = null,

	@field:SerializedName("user_popup_id")
	val userPopupId: Int? = null
)
