package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class AppVersionRes(
    val status: String,
    @SerializedName("com.itemizer360.bncnetwork")
    val mpackage: OBJ
)

data class OBJ(
    val minVersionCode: String
)
