package com.bncairnetwork.pojo.response.reminder.project_and_company

import com.google.gson.annotations.SerializedName

data class GetReminderLstByIdRes(

    @field:SerializedName("reminders")
    val reminders: ArrayList<RemindersItemByID>,

    @field:SerializedName("project")
    val project: Project? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class RemindersItemByID(
    @field:SerializedName("reminder")
    val reminder: String? = null,
    @field:SerializedName("reminder_date")
    val reminderDate: String? = null,
    var company_name: String?,
    var project_name:String?,
    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("status")
    val status: String? = null
)

data class Project(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("record_type")
    val recordType: String? = null
)
