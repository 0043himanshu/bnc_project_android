package com.bncairnetwork.pojo.response.Dashboard

import com.google.gson.annotations.SerializedName

data class NewsNdHighlightsLstRes(

    @field:SerializedName("news")
    val news: ArrayList<NewsItem>? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("total_news")
    val totalNews: Int? = null
)

data class NewsItem(

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("country")
    val country: String? = null,
    val short_description: String?,
    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("authorized")
    val authorized: Boolean? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("views_left")
    val viewsLeft: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("published")
    val published: String? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null
)
