package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class ShareWithUserLstRes(

	@field:SerializedName("company_data")
	val companyData: List<CompanyDataItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CompanyDataItem(

	@field:SerializedName("full_users")
	val fullUsers: ArrayList<FullUsersItem?>? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("users")
	val users: List<UsersItem?>? = null
)

data class UsersItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("designation")
	val designation: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)

data class FullUsersItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("designation")
	val designation: String? = null,

	@field:SerializedName("status")
	val status: String? = null
)
