package com.bncairnetwork.pojo.response

data class UpdateAddTagRes(
    val status: Int
)