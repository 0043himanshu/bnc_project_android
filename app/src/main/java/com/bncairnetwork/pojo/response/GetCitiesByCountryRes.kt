package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class GetCitiesByCountryRes(

	@field:SerializedName("cities")
	val cities: List<CitiesItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class CitiesItem(

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)
