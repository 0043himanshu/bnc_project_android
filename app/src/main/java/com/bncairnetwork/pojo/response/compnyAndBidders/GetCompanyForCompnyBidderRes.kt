package com.bncairnetwork.pojo.response.company

import com.bncairnetwork.pojo.IdentfierObjDataItem
import com.bncairnetwork.pojo.LinkedProjectDataItem
import com.bncairnetwork.pojo.ProTagObj
import com.google.gson.annotations.SerializedName

data class GetCompanyForCompnyBidderRes(

    @field:SerializedName("companies")
    val companies: ArrayList<CompaniesItem>? = null,

    @field:SerializedName("display_title")
    val displayTitle: String? = null,

    @field:SerializedName("project")
    val project: Project? = null,

    @field:SerializedName("companies_count")
    val companiesCount: Int? = null,

    @field:SerializedName("bidders_count")
    val biddersCount: Int? = null,



    @field:SerializedName("status")
    val status: Int? = null
)

data class IrisProjectDetails(

    @field:SerializedName("has_iris_project")
    val hasIrisProject: Boolean? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class Project(

    @field:SerializedName("country")
    val country: String? = null,

    @field:SerializedName("completion_percentage")
    val completionPercentage: Any? = null,

    @field:SerializedName("iris_project_details")
    val irisProjectDetails: IrisProjectDetails? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("description")
    val description: String? = null,

    @field:SerializedName("assignees")
    val assignees: List<Any?>? = null,

    @field:SerializedName("views_left")
    val viewsLeft: Any? = null,

    @field:SerializedName("bnc_project_number")
    val bncProjectNumber: String? = null,

    @field:SerializedName("type_of_project")
    val typeOfProject: String? = null,

    @field:SerializedName("project_subtype")
    val projectSubtype: String? = null,

    @field:SerializedName("authorized")
    val authorized: Boolean? = null,

    @field:SerializedName("spent_value")
    val spentValue: Any? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("value")
    val value: Double? = null,

    @field:SerializedName("sector")
    val sector: String? = null,

    @field:SerializedName("area")
    val area: String? = null,

    @field:SerializedName("image")
    val image: String? = null,

    @field:SerializedName("project_type")
    val projectType: String? = null,

    @field:SerializedName("estimated_completion")
    val estimatedCompletion: Any? = null,

    @field:SerializedName("created")
    val created: String? = null,

    @field:SerializedName("bnc_version")
    val bncVersion: Any? = null,

    @field:SerializedName("record_type")
    val recordType: String? = null,

    @field:SerializedName("assignees_obj")
    val assigneesObj: List<Any?>? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("progress")
    val progress: String? = null,

    @field:SerializedName("location")
    val location: String? = null,

    @field:SerializedName("latlongstring")
    val latlongstring: Any? = null,

    @field:SerializedName("updated")
    val updated: String? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null
)

data class KeyContactDetails(

    @field:SerializedName("name")
    val name: String? = null,

    val phoneNumber: String? = null,

    val email: String? = null


)

data class CmnyRecordTypeDataItem(
    val type: String,
    val linked: Boolean,
    val linked_projectcompany: LinkedProjectDataItem?=null
)

data class CompaniesItem(

    @field:SerializedName("country")
    val country: String? = null,

    val tag_obj: ProTagObj?,

    val linked: Boolean? = null,

    val identfierObj: IdentfierObjDataItem?,

    @field:SerializedName("company_type")
    val companyType: String? = null,

    @field:SerializedName("note")
    val note: String? = null,

    @field:SerializedName("role")
    val role: String? = null,

    @field:SerializedName("reminder")
    val reminder: Any? = null,

    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("favourite")
    var favourite: Boolean? = null,

    @field:SerializedName("record_type")
    val recordType: CmnyRecordTypeDataItem? = null,

    @field:SerializedName("key_contact")
    val keyContact: Any? = null,

    @field:SerializedName("tags")
    val tags: ArrayList<String>? = null,

    @field:SerializedName("awarded_date")
    val awardedDate: String? = null,

    @field:SerializedName("phone")
    val phone: ArrayList<String?>? = null,

    @field:SerializedName("web")
    val web: List<String?>? = null,

    @field:SerializedName("bnc_record")
    val bncRecord: Boolean? = null,

    @field:SerializedName("scope")
    val scope: String? = null,

    @field:SerializedName("name")
    var name: String? = null,

    @field:SerializedName("key_contact_details")
    val keyContactDetails: KeyContactDetails? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("country_flag")
    val countryFlag: String? = null,

    @field:SerializedName("key_contact_obj")
    val keyContactObj: ArrayList<KeyContactobj>? = null,

    @field:SerializedName("email")
    val email: ArrayList<String?>? = null
)


data class KeyContactobj(
    val name: String? = null,
    val designation: String? = null,
    val email: KeyContactEmailObj? = null,
    val id: String? = null,
    val mobile: KeyContactMobileObj? = null,
    val contactType: String? = null
)

data class KeyContactMobileObj(
    val text: String? = null,
    val number: String? = null
)

data class KeyContactEmailObj(
    val email: String? = null,
    val last_verified_on: String? = null
)