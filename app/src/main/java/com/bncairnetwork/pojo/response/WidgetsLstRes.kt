package com.bncairnetwork.pojo.response

import com.google.gson.annotations.SerializedName

data class WidgetsLstRes(

    @field:SerializedName("widgets")
    val widgets: List<WidgetsItem>? = null,

    @field:SerializedName("status")
    val status: Int? = null
)

data class WidgetsItem(

    @field:SerializedName("name_identifier")
    val nameIdentifier: String? = null,

    @field:SerializedName("user_widget_id")
    val userWidgetId: Int? = null,

    @field:SerializedName("id")
    val id: Int? = null,

    @field:SerializedName("title")
    val title: String? = null,

    @field:SerializedName("priority")
    val priority: Int? = null,

    @field:SerializedName("status")
    var status: Boolean? = null
)
