package com.bncairnetwork.pojo.response.KeyContact

import com.google.gson.annotations.SerializedName

data class GetKEyContactRes(

	@field:SerializedName("key_contacts")
	val keyContacts: List<KeyContactsItem?>? = null,

	@field:SerializedName("display_title")
	val displayTitle: String? = null,

	@field:SerializedName("key_contacts_count")
	val keyContactsCount: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class KeyContactsItem(

	@field:SerializedName("phone")
	val phone: ArrayList<String?>? = null,

	@field:SerializedName("bnc_record")
	val bncRecord: Boolean? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("designation")
	val designation: String? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("email")
	val email: ArrayList<String?>? = null,

	@field:SerializedName("record_type")
	val recordType: String? = null
)
