package com.bncairnetwork.pojo.response.reminder

import com.google.gson.annotations.SerializedName

data class ReminderRes(

	@field:SerializedName("reminder_text")
	val reminderText: String? = null,

	val errors:String?=null,

	@field:SerializedName("date")
	val date: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
