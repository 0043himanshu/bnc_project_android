package com.bncairnetwork.pojo

import com.google.gson.annotations.SerializedName

data class AttributesItem(

	@field:SerializedName("count")
	val count: Int? = null,

	@field:SerializedName("title")
	val title: String? = null,

	@field:SerializedName("url")
	val url: String? = null
)