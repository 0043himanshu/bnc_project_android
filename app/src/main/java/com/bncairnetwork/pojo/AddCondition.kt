package com.bncairnetwork.pojo

data class AddCondition(
    val companyName: String,
    val isMatchExactPhase: Boolean,
    val isIncludeBidder: Boolean,
    val isIncludeCancellationContracts: Boolean,

    )
