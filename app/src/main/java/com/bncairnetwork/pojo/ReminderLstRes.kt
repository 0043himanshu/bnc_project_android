package com.bncairnetwork.pojo

data class ReminderLstRes(
    var reminderLbl: String,
    var companyName: String,
    var createOn: String,
    var remnderDateTime: String,
    var status: String
)
