package com.bncairnetwork.pojo

data class ProSearchPojo(
    val proTitle: String,
    val proLoc: String,
    val proState: String
)
