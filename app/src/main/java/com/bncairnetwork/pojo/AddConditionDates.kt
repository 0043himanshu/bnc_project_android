package com.bncairnetwork.pojo

data class AddConditionDates(
    val types:HashMap<String,String>?,
    val subtypes:HashMap<String,String>?
)
