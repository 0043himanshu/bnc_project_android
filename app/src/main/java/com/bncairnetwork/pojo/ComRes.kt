package com.bncairnetwork.pojo

import com.google.gson.annotations.SerializedName

data class ComRes(
    val status: String? = null,
    @SerializedName("error")
    val errors: String? = null,
    val message: String? = null
)
