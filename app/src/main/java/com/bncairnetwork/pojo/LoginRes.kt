package com.bncairnetwork.pojo

import com.bncairnetwork.pojo.response.master.GetMasterDataRes
import com.google.gson.annotations.SerializedName

data class LoginRes(
    val username: String?,
    val status: String?,
    val error: String?,
    val first_name: String?,
    val policy_accepted: Boolean?,
    @SerializedName("package")
    val packageID: Int?,
    val token: String?,
    val push_notitification: String?,
    val phone: String?,
    val designation: String?,
    val company: UserCompanyDetailsDataItem?,
    val master_data: GetMasterDataRes

)

data class UserCompanyDetailsDataItem(
    val name: String?,
    val id: String?
)
