package com.bncairnetwork.pojo

data class ProjNewsPojo(
    val title: String,
    val desc: String,
    val banner:Int
)
