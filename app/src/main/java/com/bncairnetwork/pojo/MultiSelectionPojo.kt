package com.bncairnetwork.pojo

data class MultiSelectionPojo(
    val name: String,
    var flag: Boolean = false
)
