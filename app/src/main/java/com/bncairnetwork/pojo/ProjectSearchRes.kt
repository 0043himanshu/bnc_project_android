package com.bncairnetwork.pojo

import com.bncairnetwork.pojo.response.notification.RecordTypeDataItem
import com.google.gson.annotations.SerializedName

data class  ProjectSearchRes(
    val status: String,
    val total_projects: String?,
    val total_companies: String?,
    val display_title: String,
    val search_id: String,
    val projects: ArrayList<ProjectDetailsDataObj>
)

data class ProjectDetailsDataObj(
    val id: String,
    var name: String,
    val industry: String,
    val latlongstring: String? = null,
    val updated: String,
    val favourite: Boolean? = null,
    val record_type: RecordTypeDataItem?,
    val value: String,
    @SerializedName("authorized")
    val authorised: Boolean? = null,
    val tag_obj: ProTagObj?,
    val reminder: ReminderDataItem?,
    val description: String,
    var country: String,
    var city: String,
    val stage: String?,
    val views_left: String? = null,
    val image: String?=null,
    val identfierObj: IdentfierObjDataItem?,
    val tags: ArrayList<String>? = null

)

data class ProTagObj(
    var selectedTags: ArrayList<String>?,
    val isTypeIcon: Boolean?,
    var isFavourite: Boolean?,
    val isProjects: Boolean?
)

data class IdentfierObjDataItem(
    val self_id: String?,
    val self_type: String?,
    val content_type: ArrayList<String>,
    val reminder_id: String?,
    val date: String?,
    val has_reminder: Boolean?,
    val content_id: String?,
    val reminder_text: String,
    val reminder_text_display: String

)

data class ReminderDataItem(
    val date: String,
    val reminder_id: String,
    val reminder: String,
    val mobile_reminder_date: String
)

data class RecordTypeDataItem(
    val type: String,
    val linked: Boolean,
    val linked_project: LinkedProjectDataItem
)

data class LinkedProjectDataItem(
    val id: String? = null,
    val name: String? = null
)