package com.bncairnetwork.pojo

data class UserInvitationEmailPojo(
    val name: String?,
    val email: String?
)