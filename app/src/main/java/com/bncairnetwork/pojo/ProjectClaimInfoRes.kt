package com.bncairnetwork.pojo

import com.google.gson.annotations.SerializedName

data class ProjectClaimInfoRes(

	@field:SerializedName("credits")
	val credits: Int? = null,

	@field:SerializedName("claimed")
	val claimed: Boolean? = null,

	@field:SerializedName("project")
	val project: Project? = null,

	@field:SerializedName("packages")
	val packages: List<PackagesItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
)

data class PackagesItem(

	@field:SerializedName("summary")
	val summary: Any? = null,

	@field:SerializedName("short_description")
	val shortDescription: String? = null,

	@field:SerializedName("project_count")
	val projectCount: Int? = null,

	@field:SerializedName("product")
	val product: String? = null,

	@field:SerializedName("price")
	val price: Double? = null,

	@field:SerializedName("s3_image_url")
	val s3ImageUrl: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("id")
	val id: Int? = null
)

data class Project(

	@field:SerializedName("bidders")
	val bidders: Int? = null,

	@field:SerializedName("image")
	val image: String? = null,

	@field:SerializedName("country")
	val country: String? = null,

	@field:SerializedName("flag")
	val flag: String? = null,

	@field:SerializedName("city")
	val city: String? = null,

	@field:SerializedName("new_project_type")
	val newProjectType: String? = null,

	@field:SerializedName("owners")
	val owners: Int? = null,

	@field:SerializedName("contractors")
	val contractors: Int? = null,

	@field:SerializedName("consultants")
	val consultants: Int? = null,

	@field:SerializedName("stage")
	val stage: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("sector")
	val sector: String? = null,

	@field:SerializedName("updated")
	val updated: String? = null
)
