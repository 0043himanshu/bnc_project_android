package com.bncairnetwork.pojo

import com.google.gson.annotations.SerializedName

data class GetCompaniesBySearchRes(

    @field:SerializedName("companies")
    val companies: ArrayList<CompaniesItem?>? = null,

    @field:SerializedName("professional_view")
    val professionalView: Boolean? = null,

    @field:SerializedName("company_count")
    val companyCount: Int? = null,

    @field:SerializedName("spelling_suggestion")
    val spellingSuggestion: Any? = null,

    @field:SerializedName("total_projects")
    val totalProjects: Int? = null,

    @field:SerializedName("total_companies")
    val totalCompanies: Int? = null,

    @field:SerializedName("keyword")
    val keyword: String? = null,

//	@field:SerializedName("fields")
//	val fields: List<FieldsItem?>? = null,

    @field:SerializedName("status")
    val status: Int? = null,

    @field:SerializedName("search_id")
    val searchId: String? = null
)

data class FieldsItem(

    @field:SerializedName("suppressMenu")
    val suppressMenu: Boolean? = null,

    @field:SerializedName("headerName")
    val headerName: String? = null,

    @field:SerializedName("suppressSorting")
    val suppressSorting: Boolean? = null,

    @field:SerializedName("field")
    val field: String? = null,

    @field:SerializedName("width")
    val width: Int? = null,

    @field:SerializedName("minWidth")
    val minWidth: Int? = null,

    @field:SerializedName("filterParams")
    val filterParams: FilterParams? = null,

    @field:SerializedName("cellClass")
    val cellClass: String? = null,

    @field:SerializedName("unSortIcon")
    val unSortIcon: Boolean? = null,

    @field:SerializedName("suppressMenuColumnPanel")
    val suppressMenuColumnPanel: Boolean? = null,

    @field:SerializedName("suppressMenuMainPanel")
    val suppressMenuMainPanel: Boolean? = null,

    @field:SerializedName("suppressSizeToFit")
    val suppressSizeToFit: Boolean? = null,

    @field:SerializedName("suppressResize")
    val suppressResize: Boolean? = null,

    @field:SerializedName("headerClass")
    val headerClass: String? = null,

    @field:SerializedName("checkboxSelection")
    val checkboxSelection: Boolean? = null
)

data class Email(

    @field:SerializedName("last_verified_on")
    val lastVerifiedOn: String? = null,

    @field:SerializedName("tooltip")
    val tooltip: String? = null,

    @field:SerializedName("email")
    val email: String? = null
)

data class FilterParams(

    @field:SerializedName("apply")
    val apply: Boolean? = null,

    @field:SerializedName("suppressRemoveEntries")
    val suppressRemoveEntries: Boolean? = null,

    @field:SerializedName("values")
    val values: List<String?>? = null,

    @field:SerializedName("newRowsAction")
    val newRowsAction: String? = null
)

data class LinkedCompany(

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("id")
    val id: Int? = null
)

data class RecordType(

    @field:SerializedName("linked_company")
    val linkedCompany: LinkedCompany? = null,

    @field:SerializedName("type")
    val type: String? = null,

    @field:SerializedName("linked")
    val linked: Boolean? = null
)

data class IdentfierObj(

    @field:SerializedName("reminder_text")
    val reminderText: String? = null,

    @field:SerializedName("date")
    val date: Any? = null,

    @field:SerializedName("content_type")
    val contentType: List<String?>? = null,

    @field:SerializedName("content_id")
    val contentId: Int? = null,

    @field:SerializedName("self_type")
    val selfType: String? = null,

    @field:SerializedName("self_id")
    val selfId: Int? = null,

    @field:SerializedName("has_reminder")
    val hasReminder: Boolean? = null,

    @field:SerializedName("reminder_text_display")
    val reminderTextDisplay: String? = null,

    @field:SerializedName("reminder_id")
    val reminderId: Any? = null
)

data class TagObj(

    @field:SerializedName("isProjects")
    val isProjects: Boolean? = null,

    @field:SerializedName("selectedTags")
    val selectedTags: ArrayList<String>,

    @field:SerializedName("isFavourite")
    var isFavourite: Boolean? = null,

    @field:SerializedName("isTypeIcon")
    val isTypeIcon: Boolean? = null
)

data class CompaniesItem(

    @field:SerializedName("company_type")
    val companyType: String? = null,

    @field:SerializedName("country")
    val country: String? = null,

    val updated:String?=null,
    @field:SerializedName("city")
    val city: String? = null,

    @field:SerializedName("signpost")
    val signpost: Any? = null,

    @field:SerializedName("is_unassigned")
    val isUnassigned: Boolean? = null,

    @field:SerializedName("assignees")
    val assignees: String? = null,

    @field:SerializedName("company_subtypes")
    val companySubtypes: List<Any?>? = null,

    @field:SerializedName("tender_projects_pv")
    val tenderProjectsPv: Int? = null,

    @field:SerializedName("email_pv")
    val emailPv: String? = null,

    @field:SerializedName("id")
    val id: String? = null,

    @field:SerializedName("under_construction_projects_value")
    val underConstructionProjectsValue: Double? = null,

    @field:SerializedName("email")
    val email: String? = null,

    @field:SerializedName("website")
    val website: String? = null,

//    @field:SerializedName("tag_obj")
//    val tagObj: TagObj? = null,

    var favourite:Boolean?=null,
    val tags:ArrayList<String>?=null,

    @field:SerializedName("is_authorized")
    val isAuthorized: Boolean? = null,

    @field:SerializedName("active_projects_value")
    val activeProjectsValue: Double? = null,

    @field:SerializedName("record_type")
    val recordType: RecordType? = null,

    @field:SerializedName("phone")
    val contactNumber: String? = null,

    @field:SerializedName("tender_projects")
    val tenderProjects: Int? = null,

    @field:SerializedName("node")
    val node: String? = null,

    @field:SerializedName("under_construction_projects_value_pv")
    val underConstructionProjectsValuePv: Double? = null,

    @field:SerializedName("active_projects")
    val activeProjects: Int? = null,

    @field:SerializedName("under_construction_projects_pv")
    val underConstructionProjectsPv: Int? = null,

    @field:SerializedName("name")
    val name: String? = null,

    @field:SerializedName("assigned")
    val assigned: Boolean? = null,

    @field:SerializedName("company_record_type")
    val companyRecordType: String? = null,

    @field:SerializedName("under_construction_projects")
    val underConstructionProjects: Int? = null,

    @field:SerializedName("tender_projects_value")
    val tenderProjectsValue: Double? = null,

    val reminder:CompanyKeyReminderDataItem?=null,

//    @field:SerializedName("identfierObj")
//    val identfierObj: IdentfierObj? = null,

    @field:SerializedName("tender_projects_value_pv")
    val tenderProjectsValuePv: Double? = null
)

data class CompanyKeyReminderDataItem(
    val date:String?=null,
    val reminder_id:String?=null,
    val reminder:String?=null,
    val status:String?=null,
    val mobile_reminder_date:String?=null
)

        /*"reminder": {
                "date": "December 13, 2021 01:32 pm",
                "reminder_id": 27106,
                "reminder": "reminder CDM Smith (Dubai)",
                "status": "Pending",
                "mobile_reminder_date": "13-12-2021"
            }*/
