package com.bncairnetwork.pojo

data class GetAllCompaniesRes(
    val status: String,
    val display_title: String,
    val total_companies:String?,
    val companies: ArrayList<CompaniesDataObj>
)

data class CompaniesDataObj(
    val id: String,
    var name: String,
    val has_reminder:Boolean?,
    val email: String? = null,
    val phone: String? = null,
    var country: String,
    var city: String,
    val updated:String?=null,
    val reminder: ReminderDataItem? ,
    val value: String? = null,
    val country_flag: String,
    val record_type:RecordTypeDataItem?,
    val tag_obj: ComTagObj?,
    val company_type: String?,
    val projects: String
)

data class ComTagObj(
    val selectedTags: ArrayList<String>?,
    val isTypeIcon: Boolean?,
    var isFavourite: Boolean?,
    val isProjects: Boolean?
)

